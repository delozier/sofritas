------
SETUP
------

1. Unpack llvm.haswell.tar using "tar xf llvm.haswell.tar"
2. cd llvm/lib/Transforms
3. ln -s ../../../compiler/ MakeSCC
4. cd ../Analysis/
5. ln -s ../../poolalloc/lib/DSA/ DSA
6. cd ../../include/
7. ln -s ../poolalloc/include/dsa/ dsa
8. cd ../
9. mkdir build/debug
10. cd build/debug
11. cmake ../../
12. make

