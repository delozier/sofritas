// (C) 2013 University of California, Santa Cruz.
// The software is subject to an academic license.
// Terms are contained in the LICENSE file.
// Derived from LLVM LoopHelper.cpp and customized. So, LLVM license is also applicable.
//
// Helper functions to determine loop start, end etc. 
//
// --------------------------------------
#include <algorithm>
#include <deque>
#include <queue>
#include <map>
#include <stack>
#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include "llvm/IR/Constants.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/DerivedTypes.h" 
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/CaptureTracking.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"
// #include "llvm/Assembly/Writer.h"
#include "llvm/IR/CFG.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/ConstantRange.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/GetElementPtrTypeIterator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/InstIterator.h"
// #include "llvm/Support/Streams.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "LoopHelper.h"
/// ComputeExitLimitFromICmp - Compute the number of times the
/// backedge of the specified loop will execute if its exit condition
/// were a conditional branch of the ICmpInst ExitCond, TBB, and FBB. 

Value *SccLoopHelper::ComputeExitLimitFromICmp(const Loop *L,
                      ICmpInst *ExitCond,
                      BasicBlock *TBB,
                      BasicBlock *FBB)
{
  // If the condition was exit on true, convert the condition to exit on false
  ICmpInst::Predicate Cond;
  if (!L->contains(FBB))
    Cond = ExitCond->getPredicate();
  else 
    Cond = ExitCond->getInversePredicate();

#if 0
  // Handle common loops like: for (X = "string"; *X; ++X) 
  if (LoadInst *LI = dyn_cast<LoadInst>(ExitCond->getOperand(0))) {
    if (Constant *RHS = dyn_cast<Constant>(ExitCond->getOperand(1))) {
      ExitLimit ItCnt =
        ComputeLoadConstantCompareExitLimit(LI, RHS, L, Cond);
      if (ItCnt.hasAnyInfo())
        return ItCnt;
    }
  }
#endif

  const SCEV *LHS = SE->getSCEV(ExitCond->getOperand(0));
  const SCEV *RHS = SE->getSCEV(ExitCond->getOperand(1));

  // Try to evaluate any dependencies out of the loop.
  LHS = SE->getSCEVAtScope(LHS, L);
  RHS = SE->getSCEVAtScope(RHS, L);

  bool lhsInvariant = SE->isLoopInvariant(LHS, L);
  bool rhsInvariant = SE->isLoopInvariant(RHS, L);

  if(!(lhsInvariant || rhsInvariant))
    return NULL;

  Value *exitVal = lhsInvariant ?  ExitCond->getOperand(0) : ExitCond->getOperand(1);

  // At this point, we would like to compute how many iterations of the
  // loop the predicate will return true for these inputs.
  if (SE->isLoopInvariant(LHS, L) && !SE->isLoopInvariant(RHS, L)) {
    // If there is a loop-invariant, force it into the RHS. 
    std::swap(LHS, RHS);
    Cond = ICmpInst::getSwappedPredicate(Cond);
  }

  // Simplify the operands before analyzing them.
  (void)SE->SimplifyICmpOperands(Cond, LHS, RHS);

#if 0
  // If we have a comparison of a chrec against a constant, try to use value
  // ranges to answer this query.
  if (const SCEVConstant *RHSC = dyn_cast<SCEVConstant>(RHS)) {
    if (const SCEVAddRecExpr *AddRec = dyn_cast<SCEVAddRecExpr>(LHS))
      if (AddRec->getLoop() == L) { 
        // Form the constant range.
        ConstantRange CompRange(
            ICmpInst::makeConstantRange(Cond, RHSC->getValue()->getValue()));

        const SCEV *Ret = AddRec->getNumIterationsInRange(CompRange, *SE);
        if (!isa<SCEVCouldNotCompute>(Ret)) return Ret;
      }
  }
#endif

  bool retval = true;
  switch (Cond) {
  case ICmpInst::ICMP_NE: {                     // while (X != Y)
    // Convert to: while (X-Y != 0)
    // ExitLimit EL = HowFarToZero(getMinusSCEV(LHS, RHS), L);
    // if (EL.hasAnyInfo()) return EL;
    std::cerr << "    ExitCond ICmpInst::ICMP_NE\n";
    break;
  }
  case ICmpInst::ICMP_EQ: {                     // while (X == Y)
    // Convert to: while (X-Y == 0)
    // ExitLimit EL = HowFarToNonZero(getMinusSCEV(LHS, RHS), L);
    // if (EL.hasAnyInfo()) return EL;
    std::cerr << "    ExitCond ICmpInst::ICMP_EQ\n";
    break;
  }
  case ICmpInst::ICMP_SLT: {
    // ExitLimit EL = HowManyLessThans(LHS, RHS, L, true);
    // if (EL.hasAnyInfo()) return EL;
    std::cerr << "    ExitCond ICmpInst::ICMP_SLT\n";
    break;
  }
  case ICmpInst::ICMP_SGT: {
    /// ExitLimit EL = HowManyLessThans(getNotSCEV(LHS), getNotSCEV(RHS), L, true);
    // if (EL.hasAnyInfo()) return EL;
    std::cerr << "    ExitCond ICmpInst::ICMP_SGT\n";
    break;
  }
  case ICmpInst::ICMP_ULT: {
    // ExitLimit EL = HowManyLessThans(LHS, RHS, L, false);
    // if (EL.hasAnyInfo()) return EL;
    std::cerr << "    ExitCond ICmpInst::ICMP_ULT\n";
    break;
  }
  case ICmpInst::ICMP_UGT: {
    // ExitLimit EL = HowManyLessThans(getNotSCEV(LHS), getNotSCEV(RHS), L, false);
    // if (EL.hasAnyInfo()) return EL;
    std::cerr << "    ExitCond ICmpInst::ICMP_UGT\n";
    break;
  }
  default:
#if 0
    dbgs() << "ComputeBackedgeTakenCount ";
    if (ExitCond->getOperand(0)->getType()->isUnsigned())
      dbgs() << "[unsigned] ";
    dbgs() << *LHS << "   "
         << Instruction::getOpcodeName(Instruction::ICmp)
         << "   " << *RHS << "\n";
#endif
    std::cerr << "    ExitCond Hit default!\n";
    retval = false;
    break;
  }
  return (retval ? exitVal : NULL);
  // return ComputeExitCountExhaustively(L, ExitCond, !L->contains(TBB));
}

/// isLoopInvariant - Perform a quick domtree based check for loop invariance
/// assuming that V is used within the loop. LoopInfo::isLoopInvariant() seems
/// gratuitous for this purpose.
bool SccLoopHelper::isLoopInvariant(Value *V, const Loop *L) 
{
  Instruction *Inst = dyn_cast<Instruction>(V);
  if (!Inst)
    return true;

  return DT->properlyDominates(Inst->getParent(), L->getHeader());
}


/// getLoopPhiForCounter - Return the loop header phi IFF IncV adds a loop
/// invariant value to the phi.
PHINode *SccLoopHelper::getLoopPhiForCounter(Value *IncV, Loop *L) 
{
  Instruction *IncI = dyn_cast<Instruction>(IncV);
  if (!IncI)
    return 0;

  switch (IncI->getOpcode()) {
  case Instruction::Add:
  case Instruction::Sub:
    break;
  case Instruction::GetElementPtr:
    // An IV counter must preserve its type.
    if (IncI->getNumOperands() == 2)
      break;
  default:
    return 0;
  }

  PHINode *Phi = dyn_cast<PHINode>(IncI->getOperand(0));
  if (Phi && Phi->getParent() == L->getHeader()) {
    if (isLoopInvariant(IncI->getOperand(1), L))
      return Phi;
    return 0;
  }
  if (IncI->getOpcode() == Instruction::GetElementPtr)
    return 0;

  // Allow add/sub to be commuted.
  Phi = dyn_cast<PHINode>(IncI->getOperand(1));
  if (Phi && Phi->getParent() == L->getHeader()) {
    if (isLoopInvariant(IncI->getOperand(0), L))
      return Phi;
  }
  return 0;
}



// Get start value for this inductionvariable that is the array index
// In addition, match array index to PhiNode of header, to ensure that
// array index is a loop induction variable.
Value *SccLoopHelper::ComputeStart(Loop *L, Value *arrayIndex)
{
  bool firstPhi = true;
  for (BasicBlock::iterator I = L->getHeader()->begin(); isa<PHINode>(I); ++I) {
    PHINode *Phi = cast<PHINode>(I);
    if (!SE->isSCEVable(Phi->getType()))
      continue;

    if(firstPhi) {
      if(arrayIndex != I) {
        fprintf(stderr,"  LILM: array index is not loop induction variable. Skipping.\n");
        return NULL; // loop induction var is not array index
      }
    }

    // Avoid comparing an integer IV against a pointer Limit.
    // if (BECount->getType()->isPointerTy() && !Phi->getType()->isPointerTy())
    //  continue;

    const SCEVAddRecExpr *AR = dyn_cast<SCEVAddRecExpr>(SE->getSCEV(Phi));
    if (!AR || AR->getLoop() != L || !AR->isAffine())
      continue;

    // AR may be a pointer type, while BECount is an integer type.
    // AR may be wider than BECount. With eq/ne tests overflow is immaterial.
    // AR may not be a narrower type, or we may never exit.
#if 0
    uint64_t PhiWidth = SE->getTypeSizeInBits(AR->getType());
    if (PhiWidth < BCWidth || (TD && !TD->isLegalInteger(PhiWidth)))
      continue;
#endif

    const SCEV *Step = dyn_cast<SCEVConstant>(AR->getStepRecurrence(*SE));
    if (!Step || !Step->isOne()) {
      std::cerr << "Step is not one!\n";
      return NULL;
    }

    int LatchIdx = Phi->getBasicBlockIndex(L->getLoopLatch());
    Value *IncV = Phi->getIncomingValue(LatchIdx);
    if (getLoopPhiForCounter(IncV, L) != Phi)
      continue;

    // const SCEV *Init = AR->getStart();
    // if(Init) return Init->getValue();
    int startIdx = LatchIdx ? 0 : 1;
    if(Phi->getNumOperands() != 2) {
      fprintf(stderr, "Complex Phi node with %d operands for loop\n", Phi->getNumOperands());
    }
    return Phi->getIncomingValue(startIdx);  // starting indvar
  }
  return NULL;
}


/// ComputeExitLimitFromCond - Compute the number of times the
/// backedge of the specified loop will execute if its exit condition
/// were a conditional branch of ExitCond, TBB, and FBB.
Value *SccLoopHelper::ComputeExitLimitFromCond(const Loop *L,
                                          Value *ExitCond,
                                          BasicBlock *TBB,
                                          BasicBlock *FBB) 
{
  // Check if the controlling expression for this loop is an And or Or.
  if (BinaryOperator *BO = dyn_cast<BinaryOperator>(ExitCond)) {
#if 0
    if (BO->getOpcode() == Instruction::And) {
      // Recurse on the operands of the and.
      ExitLimit EL0 = ComputeExitLimitFromCond(L, BO->getOperand(0), TBB, FBB);
      ExitLimit EL1 = ComputeExitLimitFromCond(L, BO->getOperand(1), TBB, FBB);
      const SCEV *BECount = getCouldNotCompute();
      const SCEV *MaxBECount = getCouldNotCompute();
      if (L->contains(TBB)) {
        // Both conditions must be true for the loop to continue executing.
        // Choose the less conservative count.
        if (EL0.Exact == getCouldNotCompute() ||
            EL1.Exact == getCouldNotCompute())
          BECount = getCouldNotCompute();
        else
          BECount = getUMinFromMismatchedTypes(EL0.Exact, EL1.Exact);
        if (EL0.Max == getCouldNotCompute())
          MaxBECount = EL1.Max;
        else if (EL1.Max == getCouldNotCompute())
          MaxBECount = EL0.Max;
        else
          MaxBECount = getUMinFromMismatchedTypes(EL0.Max, EL1.Max);
      } else {
        // Both conditions must be true at the same time for the loop to exit.
        // For now, be conservative.
        assert(L->contains(FBB) && "Loop block has no successor in loop!");
        if (EL0.Max == EL1.Max)
          MaxBECount = EL0.Max;
        if (EL0.Exact == EL1.Exact)
          BECount = EL0.Exact;
      }

      return ExitLimit(BECount, MaxBECount);
    }
    if (BO->getOpcode() == Instruction::Or) {
      // Recurse on the operands of the or.
      ExitLimit EL0 = ComputeExitLimitFromCond(L, BO->getOperand(0), TBB, FBB);
      ExitLimit EL1 = ComputeExitLimitFromCond(L, BO->getOperand(1), TBB, FBB);
      const SCEV *BECount = getCouldNotCompute();
      const SCEV *MaxBECount = getCouldNotCompute();
      if (L->contains(FBB)) {
        // Both conditions must be false for the loop to continue executing.
        // Choose the less conservative count.
        if (EL0.Exact == getCouldNotCompute() ||
            EL1.Exact == getCouldNotCompute())
          BECount = getCouldNotCompute();
        else
          BECount = getUMinFromMismatchedTypes(EL0.Exact, EL1.Exact);
        if (EL0.Max == getCouldNotCompute())
          MaxBECount = EL1.Max;
        else if (EL1.Max == getCouldNotCompute())
          MaxBECount = EL0.Max;
        else
          MaxBECount = getUMinFromMismatchedTypes(EL0.Max, EL1.Max);
      } else {
        // Both conditions must be false at the same time for the loop to exit.
        // For now, be conservative.
        assert(L->contains(TBB) && "Loop block has no successor in loop!");
        if (EL0.Max == EL1.Max)
          MaxBECount = EL0.Max;
        if (EL0.Exact == EL1.Exact)
          BECount = EL0.Exact;
      }

      return ExitLimit(BECount, MaxBECount);
    }
#else
    return NULL;
#endif
  }

  // With an icmp, it may be feasible to compute an exact backedge-taken count.
  // Proceed to the next level to examine the icmp.
  if (ICmpInst *ExitCondICmp = dyn_cast<ICmpInst>(ExitCond)) {
    // std::cerr << " TBD: Compute exit condition from ICMP instruction\n";
    return ComputeExitLimitFromICmp(L, ExitCondICmp, TBB, FBB);
  }

  return NULL;
#if 0
  // Check for a constant condition. These are normally stripped out by
  // SimplifyCFG, but ScalarEvolution may be used by a pass which wishes to
  // preserve the CFG and is temporarily leaving constant conditions
  // in place.
  if (ConstantInt *CI = dyn_cast<ConstantInt>(ExitCond)) {
    if (L->contains(FBB) == !CI->getZExtValue())
      // The backedge is always taken.
      return getCouldNotCompute();
    else
      // The backedge is never taken.
      return getConstant(CI->getType(), 0);
  }

  // If it's not an integer or pointer comparison then compute it the hard way.
  return ComputeExitCountExhaustively(L, ExitCond, !L->contains(TBB));
#endif
}

