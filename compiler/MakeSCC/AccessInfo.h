#pragma once

#include "llvm/IR/Constants.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/DerivedTypes.h" 
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/CaptureTracking.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/CFG.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/ConstantRange.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/GetElementPtrTypeIterator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/DebugInfo.h"

namespace llvm {

struct AccessInfo : public ilist_node<AccessInfo> 
{
  enum AccessType { None, Load, Store, ExclusiveLoad };

  AccessInfo()
    : type(None), original(NULL), inst(NULL), addr(NULL), size(NULL), alignment(1), ROInMT(true)
  { 
    stModeOnly = false;
    mutexProtected = false;
    escaping = true;
  }
  AccessInfo(AccessType t, Instruction* i)
    : type(t), original(i), inst(i), addr(NULL), size(NULL), alignment(1), ROInMT(true)
  {   
    stModeOnly = false;  
    mutexProtected = false;
    escaping = true; 
  }
  AccessInfo(const AccessInfo &a)
    : ilist_node<AccessInfo>(),
      type(a.type), original(a.original), inst(a.inst), addr(a.addr), size(a.size),
    alignment(a.alignment), ROInMT(a.ROInMT)
  {
    stModeOnly = a.stModeOnly;
    mutexProtected = a.mutexProtected;
    escaping = a.escaping;
  }

  // Attempts to classify an instruction as a memory access instruction.
  // An instruction can have at most two memory accesses (e.g., memcpy).
  // Returns the number of memory accesses found (some instructions, e.g.
  // the intrinstic memcpy, can have two accesses).
  static int classify(const DataLayout& TD, LLVMContext& context, Instruction* inst, AccessInfo a[2]);
  static bool isAtomicOp(Instruction* inst);

  void setSizeFromAddr(const DataLayout& TD, LLVMContext& context);
  void setAlignmentFromAddr(const DataLayout& TD);
  bool isLoad() const { return type == Load || type == ExclusiveLoad; }
  bool isStore() const { return type == Store; }
  bool isConstantSize() const { return dyn_cast<ConstantInt>(size) != NULL; }
  uint64_t constantSize() const { return cast<ConstantInt>(size)->getZExtValue(); }

  AccessType type;
  // Instruction containing the original access (debugging only!)
  Instruction* original;
  // The access should be instrumented immediately before 'inst'.
  // Note that 'inst' need not access 'addr'.  For example, ACO will
  // coalesce multiple accesses into one, and PRE will instrument an
  // access in a different basic block than the actual access.
  Instruction* inst;
  // The base address and size of the access.
  Value* addr;
  Value* size;
  // Alignment of this access
  unsigned alignment;

  bool stModeOnly; // Only executed in single threaded mode. But the memory it accesses may be escaping to other threads
  bool mutexProtected; // always accessed in every phase with same lock
  bool escaping; // in systems with restart, this needs to be instrumented only for restarts, not conflicts

  bool ROInMT;
};

// A pair of memory accesses.
struct AccessPairInfo 
{
  AccessPairInfo(AccessInfo &a, AccessInfo &b) : A(a), B(b) {}

  // A pair of accesses: 'A' may dominate 'B', but 'B' doesn't dominate 'A'.
  AccessInfo &A;
  AccessInfo &B;

  // Basic analysis
  bool dominates;     // true iff 'A' dominates 'B'
  bool postdominated; // true iff 'A' is postdominated by 'B'
  bool sameTask;   // true iff 'A' and 'B' are in the same task
  bool sameOFR;   // true iff 'A' and 'B' are part of the same OFR 
                  // (not killed by a barrier, join, cond_wait, etc)
  bool mustAlias;     // true iff 'A' and 'B' must alias

  // PRE analysis
  Instruction* join;  // this dominates 'A' and 'B' in the same task
};

}
