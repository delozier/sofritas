// Identify Thread Sections in a Program.
// This file is just part of ThreadEscapeAnalysis class, but moved to separate file as a separate piece
// released under the University of Santa Cruz Open Source License


#define DEBUG_TYPE "scc"

#include <iostream>
#include "llvm/IR/Operator.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h" 
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/Debug.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Analysis/DominanceFrontier.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopPass.h"
// #include "llvm/Analysis/BlockFrequencyInfo.h"
#include "dsa/DataStructure.h"
#include "dsa/DSGraph.h"
#include "dsa/DSNode.h"
#include "SccProfile.h"
#include "ThreadEscapeAnalysis.h"
#include "identifyTS.h"
#include "SccICFG.h"

// #include "llvm/Transforms/Scalar.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <set>
#include <algorithm>
#include <queue>

using std::vector;
using std::set;
using std::queue;

// std::ofstream escapeDebugOut("escapeAnaDebug.out");

// #define PRINTNODE N->print(dbgs(), ResultGraph);
#define PRINTNODE 

using namespace llvm;

namespace {

static const uint32_t SccUnknownTS = (1<<31);
static bool llvm_function(const Function *F)
{
  return (strncmp(F->getName().data(), "llvm.",5) == 0);
}

// Is this call instcalling this function?
bool isFunction(Instruction *instr, const Function *fn)
{
  CallInst *ci  = dyn_cast<CallInst>(instr);
  if(ci) 
    return (ci->getCalledFunction() == fn);
  InvokeInst *invoke = dyn_cast<InvokeInst>(instr);
  if(invoke)
     return (invoke->getCalledFunction() == fn);

  return false;
}

static bool markTs(Function *func, Instruction* topTsPoint, Instruction *bottomPt)
{
  Module *M = func->getParent();
  assert(M && "No module!");

  Type *VoidTy   = Type::getVoidTy(M->getContext());
  assert(VoidTy && "No VoidTy");

  SmallVector<Value*,1>  operands;
  if(topTsPoint) {
    Constant* startTsMarker = M->getOrInsertFunction(
      "scc_start_ts_auto", VoidTy, NULL);
    assert(startTsMarker && "no function scc_start_ts_auto");
    CallInst *call = CallInst::Create(startTsMarker, operands, "");
    call->insertBefore(topTsPoint);
  }

  if(bottomPt) {
    Constant* endTsMarker = M->getOrInsertFunction(
      "scc_end_ts_auto", VoidTy, NULL);
    assert(endTsMarker && "no function scc_end_ts_auto");
    // Constant* endTsMarker   = M->getFunction("scc_end_ts_auto");
    (void)  CallInst::Create(endTsMarker, operands, "", bottomPt);
  }
  return true;
}

}

// Local classes
// Sort call insts by dominance relationships
struct SCC_sortCallInstDomPostDom
{
   SplicedCFG *spCfg;

   SCC_sortCallInstDomPostDom(SplicedCFG *cfg) { spCfg = cfg; }

   bool operator()(const SCC_CallInstDomPostDom& a, const SCC_CallInstDomPostDom& b) 
   {
      if(a.domOrPostDom != b.domOrPostDom)
        return spCfg->dominates(a.domOrPostDom, b.domOrPostDom);

      return false;
   }
};


// A grouping class for grouping thread start and end calls
// This should form a lattic structure. If not, split into multiple.
class SCC_CreateJoinTSGroup
{
  public:
    SplicedCFG *spCfg; // doesn't own
    Loop *parentLoop;

    // top (supremum) and bottom ( infinimum)  points of the lattice
    Instruction *top;
    Instruction *bottom; 

    vector<SCC_CallInstDomPostDom> startTsCalls;
    vector<SCC_CallInstDomPostDom> endTsCalls;
    vector<SCC_CallInstDomPostDom> createCalls;
    vector<SCC_CallInstDomPostDom> joinCalls;

    vector<SCC_CallInstDomPostDom> barriers;
    vector<SCC_CallInstDomPostDom> functionCalls;
    bool _difficult;

    vector<SCC_CallInstDomPostDom> allCalls;

    void init() {
      parentLoop = NULL;
      spCfg = NULL;
      _difficult = false; 
      top = NULL;
      bottom = NULL;
    }

    SCC_CreateJoinTSGroup() { 
      init();
    } 

    SCC_CreateJoinTSGroup(SplicedCFG *cfg) 
    {
      init();
      spCfg = cfg;
    } 

    void setLoop(Loop *l) { parentLoop = l; }
    void setSpCfg(SplicedCFG *cfg) { spCfg = cfg; }

    void sortCalls(vector<CallInst *>& calls)
    {
      for(int i = 0; i < (int) calls.size() -1; i++) {
        int minIdx = i;
        for(int j = i+1; j < (int) calls.size(); j++) {
          if(spCfg->dominates(calls[j], calls[minIdx])) {
            minIdx = j;
          }
        }

        if(minIdx != i) { // swap the two
          CallInst *ci = calls[minIdx];
          calls[minIdx] = calls[i];
          calls[i] = ci;
        }
      }
    }

    bool empty() {
      bool retval = startTsCalls.empty() && endTsCalls.empty() && createCalls.empty()
                      && joinCalls.empty() && barriers.empty() && functionCalls.empty();
      return retval;
    }

    void transfer(vector<SCC_CallInstDomPostDom>& dest, 
      vector<SCC_CallInstDomPostDom>& src, bool clearSrc) 
    {
      for(size_t i = 0; i < src.size(); i++) 
        dest.push_back(src[i]); // no uniqueness check here; caller assumes risk

      if(clearSrc) src.clear();
    }

   // merge contents of another into this
   void merge(SCC_CreateJoinTSGroup& other) {
      this->transfer(startTsCalls, other.startTsCalls, true);
      this->transfer(endTsCalls, other.endTsCalls, true);
      this->transfer(createCalls, other.createCalls, true);
      this->transfer(joinCalls, other.joinCalls, true);
      this->transfer(barriers, other.barriers, true);
      this->transfer(functionCalls, other.functionCalls, true);
   }

   // sort start_ts, end_ts, pthread_create and pthread_join calls
   void sortAllCalls()
   {
     // vector<CallInst *> allCalls;
     this->transfer(allCalls, startTsCalls, false);
     this->transfer(allCalls, endTsCalls, false);
     this->transfer(allCalls, createCalls, false);
     this->transfer(allCalls, joinCalls, false);

     if(allCalls.size() > 1)  {
        SCC_sortCallInstDomPostDom sortObj(spCfg);
        std::sort(allCalls.begin(), allCalls.end(), sortObj);
     }
   }

  // 
  bool isOKForTSA() {
     bool hasStartEndTS = (startTsCalls.size() > 0 || endTsCalls.size() > 0);
     if(hasStartEndTS && (startTsCalls.size()  == 0 || endTsCalls.size() == 0))
       return false;

     SCC_CallInstDomPostDom lastCreateCall(NULL);
     for(size_t i = 0, ie = allCalls.size(); i < ie; i++) {
       // TBD
     }
     return true;
  }

  bool hasMeetJoin() {
    bool noStartTSOrEndTS = (startTsCalls.empty() && endTsCalls.empty());
    vector<SCC_CallInstDomPostDom>& starts = noStartTSOrEndTS ? createCalls : startTsCalls;
    vector<SCC_CallInstDomPostDom>& ends = noStartTSOrEndTS ? joinCalls : endTsCalls;
    
    if(starts.empty() || ends.empty())
      return false;

    for(size_t i = 0; i < starts.size(); i++) {
      for(size_t j = 0; j < ends.size(); j++) {
        if(spCfg->dominates(starts[i].domOrPostDom, ends[j].domOrPostDom) == false || 
          spCfg->postDominates(ends[j].domOrPostDom, starts[i].domOrPostDom) == false) { 

          return false;
        }
      }
    }

    return true;
  }

  void moveToOuterLoop();

  bool hasOnlyStarts() {
    return ((startTsCalls.size() + createCalls.size()) > 0  &&
            (endTsCalls.size() + joinCalls.size()) == 0);
  }

  bool hasOnlyEnds() {
    return ((startTsCalls.size() + createCalls.size()) == 0  &&
            (endTsCalls.size() + joinCalls.size()) > 0);
  }

  // void createEdges(); // create dom or post-dom edges
  // void shiftControlPoints(vector<SCC_CallInstDomPostDom>& starts, vector<SCC_CallInstDomPostDom>& ends);

  // Lattice operations 
  void computeCover();
  bool dominates(SCC_CreateJoinTSGroup& other) {
    assert(spCfg && spCfg == other.spCfg && "different spCfg in SCC_CreateJoinTSGroup::includes");
    return spCfg->dominates(bottom, other.top);
  }

  void markReachableNodes(SplicedCFGNode *start, SplicedCFGNode *bottom, vector<bool>& mark);
  bool overlaps(SCC_CreateJoinTSGroup& other);

  bool includes(SCC_CreateJoinTSGroup& other) {
    assert(spCfg && spCfg == other.spCfg && "different spCfg in SCC_CreateJoinTSGroup::includes");
    assert(top && other.top && "no top in one of envelopes in SCC_CreateJoinTSGroup::includes");
    assert(bottom && other.bottom && "no top in one of envelopes in SCC_CreateJoinTSGroup::includes");
    return (spCfg->dominates(top, other.top) && spCfg->postDominates(bottom, other.bottom));
  }

  bool includes(Instruction *inst)
  {
    return (spCfg->dominates(top, inst) && spCfg->postDominates(bottom, inst));
  }

  bool isMultiLattice(); // TBD
  void splitMultiLattice(); // TBD
  
};

// Do a dfs and mark reachable nods in graph
void SCC_CreateJoinTSGroup::markReachableNodes(
  SplicedCFGNode *node, SplicedCFGNode *bottom, vector<bool>& mark)
{
  if(!node || node->isExitBlk())
    return;

  int id = node->getId();
  if(mark[id]) return;  // reached already

  mark[id] = true;

  if(node == bottom) return;

  vector<SplicedCFGNode *>& outEdges = node->getOutEdges();
  for(size_t i = 0; i < outEdges.size(); i++)
    markReachableNodes(outEdges[i], bottom, mark);
}

// Check if other lattice overlaps with this
bool SCC_CreateJoinTSGroup::overlaps(SCC_CreateJoinTSGroup& other)
{
  if(dominates(other) || other.dominates(*this))
        return false;

  if(includes(other) || other.includes(*this))
        return false;

  vector<SplicedCFGNode *>& nodes = spCfg->getNodes();
  size_t maxNodeId = nodes.size();
  vector<bool> in1(maxNodeId+1);
  vector<bool> in2(maxNodeId+1);

  // Clear all markers of all nodes.
  for(size_t i = 0; i <= maxNodeId; i++) {
    in1.push_back(false);
    in2.push_back(false);
  }

  SplicedCFGNode *topNode = spCfg->getNodeFor(other.top->getParent());
  SplicedCFGNode *bottomNode = spCfg->getNodeFor(other.bottom->getParent());
  markReachableNodes(topNode, bottomNode, in1);

  SplicedCFGNode *otherTopNode = spCfg->getNodeFor(top->getParent());
  SplicedCFGNode *otherBottomNode = spCfg->getNodeFor(bottom->getParent());
  markReachableNodes(otherTopNode, otherBottomNode, in2);

  // check if same BB in both
  bool overlap = false;
  for(size_t i = 0; i <= maxNodeId && overlap == false; i++) {
    const BasicBlock *bb = nodes[i]->getBlock();
    if(bb == topNode->getBlock() && bb == otherBottomNode->getBlock())  {
        // check if top instruction is lower than other.bottom in same BB
        if(!spCfg->dominates(other.bottom, top))
           overlap = true;
    }
    else if(bb == bottomNode->getBlock() && bb == otherTopNode->getBlock()) {
        if(!spCfg->dominates(bottom, other.top))
          overlap = true;
    }
    else  {
        overlap = true;
    }
  }
  return overlap;
}

// Compute supremum and infinimum for the calls in this lattice
void SCC_CreateJoinTSGroup::computeCover()
{
  assert(spCfg);

  vector<SCC_CallInstDomPostDom> allCalls;
  transfer(allCalls, startTsCalls, false);
  transfer(allCalls, endTsCalls, false);
  transfer(allCalls, createCalls, false);
  transfer(allCalls, joinCalls, false);

  assert( !allCalls.empty());
  const Instruction *topc = allCalls[0].domOrPostDom;
  const Instruction *bottomc = allCalls[0].domOrPostDom;
  for(size_t i = 1, ie = allCalls.size(); i < ie; i++) {
    if(topc)
      topc = spCfg->findNearestCommonDominator(topc, allCalls[i].domOrPostDom);
    if(bottomc)
      bottomc = spCfg->findNearestCommonPostDominator(bottomc, allCalls[i].domOrPostDom);
  }

  top = const_cast<Instruction *>(topc);
  bottom = const_cast<Instruction *>(bottomc);
}

// Move to outer loop
void SCC_CreateJoinTSGroup::moveToOuterLoop()
{
  if(!parentLoop) 
    return; // no current loop, can't move to outer loop

  // Move start_ts and create call to preheader
  Instruction *loopDominator = spCfg->getLoopDominator(parentLoop);
  assert(loopDominator && "Loop dominator not found");
  for(size_t i = 0; i < startTsCalls.size(); i++) {
      startTsCalls[i].domOrPostDom = loopDominator;
  }
  for(size_t i = 0; i < createCalls.size(); i++) {
      createCalls[i].domOrPostDom = loopDominator;
  }

  // Mover the join and end_ts call downwards
  Instruction *lowerPt = spCfg->getLoopPostDominator(parentLoop);
  if(lowerPt) {
    for(size_t i = 0; i < endTsCalls.size(); i++)
      endTsCalls[i].domOrPostDom = lowerPt;
    for(size_t i = 0; i < joinCalls.size(); i++)
      joinCalls[i].domOrPostDom = lowerPt;
  }

  parentLoop = parentLoop->getParentLoop();
}


#if 0
// Create a-> b if a dominates b
void SCC_CreateJoinTSGroup::createEdges()
{
  vector<SCC_CallInstDomPostDom> allPoints;

  transfer(allPoints, startTsCalls, false);
  transfer(allPoints, endTsCalls, false);
  transfer(allPoints, createCalls, false);
  transfer(allPoints, joinCalls, false);

  // first clear all old edges
  for(size_t i = 0, ie = allPoints.size(); i < ie; i++) {
    SCC_CallInstDomPostDom& p = allPoints[i];
    p.dominators.clear();
    p.postDominators.clear();
  }

  // create edges.
  for(size_t i = 0, ie = allPoints.size(); i < ie; i++) {
    for(size_t j = 0, je = allPoints.size(); j < je; j++) {
      if(i == j) continue;

      SCC_CallInstDomPostDom& p1 = allPoints[i];
      SCC_CallInstDomPostDom& p2 = allPoints[j];

      if(spCfg->dominates(p1.domOrPostDom, p2.domOrPostDom)) {
          p2.dominators.insert(&p1);
      }
      else if(spCfg->dominates(p2.domOrPostDom, p1.domOrPostDom)) {
          p1.dominators.insert(&p2);
      }

      // Now check post-dominance
      if(spCfg->postDominates(p1.domOrPostDom, p2.domOrPostDom)) {
          p2.postDominators.insert(&p1);
      }
      else if(spCfg->postDominates(p2.domOrPostDom, p1.domOrPostDom)) {
          p1.postDominators.insert(&p2);
      }
    } // for j
  } // for i

  // Remove transitive edges
  for(size_t i = 0, ie = allPoints.size(); i < ie; i++) {

    // Remove 'a' if it dominates some 'b' in dominators
    set<SCC_CallInstDomPostDom *>& dominators = allPoints[i].dominators;
    vector<SCC_CallInstDomPostDom *> all;
    for( set<SCC_CallInstDomPostDom *>::iterator itr = dominators.begin(); itr != dominators.end(); itr++)
      all.push_back(*itr);

    dominators.clear();
    for(size_t j = 0; j < all.size(); j++) {
      bool dominatesSomeone = false;
      for(size_t k = 0; k < all.size(); k++) {
        if(k != j && spCfg->dominates(all[j]->domOrPostDom, all[k]->domOrPostDom)) {
          dominatesSomeone = true;
          break;
        }
      }

      if(!dominatesSomeone)
        dominators.insert(all[j]);
    }

    // Do the same for post-dominators. Remove a if some b post-dominates a
    set<SCC_CallInstDomPostDom *>& postDominators = allPoints[i].postDominators;
    all.clear();
    for(set<SCC_CallInstDomPostDom *>::iterator itr = postDominators.begin(); itr != postDominators.end(); itr++)
      all.push_back(*itr);

    postDominators.clear();
    for(size_t j = 0; j < all.size(); j++) {
      bool dominatedBySomeone = false;
      for(size_t k = 0; k < all.size(); k++) {
        if(k != j && spCfg->dominates(all[k]->domOrPostDom, all[j]->domOrPostDom)) {
          dominatedBySomeone = true;
          break;
        }
      }

      if(!dominatedBySomeone)
        postDominators.insert(all[j]);
    }
  }

  // Now, if a dominates b, but b doesn't post-dominate a, move a's control point higher up
  // till b post-dominates a.
  shiftControlPoints(startTsCalls, endTsCalls);
  shiftControlPoints(createCalls, joinCalls);
}


// Try to fix these situations  : try to merge J1 & J2, C1 & C2. LLVM IR sometimes creates case 1
// to optimize something else.
//    case 1         case 2
//      C           C1   C2
//      |           |    |
//    ------        ------
//   |     |          |
//   J1    J2         J

// TBD 
void SCC_CreateJoinTSGroup::shiftControlPoints(vector<SCC_CallInstDomPostDom>& starts,
            vector<SCC_CallInstDomPostDom>& ends)
{
  // TBD
  for(size_t i = 0; i < starts.size(); i++) {
    for(size_t j = i+1; j < starts.size(); j++) {
    }
  }
}
#endif


/*********************************************************************/
/* Find thread sections in function - more general and robust method */
/*********************************************************************/

// Return: true if succesful, false if failed
bool ThreadEscapeAnalysis::findThreadSectionsInFunction2(Function *F, 
    std::set<Function *>& visitedFuncs, bool recursive)
{
  if(visitedFuncs.find(F) != visitedFuncs.end())
    return true; // already visited

  visitedFuncs.insert(F);

  LoopInfo *LI = getLoopInfo(F);
  assert(LI);

  // Group the TS start and end calls by Loop.
  std::map<Loop *, SCC_CreateJoinTSGroup> callGroups;
  std::map<Loop *, SCC_CreateJoinTSGroup>::iterator  callGroupItr;
  std::set<Function *> calledFuncs;

  // Check if there is a pthread_create call here
  int numCreate = 0, numJoin = 0;
  int numStartTS = 0, numEndTS = 0;
  for(Function::iterator itr1 = F->begin(); itr1 != F->end(); itr1++) 
  {
    BasicBlock *bb = itr1;
    Loop *loop = LI->getLoopFor(bb); // loop can be NULL if bb is outside all loops
    for(BasicBlock::iterator itr = bb->begin(); itr != bb->end(); itr++) {
       Instruction *I = itr; 
       if(isa<CallInst>(I) || isa<InvokeInst>(I)) {
          CallInst *CI = dyn_cast<CallInst>(I);
          InvokeInst *II = dyn_cast<InvokeInst>(I);
          Function *CF = CI ? CI->getCalledFunction() :
                               II ? II->getCalledFunction() : NULL;
          if(CF && !llvm_function(CF)) {
              if(CF == ThreadCreate) {
                callGroups[loop].createCalls.push_back(I);
                numCreate++;
              }
              else if(CF == ThreadJoin) {
                callGroups[loop].joinCalls.push_back(I);
                numJoin++;
              }
              else if(!CF->isDeclaration() && visitedFuncs.find(CF) == visitedFuncs.end()) 
                calledFuncs.insert(CF);
            } // if CF
      } // if call inst or invoke inst
    } // for each instruction
  } // for each BB

  // User specified regions
  if(numStartTS > 0 || numEndTS > 0) {
    if(!(numStartTS > 0 && numEndTS > 0 /* && numCreate > 0 && numJoin > 0 */))  {
      std::string fname = F->getName();
      fprintf(stderr,"SCC-ERROR: Function %s has start_ts or end_ts, but missing thread_create or thread_join call.\n",
              fname.c_str());
      return false;
    }
    return findThreadSectionsWithUserDirectives(F, visitedFuncs, recursive, callGroups, calledFuncs);
  }

  // If found thread starts and end, populate new thread section
  bool okayForTSA = true;
  if(numCreate == 0 && numJoin == 0) {
      // fprintf(stderr,"   No thread create or join in function.\n");
    okayForTSA = false;
  }
  
  if(okayForTSA) {
    return findThreadSectionsWithoutUserDirectives(F, visitedFuncs, recursive, callGroups, calledFuncs);
  }

  // Now, recurse into other called functions, for each TS. Only do this if this is not part
  // of any TS here
  if(recursive) {
    for(std::set<Function *>::iterator itr = calledFuncs.begin(); itr != calledFuncs.end(); itr++) 
    {
      Function *func = *itr;
      findThreadSectionsInFunction2(func, visitedFuncs, recursive);
    }
  }

  return true;
}

// Find thread sections in a function that has start_ts and end_ts
// Return: false if error
// Assume: Caller already checked that it has at least one start_ts, end_ts, thread_create and thread_join
bool ThreadEscapeAnalysis::findThreadSectionsWithUserDirectives(Function *F,
    std::set<Function *>& visitedFuncs, bool recursive,
    std::map<Loop *, SCC_CreateJoinTSGroup>& callGroups,
    std::set<Function *> calledFuncs)
{
  // Algo:
  // 0. Collect all the S,E,C and J calls
  // 1. Create S->E edges if E postdom S ( Match S & E)
  // 2. Remove transitive edges if any
  // 3. Create lattice S(i) -> E(i) if connected.
  // 4. Distribute the create and join calls to each of these lattices
  // 5. if floating S/E or C/J, return false;
  // 6. Merge overlapping lattices
  // 7. Create TS for each lattice
 
  // Merge into a single group first
  SCC_CreateJoinTSGroup oneGroup;
  map<Loop *, SCC_CreateJoinTSGroup>::iterator itr;

  vector<SCC_CallInstDomPostDom> startTsCalls;
  vector<SCC_CallInstDomPostDom> endTsCalls;
  vector<SCC_CallInstDomPostDom> createCalls;
  vector<SCC_CallInstDomPostDom> joinCalls;

  SplicedCFG spCfg(F);
  
  // 0. Collect all the S,E,C and J calls
  for(itr = callGroups.begin(); itr != callGroups.end(); itr++) {
      SCC_CreateJoinTSGroup& grp = itr->second;

      grp.setSpCfg(&spCfg);
      grp.setLoop(itr->first);

      grp.transfer(startTsCalls, grp.startTsCalls, false);
      grp.transfer(endTsCalls, grp.endTsCalls, false);
      grp.transfer(createCalls, grp.createCalls, false);
      grp.transfer(joinCalls, grp.joinCalls,false);
  }
  callGroups.clear();


  // 2. Create edges from one to other if dominates
  // 3. remove transitive edges
  /* SCC_sortCallInstDomPostDom sortObj(&spCfg);
  if(startTsCalls.size() > 1)
    std::sort(startTsCalls.begin(), startTsCalls.end(), sortObj); */

  vector<SCC_CreateJoinTSGroup> newGrps;
  for(size_t i = 0; i < endTsCalls.size(); i ++) {
    SCC_CallInstDomPostDom& e = endTsCalls[i];

    vector<SCC_CallInstDomPostDom *> matchedStarts;

    // We are checking start_ts from top to bottom, 
    for(size_t j = 0; j < startTsCalls.size(); j++) {
      SCC_CallInstDomPostDom& s = startTsCalls[j];

      if(spCfg.postDominates(e.domOrPostDom, s.domOrPostDom)) {
    
        // if this s is post-dominated by some other e that domiinates this e, skip that s
        bool hasOtherEndTs = false;
        for(size_t k = 0; k < endTsCalls.size(); k++) {
          if(i != k && spCfg.postDominates(endTsCalls[k].domOrPostDom,s.domOrPostDom)) {
              hasOtherEndTs = true;
          }
        }

        if(!hasOtherEndTs) {
          s.postDominators.insert(&e);
          matchedStarts.push_back(&s);
        }
      }
    } // for each start ts call

    // Create envelope with e as bottom
    SCC_CreateJoinTSGroup grp(&spCfg);
    e.included = true;
    grp.endTsCalls.push_back(e);

    bool unmatchedEnd = matchedStarts.empty();
    if(unmatchedEnd)  {
      fprintf(stderr,"SCC-ERROR: Could not find scc_start_ts for scc_end_ts.\n");
      return false;
    }
    for(size_t j = 0; j < matchedStarts.size(); j++) {
      SCC_CallInstDomPostDom *m = matchedStarts[j];
      grp.startTsCalls.push_back(*m);
      m->included = true;
    } // for each matched start_ts call

    grp.computeCover(); // compute top and bottom

    newGrps.push_back(grp);
  } // for each end ts calls

  bool floatingCreate = false, floatingJoin = false;
  // 4.a) Assign create calls to lattices
  for(size_t i = 0; i < createCalls.size(); i++)  {
    SCC_CallInstDomPostDom& c = createCalls[i];
    for(size_t j = 0; j < newGrps.size(); j++) {
      if(newGrps[j].includes(c.domOrPostDom)) {
        newGrps[j].createCalls.push_back(c);
        c.included = true;
        break;
      }
    } // try each group
    if(!c.included)
        floatingCreate = true;
  }

  // 4.b) Assign join calls to lattices
  for(size_t i = 0; i < joinCalls.size(); i++)  {
    SCC_CallInstDomPostDom& c = joinCalls[i];
    for(size_t j = 0; j < newGrps.size(); j++) {
      if(newGrps[j].includes(c.domOrPostDom)) {
        newGrps[j].joinCalls.push_back(c);
        c.included = true;
        break;
      }
    } // try each group
    if(!c.included)
        floatingJoin = true;
  }

  // 5.b) Check that all S and E calls are covered
  bool floatingStarts = false;
  for(size_t i = 0; i < startTsCalls.size(); i++)
    if(!startTsCalls[i].included)
      floatingStarts = true;

  if(floatingCreate || floatingJoin || floatingStarts) {
    fprintf(stderr,"Unmatched create or join or tsart_ts call in function with start_ts and end_ts.\n");
    return false;
  }

  // 6. Merge overlapping groups


  // 7. Create TS for each group
  int numTS = 0;
  for(size_t i = 0; i < newGrps.size(); i++) {
    SCC_CreateJoinTSGroup& grp = newGrps[i];
    assert(grp.top && "No top in lattice!");
    assert(grp.bottom && "No bottom in lattice with start_ts and end_ts!");

    vector<Instruction *> creates;
    vector<Instruction *> joins;
    vector<SCC_CallInstDomPostDom>& calls1 = grp.createCalls;
    vector<SCC_CallInstDomPostDom>& calls2  = grp.createCalls;
    for(size_t k = 0; k < calls1.size(); k++) {
      creates.push_back(calls1[k].ci);
    }
    for(size_t k = 0; k < calls2.size(); k++) {
      joins.push_back(calls2[k].ci);
    }

    if(createTS(&spCfg, F, grp.top->getParent(), grp.bottom->getParent(), grp.top, grp.bottom,
            creates, joins))
        numTS++;
  }

  // Now, recurse into other called functions, for each TS. Only do this if this is not part
  // of any TS here
  if(recursive) {
    for(std::set<Function *>::iterator itr = calledFuncs.begin(); itr != calledFuncs.end(); itr++) 
    {
      Function *func = *itr;
      findThreadSectionsInFunction2(func, visitedFuncs, recursive);
    }
  }

  return true;
}

// Find thread sections in a function with no start_ts and end_ts
// Return: false if error
bool ThreadEscapeAnalysis::findThreadSectionsWithoutUserDirectives(Function *F,
    std::set<Function *>& visitedFuncs, bool recursive,
    std::map<Loop *, SCC_CreateJoinTSGroup>& callGroups,
    std::set<Function *> calledFuncs)
{
  // Algo:
  // 0. Collect all the C and J calls
  // 1. Create C->J edges if J postdom C ( Match C & J)
  // 2. Remove transitive edges if any
  // 3. Create lattice C(i) -> J(i) if connected.
  // 5. if floating C/J, return false;
  // 6. Merge overlapping lattices
  // 7. Create TS for each lattice
 
  // Merge into a single group first
  SCC_CreateJoinTSGroup oneGroup;
  map<Loop *, SCC_CreateJoinTSGroup>::iterator itr;


  vector<SCC_CallInstDomPostDom> startTsCalls;
  vector<SCC_CallInstDomPostDom> endTsCalls;
  vector<SCC_CallInstDomPostDom> createCalls;
  vector<SCC_CallInstDomPostDom> joinCalls;
  
  SplicedCFG spCfg(F);

  // 0. Collect all the S,E,C and J calls
  for(itr = callGroups.begin(); itr != callGroups.end(); itr++) {
      SCC_CreateJoinTSGroup& grp = itr->second;
      grp.setSpCfg(&spCfg);
      grp.setLoop(itr->first);

      assert(grp.startTsCalls.empty()); // grp.transfer(startTsCalls, grp.startTsCalls, false);
      assert(grp.endTsCalls.empty());   // grp.transfer(endTsCalls, grp.endTsCalls, false);
  
      // 0.5 move lonely C/J out of loops
      if((grp.joinCalls.empty() || grp.createCalls.empty()) && itr->first != NULL) {
        grp.moveToOuterLoop(); 
      }

      grp.transfer(createCalls, grp.createCalls, false);
      grp.transfer(joinCalls,   grp.joinCalls, false);
  }

  if(!(createCalls.size() > 0 && joinCalls.size() > 0)) {
    std::string fname = F->getName();
    fprintf(stderr,"SCC-ERROR: Cannot evaluate Thread Sections where thread create and end in different functions! Function is %s\n",
              fname.c_str());
    return false;
  }

  callGroups.clear();

  // 2. Create edges from one to other if dominates
  // 3. Remove transitive edges
  /* SCC_sortCallInstDomPostDom sortObj(&spCfg);
  if(createCalls.size() > 1)
    std::sort(createCalls.begin(), createCalls.end(), sortObj); */

  vector<SCC_CreateJoinTSGroup> newGrps;
  for(size_t i = 0; i < joinCalls.size(); i ++) {
    SCC_CallInstDomPostDom& e = joinCalls[i];

    vector<SCC_CallInstDomPostDom *> matchedStarts;

    // We are checking start_ts from top to bottom, 
    for(size_t j = 0; j < createCalls.size(); j++) {
      SCC_CallInstDomPostDom& s = createCalls[j];

      if(spCfg.postDominates(e.domOrPostDom, s.domOrPostDom)) {
    
        // if this s is post-dominated by some other e that domiinates this e, skip that s
        bool hasOtherEnd = false;
        for(size_t k = 0; k < joinCalls.size(); k++) {
          if(i != k && spCfg.postDominates(joinCalls[k].domOrPostDom,s.domOrPostDom)) {
              hasOtherEnd = true;
          }
        }

        if(!hasOtherEnd) {
          s.postDominators.insert(&e);
          matchedStarts.push_back(&s);
        }
      }
    } // for each start ts call

    // Create envelope with e as bottom
    SCC_CreateJoinTSGroup grp(&spCfg);
    e.included = true;
    grp.joinCalls.push_back(e);

    bool unmatchedEnd = matchedStarts.empty();
    if(!unmatchedEnd)  {
      for(size_t j = 0; j < matchedStarts.size(); j++) {
        SCC_CallInstDomPostDom *m = matchedStarts[j];
        grp.createCalls.push_back(*m);
        m->included = true;
      } // for each matched start_ts call

      grp.computeCover(); // compute top and bottom

      newGrps.push_back(grp);
    }
  } // for each end ts calls

  bool floatingCreate = false, floatingJoin = false;
  // 4.a) Assign create calls to lattices
  for(size_t i = 0; i < createCalls.size(); i++)  {
    SCC_CallInstDomPostDom& c = createCalls[i];
    if(c.included) continue;
    for(size_t j = 0; j < newGrps.size(); j++) {
      if(newGrps[j].includes(c.domOrPostDom)) {
        newGrps[j].createCalls.push_back(c);
        c.included = true;
        break;
      }
    } // try each group
    if(!c.included)
        floatingCreate = true;
  }

  // 4.b) Assign join calls to lattices
  for(size_t i = 0; i < joinCalls.size(); i++)  {
    SCC_CallInstDomPostDom& c = joinCalls[i];
    if(c.included) continue;
    for(size_t j = 0; j < newGrps.size(); j++) {
      if(newGrps[j].includes(c.domOrPostDom)) {
        newGrps[j].joinCalls.push_back(c);
        c.included = true;
        break;
      }
    } // try each group
    if(!c.included)
        floatingJoin = true;
  }

  if(floatingCreate || floatingJoin) {
    if(true /* newGrps.empty() */) {
      fprintf(stderr,"SCC-WARNING: Unmatched create or join in function with no start_ts/end_ts. Creating one TS for %d create calls and %d join calls.\n", createCalls.size(), joinCalls.size());
      SCC_CreateJoinTSGroup grp(&spCfg);

      for(size_t i = 0; i < createCalls.size(); i++)  {
        SCC_CallInstDomPostDom& c = createCalls[i];
        grp.createCalls.push_back(c);
      }
      
      for(size_t i = 0; i < joinCalls.size(); i++)  {
        SCC_CallInstDomPostDom& c = joinCalls[i];
        grp.joinCalls.push_back(c);
      }

      grp.computeCover();
      newGrps.push_back(grp);
    }
    else {
      fprintf(stderr,"SCC-ERROR: Unmatched create or join in function with no start_ts/end_ts.\n");
      return false;
    }
  }

  // 6. TBD: Merge overlapping groups


  // 7. Create TS for each group
  int numTS = 0;
  for(size_t i = 0; i < newGrps.size(); i++) {
    SCC_CreateJoinTSGroup& grp = newGrps[i];
    assert(grp.top && "No top in lattice!");
    assert(grp.bottom && "No bottom in lattice with start_ts and end_ts!");

    vector<Instruction *> creates;
    vector<Instruction *> joins;
    vector<SCC_CallInstDomPostDom>& calls1 = grp.createCalls;
    vector<SCC_CallInstDomPostDom>& calls2  = grp.createCalls;
    for(size_t k = 0; k < calls1.size(); k++) {
      creates.push_back(calls1[k].ci);
    }
    for(size_t k = 0; k < calls2.size(); k++) {
      joins.push_back(calls2[k].ci);
    }

    if(createTS(&spCfg, F, grp.top->getParent(), grp.bottom->getParent(), grp.top, grp.bottom,
            creates, joins))
        numTS++;
  }

  // Now, recurse into other called functions, for each TS. Only do this if this is not part
  // of any TS here
  if(recursive) {
    for(std::set<Function *>::iterator itr = calledFuncs.begin(); itr != calledFuncs.end(); itr++) 
    {
      Function *func = *itr;
      findThreadSectionsInFunction2(func, visitedFuncs, recursive);
    }
  }

  return true;
}

static bool callIsEnclosedBy(const Instruction *beginInst,
            const Instruction *endInst, Instruction *callInst, SplicedCFG *spCfg)
{
  assert(beginInst && endInst && callInst && spCfg);
  return (spCfg->dominates(beginInst,callInst) && spCfg->postDominates(endInst,callInst));
}

// Get the create and starts enclosed in the (start and end) instruction.
// Remove those from original vector, and 
// CAUTION: Modifies input vectors origCreates and origEnds
static void getCreateJoinsEnclosedByInstructions(Instruction *startTsInst, Instruction *endTsInst,
    vector<SCC_CallInstDomPostDom>& createCalls, vector<SCC_CallInstDomPostDom>& joinCalls,
    vector<Instruction *>& enclosedCreates, vector<Instruction *>& enclosedJoins,
    SplicedCFG *spCfg)
{
  assert(startTsInst && endTsInst && "startTsInst && endTsInst false");

  // Collect all scc_thread_create and scc_thread_join that are enclosed by these.
  // If their parent block is startTsBlock or endTsBlock, check that the instruction belongs to
  // a region included by the actual start_ts or end_ts function call.
  for(size_t i = 0; i < createCalls.size(); ) {
    if(callIsEnclosedBy(startTsInst, endTsInst, createCalls[i].ci, spCfg)) {
      enclosedCreates.push_back(createCalls[i].ci);

      // remove i-th elem from createCalls
      createCalls[i] = createCalls.back();
      createCalls.pop_back();
    }
    else
      i++;
  }

  for(size_t i = 0; i < joinCalls.size(); ) {
    if(callIsEnclosedBy(startTsInst, endTsInst, joinCalls[i].ci, spCfg)) {
      enclosedJoins.push_back(joinCalls[i].ci);

      // remove i-th elem from joinCalls
      joinCalls[i] = joinCalls.back();
      joinCalls.pop_back();
    }
    else
      i++;
  }
}

// Create one TS from the pthread_create and pthread_jooin calls
bool ThreadEscapeAnalysis::createOneTS(vector<SCC_CallInstDomPostDom>& calls, 
    size_t startIdx, size_t endIdx, SplicedCFG *spCfg)
{
  vector<SCC_CallInstDomPostDom> starts, joins;
  for(size_t i = startIdx; i  < calls.size() && i < endIdx; i++) {
    if(isFunction(calls[i].ci, ThreadCreate))
      starts.push_back(calls[i]);
    else
      joins.push_back(calls[i]);
  }

  Instruction *nearestStartDominator  = starts[0].domOrPostDom;
  for(size_t i = 1; nearestStartDominator && i < starts.size(); i++) {
    Instruction *inst  = starts[i].domOrPostDom;
    nearestStartDominator = const_cast<Instruction *>(spCfg->findNearestCommonDominator(nearestStartDominator, inst));
  }

  // Find nearest common post dom of end calls
  Instruction *nearestEndPostDom = joins[0].domOrPostDom;
  for(size_t i = 1; nearestEndPostDom && i < joins.size(); i++) {
      Instruction *inst  =  joins[i].domOrPostDom;
      nearestEndPostDom = inst ? const_cast<Instruction *>(spCfg->findNearestCommonDominator(nearestEndPostDom, inst)) 
                          : NULL;
  }

  vector<Instruction *> tStarts;
  vector<Instruction *> tJoins;
  for(size_t i = 0; i < starts.size(); i++)
    tStarts.push_back(starts[i].ci);
  for(size_t i = 0; i < joins.size(); i++)
    tJoins.push_back(joins[i].ci);

  if((nearestStartDominator && nearestEndPostDom) &&
    (spCfg->dominates(nearestStartDominator, nearestEndPostDom) ||
     spCfg->postDominates(nearestEndPostDom, nearestStartDominator))) {

      markTs(spCfg->getFunction(), nearestStartDominator, nearestEndPostDom);

      return createTS(spCfg, spCfg->getFunction(), nearestStartDominator->getParent(), nearestEndPostDom->getParent(),
                    nearestStartDominator, nearestEndPostDom, tStarts, tJoins);
  }
  return false;
}


// Create TS for create and join calls in a group of calls. 
// 0) This shouldn't be called when user has specified sections with start_ts and end_ts
// 1) Assume that the lonely creates in loop are moved to nearest loop dominator. 
// 2) Lonely join calls are moved to nearest loop post-dominator.
// 3) The create(C) and joins(J) calls can be arranged as C...C-J..J -> C...C-J...J-> and so on.
//    That is, we can create multiple or single TS from these. If it is CJCJ mixup, this function won't work.
//    That must be checked before calling this function.

bool ThreadEscapeAnalysis::createThreadSectionsInGroup(Function *F,  SCC_CreateJoinTSGroup *tsGroup, bool recursive)
{
  SplicedCFG *spCfg = tsGroup->spCfg;
  LoopInfo *LI = getLoopInfo(F);
  SCC_sortCallInstDomPostDom sortObj(spCfg);
  assert(LI);

  // Find enclosing blocks for these thread starts and joins

  vector<SCC_CallInstDomPostDom>& threadStarts = tsGroup->createCalls;
  vector<SCC_CallInstDomPostDom>& threadJoins = tsGroup->joinCalls;
  vector<SCC_CallInstDomPostDom>& startTSCalls = tsGroup->startTsCalls;
  vector<SCC_CallInstDomPostDom>& endTSCalls = tsGroup->endTsCalls;

  /* vector<CallInst *> barriers;
  vector<CallInst *> functionCalls; */

  assert(startTSCalls.empty() && endTSCalls.empty() && "createThreadSectionsInGroup called for user specified TS");

#if 0
  if(hasStartEndTS && ( startTSCalls.size() + endTSCalls.size()) >= 2)  {
    //  User directive. Arrange as start_ts -> (creates,ends) -> end_ts ->  start_ts -> (creates, ends) -> end_ts ...
    //  TBD:  How to handle replication of calls in multiple blocks by llvm?

    vector<SCC_CallInstDomPostDom> calls = startTSCalls;
    for(size_t i = 0, ie = endTSCalls.size(); i < ie; i++) {
      calls.push_back(endTSCalls[i]);
    }

    if(calls.size() > 2) {
      std::sort(calls.begin(), calls.end(), sortObj);
    }

    unsigned s = 0, e = 1;
    for(; e < calls.size(); e++) {
      if(e+1 == calls.size() ) {
        // time to create a TS.

        const Instruction *nearestStartDominator = NULL;
        const Instruction *nearestEndPostDom = NULL;
        for(unsigned i = s; i < e; i++) {
          Instruction *inst  = calls[i].domOrPostDom;
          if(isFunction(calls[i].ci, StartTSFn)) {
            nearestStartDominator = nearestStartDominator ?
                spCfg->findNearestCommonDominator(nearestStartDominator, inst) : inst;
            assert(nearestStartDominator);
          }
          else {
            nearestEndPostDom = nearestEndPostDom ? 
                  spCfg->findNearestCommonDominator(nearestEndPostDom, inst) : inst;
            assert(nearestEndPostDom);
          }
        } 

        vector<CallInst *> enclosedCreates;
        vector<CallInst *> enclosedJoins;
        SCC_CallInstDomPostDom& start_ts = calls[s];
        SCC_CallInstDomPostDom& end_ts = calls[e];

        getCreateJoinsEnclosedByInstructions(start_ts.domOrPostDom, end_ts.domOrPostDom, 
                      threadStarts, threadJoins, enclosedCreates, enclosedJoins, tsGroup->spCfg);
        fprintf(stderr,"TSA: Creating a thread section\n");


        if(createTS(spCfg, F, start_ts.getParent(), end_ts.getParent(), start_ts.domOrPostDom, end_ts.domOrPostDom, 
            enclosedCreates, enclosedJoins))
            numTS++;

        s = e; // starts a new TS
      } // 
    }

    return true;
  }
#endif

  // sort the create and joins by dominance
  vector<SCC_CallInstDomPostDom> calls = threadStarts;
  for(size_t i = 0; i < threadJoins.size(); i++) {
    calls.push_back(threadJoins[i]);
  }

  if(calls.size() > 1) { 
    std::sort(calls.begin(), calls.end(), sortObj);
  }    

  // bool okayForTSA = true;
  size_t start = 0, end = 0;
  while(end < calls.size()) {
    if(end+1 < calls.size() && 
      isFunction(calls[end].ci, ThreadJoin) && isFunction(calls[end+1].ci, ThreadCreate)) {
      // TBD: check that this join dominates rest of the funcs
      // TBD: prove with trip counts that all created functions are joined

      createOneTS(calls, start, end+1, spCfg);
      start = end+1;
    }
    else if(end+1 == calls.size()) {
      createOneTS(calls, start, end+1, spCfg);
      start = end+1;
    }
    end++;
  }

  return true;
}


// Traverse the globa icfg from the top of each TS, and also from each sync_all node
// that has a single incoming edge and single outgoing edge. 
// Mark traversed portion with a new phaseId
// Return: Number of such phases
// THIS IS CALLED FOR AN ICFG FOR EACH TS. So, should have single startTsNodes,
// and single endTsNode
void ThreadEscapeAnalysis::markDTSInTS(SccICFG *icfg)
{
  vector<SccICFGNode *>& startDTSNodes = icfg->getTsStarts();
  vector<SccICFGNode *>& endTsNodes = icfg->getTsEnds();
  assert(startDTSNodes.size() == 1 && "multiple startTsNodes");
  assert(endTsNodes.size() == 1 && "multiple endTsNodes");

  SccICFGNode *s = startDTSNodes[0]; 
  SccICFGNode *e = endTsNodes[0];

  // get all the DTS starts
  vector<SccICFGNode *> barriers ; 
  bool singleBarrier = true;
  while(s != e && singleBarrier) {
    barriers.push_back(s);

    set<SccICFGNode *> terminals;
    icfg->collectTerminalNodes(s, e, terminals);
    if(terminals.size() > 1) {
       singleBarrier = false;
    }
    else if(terminals.size() == 1) {
      s = *(terminals.begin());
      // push this only if the sync node is not already in barriers
      bool found = false;
      for(size_t i = 0; i < barriers.size(); i++) {
        if(barriers[i] == s) {
          found = true;
        }
      }

      if(found) 
        break;
      else
        fprintf(stderr,"Found barrier with ID %d\n", s->getSyncNodeId());
    }
    else
      break; // multiple barriers reachable, end quest for barriers.
  }

  barriers.push_back(e);

  set<SccICFGNode *> allBarriers;
  for(size_t i = 0; i < barriers.size(); i++)
    allBarriers.insert(barriers[i]);

  for(int i = 0, ie = barriers.size(); i < ie-1; i++) {
    curDTSId++;
    markOneDTS(barriers[i], allBarriers , curDTSId);
  }
}

// Mark the begin and end of DTS. Stop at all global barriers identified earlier
void ThreadEscapeAnalysis::markOneDTS(SccICFGNode *start, const set<SccICFGNode *>& allBarriers, int dtsId)
{
    SccICFGNode *node = start;

    // q for bsf
    queue<SccICFGNode *> Q;
    Q.push(node);

    set<SccICFGNode *> seenNodes;
    seenNodes.insert(node);

    // Functions called from here
    set<const Function *> calledFunctions;

    // 2.1 Do BFS till ends are seen
    while(!Q.empty()) {
      node = Q.front();
      Q.pop();

      if(node != start && allBarriers.count(node) > 0)
        continue;

      // For each instruction in node , visit TSA Inst
      BasicBlock *bb = node->getBB();
      int startPos = node->getStartPos();
      int endPos = node->getEndPos();
      int pos = 0;
      for(BasicBlock::iterator itr = bb->begin(); itr != bb->end(); itr++, pos++) {
        if(pos >= startPos && pos < endPos) {
          const Instruction *I = itr;
          visitTSAInstruction(I, calledFunctions, dtsId);
        }
      }

      // Add successor nodes to Q if not seen already
      // Now visit successors
      vector<SccICFGNode *>& succs = node->getSuccs();
      for(size_t j = 0, je = succs.size(); j < je; j++) {
        SccICFGNode *n = succs[j];
        if(seenNodes.find(n) == seenNodes.end()) {
          seenNodes.insert(n);
          Q.push(n);
        }
      }
    } /// while Q is not empty

    // Now, also visit each function call that is without sync_all.
    set<const Function *> visitedFuncs;
    for(std::set<const Function *>::iterator itr = calledFunctions.begin();
         itr != calledFunctions.end(); itr++)
    {
      const Function *F =  const_cast<const Function *>(*itr);
      if(!F->isDeclaration())
        doDFSOnFunction(NULL, F, NULL, NULL, dtsId, visitedFuncs);
    }
}

/***********************************************************************************************/
/*  Build spliced CFG and answer dominance and post-dominance questions */
SplicedCFG::SplicedCFG(Function *func)
{
  assert(func && "Null function ponter");
  _func = func;
  _sentinnelSink = NULL; // built in post-dominator construction if multiple terminal nodes

  // First build all nodes
  for(Function::const_iterator itr = func->begin(); itr != func->end(); itr++) {
     const BasicBlock *bb = itr;
     if(bb->isLandingPad()) continue;
     SplicedCFGNode *n = new SplicedCFGNode(bb);
     assert(n && "Failed to allocate node!");

      _nodes.push_back(n);
      _nodeForBB[bb] = n;
  }

  // Next, add the edges. successors of basic block are outgoing edges.
  for(Function::iterator itr = func->begin(); itr != func->end(); itr++) {
    BasicBlock *bb = itr;
    SplicedCFGNode *n = _nodeForBB[bb];
    if(bb->isLandingPad()) continue;
    assert(n && "Failed to get node for basic block!");

    const TerminatorInst *BBTerm = bb->getTerminator();
    if(!BBTerm) continue;

    for (unsigned i = 0, e = BBTerm->getNumSuccessors(); i < e; ++i) {
      BasicBlock *succ = BBTerm->getSuccessor(i);
      if(succ->isLandingPad() && e > 1) continue;  // target of unwind of an invoke instruction
      SplicedCFGNode *succNode = _nodeForBB[succ];
      if(succNode) {
         n->addOutEdge(succNode);
         succNode->addInEdge(n);
      } // add to-from edge
    } //  for each successor
  } // for each bb

  RemoveExitBranches();

  buildDominatorTree();

  // Remove exit branches before post-dominator analysis, so we ignore the assert etc.
  buildPostDominatorTree();
}

// Delete the spliced cfg
SplicedCFG::~SplicedCFG()
{
  for(size_t i = 0, ie = _nodes.size(); i < ie; i++) {
    delete _nodes[i];
  }
}

static void printBBSet(set<SplicedCFGNode *>& nodes)
{
  for(set<SplicedCFGNode *>::iterator itr = nodes.begin(); itr != nodes.end(); itr++) {
    std::string name = (*itr)->getBlock()->getName();
    std::cerr << "  " << name << std::endl;
  }
}

// Compte a post-order of the CFG
void SplicedCFG::computePostOrder(SplicedCFGNode *node,
        bool postdom, vector<SplicedCFGNode *>& orderedNodes)
{
  if(node->getColor() == SplicedCFGNode::BLACK)
    return;

  node->setColor(SplicedCFGNode::GRAY);

  vector<SplicedCFGNode *>& children = postdom ? node->getInEdges() : node->getOutEdges();
  for(size_t i = 0; i < children.size(); i++) {
    SplicedCFGNode *child = children[i];
    if(child->getColor() == SplicedCFGNode::WHITE) {
      computePostOrder(child,postdom, orderedNodes);
    }
  }

  orderedNodes.push_back(node);
  node->setColor(SplicedCFGNode::BLACK);
}

// Start from entry block. Keep building dominators for successors iteratively, till convergence.
bool SplicedCFG::buildDominatorTree()
{
  unsigned nodeId = 0;
  for(size_t i = 0, ie = _nodes.size(); i < ie; i++) {
    _nodes[i]->setId(nodeId++);
  }
  unsigned maxNodeId = nodeId;

  BasicBlock& entryBlock = _func->getEntryBlock();
  SplicedCFGNode *entryNode = _nodeForBB[&entryBlock];
  assert(entryNode && "No node for entry block");

  /* initialize the dominators array */
  for(size_t i = 0; i < maxNodeId; i++) {
    SplicedCFGNode *n = _nodes[i];
    n->iDom = NULL;
    n->setColor(SplicedCFGNode::WHITE);
  }

  // sort nods in post-order
  vector<SplicedCFGNode *> postOrder;
  computePostOrder(entryNode, false, postOrder);

  // nodeOrder[i] == postion of node with id i in postOrder
  unsigned *nodeOrder = (unsigned *) calloc(maxNodeId+2, sizeof(unsigned));
  for(size_t i = 0; i < postOrder.size(); i++) {
    SplicedCFGNode *n = postOrder[i];
    nodeOrder[n->getId()] = i;
  }

  entryNode->iDom = entryNode;
  bool Changed = true;
  while (Changed) {
    Changed = false;

    // for all nodes, b, in reverse postorder (except entryNode node)
    for(int idx = postOrder.size()-2; idx >= 0; idx--) {
      // last node is entryNode node, so skip that
      SplicedCFGNode *b = postOrder[idx];
      SplicedCFGNode *new_idom =  NULL;

      vector<SplicedCFGNode *>& preds = b->getInEdges();
      for(size_t i = 0; i < preds.size(); i++) {
        SplicedCFGNode *p = preds[i];
        /* i.e., if doms[p] already calculated */
        if (p->iDom) {
          new_idom = new_idom ? intersectDom(p, new_idom, nodeOrder) : p;
        }
      } // for each pred

      if (b->iDom  != new_idom) {
        b->iDom = new_idom;
        Changed = true;
      }
    } // in reverse post order
  } // while changed
  free(nodeOrder);

  // printDominators();
  return true;
}

// Start from exit blocks. Keep building dominators for predecessors iteratively, till convergence.
bool SplicedCFG::buildPostDominatorTree()
{
  unsigned nodeId = 0;

  if(_nodes.size() <= 1) {
    if(_nodes.size())
      _nodes[0]->iPostDom = _nodes[0];

    return true;
  }

  vector<SplicedCFGNode *> terminals;

  for(size_t i = 0, ie = _nodes.size(); i < ie; i++) {
    SplicedCFGNode *node = _nodes[i];
    node->setId(nodeId++);
    node->setColor(SplicedCFGNode::WHITE);
    if(node->getOutEdges().empty()) { // terminal nodes
        if(!node->isExitBlk())
          terminals.push_back(node);
    }
  }

  if(terminals.size() == 0){
    return false;
  }

  if(_func->getName() == "_ZNSt6vectorI12netlist_elemSaIS0_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS0_S2_EEmRKS0_")
      fprintf(stderr,"COmputing post order for problem func");

  SplicedCFGNode *sinkNode = terminals[0];
  if(terminals.size() > 1) {
     sinkNode = _sentinnelSink = new SplicedCFGNode(NULL);
     sinkNode->setId(nodeId++);
     sinkNode->setColor(SplicedCFGNode::WHITE);
     // _nodes.push_back(_sentinnelSink);

     // from each terminal, add a sink node
     for(size_t i = 0; i < terminals.size(); i++) {
        terminals[i]->addOutEdge(sinkNode);
        sinkNode->addInEdge(terminals[i]);
     }
  }

  unsigned maxNodeId = nodeId;
  // sort nodes in post-order
  vector<SplicedCFGNode *> postOrder;
  sinkNode->iPostDom = sinkNode;
  computePostOrder(sinkNode, true, postOrder);

  // nodeOrder[i] == position of node with id i in postOrder
  unsigned *nodeOrder = (unsigned *) calloc(maxNodeId+2, sizeof(unsigned));
  for(size_t i = 0; i < postOrder.size(); i++) {
    SplicedCFGNode *n = postOrder[i];
    nodeOrder[n->getId()] = i;
  }

  bool Changed = true;
  while (Changed) {
    Changed = false;

    // for all nodes, b, in reverse postorder (except start node)
    for(int idx = postOrder.size()-1; idx >= 0; idx--) {
      // last node is start node, so skip that
      SplicedCFGNode *b = postOrder[idx];
      if(b->iPostDom == b) continue; // skip terminals
      SplicedCFGNode *new_pdom =  NULL;

      vector<SplicedCFGNode *>& succs = b->getOutEdges();
      for(size_t i = 0; i < succs.size(); i++) {
        SplicedCFGNode *s = succs[i];
        /* i.e., if pdoms[p] already calculated */
        if (s->iPostDom && s->isExitBlk() == false) {
          new_pdom = new_pdom ? intersectPDom(s, new_pdom, nodeOrder) : s;
        }
      } // for each pred

      if (b->iPostDom  != new_pdom) {
        b->iPostDom = new_pdom;
        Changed = true;
      }
    } // in reverse post order
  } // while changed

  free(nodeOrder);

  // printPostDominators();
  return true;
}

//  i1 always appears before i2
bool SplicedCFG::dominates(const Instruction *i1, const Instruction *i2)
{
  if(!(i1 && i2)) 
    return false;

  const BasicBlock *bb1 = i1->getParent();
  const BasicBlock *bb2 = i2->getParent();
  if(bb1 == bb2) {
    for(BasicBlock::const_iterator itr = bb1->begin(); itr != bb1->end(); itr++)  {
      const Instruction *I = itr;
      if(I == i1) return true;
      else if(I == i2) return false;
    }
    assert(0 && "should have found instruction of BB");
  }

  SplicedCFGNode *node1 = _nodeForBB[bb1];
  SplicedCFGNode *node2 = _nodeForBB[bb2];
  assert(node1 && node2);

  // Go upwards from node2 towards entrynode, and check if we hit node1
  for( node2 = node2->iDom; node2; node2 = node2->iDom) {
    if(node2 == node1)
      return true;
    if(node2->iDom == node2) break; // check must be after comparing with node1
  }
  return false;
}

// i1 alwats appears after i2
bool SplicedCFG::postDominates(const Instruction *i1, const Instruction *i2) 
{
  if(!(i1 && i2)) 
    return false;

  const BasicBlock *bb1 = i1->getParent();
  const BasicBlock *bb2 = i2->getParent();
  if(bb1 == bb2) {
    for(BasicBlock::const_iterator itr = bb1->begin(); itr != bb1->end(); itr++)  {
      const Instruction *I = itr;
      if(I == i2) return true;
      else if(I == i1) return false;
    }
    assert(0 && "should have found instruction of BB");
  }

  SplicedCFGNode *node1 = _nodeForBB[bb1];
  SplicedCFGNode *node2 = _nodeForBB[bb2];
  assert(node1 && node2);
 
  // go upwards in post-dom tree (down in cfg) from nod2 and check if we hit node1
  for( node2 = node2->iPostDom; node2; node2 = node2->iPostDom) {
    if(node2 == node1)
      return true;
    if(node2->iPostDom == node2)
      break; // reached end. this check must be after comparing with node1
  }
  return false;
}

// Remove edges into a block if this is an exit block. This is needed for
// post-dominance relationship when we are only concerned if pthread_joins
// post-dominate the pthread_creates. The asserts are also stripped by this method.

void SplicedCFG::RemoveExitBranches()
{
 for(size_t i = 0, ie = _nodes.size(); i < ie; i++) {
    SplicedCFGNode *node = _nodes[i];
    if(node->isExitBlk()) continue;
    if(node->getOutEdges().empty()) { // terminal nodes
      node->checkAndMarkExitBlock(); // recursively goes backwards
    }
  }
}

// Does this have unreachable end (like exit) in it? Or all outgoing edges marked exit blocks?
// TBD: Check if we can just find the 'unreachable' as the last instruction
bool SplicedCFGNode::checkAndMarkExitBlock()
{
  if(_exitBlk) return true;
  const TerminatorInst *BBTerm = _bb->getTerminator();
  if(isa<UnreachableInst>(BBTerm))
    _exitBlk = true;

  // Next, check if every outbound edge is exit block already
  if(!_exitBlk && _outEdges.size()) {
    bool onlyExitDests = true;
    for(size_t i = 0; i < _outEdges.size(); i++) {
      if(!_outEdges[i]->isExitBlk()) {
        onlyExitDests = false;
        break;
      }
    }

    if(onlyExitDests) 
      _exitBlk = true;
  }

  if(_exitBlk)  {
    string bbName = _bb->getName();
    // std::cerr << "Making basic block " << bbName << " as exit block\n";
    for(size_t i = 0; i < _inEdges.size(); i++) {
      _inEdges[i]->checkAndMarkExitBlock();
    }
  }

  return _exitBlk;
}


// Find the nearest common dominator
const Instruction *SplicedCFG::findNearestCommonDominator(const Instruction *i1, const Instruction *i2)
{
  if(!(i1 && i2))
    return NULL;

  const BasicBlock *bb1 = i1->getParent();
  const BasicBlock *bb2 = i2->getParent();

  if(bb1 == bb2) {
    // return first instruction
    for(BasicBlock::const_iterator itr = bb1->begin(); itr != bb1->end(); itr++) {
      const Instruction *I = itr;
      if(I == i1) 
        return i1;
      else if(I == i2)
        return i2;
    }
    return NULL; // should never be here
  }

  // Find intersection of dominators
  vector<SplicedCFGNode *> ancestors1, ancestors2;
  SplicedCFGNode *node1 = _nodeForBB[bb1];
  ancestors1.push_back(node1);
  while(node1->iDom && node1->iDom != node1) {
    node1 = node1->iDom;
    ancestors1.push_back(node1);
  }

  SplicedCFGNode *node2 = _nodeForBB[bb2];
  ancestors2.push_back(node2);
  while(node2->iDom && node2->iDom != node2) {
    node2 = node2->iDom;
    ancestors2.push_back(node2);
  }

  // Find the last common part. At 0-th position must be the entry node
  int idx1 = ancestors1.size() -1;
  int idx2 = ancestors2.size() -1;
  assert(ancestors1[idx1] == ancestors2[idx2] && "root ancestor not same!");
  SplicedCFGNode *nearestCommonParent = NULL;
  while(idx1 >= 0 && idx2 >= 0 && ancestors1[idx1] == ancestors2[idx2]) {
    nearestCommonParent = ancestors1[idx1];
    idx1--;
    idx2--;
  }

  // if dominator is one of bb1 or bb2, return one of th eoriginal instructions
  assert(nearestCommonParent && "Didn't find dominator");
  const BasicBlock *bb = nearestCommonParent->getBlock();
  if(bb == bb1)
    return i1;
  else if(bb == bb2)
    return i2;
  else
    return bb->getTerminator();
}

// Find the nearest common post dominator
const Instruction *SplicedCFG::findNearestCommonPostDominator(const Instruction *i1, const Instruction *i2)
{
  if(!(i1 && i2))
    return NULL;

  const BasicBlock *bb1 = i1->getParent();
  const BasicBlock *bb2 = i2->getParent();

  if(bb1 == bb2) {
    // return later instruction
    for(BasicBlock::const_iterator itr = bb1->begin(); itr != bb1->end(); itr++) {
      const Instruction *I = itr;
      if(I == i1) 
        return i2;
      else if(I == i2)
        return i1;
    }
    return NULL; // should never be here
  }

  // Find intersection of post-dominators
  SplicedCFGNode *node1 = _nodeForBB[bb1];
  SplicedCFGNode *node2 = _nodeForBB[bb2];
  if(!(node1 && node2))
    return NULL;

  // Find intersection of dominators
  vector<SplicedCFGNode *> desc1, desc2;
  desc1.push_back(node1);
  while(node1->iPostDom && node1->iPostDom != node1) {
    node1 = node1->iPostDom;
    desc1.push_back(node1);
  }

  desc2.push_back(node2);
  while(node2->iPostDom && node2->iPostDom != node2) {
    node2 = node2->iPostDom;
    desc2.push_back(node2);
  }

  // Find the last common part. At 0-th position must be the entry node
  int idx1 = desc1.size() -1;
  int idx2 = desc2.size() -1;
  SplicedCFGNode *nearestCommonChild = NULL; // last item
  while(idx1 >= 0 && idx2 >= 0 && desc1[idx1] == desc2[idx2]) {
    nearestCommonChild = desc1[idx1];
    idx1--;
    idx2--;
  }

  // if post dominator is one of bb1 or bb2, return one of the original instructions
  assert(nearestCommonChild && "Didn't find post-dominator");
  if(!nearestCommonChild || nearestCommonChild == _sentinnelSink)
    return NULL;

  const BasicBlock *bb = nearestCommonChild->getBlock();
  if(bb == bb1)
    return i1;
  else if(bb == bb2)
    return i2;
  else
    return bb->getFirstNonPHI();
}

// Get last instruction of dominating basic block
Instruction *SplicedCFG::getLoopDominator(Loop *loop)
{
  if(!loop)
    return NULL; // no current loop, can't move to outer loop

  // Move start_ts and create call to preheader
  BasicBlock *preheader = loop->getLoopPreheader();
  if(preheader)
    return preheader->getTerminator();

  // find block that dominators all predecessors of header
  BasicBlock *header = loop->getHeader();
  assert(header && "Loop with no pre-header or header");
  assert(pred_begin(header) != pred_end(header));

  const Instruction *dominator = header->getTerminator();
  for (pred_iterator PI = pred_begin(header), PE = pred_end(header); PI != PE && dominator; ++PI) {
    BasicBlock *bb = *PI;
    if(bb != header) 
      dominator = findNearestCommonDominator(dominator, bb->getTerminator());
  }

  return const_cast<Instruction *>(dominator);
}

// Get post dominator of loop
Instruction *SplicedCFG::getLoopPostDominator(Loop *loop)
{
  if(!loop)
    return NULL; // no current loop, can't move to outer loop

  const Instruction *lowerPt = NULL;
  SmallVector<BasicBlock *,4> exitblks;
  loop->getExitBlocks(exitblks);

  // ignore unreachable ones
  SmallVector<BasicBlock *,4> realExits;
  for(size_t i = 0; i < exitblks.size(); i++) {
    SplicedCFGNode *n = getNodeFor(exitblks[i]);
    if(n && !n->isExitBlk())
        realExits.push_back(exitblks[i]);
  }

  if(realExits.size() > 0) {
    lowerPt = realExits[0]->getFirstNonPHI();
    for(size_t i = 1; lowerPt && i < realExits.size(); i++) {
      lowerPt = findNearestCommonDominator(lowerPt, realExits[i]->getFirstNonPHI());
    }
  }

  return const_cast<Instruction *>(lowerPt);
}

// Find common parent. Move higher up. In worst case, return entry Node.
SplicedCFGNode *SplicedCFG::intersectDom(SplicedCFGNode *n1, SplicedCFGNode *n2, unsigned *nodeOrder)
{
  while (n1 != n2) {
    while (nodeOrder[n1->getId()] < nodeOrder[n2->getId()] && n1->iDom != n1) {
      n1 = n1->iDom;
    }
    while (nodeOrder[n2->getId()] < nodeOrder[n1->getId()] && n2->iDom != n2) {
      n2 = n2->iDom; 
    }
  }
  return (n1 != n2 ? NULL: n1);
}

// Find common descendant. Move lower down. In worst case, return NULL, if different terminals reached.
SplicedCFGNode *SplicedCFG::intersectPDom(SplicedCFGNode *n1, SplicedCFGNode *n2, unsigned *nodeOrder)
{
  bool moved = true;
  while (n1 != n2 && moved) {
    moved = false;
    while (nodeOrder[n1->getId()] < nodeOrder[n2->getId()] && n1->iPostDom != n1) {
      n1 = n1->iPostDom;
      moved = true;
    }
    while (nodeOrder[n2->getId()] < nodeOrder[n1->getId()] && n2->iPostDom != n2) {
      n2 = n2->iPostDom;
      moved = true;
    }
  }
  return (n1 != n2 ? NULL: n1);
}


// Print basic block name
void SplicedCFGNode::printName()
{
  std::string name = _bb->getName();
  fprintf(stderr," (ID: %d BB %s )", _id, name.c_str());
}

// Print immediate dominators
void SplicedCFG::printDominators()
{
  std::string fname = _func->getName();
  fprintf(stderr,"***  DOMINATOR TREE FOR FUNCTION %s\n", fname.c_str());
  for(size_t i = 0; i < _nodes.size(); i++) {
    SplicedCFGNode *n = _nodes[i];
    n->printName();
    fprintf(stderr," has i-dom ");
    if(n->iDom)
      n->iDom->printName();

    fprintf(stderr,"\n");
  }
}

void SplicedCFG::printPostDominators()
{
  std::string fname = _func->getName();
  fprintf(stderr,"***  POST DOMINATOR TREE FOR FUNCTION %s\n", fname.c_str());
  for(size_t i = 0; i < _nodes.size(); i++) {
    SplicedCFGNode *n = _nodes[i];
    n->printName();
    fprintf(stderr," has i-pdom ");
    if(n->iPostDom)
      n->iPostDom->printName();

    fprintf(stderr,"\n");
  }
}



/********************************************************************************************
 * FOLLOWING SECTION IS RELATED TO DISJOINT THREAD SECTIONS IDENTIFICATION WITHIN MULTI-
 * THREADED SECTION. AND CODE FOR CREATING ICFG */


//Does this function or its descendants have ICFG? 
bool SccICFG::hasSyncAll(Function* func, set<Function *>& visitedFuncs)
{
  if(!(_syncAllFunc || _threadCreateFunc))
    return false; // no BSA
  if(_functionHasSyncAlls.find(func) != _functionHasSyncAlls.end())
    return _functionHasSyncAlls[func];

  if(visitedFuncs.count(func) > 0)
      return false;

  visitedFuncs.insert(func); 

  bool foundSyncAll = false;
  for (inst_iterator II = inst_begin(func), E = inst_end(func); II != E; ++II) {
    Instruction *inst = &*II;

    if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
      CallSite cs(inst);
      Function *CF = cs.getCalledFunction();
      // for indirect function call, assume all code in on epiece.
      if (CF && (CF == _syncAllFunc || CF == _threadCreateFunc || hasSyncAll(CF,visitedFuncs))) {
        foundSyncAll = true;
        break;
      } 
    } // if function call
  } // for each instruction

  _functionHasSyncAlls[func] = foundSyncAll;
  return foundSyncAll;
}

// Mark the functions with sync_all
void SccICFG::markFunctionsWithSyncAlls(Module& M)
{
  _functionHasSyncAlls.clear();
  
  for(Module::iterator itr = M.begin(); itr != M.end(); itr++) {
    Function *F = itr;

    bool val = hasSyncAll(F);
    _functionHasSyncAlls[F] = val;
  }
}

// Classify the type of instruction called here
SccICFG::funcCallType SccICFG::classifyInstr(Instruction *inst, vector<Function *>& calledFuncs)
{
  if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
    CallSite cs(inst);
    Function *F = cs.getCalledFunction();
    if(!F) {
      fprintf(stderr,"SCC-ERROR: Null function in classifyInstr\n");
    }
    else if (F == _syncAllFunc) 
      return SYNC_CALL;
    else if(F == _threadCreateFunc || F == _threadStartFunc) {
      int argIdx = (F == _threadCreateFunc) ? 2 : 0;
      Value *v = cs.getArgument(argIdx);
      if(v) v = GetUnderlyingObject(v);
      if(dyn_cast<Function>(v)) {
        Function *cf = dyn_cast<Function>(v);
        calledFuncs.push_back(cf);
      }
      else {
        std::string fname = F ? F->getName() : "";
        fprintf(stderr,"SCC-ERROR: Cannot find function in %s\n", fname.c_str());
      }
      return THREAD_CREATE_CALL;
    }
    else if(F->isDeclaration())
      return NO_SPLIT;
    else if(hasSyncAll(F)) {
      calledFuncs.push_back(F);
      return FUNC_CALL;
    }
    else {
       return NO_SPLIT;
    }
  }

  return NO_SPLIT;
}

// helper func to find position of inst in bb
int SccICFG::getInstrPos(const BasicBlock *bb, const Instruction *inst)
{
  int pos = 0;
  for(BasicBlock::const_iterator itr = bb->begin(); itr != bb->end(); itr++, pos++) {
    const Instruction *I = itr;
    if(I == inst) return pos;
  }
  return -1;
}

SccICFGNode *SccICFG::getNewNode(BasicBlock *bb, int startPos, int endPos)
{
  SccICFGNode *n = new SccICFGNode(bb,startPos, endPos);
  _nodes.push_back(n);
  return n;
}

// Find the sync_all nod with ID in this instruction
SccICFGNode *SccICFG::getSyncNode(Instruction *inst)
{
 if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
    CallSite cs(inst);
    Function *F = cs.getCalledFunction();
    if (F != _syncAllFunc)
      return NULL;
    Value *v = cs.getArgument(1); // 2nd arg is a constant number
    ConstantInt *intv = dyn_cast<ConstantInt>(v);
    if(!intv)
      return NULL;

    int id = (int) intv->getSExtValue();
    if(_syncAllNodes.find(id) != _syncAllNodes.end())
      return _syncAllNodes[id];
 
    SccICFGNode *n = new SccICFGNode(inst->getParent(), -1,  -1);
    n->setSyncNodeId(id);
    _syncAllNodes[id] = n;

    _nodes.push_back(n);
    return n;
  }
  else
    return NULL;

}

// Create an inter-procedural cfg, including the thread create call.
// Split the BB at the function call, remove the function call. 

SccICFGFuncInfo *SccICFG::addFunction(Function *func, 
      BasicBlock *start, Instruction *startInstr,
      BasicBlock *end, Instruction *endInstr)
{
  if(start == NULL && icfgInfoForFunction.find(func) != icfgInfoForFunction.end())
        return icfgInfoForFunction[func];

  bool fullFuncIcfg = (start == NULL && end == NULL);
  if(!start) {
    start = &(func->getEntryBlock());
    startInstr = start->getFirstNonPHI();
  }

  // Do BFS from start or entryNode of func.
  queue<BasicBlock *> Q;
  set<BasicBlock *> alreadySeen;
  Q.push(start);
  alreadySeen.insert(start);

  SccICFGNode *entryNode = NULL;
  vector<SccICFGNode *> returnNodes;

  while(!Q.empty()) {
    BasicBlock *bb = Q.front();
    Q.pop();

    vector<Instruction *> bbInstrs;
    vector<funcCallType> typeOfInst;
    vector<vector<SccICFGFuncInfo *> > sccIcfgInfos; // could be multiple function calls
    vector<SccICFGNode *> bbNodes;

    int startPos = (bb == start) ?  getInstrPos(bb, startInstr) : 0;
    int endPos = (bb == end) ?  getInstrPos(bb, endInstr) :  INT_MAX;
    if(endPos == 0) endPos++;

    // pass 1: create nodes if we need to insert a function icfg at this place 
    vector<SccICFGFuncInfo *> emptyVec;
    int instrPos = 0;
    for(BasicBlock::iterator itr = bb->begin(); itr != bb->end(); itr++, instrPos++) {
      if(instrPos < startPos || instrPos >= endPos)  continue;

      Instruction *inst = itr;
      bbInstrs.push_back(inst);
      SccICFGNode *node = NULL;
      sccIcfgInfos.push_back(emptyVec);

      vector<Function *> calledFuncs;
      funcCallType type = classifyInstr(inst, calledFuncs);
      typeOfInst.push_back(type);

      if(type == FUNC_CALL || type == THREAD_CREATE_CALL) {
        node = getNewNode(bb, instrPos, instrPos);
        for(size_t i = 0; i < calledFuncs.size(); i++) {
          Function *f = calledFuncs[i];
          SccICFGFuncInfo *info = this->addFunction(f, NULL, NULL, NULL, NULL);
          sccIcfgInfos.back().push_back(info);
        }
      }
      else if(type == SYNC_CALL) {
        node = getSyncNode(inst);
      }

      bbNodes.push_back(node);
    }

    int numInsts = bbInstrs.size(); // Caution: might be nly part of BB
    // create icfg nodes for instructions that don't cause split
    int i = 0;
    while(i < numInsts) {
      if(bbNodes[i] == NULL) {
        SccICFGNode *n = getNewNode(bb,i,i);
        while(i < numInsts && bbNodes[i] == NULL) {
          bbNodes[i] = n;
          i++;
        }
        n->setEndPos(i);
      }
      else
        i++;
    }

    // Now, link prev. node to next node
    for(int i = 0; i < numInsts-1; i++) {
      if(bbNodes[i] != bbNodes[i+1]) {
        // link node[i] to next one
        switch (typeOfInst[i])  {
          case NO_SPLIT:
            bbNodes[i]->addEdgeTo(bbNodes[i+1]);

            // node[i+1]->addEdgeFrom(node[i]);

            // update return nodes
            if(isa<ReturnInst>(bbInstrs[i]))
              returnNodes.push_back(bbNodes[i]);
            break;
          case THREAD_CREATE_CALL:
            {
              vector<SccICFGFuncInfo *>& icfgs = sccIcfgInfos[i];
              for(size_t j = 0; j < icfgs.size(); j++) {
                SccICFGNode *entry = icfgs[j]->entry;
                bbNodes[i]->addEdgeTo(entry); 
                bbNodes[i]->addEdgeTo(bbNodes[i+1]); 
              }
              // no edge from return node
            }
            break;
          case FUNC_CALL:
            {
              vector<SccICFGFuncInfo *>& icfgs = sccIcfgInfos[i];
              for(size_t j = 0; j < icfgs.size(); j++) {
                SccICFGNode *entry = icfgs[j]->entry;
                bbNodes[i]->addEdgeTo(entry);
                vector<SccICFGNode *>& retNodes = icfgs[j]->returnNodes;
                for(size_t k = 0; k < retNodes.size(); k++) {
                  retNodes[k]->addEdgeTo(bbNodes[i+1]);
                }
              } // for each function called here
            }
            break;
          case SYNC_CALL:
            {
              // this is a sync node. simply add outedge to netx node
              bbNodes[i]->addEdgeTo(bbNodes[i+1]);
            }
            break;
        } // switch
      }
    } // For number of instructions

    
    int lastIdx = numInsts-1;
    if(isa<ReturnInst>(bbInstrs[lastIdx]))
       returnNodes.push_back(bbNodes[lastIdx]);

    // record first and last node, so we can complete the edges to first icfgnode for bb
    _firstNode[bb] = bbNodes.front();
    _lastNode[bb] = bbNodes.back();

    // Update entry Node
    if(bb == start)
      entryNode = bbNodes[0];
    else if(bb == end)
      fprintf(stderr,"Found end node\n");

    // push successors into Q
    if(bb == end) continue;
    const TerminatorInst *BBTerm = bb->getTerminator();
    if(!BBTerm) continue;
    for (unsigned i = 0, e = BBTerm->getNumSuccessors(); i < e; ++i) {
      BasicBlock *succ = BBTerm->getSuccessor(i);
      if(alreadySeen.find(succ) == alreadySeen.end()) {
          Q.push(succ);
          alreadySeen.insert(succ);
      }
    } // add successors to Q

  } // while Q is not empty

  // Now, for each bb seen, connect first to pred, last to successors
  for(set<BasicBlock *>::iterator itr = alreadySeen.begin(); itr != alreadySeen.end(); itr++) 
  {
    BasicBlock *bb = *itr;
    SccICFGNode *lastNode = _lastNode[bb];

    assert(lastNode && "Failed to get last node for basic block!");

    const TerminatorInst *BBTerm = bb->getTerminator();
    if(!BBTerm) continue;
    for (unsigned i = 0, e = BBTerm->getNumSuccessors(); i < e; ++i) {
      BasicBlock *succ = BBTerm->getSuccessor(i);
      SccICFGNode *succNode = _firstNode[succ];
      if(succNode) {
         lastNode->addEdgeTo(succNode);
      } // add successor edges
    } // for each successor
  } // for each basic block

  // Update func-> SccICFGInfo map
  SccICFGFuncInfo *info = NULL;
  if(fullFuncIcfg) {
    info = new SccICFGFuncInfo(entryNode, returnNodes);
    icfgInfoForFunction[func] = info;
  }
  else {
    // this is a TS
    SccICFGNode *startNode = _firstNode[start];
    SccICFGNode *endNode = end ? _lastNode[end] : NULL;
    tsStarts.push_back(startNode);
    tsEnds.push_back(endNode);
  }

  return info;
} // add part of full of a function to icfg

// Collect the terminal vertices when forward traversing from the start node provided.
// The start node can be a TS-begin node, or some sync_node within a MTCS.
// Ignore the end node, so that code that reaches both a sync node and the end of TS (like
// in looped barriered execution) can be identified as a CS. It is mainly for root thread,
// that could go directly to the thread_join, which acts as first barrier.
void SccICFG::collectTerminalNodes(SccICFGNode *start, SccICFGNode *endTsNode, set<SccICFGNode *>& terminals)
{
  terminals.clear();
  if(!start) return;

  set<SccICFGNode *> visited;
  
  queue<SccICFGNode *> Q;

  Q.push(start);
  visited.insert(start);

  while(!Q.empty()) {
    SccICFGNode *node = Q.front();
    Q.pop();

    vector<SccICFGNode *>& succs = node->getSuccs();
    for(size_t i = 0; i < succs.size(); i++) {
      SccICFGNode *s = succs[i];
      if(visited.find(s) != visited.end()) continue;

      if(s->isSyncNode() || s == endTsNode) {
        if(terminals.find(s) == terminals.end() && s != endTsNode)
          terminals.insert(s);
      }
      else {
        Q.push(s);
        visited.insert(s);
      }
    }
  } // while Q is not empty
}


// Constructor
SccICFG::SccICFG(Module *M, ThreadEscapeAnalysis *ea, bool doBSA)
{
  _module = M;
  EA = ea;
  _threadCreateFunc = M->getFunction(ea->_THREAD_CREATE_FN_NAME);
  _threadStartFunc = M->getFunction("thread_start");
  _syncAllFunc = doBSA ? M->getFunction("scc_sync_threads") : NULL;
}

SccICFG::~SccICFG() 
{
  for(size_t i = 0; i < _nodes.size(); i++)  {
    delete _nodes[i];
  }
  _nodes.clear();

  for(map<const Function *, SccICFGFuncInfo *>::iterator itr = icfgInfoForFunction.begin();
                  itr != icfgInfoForFunction.end(); itr++) {
    delete itr->second;
  }
  icfgInfoForFunction.clear();
}
