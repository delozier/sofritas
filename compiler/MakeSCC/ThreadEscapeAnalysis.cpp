// (C) 2010 University of Washington.
// (C) 2011 University of California, Santa Cruz.
// The software is subject to an academic license.
// Terms are contained in the LICENSE file.

// Thread-local escape analysis
// The Steensgaard points-to analysis is borrowed heavily from llvm-poolalloc,
// which is released under the University of Illinois Open Source License.
//
// TODO:
//  Instead of using non-context-sensitive Steensgaard, we could use
//  DSGraph::computeNodeMapping to map nodes across functions/globals
//  in the context-sensitive TDDataStructure.

#define DEBUG_TYPE "scc"

#include <iostream>
#include <fstream>
#include "llvm/IR/Operator.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h" 
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Analysis/DominanceFrontier.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopPass.h"
// #include "llvm/Analysis/BlockFrequencyInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"

#include "dsa/DataStructure.h"
#include "dsa/DSGraph.h"
#include "dsa/DSNode.h"
#include "SccProfile.h"
#include "ThreadEscapeAnalysis.h"
#include "identifyTS.h"
#include "LoopHelper.h"

// #include "llvm/Transforms/Scalar.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <set>

using std::vector;

// std::ofstream escapeDebugOut("escapeAnaDebug.out");

// #define PRINTNODE N->print(dbgs(), ResultGraph);
#define PRINTNODE 

using namespace llvm;
namespace {
cl::opt<bool> doNonUnifSteensGaard("scc-nonunif-steensgaard", cl::init(false),
              cl::desc("Use non-unification Steensgaard - don't mere function argument actuals"));
cl::opt<bool> doFieldSensitiveModRef("scc-field-sensitive-modref", cl::init(false),
              cl::desc("Do field sensitive mod-ref"));

cl::opt<bool> doTSA("scc-tsa", cl::init(false),
              cl::desc("Do thread section analysis, and instrument threaded sections only"));

cl::opt<bool> doBSA("scc-bsa", cl::init(false),
              cl::desc("Do basrrier-sensitive analysis within each thread section"));

cl::opt<bool> doRdonlyOpt("scc-tsa-modref", cl::init(false),
              cl::desc("Skip load isntrumentation for mt-rdonly. Only works if scc-tsa is also true."));

cl::opt<bool> doCriticalRegionOpt("scc-tsa-critregion", cl::init(false),
              cl::desc("Skip load instrumentation if writes are only in mutex protected regions. Only works if scc-tsa is also true."));

cl::opt<int> doSccBlacklist("scc-blacklist", cl::init(0),
              cl::desc("Max. Number of nodes to blacklist. Default 0"));

cl::opt<bool> doReportRace("scc-report-race", cl::init(false),
              cl::desc("Report races using SBPA. Default false"));
cl::opt<bool> doRaceOncePerValue("scc-report-race-once-per-value", cl::init(false),
              cl::desc("Report races using SBPA. Report each value only once. Default false"));

cl::opt<bool> doTsanPass("scc-tsanpass", cl::init(false),
              cl::desc("Do thread sanitizer optimizations. Needed in addition to -opttsan, to set some function names in pass."));

// SCC statistics
STATISTIC(NumSCCBlackListed, "SccBlackListedNodes");

STATISTIC(NumBarrieredSections, "SccBarrieredSections");
STATISTIC(NumCriticalRegionAccessNode, "Number of nodes * phases with critical region access only");

// For non-unification steensgaard
STATISTIC(NumNonUnifiedCallSiteArgMap, "Number of call site argument mapping.");
STATISTIC(NumNonUnifiedArgMapping, "Number of arguments mapped.");
STATISTIC(NumFieldSensitiveLoadRemoved, "Number of loads removed by field sensitive mod-ref.");

STATISTIC(NumRacyNodes, "Number of DSNode with static races");
STATISTIC(AvgWriteLocsPerNode, "Average number of write locations for racy nodes");
STATISTIC(TotalRacyWriteLocs, "Total number of write locations for racy nodes");
STATISTIC(AvgReadLocsPerNode, "Average read locations impacted for racy nodes");
STATISTIC(TotalRacyReadLocs, "Total read locations impacted for racy nodes");
STATISTIC(TotalProgramLines, "Total Lines of Code in Program with used functions");

static const uint32_t SccDontMarkTS = (1<<20);

const char *MAIN_FUNC_NAME = "main";
}

//------------------------------------------------------------------------------
// Util
//------------------------------------------------------------------------------

// Is the node accessed in loadStoreInst modified in current thread section/phase ?
// Call this only when the node is escaping thread section or threads.
scc_escaping_type ThreadEscapeAnalysis::TSNodeIsModified(const Value* object, 
          DSNode *N, const Instruction *loadStoreInst)
{
  if(!N)
    return SCC_ESCAPING_NONODE;

  if(!doRdonlyOpt)
    return SCC_ESCAPING_KNOWN_NODE; // no modref based optimization

  if(!doTSA)
    return SCC_ESCAPING_KNOWN_NODE;

  if(!N->isModifiedNode())
    return SCC_NON_ESCAPING_NODE;

  if(!loadStoreInst) {
    DEBUG(dbgs() << "[ThreadEscapeAnalysis] Value escapes (known modified, no instruction):\n" << (*object));
    PRINTNODE
    return SCC_ESCAPING_KNOWN_NODE;
  }

  if(writtenInSamePhase(loadStoreInst, N, doCriticalRegionOpt)) {
    // if node is modified in this phase of parallel section
    DEBUG(dbgs() << "[ThreadEscapeAnalysis] Value escapes (known modified in same phase or write:\n" << (*object));
    PRINTNODE
    return SCC_ESCAPING_KNOWN_NODE;
  }

  return SCC_NON_ESCAPING_NODE;
}

static bool llvm_function(const Function *F)
{
  return (strncmp(F->getName().data(), "llvm.",5) == 0);
}

static bool callMayBeExternal(const DSCallSite& call,
                              std::vector<const Function*>& callTargets) {
  if (call.isDirectCall()) {
    callTargets.push_back(call.getCalleeFunc());
    return call.getCalleeFunc()->isDeclaration();
  }

  DSNode* N = call.getCalleeNode();
  N->addFullFunctionList(callTargets);

  if (N->isIncompleteNode() || N->isUnknownNode())
    return true;
  for (size_t i = 0; i < callTargets.size(); ++i)
    if (callTargets[i]->isDeclaration())
      return true;

  return false;
}


// For deciding how to mark arguments of external function calls during TSA and DTS
SccFunctionArgClassifier::SccFunctionArgClassifier()
{
  _threadStartName = NULL;
  _threadJoinName = NULL;
  
  _funcArgMap["strcmp"] = "rr";
  _funcArgMap["strncmp"] = "rrn";
  _funcArgMap["strcpy"] = "wr";
  _funcArgMap["strncpy"] = "wrn";
  _funcArgMap["strstr"] = "rr";
  _funcArgMap["strlen"] = "r";
  _funcArgMap["log"] = "r";
  _funcArgMap["exp"] = "r";
  _funcArgMap["sqrtf"] = "r";
  _funcArgMap["sqrt"] = "r";
  _funcArgMap["exit"] = "n";
  _funcArgMap["puts"] = "nn";

  _funcArgMap["fabs"] = "n";
  _funcArgMap["fflush"] = "n";
  _funcArgMap["cos"] = "n";
  _funcArgMap["fmod"] = "n";
  _funcArgMap["ldexp"] = "n";
  _funcArgMap["perror"] = "nnn";

  _funcArgMap["free"] = "n";
  _funcArgMap["malloc"] = "nnn";
  _funcArgMap["__assert_fail"] = "nnn";

/*
  _THREAD_CREATE_FN_NAME = strdup("pthread_create");
  _THREAD_JOIN_FN_NAME = strdup("pthread_join");
  _funcArgMap[THREAD_CREATE_FN_NAME] = "n";
  _funcArgMap[THREAD_JOIN_FN_NAME] = "n";
*/

  // don't mark anything for these
  _funcArgMap["scc_sync_threads"] = "n";
  _funcArgMap["scc_sync_safe"] = "n";
  _funcArgMap["scc_split_task"] = "n";
  _funcArgMap["scc_thread_mutex_lock"] = "n";
  _funcArgMap["scc_thread_mutex_unlock"] = "n";
}

void SccFunctionArgClassifier::setThreadFunctionNames(const char *start, const char *join) {
  _threadStartName = strdup(start);
  _threadJoinName = strdup(join);

  _funcArgMap[_threadStartName] = "n";
  _funcArgMap[_threadJoinName] = "n";
}

// Classify the argument
const char *SccFunctionArgClassifier::getArgTypes(const char *fname) 
{
  if(!fname) 
    return NULL;

  map<const char *, const char *, scc_ltstr>::iterator itr = _funcArgMap.find(fname);
  if(itr != _funcArgMap.end()) 
    return itr->second;
  else
    return NULL;
}


// Find a CallInst of this function. instrPos will have valid value ONLY if 
// return value is not NULL
static Instruction *findCallInstInBB(const Function *func, const BasicBlock *const_bb, int& instrPos)
{
  if(!func) return NULL;

  BasicBlock *bb = const_cast<BasicBlock *>(const_bb);
  int pos = 0;
  for(BasicBlock::iterator inst = bb->begin(); inst != bb->end(); inst++, pos++) {
    if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
        const CallSite cs(inst);
        if (cs.getCalledFunction() == func) {
          instrPos = pos;
          return inst;
        }
    }
  }
  return NULL;
}


static const Value *getUnderlyingObjectFromInt(const Value *V) {
  do {
    const Operator *U = dyn_cast<Operator>(V);
    if (U) {
      // If we find a ptrtoint, we can transfer control back to the
      // regular getUnderlyingObjectFromInt.
      if (U->getOpcode() == Instruction::PtrToInt)
        return U->getOperand(0);
      // If we find an add of a constant or a multiplied value, it's
      // likely that the other operand will lead us to the base
      // object. We don't have to worry about the case where the
      // object address is somehow being computed by the multiply,
      // because our callers only care when the result is an
      // identifiable object.
      if (U->getOpcode() != Instruction::Add ||
          (!isa<ConstantInt>(U->getOperand(1)) &&
           Operator::getOpcode(U->getOperand(1)) != Instruction::Mul))
        return V;
      V = U->getOperand(0);
    } else {
      return V;
    }
    assert(V->getType()->isIntegerTy() && "Unexpected operand type!");
  } while (1);
}

static const Value *getUnderlyingObject(const Value *V) {
  // First just call Value::getUnderlyingObject to let it do what it does.
  do {
    V = GetUnderlyingObject(V);
    // If it found an inttoptr, use special code to continue climing.
    if (Operator::getOpcode(V) != Instruction::IntToPtr)
      break;
    const Value *O = getUnderlyingObjectFromInt(cast<User>(V)->getOperand(0));
    // If that succeeded in finding a pointer, continue the search.
    if (!O->getType()->isPointerTy())
      break;
    V = O;
  } while (1);
  return V;
}

const Value* ThreadEscapeAnalysis::getRealUnderlyingObject(const Value* v) const
{
  // Value::getUnderlyingObject() gives up after 6 iterations?!!!
  // For correctness, we may need to go all the way down the rabbit hole.
  const Value* o = GetUnderlyingObject(v);
  while (o != v) {
    v = o;
    o = GetUnderlyingObject(v);
  }
  return o;
}


// Compare actual nodes of function calls
int CallSiteArgMap::different(const CallSiteArgMap *other_const) const
{
  int numDiff = 0;
  CallSiteArgMap *other = const_cast<CallSiteArgMap *>(other_const);
  for(map<DSNodeHandle, DSNodeHandle>::const_iterator itr = _formalToActualMap.begin();
    itr != _formalToActualMap.end(); itr++)
  {
    const DSNodeHandle& formal = itr->first;
    const DSNodeHandle& actual = itr->second;
    if(other->getActualNode(formal) != actual) {
      numDiff++;
    }
  }

  return numDiff;
}

// Get actual nodes recursively going up call tree
// Use recursive when marking read/modified. Use recursive when deciding whether to instrument something.
void ThreadEscapeAnalysis::debugNonUnifiedFuncCalls() const
{
  map<Function *, std::set<DSNodeHandle> >::const_iterator itr;
  for(itr = _funcArgs.begin(); itr != _funcArgs.end(); itr++) 
    {
      Function *F = itr->first;
      // const std::set<DSNodeHandle>& fArgs = itr->second; 
      const std::set<CallSiteArgMap *>& callSiteInfo = (_callSiteArgMaps.find(F))->second;
      if(callSiteInfo.size() <= 1) continue; // called only once, didn't help in non-unified ptr analysis
      
      std::set<CallSiteArgMap *>::const_iterator itr2 = callSiteInfo.begin();
      CallSiteArgMap *firstArgmap = *itr2;
      int numDiffArgMap = 0, totCalls = 1;;
      
      for( itr2++; itr2 != callSiteInfo.end(); itr2++) {
        const CallSiteArgMap *argmap = *itr2;
        totCalls++;
        if(firstArgmap->different(argmap))
          numDiffArgMap++;
      } // function args
    }
}

// Get actual nodes recursively going up call tree
// Use recursive when marking read/modified. Use recursive when deciding whether to instrument something.
bool ThreadEscapeAnalysis::getPointedNodeRecursive(Function *F, DSNodeHandle&  nodeInFunc, uint32_t phase,
              vector<DSNodeHandle>& retNodes, bool recursive, set<Function *>& visitedFuncs) const
{
  retNodes.push_back(nodeInFunc);
  if(!recursive) return true;

  // check that we didn't see this already. All callsites of this function is visited in loop below
  if(visitedFuncs.count(F) > 0) 
    return true;
  visitedFuncs.insert(F);

  map<Function *, std::set<DSNodeHandle> >::const_iterator itr = _funcArgs.find(F);
  if(itr != _funcArgs.end()) {
    const std::set<DSNodeHandle>& fArgs = itr->second; 
    if(fArgs.count(nodeInFunc) > 0) {
      // recursively get more nods
      const std::set<CallSiteArgMap *>& callSiteInfo = (_callSiteArgMaps.find(F))->second;
      for(std::set<CallSiteArgMap *>::const_iterator itr = callSiteInfo.begin();
        itr != callSiteInfo.end(); itr++) {

        CallSiteArgMap *argmap = *itr;
        const CallSite& cs = argmap->getCallSite();
          
        Function *caller = cs.getCaller();
        DSNodeHandle actNode = argmap->getActualNode(nodeInFunc);
        getPointedNodeRecursive(caller, actNode, phase, retNodes, recursive, visitedFuncs); // recursively go up the call tree
      }
    } // function args
  }

  return true;
}

// Recursivbely go up the call tree, and collect potential nods this variable might point to.
bool ThreadEscapeAnalysis::getAllPointedNodes(const Value* object, const Instruction *inst,
              vector<DSNodeHandle>& retNodes, unsigned phase, bool recursive) const
{
  // Value *v = const_cast<Value *>(object);
  Function *F = const_cast<Function *>(inst->getParent()->getParent());
  DSNodeHandle localNodeHandle;
  if(ResultGraph)  {
    DSGraph::ScalarMapTy::const_iterator I = ResultGraph->getScalarMap().find(object);
    if (I != ResultGraph->getScalarMap().end()) {
      localNodeHandle = I->second;
    }    
  }

  // if not, try the globals graph
  if(localNodeHandle.getNode() == NULL  &&  GlobalsGraph) {
    DSGraph::ScalarMapTy::const_iterator I = GlobalsGraph->getScalarMap().find(object);
    if(I != GlobalsGraph->getScalarMap().end()) {
      localNodeHandle = I->second;
    }    
  }

  set<Function *> visitedFuncs;
  getPointedNodeRecursive(F, localNodeHandle, phase, retNodes, recursive, visitedFuncs);
  return (retNodes.size() > 0);
}


// Get all DSNode this variable might point.
// Version 1: Unification based Steensgaard: Returns at most 1 DSNodeHandle
// Version 2: Return  a set of DSNodes this variable might point. So, the points-to sets are
//     not merged if same function is called with different actuals
// Version 3: Return set of DSNodes depending on points-to in each barrier

bool ThreadEscapeAnalysis::getNodesForObject(const Value* object, 
      vector<DSNodeHandle>& retNodes, const Instruction *inst) const
{
  retNodes.clear();
  if(!object) return false;
  if(doNonUnifSteensGaard) {
    bool retval = getAllPointedNodes(object, inst, retNodes, 0, true);  // last entry should be phase later
    if(retNodes.size() != 1) {
      const Function *func = inst->getParent()->getParent();
      std::string name = func->getName();
      // fprintf(stderr,"Got %lu nodes by non-unif in function %s\n", retNodes.size(), name.c_str());
    }
    return retval;
  }

  if(ResultGraph)  {
    DSGraph::ScalarMapTy::const_iterator I = ResultGraph->getScalarMap().find(object);
    if (I != ResultGraph->getScalarMap().end()) {
      retNodes.push_back(I->second);
      /* node = I->second.getNode();
      offset = I->second.getOffset(); */
    }
  }

  // if not, try the globals graph
  if(retNodes.empty() &&  GlobalsGraph) {
    DSGraph::ScalarMapTy::const_iterator I = GlobalsGraph->getScalarMap().find(object);
    if(I != GlobalsGraph->getScalarMap().end()) {
      retNodes.push_back(I->second);
      /* node = I->second.getNode();
      offset = I->second.getOffset(); */
    }
  }

  return (retNodes.size() > 0);
}

DSNodeHandle ThreadEscapeAnalysis::getHandleForObject(const Value* object) const {
  if(!ResultGraph) return NULL;
  DSGraph::ScalarMapTy::const_iterator I = ResultGraph->getScalarMap().find(object);
  if (I == ResultGraph->getScalarMap().end())
    return DSNodeHandle();
  else
    return I->second;
}

// Attach metadata to 
void ThreadEscapeAnalysis::attachDSNodeMetaData(Instruction *inst)
{
#if 0
  if(!inst) return;
  const Value *v = NULL;
  
  if(StoreInst *store = dyn_cast<StoreInst>(inst)) {
    v = store->getPointerOperand();
  }
  else if(LoadInst *load = dyn_cast<LoadInst>(inst)) {
    v = load->getPointerOperand();
  }
  else if(BitCastInst *bitcast = dyn_cast<BitCastInst>(inst)) {
    v = bitcast->getOperand(0);
  }
  else if(GetElementPtrInst* GepA = dyn_cast<GetElementPtrInst>(inst)) {
     v = GepA->getPointerOperand();
  }

  v = v ? getRealUnderlyingObject(v) : NULL;
  if(v) {
    vector<DSNodeHandle> nodes;
    getNodesForObject(v, nodes, inst);
    assert(nodes.size() > 0);
    for(size_t i = 0; i < nodes.size(); i++) {
      DSNode *N = nodes[i].getNode();
      if(!N) continue;
      char str[20];
      // sprintf(str,"%x:%d", nodes[i].getNode(), nodes[i].getOffset());
      sprintf(str,"%d:%d", nodes[i].getNode()->getNodeId(), nodes[i].getOffset());

      LLVMContext& C = inst->getContext();
      MDNode* mdnode = MDNode::get(C, MDString::get(C, str));
      inst->setMetadata("DSNode", mdnode );
    }
  }
#endif
}


/**********************************************************************************
 * BLACK LISTING ( MUTUAL EXCUSION) SECTION
 *********************************************************************************/
SCCMutualExclusionBlock::SCCMutualExclusionBlock(DSNode *node)
{
  _node = node;
  _readFreq = _writeFreq = 0;
  _serialPenalty = 0.0;

  // populate acceses
  for(int write = 0; write < 2; write++) {
    std::map<const Instruction *, int>& locs = write ? node->getWriteLocs() : node->getReadLocs();
    for(std::map<const Instruction *, int>::iterator itr = locs.begin(); itr != locs.end(); itr++)
    {
      Instruction *instr = const_cast<Instruction *>(itr->first);
      SCCAccessRegion *r = new SCCAccessRegion(instr, (bool) write);
      _accesses.push_back(r);
    }
  }
}


SCCMutualExclusionBlock::~SCCMutualExclusionBlock()
{
  for(size_t i = 0, ie = _accesses.size(); i < ie; i++)
    delete _accesses[i];

  _accesses.clear();
}

// compute outerloops for access regions
void SCCMutualExclusionBlock::computeOuterLoops(ThreadEscapeAnalysis *ea)
{
  for(size_t i = 0, ie = _accesses.size(); i < ie; i++) {
     _accesses[i]->expand(ea);
  }
}

// Expand all the regions for this node's read and write locations, so they contain
// the outermost loop they can.
void SCCMutualExclusionBlock::expandRegions(ThreadEscapeAnalysis *ea)
{
  for(size_t i = 0, ie = _accesses.size(); i < ie; i++) {
     _accesses[i]->expand(ea);
  }

  // TBD: remove duplicate regions, when same outer loop
  std::sort(_accesses.begin(), _accesses.end(), SCCAccessRegion::cmpRegionByLoop);
  std::vector<SCCAccessRegion *> newRegions;
  newRegions.reserve(_accesses.size());

  SCCAccessRegion *prev = NULL;
  for(size_t i = 0, ie = _accesses.size(); i < ie; i++) {
    if(prev && 
        ( (prev->getPreHeader() && prev->getPreHeader() == _accesses[i]->getPreHeader()) || 
        prev->getBlock() == _accesses[i]->getBlock()) ) 
    {
      prev->merge(_accesses[i]);
    }
    else
    {
      // non overlapping region with 
      prev = _accesses[i];
      newRegions.push_back(prev);
    }
  }

  if(newRegions.size() < _accesses.size()) {
    _accesses = newRegions;
  }

  // Update total frequency
  _readFreq = _writeFreq = 0;
  for(size_t i = 0, ie = _accesses.size(); i < ie; i++) {
    SCCAccessRegion *r = _accesses[i];
    int f = r->freq();
    if(r->isWrite()) _writeFreq += f;
    if(r->isRead()) _readFreq += f;
  }
}


// Merge one region into another
void SCCAccessRegion::merge(SCCAccessRegion *a) {
  if(a) {
    _freq += a->freq();
    if(a->isWrite())
      _write = true;
    if(a->isRead())
      _read = true;
  }
}

// Estimate loop trip count. TBD
int SCCAccessRegion::estimateTripCnt(Loop *l) 
{ 
  return (l ? 10 : 1); 
}

// Check that the loop has no split task or thread directive, or major function call
// TBD
bool SCCAccessRegion::loopOKToInclude(Loop *l)
{
  if(!l) return false;

  // if multiple exit blocks, skip
  if(!l->getExitingBlock()) return false;
  // check every basic block in it
  const std::vector<BasicBlock *>& blks = l->getBlocks();
  for(size_t i = 0, ie = blks.size(); i < ie; i++) {
    BasicBlock *bb = blks[i];
    for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
      if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
        const CallSite cs(inst);
        if (cs.getCalledFunction() == NULL) {
          return false;
        } else {
          Function* F = cs.getCalledFunction();

          // const char* opcode = cs.getInstruction()->getOpcodeName();
          std::string name = F->getName();

          // Can't include loop with split task or thread create call
          if(name == "scc_split_task")
            return false;
          if(name.substr(0,10)  == "scc_thread")
            return false;
        }
      } // if it is a function 
    }
  }

  // Now also check subLoops, in case the above loop doesn't cover basic blocks of sub loops
  // This miht be an overkill
  const std::vector<Loop *>& subloops = l->getSubLoops();
  for(size_t i = 0, ie = subloops.size(); i < ie; i++) {
    if(!loopOKToInclude(subloops[i]))
      return false;
  }
  
  return true;
}

// Find the tasks that executes the instruction
bool SCCAccessRegion::findTasks(SccProfile *sccProf)
{
  if(!(_origInst && sccProf)) 
    return false;;
  sccProf->getTasks(_origInst->getParent(), _tasks);
  return true;
}

// Compute a score based on overlap among tasks that execute the various access regions of this node
// if all writes happend in single threaded mode, then it has low score.
// The computed score is actualy a penalty
void SCCMutualExclusionBlock::computeTaskOverlapScore(SccProfile *sccProf)
{
  _serialPenalty = 0.0; // ideally, no serialization if writes happen solo
  if(!sccProf) return;

  // First find all tasks in each access region
  for(size_t i = 0, ie = _accesses.size(); i < ie; i++)
    _accesses[i]->findTasks(sccProf);

  for(size_t i = 0, ie = _accesses.size(); i < ie; i++) {
    SCCAccessRegion *r1 = _accesses[i];
    if(!r1->isWrite()) continue;
    
    std::vector<uint32_t>& tasks1 = r1->tasks();
    for(size_t j = 0, je = ie; j < je; j++) {
      SCCAccessRegion *r2 = _accesses[j];
      // if(r1 == r2) continue;
      std::vector<uint32_t>& tasks2 = r2->tasks();

      // Compute how many edges in conflict graph. That indicates potential serialization
      // TBD: Skip same task id?
      for(size_t k = 0, ke = tasks1.size(); k < ke; k++) {
        for(size_t l = 0, le = tasks2.size(); l < le; l++) {
            uint32_t t1 = tasks1[k];
            uint32_t t2 = tasks2[l];

            // if t1 == t2, tasksOverlap should return false; still adding check 't1 != t2'
            if(t1 != t2)  // composite task ids here
              _serialPenalty += sccProf->tasksOverlap(t1, t2);
        }  // for each task in tasks2
      } // for each task in tasks1
    } // for j
  } // for each access
}


// Expand till out of loop, or no scc_split_task or thread primitive
void SCCAccessRegion::expand(ThreadEscapeAnalysis *ea)
{
  if(!_origInst) {
    _freq = 0;
    return;
  }

  _freq = 1;
  _preheader = NULL;

  Function *func = _origInst->getParent()->getParent();
  LoopInfo *LI = ea->getLoopInfo(func);
  if(!LI) return;
  std::string fname = func->getName();
  std::cerr << "Got loop info for " << fname << "\n";
  Loop *loop = LI->getLoopFor(_origInst->getParent());
  /* if(loop) {
    std::cerr << "Loop is\n";
    loop->print(std::cerr);
  } */
  
  while(loop) {
    if(loopOKToInclude(loop)) {
      _preheader = loop->getLoopPreheader(); // loop->getHeader();
       _freq *= estimateTripCnt(loop);
      loop = loop->getParentLoop();
     }
     else
      break;
  }
}

// Compare regions by outermost loop where access can be hoisted
// When no loop, the order is arbitrary within those accesses that are not part of any loop.
bool SCCAccessRegion::cmpRegionByLoop(SCCAccessRegion *a, SCCAccessRegion *b)
{
   return (a->getPreHeader() < b->getPreHeader()); // TBD: Remove non-det through use of pointer comparison
}

// Guard this region with  locks
void SCCAccessRegion::protectRegion(int lockId, 
    Constant *readLockFunc, Constant *writeLockFunc, IntegerType *Int32Ty)
{
  Constant *startPhaseFn = _write ? writeLockFunc : readLockFunc;
  BasicBlock * first = _origInst->getParent();
  // BasicBlock* last = first; 

  if(_preheader)
    first = _preheader;

  // Now create the function call
  SmallVector<Value*,1>  operands;
  ConstantInt *v = ConstantInt::get(Int32Ty, lockId, true);
  operands.push_back(v);

  if(first) {
    CallInst::Create(startPhaseFn, operands, "", first->getFirstNonPHI());
    // 9/18/12: Don't add any end call; hold lock till task end.
    // CallInst* endCall   = CallInst::Create(endPhaseFn, operands, "", last->getTerminator());
  }
}

// Update extra locks needed to protect this write/non-write region when its loop/BB is
// already previously known to be protected.
void SCCProtectingLocks::updateCnts(bool isWrite, int& numRdLocks, int& numWrLocks, int& numRdToWr)
{
  bool written = false, read = false;
  for(size_t i = 0, ie = _protectedRegions.size(); i < ie; i++) {
    if(_protectedRegions[i]->isWrite()) {
      written = true;
      break;
    }
    else
      read = true;
  }

  if(!written && isWrite)
    numRdToWr++;
  return;
}

// Protect all accesses of a mutual exclusion block
bool ThreadEscapeAnalysis::protectRegions(SCCMutualExclusionBlock *node, int lockId)
{
  std::vector<SCCAccessRegion *>& accesses = node->getAccesses();
  size_t ie = accesses.size();

  int numRdLocks = 0, numWrLocks = 0, numRdToWr = 0;
  for(size_t i = 0; i < ie; i++) {
    SCCAccessRegion *r = accesses[i];
    BasicBlock *preheader = r->getPreHeader();
    BasicBlock *bb = r->getBlock();
    if(preheader) {
      std::map<BasicBlock *, SCCProtectingLocks>::iterator itr = _protectedLoops.find(preheader);
      if(itr != _protectedLoops.end()) {
        SCCProtectingLocks& tmp = itr->second;
        tmp.updateCnts(r->isWrite(), numRdLocks, numWrLocks, numRdToWr);
      }
    }
    else if(bb) {
      std::map<BasicBlock *, SCCProtectingLocks>::iterator itr = _protectedBlocks.find(bb);
      if(itr != _protectedBlocks.end()) {
        SCCProtectingLocks& tmp = itr->second;
        tmp.updateCnts(r->isWrite(), numRdLocks, numWrLocks, numRdToWr);
      }
    }
  }

  // Based on extra locks, and lockId, decide whether to protect this node
  // TBD: Tune this heuristic
  bool doProtect = (lockId <= 1 || (numRdLocks + numWrLocks + numRdToWr) <= 10);
  if(doProtect) {
    for(size_t i = 0; i < ie; i++) {
      SCCAccessRegion *r = accesses[i];
      BasicBlock *preheader = r->getPreHeader();
      BasicBlock *bb = r->getBlock();
      if(preheader)
        _protectedLoops[preheader].addRegion(r, lockId);
      else
        _protectedBlocks[bb].addRegion(r, lockId);

      accesses[i]->protectRegion(lockId, _readLockFunc, _writeLockFunc, Int32Ty);
    }
  }

  node->getDSNode()->setMutexProtected(true);
  return doProtect;
}

// Find suitable escaping nodes to be protected by mutual exclusion, so that there is no 
// read/write or write/write overlap. We call this 'blacklisting'. If a variable is protected,
// all its reads and writes must always be protected; else an access conflict may go unnoticed.

int ThreadEscapeAnalysis::doBlackListing()
{
  if(!doSccBlacklist)
    return 0;

  sccProfPass = &getAnalysis<SccProfile>();
  if(sccProfPass) {
    std::cerr << "GOT SCC profile pass.\n";
  }

  // 1. Find subset of escaping nodes that have write accesses
  std::vector<SCCMutualExclusionBlock *> writeEscapedNodes;
  int totalNodes = 0, rdNodes = 0;
  for(DenseSet<const DSNode*>::iterator itr = EscapingNodes.begin(); itr != EscapingNodes.end(); ++itr) {
    DSNode *node = const_cast<DSNode *>(*itr);
    if(!node) continue;
    totalNodes++;
    node->setMutexProtected(false);
    if(node->getReadLocs().size() > 0)
      rdNodes++;
    if(node->getWriteLocs().size() == 0) {
      // node->print(dbgs(), ResultGraph)
      continue;
    }
    SCCMutualExclusionBlock *blackListNode = new SCCMutualExclusionBlock(node);
    writeEscapedNodes.push_back(blackListNode);
  }

  std::cerr << "RUNNING BLACKLISTING (max " << doSccBlacklist << " nodes) with " << writeEscapedNodes.size() << " escaping write nodes out of total " 
        << totalNodes << "(rd nodes " << rdNodes << ") ****\n";

  // 2. Expand each access till out of loops, and update frequency. Only move if no scc_scplit_task
  //    and no pthread directive.
  for(size_t i = 0, ie = writeEscapedNodes.size(); i < ie; i++) {
    writeEscapedNodes[i]->expandRegions(this);
    writeEscapedNodes[i]->computeTaskOverlapScore(sccProfPass);
  }

  // 3. Here is where we can do some research by trying different strategies. 
  // 3.a sort these nodes by frequency/number of places to protect 
  // TBD 

  // mutually exclude first node. Then, remove all other nodes covered by this node.
  // Then, mutually exclude some other node, and remove all covered nodes. Keep on going as desired.
#if 0
  std::sort(writeEscapedNodes.begin(), writeEscapedNodes.end(), SCCMutualExclusionBlock::cmpFreq);
#else
  std::sort(writeEscapedNodes.begin(), writeEscapedNodes.end(), SCCMutualExclusionBlock::cmpSerialPenalty);
#endif

  // Just protect the first one for now
  int lockId = 1;

  for(size_t i = 0; i < writeEscapedNodes.size(); i++) 
  {
    SCCMutualExclusionBlock *wn = writeEscapedNodes[i];
    DSNode *node = wn->getDSNode();
    DEBUG( dbgs() << "Escape node for blacklist has serial penalty " << wn->serialPenalty() << "\n") ;
    std::cerr << "Escape node " << i << " for blacklist has serial penalty " << wn->serialPenalty() << "\n";
    std::cerr << "  Node has " << node->getWriteLocs().size() << " writes and " <<
            node->getReadLocs().size() << " reads\n";
    std::cerr << "  DSNode " << node << "\n";

    // wn->computeOuterLoops(this);
    if(protectRegions(wn, lockId)) {
      lockId++;
      std::cerr << "  Node is blacklisted\n";
      NumSCCBlackListed++;
    }
  }

  return NumSCCBlackListed;
}


// Print info for racy locations
void ThreadEscapeAnalysis::printLineInfo(const Instruction *I) 
{
  if (MDNode *N = I->getMetadata("dbg")) {
    DILocation Loc(N);                      // DILocation is in DebugInfo.h
    unsigned Line = Loc.getLineNumber();
    StringRef File = Loc.getFilename();
    // StringRef Dir = Loc.getDirectory();
    std::string fname = File;

    DEBUG(dbgs() << I << " File " << File << ", Line " << Line << "\n");
    std::cerr << "File " << fname << ":" << Line << "\n";
  }
  else {
    DEBUG(dbgs() << I << " File: NA Line: NA\n");
  }
  
  const Value *val = NULL;
  if (const LoadInst* load = dyn_cast<LoadInst>(I)) {
    val = load->getPointerOperand();
  }
  else if (const StoreInst* store = dyn_cast<StoreInst>(I)) {
    val = store->getPointerOperand();
  }

  if(val) {
    const Value *underlying_val = getRealUnderlyingObject(val);
    std::string underlying_name = underlying_val->getName();
    std::string name = val->getName();
    std::cerr << "Value is " << name << " root value: " << underlying_name << std::endl;
    I->dump();
  }
}

struct ltstr
{
  bool operator()(const char* s1, const char* s2) const
  {
    return strcmp(s1, s2) < 0;
  }
};

typedef std::map<const char *, set<unsigned>, ltstr> ldStCounter;
typedef std::map<const char *, int, ltstr>  lineCounter;


// count the total number of lines in all files compiles ( or, at least a func used init)
static int countTotalProgramLines(Module& M, ofstream& statOut)
{

  lineCounter filesToNumLines;
  ldStCounter allRdLines;
  ldStCounter allWrLines;
  unsigned  readInstrCnt = 0, writeInstrCnt = 0;

  for (Module::iterator F = M.begin(); F != M.end(); ++F) {
    if (F->isDeclaration())
      continue;

    for(Function::iterator bb = F->begin(), E = F->end(); bb != E; ++bb) {
      for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
        Instruction *I = inst;
        if (MDNode *N = I->getMetadata("dbg")) {
          DILocation Loc(N);                      // DILocation is in DebugInfo.h
          StringRef File = Loc.getFilename();
          const char *fname = File.data();
          // StringRef Dir = Loc.getDirectory();
          if(fname) {
            lineCounter::iterator itr = filesToNumLines.find(fname);
            if(itr == filesToNumLines.end()) {
              filesToNumLines[fname] = 0;
            }

	          bool isStore = isa<StoreInst>(inst);
	          bool isLoad = isa<LoadInst>(inst);
	          if(isStore || isLoad) {
	            unsigned Line = Loc.getLineNumber();
	            if(isStore) {
                (allWrLines[fname]).insert(Line);
                writeInstrCnt++;
              }
	            if(isLoad) {
                (allRdLines[fname]).insert(Line);
                readInstrCnt++;
              }
            }
          }
        }
      }   // for each inst in bb
    }  // for each bb
  }  // for each func

  // first count rd/wr lines
  int totalRdWrLines = 0;
  for(int i = 0; i < 2; i++) {
    ldStCounter& m = i ? allRdLines : allWrLines;
    int cnt = 0;
    for(ldStCounter::iterator itr = m.begin(); itr != m.end(); itr++) {
      cnt += (itr->second).size();
    }
    totalRdWrLines += cnt;
    statOut << (i ? "TotalRdLines " : "TotalWrLines ") << cnt << endl;
  }
  statOut << "TotalRdWrLines " << totalRdWrLines << endl;

  // Also print instruciton counts
  statOut << "TotalRdInsts " << readInstrCnt << endl;
  statOut << "TotalWrInsts " << writeInstrCnt << endl;
  statOut << "TotalRdWrInsts " << (readInstrCnt + writeInstrCnt) << endl;

  // Now count total lines
  int totalLines = 0;
  for(std::map<const char *, int, ltstr>::iterator itr = filesToNumLines.begin();
      itr != filesToNumLines.end(); itr++) 
  {
      char fname[1000];
      strcpy(fname, itr->first);
      FILE *fp = fopen(fname,"r");
      if(!fp) {
        // llvm sometimes changes .c to '.C'. fix that
        char *end = fname + strlen(fname) -1;
        if(end > fname && *end == 'C')
          *end = 'c';
        fp = fopen(fname, "r");
      }

      if(!fp) {
        fprintf(stderr, "ERROR: CANNOT OPEN FILE %s\n", fname);
        continue;
      }

      int n = 0;
      char line[2000];
      while(fgets(line, 2000, fp)) n++;

      fprintf(stderr, "File %s: Lines %d\n", fname, n);
      totalLines += n;
  }
  filesToNumLines.clear();

  statOut << "TotalProgramLines " << totalLines << endl;
  return totalLines;
}

// Insert the linenumber, indexed by file, in this map
static void insertLineInfo(ldStCounter& counter, const Instruction *inst)
{
  if(!inst) return;
  if (MDNode *N = inst->getMetadata("dbg")) {
    DILocation Loc(N);                      // DILocation is in DebugInfo.h
    StringRef File = Loc.getFilename();
    const char *fname = File.data();
    // StringRef Dir = Loc.getDirectory();
    if(fname) {
      unsigned Line = Loc.getLineNumber();
      counter[fname].insert(Line);
    }
  }
}

// report races with variable names and instructions if possible. 
// Fine read/write or write/write overlapsi. 
int ThreadEscapeAnalysis::reportRaces(Module& M)
{
  if(!doReportRace)
    return 0;

  ofstream statOut("race.stat");
  TotalProgramLines = countTotalProgramLines(M, statOut);

  // If TSA not run, the read/write locations are not populated. Populate from every
  // instruction in the program
  if(!doTSA) {
    uint32_t phase = 1;
    std::set<const Function *> calledFunctions;
    for (Module::iterator F = M.begin(); F != M.end(); ++F) {
      if (F->isDeclaration())
        continue;

      for(Function::iterator bb = F->begin(), E = F->end(); bb != E; ++bb) {
        for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
          Instruction *I = inst;
          visitTSAInstruction(inst, calledFunctions, phase);
        }
      }
    }
  } // if do tsa

  // Collect mtrom values
  Function *markMtrom = M.getFunction("scc_assure_mtrom");
  std::set<const Value *> mtromValues;
  for (Module::iterator F = M.begin(); F != M.end(); ++F) {
    if (F->isDeclaration())
      continue;
    for(Function::iterator bb = F->begin(); bb != F->end(); bb++) {
      for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
        if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
          const CallSite cs(inst);
          if (markMtrom && cs.getCalledFunction() == markMtrom) {
            const CallInst* CI = dyn_cast<CallInst>(inst);
            const Value *value = CI->getOperand(0);
            if(value) {
              mtromValues.insert(value);
              value = getRealUnderlyingObject(value);
              if(value) mtromValues.insert(value);
            }
          }
        } // if callInst
      } // for each instruction in BB
    } // for each basic block
  } // for each function


  // 1. Find subset of escaping nodes that have write accesses
  std::vector<DSNode *> writeEscapedNodes;
  int totalNodes = 0, rdNodes = 0;
  for(DenseSet<const DSNode*>::iterator itr = EscapingNodes.begin(); itr != EscapingNodes.end(); ++itr) {
    DSNode *node = const_cast<DSNode *>(*itr);
    if(!node) continue;
    totalNodes++;
    if(node->getReadLocs().size() > 0)
      rdNodes++;
    if(node->getWriteLocs().size() == 0) {
      // node->print(dbgs(), ResultGraph)
      continue;
    }
    writeEscapedNodes.push_back(node);
  }

  std::cerr << "REPORTING RACES with " << writeEscapedNodes.size() << " escaping write nodes out of total " 
        << totalNodes << "(rd nodes " << rdNodes << ") ****\n";
  NumRacyNodes = 0;

  
  ldStCounter racyReadLines, racyWriteLines, racyReadWriteLines;  // collect racy rd/wr lines per file
  set<const Instruction *> racyReadInstrs, racyWriteInstrs;

  float totalWriteInstrs = 0, totalRdLocsImpacted = 0;
  for(size_t i = 0; i < writeEscapedNodes.size(); i++) 
  {
    DSNode *node = writeEscapedNodes[i];

    // Check for overlaps in parallel phases where N is modified, and where loadStoreInst is executed
    if (_nodeModifiedInPhases.find(node) == _nodeModifiedInPhases.end())
      continue ;

    // check if written in same phase. This considers mutex protected writes, and field sensitive writes if needed
    // The function accessedInCriticalRegionOnly below considers lock proetction of each access, including reads
    bool samePhaseWrite = false;
    std::map<const Instruction *, int>& writeLocs = node->getWriteLocs();
    vector< pair<const Instruction *, bool> > writeLocsWithMutex; // fidn which writes are mutex protected
    for(std::map<const Instruction *, int>::iterator itr = writeLocs.begin(); itr != writeLocs.end();  itr++) 
    {
      Instruction *inst = const_cast<Instruction *>(itr->first);
      bool hasUnprotectedWrite = false;
      // find read locs in phases that this write appears
      if(_storeInstsInPhases.find(inst) != _storeInstsInPhases.end()) {
        std::set<uint32_t>& execPhases = _storeInstsInPhases[inst];
        for(std::set<uint32_t>::iterator itr2 = execPhases.begin(); itr2 != execPhases.end(); itr2++) {
          uint32_t phaseId = *itr2;
          if(doCriticalRegionOpt && accessedInCriticalRegionOnly(node, phaseId, inst)) {
            // no race through this in this phase
            continue;
          }
          hasUnprotectedWrite = true;
        }
      }

      writeLocsWithMutex.push_back(make_pair(inst, hasUnprotectedWrite));

      if(hasUnprotectedWrite) samePhaseWrite = true;
    }

    if(!samePhaseWrite) {
        std::cerr << " Skip node as writes protected by mutex locks\n";
        continue; // this node was never written in same phase, so no race through it
    }

    NumRacyNodes++;

    // Check how many reads and write in each phase it is written


    // Count the number of reads for this node in each phase 
    vector<int> readCountPerPhase;
    vector<set<const Value *> *> coveredValuesPerPhase;
    for(size_t jj = 0; jj < 100; jj++) {
      readCountPerPhase.push_back(0);

      if(doRaceOncePerValue) {
        set<const Value *> *valueMap = new set<const Value *>;
        coveredValuesPerPhase.push_back(valueMap);
      }
    }

    std::set<const Value *> coveredValues;
    std::map<const Instruction *, int>& readLocs = node->getReadLocs();
    for(std::map<const Instruction *, int>::iterator itr = readLocs.begin(); itr != readLocs.end(); itr++) {
      const Instruction *inst = itr->first;
      const Value *v = NULL;
      if(const StoreInst *store = dyn_cast<StoreInst>(inst)) {
          v = store->getPointerOperand();
      }
      else if(const LoadInst *load = dyn_cast<LoadInst>(inst)) {
        v = load->getPointerOperand();
      }
      if(!v) continue;
      if(mtromValues.count(v)) continue; // MTROM

      if(_loadInstInPhases.find(inst) != _loadInstInPhases.end()) {
        std::set<uint32_t>& execPhases = _loadInstInPhases[inst];
        for(std::set<uint32_t>::iterator itr2 = execPhases.begin(); itr2 != execPhases.end(); itr2++) {
          uint32_t phaseId = *itr2;

          // If already covered, skip
          if(doRaceOncePerValue) {
            v = getRealUnderlyingObject(v);
            set<const Value *> *vMap = coveredValuesPerPhase[0]; // should it check per phase? then change 0 to phaseId
            if(vMap->find(v) != vMap->end()) continue;
            vMap->insert(v); 
          }
          readCountPerPhase[phaseId]++;
    

          racyReadInstrs.insert(inst);

          // Also insert in racy Nodes.
          insertLineInfo(racyReadLines,inst);
          insertLineInfo(racyReadWriteLines, inst);
        } 
      }
    } // for each read loc

    // Insert the racy write lines
    for(std::map<const Instruction *, int>::iterator itr = writeLocs.begin(); itr != writeLocs.end();  itr++) 
    {
      Instruction *inst = const_cast<Instruction *>(itr->first);
      racyWriteInstrs.insert(inst);
      insertLineInfo(racyWriteLines, inst);
      insertLineInfo(racyReadWriteLines, inst);
    }

    if(doRaceOncePerValue) {
      for(size_t jj = 0; jj < 100; jj++)  {
        coveredValuesPerPhase[jj]->clear(); 
        delete (coveredValuesPerPhase[jj]);
      }
    }
    

    std::cerr << "  DSNode " << node << " has " << node->getWriteLocs().size() << " writes and " <<
            node->getReadLocs().size() << " reads\n";
    std::cerr << "=====================================================\n";
    std::cerr << "Write Locations: " << std::endl;

    set<const Value *> visitedValues;
    for(size_t jj = 0; jj < writeLocsWithMutex.size(); jj++) { 
      if(writeLocsWithMutex[jj].second == false) continue; // always mutex protected
      int rdLocsImpacted = 0;
      const Instruction *inst =  writeLocsWithMutex[jj].first;

      if(doRaceOncePerValue) {
        // Report only once per value. 
        const Value *v = NULL;
        if(const StoreInst *store = dyn_cast<StoreInst>(inst)) {
          v = store->getPointerOperand();
        }
        else if(const LoadInst *load = dyn_cast<LoadInst>(inst)) {
          v = load->getPointerOperand();
        }
        if(!v) continue;
        v = getRealUnderlyingObject(v);
        if(visitedValues.find(v) != visitedValues.end()) continue; 
        visitedValues.insert(v);
      }

      /* const DebugLoc& debugLoc = inst->getDebugLoc();
      debugLoc.dump(inst->getContext()); */
      // std::cerr << (itr->first)->getDebugLoc();

      // Metadata
      totalWriteInstrs++;
      if(_storeInstsInPhases.find(inst) != _storeInstsInPhases.end()) {
        std::set<uint32_t>& execPhases = _storeInstsInPhases[inst];
        for(std::set<uint32_t>::iterator itr2 = execPhases.begin(); itr2 != execPhases.end(); itr2++) {
          uint32_t phaseId = *itr2;
          rdLocsImpacted += readCountPerPhase[phaseId]++;
        }
      }
      printLineInfo(inst);
      std::cerr << "Impacts " << rdLocsImpacted << " read locations in same phases\n";
      totalRdLocsImpacted += rdLocsImpacted;
    }
  }  // for each escaping write node

  AvgWriteLocsPerNode = totalWriteInstrs/NumRacyNodes;
  TotalRacyWriteLocs = totalWriteInstrs;
  AvgReadLocsPerNode = totalRdLocsImpacted/NumRacyNodes;
  TotalRacyReadLocs = totalRdLocsImpacted;

  // create a stat file for ease of results gathering
  statOut << "TotalRacyWrLocs " <<  TotalRacyWriteLocs << endl;
  statOut << "TotalRacyRdLocs " << TotalRacyReadLocs << endl;
  statOut << "TotalRacyRdWrLocs " << (TotalRacyWriteLocs + TotalRacyReadLocs) << endl;
  statOut << "RacyRdInsts " << racyReadInstrs.size() << endl;
  statOut << "RacyWrInsts " << racyWriteInstrs.size() << endl;
  statOut << "RacyRdWrInsts " << (racyReadInstrs.size() + racyWriteInstrs.size()) << endl;

  unsigned totRacyRdLines = 0;
  for(ldStCounter::iterator itr = racyReadLines.begin(); itr !=  racyReadLines.end(); itr++) 
    totRacyRdLines += (itr->second).size();
  statOut << "RacyRdLines " << totRacyRdLines << endl;

  unsigned totRacyWrLines = 0;
  for(ldStCounter::iterator itr = racyWriteLines.begin(); itr !=  racyWriteLines.end(); itr++) 
    totRacyWrLines += (itr->second).size();
  statOut << "RacyWrLines " << totRacyWrLines<< endl;

  unsigned totRacyRdWrLines = 0;
  for(ldStCounter::iterator itr = racyReadWriteLines.begin(); itr !=  racyReadWriteLines.end(); itr++) 
    totRacyRdWrLines += (itr->second).size();
  statOut << "RacyRdWrLines " << totRacyRdWrLines<< endl;

  statOut.close();
  return NumRacyNodes;
}

ThreadEscapeAnalysis::ThreadEscapeAnalysis()
  : DataStructures(ID, "threadescape."),
    ResultGraph(NULL), sccProfPass(NULL)
{
  DT = NULL;
  PDT = NULL;
  DF = NULL;
  curDTSId = 0;

  _readLockFunc = NULL;
  _writeLockFunc = NULL;

  VoidTy = NULL;
  Int32Ty = NULL;

  //_THREAD_CREATE_FN_NAME = strdup("SOFRITAS_threadCreate");
  //_THREAD_JOIN_FN_NAME = strdup("SOFRITAS_join");
  _THREAD_CREATE_FN_NAME = strdup("pthread_create");
  _THREAD_JOIN_FN_NAME = strdup("pthread_join");

  argClassifier.setThreadFunctionNames(_THREAD_CREATE_FN_NAME, _THREAD_JOIN_FN_NAME);
}

void ThreadEscapeAnalysis::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<StdLibDataStructures>();
  AU.addRequired<DataLayoutPass>();


  AU.addRequired<DominatorTreeWrapperPass>();
  AU.addPreserved<DominatorTreeWrapperPass>();

  AU.addRequired<PostDominatorTree>();
  AU.addPreserved<PostDominatorTree>();

  AU.addRequired<DominanceFrontier>();
  AU.addPreserved<DominanceFrontier>();

  AU.addRequired<LoopInfo>();
  AU.addPreserved<LoopInfo>();

  AU.addRequired<ScalarEvolution>();
  AU.addPreserved<ScalarEvolution>();
  // AU.addRequiredID(LoopSimplifyID);

#if 0
  if(doSccBlacklist) {
    AU.addRequired<BlockFrequencyInfo>();
    AU.addRequired<SccProfile>();
  }
#endif

  AU.setPreservesAll();
}

//------------------------------------------------------------------------------
// Pass API
//------------------------------------------------------------------------------

bool ThreadEscapeAnalysis::runOnModule(Module &M) 
{
  bool foundThreadSections = doSteensgaard(M);

  debugNonUnifiedFuncCalls();
  //ResultGraph->writeGraphToFile(dbgs(), "steensgaards");

  VoidTy   = Type::getVoidTy(M.getContext());
  Int32Ty  = Type::getInt32Ty(M.getContext());


  if(foundThreadSections) {
    doEscapeAnalysis(M);

    if(doTSA) {
      findAllThreadSections(M, false);

      // Print some stats
#if 0
      fprintf(stderr,"   Load Instructions in phases\n");
      for(map<uint32_t, int>::iterator itr = _dtsLdCnts.begin(); itr != _dtsLdCnts.end(); itr++) {
        fprintf(stderr,"     %d : %d\n", itr->first, itr->second);
      }
      fprintf(stderr,"   Store Instructions in phases\n");
      for(map<uint32_t, int>::iterator itr = _dtsStCnts.begin(); itr != _dtsStCnts.end(); itr++) {
        fprintf(stderr,"     %d : %d\n", itr->first, itr->second);
      }
      fprintf(stderr,"   Nodes accessed in phases\n");
      for(map<uint32_t, set<DSNode *> >::iterator itr = _dtsAccessedNodes.begin(); 
          itr != _dtsAccessedNodes.end(); itr++) {
        fprintf(stderr,"     %d : ", itr->first);
        set<DSNode *>& nodes = itr->second;
        for(set<DSNode *>::iterator itr1 = nodes.begin(); itr1 != nodes.end(); itr1++) {
          fprintf(stderr," %d", (*itr1)->getNodeId());
        }
        fprintf(stderr,"\n");
      }
#endif

      // If we already marked DTS, don't re-mark DTS now. Just collect MT functions
      identifyMultiThreadedFunctions(M);

      for(std::map<const Function *, int>::iterator it = threadReachableFuncs.begin(), end = threadReachableFuncs.end(); it != end; ++it){
	llvm::errs() << it->first->getName() << "\n";
      }
    }

    // Do black listing (mutual exclusion) analysis

    // start_rd, start_write functions
    _readLockFunc = M.getOrInsertFunction("scc_start_read_phase", VoidTy,  Int32Ty, NULL);
    _writeLockFunc = M.getOrInsertFunction("scc_start_write_phase", VoidTy,  Int32Ty, NULL);

    if(_readLockFunc && _writeLockFunc) {
      doBlackListing();
    }
  }
  else {
    DEBUG(dbgs() << "Didn't find thread sections!!!\n");
  }

  if(doCriticalRegionOpt) {
    findMutexProtectedInstructions(M);
  }

  // Report static races
  if(doReportRace) {
    reportRaces(M);
  }

  return true;
}

//
// Steensgaard
//

bool ThreadEscapeAnalysis::doSteensgaard(Module &M) 
{
  ThreadCreate = M.getFunction(_THREAD_CREATE_FN_NAME);
  ThreadStart = M.getFunction("thread_start");

  //  These may ve absent if no mutex_lock and mutex_unlcok used in program 
  if(!(SCCMutexLockFunc   = M.getFunction("scc_thread_mutex_lock")))
    SCCMutexLockFunc = M.getFunction("pthread_mutex_lock");
  if(!(SCCMutexUnlockFunc = M.getFunction("scc_thread_mutex_unlock")))
    SCCMutexUnlockFunc = M.getFunction("pthread_mutex_unlock");

  if(doCriticalRegionOpt) {
    if(!SCCMutexLockFunc)  
      fprintf(stderr,"SCC-WARNING: scc_thread_mutex_lock not found in program!\n");
    if(!SCCMutexUnlockFunc)
      fprintf(stderr,"SCC-WARNING: scc_thread_mutex_unlock not found in program!\n");
  }

  ThreadJoin = M.getFunction(_THREAD_JOIN_FN_NAME);
  ThreadSync = M.getFunction("scc_sync_threads");

  StartTSFn  = M.getFunction("scc_start_ts");
  if(!StartTSFn) {
    // try stamp function
    StartTSFn = M.getFunction("thread_startup");
  }

  EndTSFn    = M.getFunction("scc_end_ts");
  if(!EndTSFn) {
    EndTSFn = M.getFunction("TM_SHUTDOWN");
    if(!EndTSFn) {
      EndTSFn = M.getFunction("thread_shutdown");
    }
  }

  if(!(ThreadCreate || (StartTSFn && EndTSFn))) {
    std::cerr << "*******************************************************************************\n";
    std::cerr << " ERROR: Neither " << _THREAD_CREATE_FN_NAME << " nor user-marked thread sections were found!! Analysis may be incorrect.\n";
    std::cerr << "IF YOU HAVE pthread_create CALLS, YOU SHOULD FIX THIS ERROR.\n";
    std::cerr << "*******************************************************************************\n";
    return false;
  }

  DataStructures* DS = &getAnalysis<StdLibDataStructures>();
  const DataLayout *const_TD = &(getAnalysis<DataLayoutPass>().getDataLayout());
  TD = const_cast<DataLayout *>(const_TD);

  init(TD); // Initialize DSA.

  // Get a copy for the global graph.
  DSGraph* GG          = DS->getGlobalsGraph();
  SuperSet<Type*>& tss = DS->getTypeSS();
  GlobalsGraph         = new DSGraph(GG, GG->getGlobalECs(), tss);

  // Create a new, empty, graph.
  ResultGraph = new DSGraph(GlobalECs, *TD, tss);
  ResultGraph->setGlobalsGraph(GlobalsGraph);

  // Loop over the rest of the module, merging graphs for non-external
  // functions into this graph.
  for (Module::iterator F = M.begin(); F != M.end(); ++F) {
    if (!F->isDeclaration()) {
      ResultGraph->spliceFrom(DS->getDSGraph(*F));
      DEBUG(dbgs() << "Madan: Thread escape analysis for " << F->getName() << "\n" );
    }
  }

  ResultGraph->removeTriviallyDeadNodes();

  //
  // Now we have all the function graphs merged in.
  // We need to simulate function calls by merging args with formals.
  // In addition to the standard (direct and indirect) function calls,
  // we need to simulate thread creation.  For example, the following
  // includes an implicit call to 'startfn(startarg)':
  //
  //    call @SCCthread_create(id, attr, startfn, startarg)
  //
  // Once the call is simulated, we can delete the callsite.
  //

  std::list<DSCallSite>& functionCalls = ResultGraph->getFunctionCalls();
  std::vector<const Function*> callTargets;

  for (std::list<DSCallSite>::iterator
      CI = functionCalls.begin(); CI != functionCalls.end(); ) {
    DSCallSite &call = *CI;
    CI++;

    // gather targets
    callTargets.clear();
    if (call.isDirectCall())
      callTargets.push_back(call.getCalleeFunc());
    else
      call.getCalleeNode()->addFullFunctionList(callTargets);

    unsigned known = 0;

    // simulate calls
    for (unsigned c = 0; c != callTargets.size(); ++c) {
      const Function *F = callTargets[c];
      if (!F->isDeclaration()) {
        ResolveFunctionCall(F, call);
        ++known;
      }
      if (F == ThreadCreate || F == ThreadStart) {
        ResolveThreadCreation(F, call);
        assert(call.isDirectCall());
      }
    }

    // this callsite can be removed if it is complete
    if (call.isDirectCall() && known == 1) {
      std::list<DSCallSite>::iterator I = CI;
      functionCalls.erase(--I);
    }
  }

  //
  // Finally, we mark the following nodes incomplete:
  // -- Return values and formals of externally visible functions
  //    (we also consider a function externally visible if its address is taken)
  // -- External globals
  //

  // We have to collect functions that have their address taken by hand (ugh...)
  DenseSet<const DSNode*> Incomplete;

  for (DSGraph::ReturnNodesTy::iterator
       FI = ResultGraph->getReturnNodes().begin();
       FI != ResultGraph->getReturnNodes().end();
       ++FI) {
    Function *F = const_cast<Function*>(FI->first);
    for (Function::use_iterator U = F->use_begin(); U != F->use_end(); ++U) {
      // Skip direct calls.
      CallInst* call = dyn_cast<CallInst>(*U);
      if (call) {
        if (call->getCalledFunction() == F)
          continue;
      }
      // Collect.
      for (Function::const_arg_iterator I = F->arg_begin(), E = F->arg_end(); I != E; ++I)
        if (isa<PointerType>(I->getType()))
          ResultGraph->getNodeForValue(I).getNode()->markReachableNodes(Incomplete);
      FI->second.getNode()->markReachableNodes(Incomplete);
      break;
    }
  }

  // -- Mark externally visible functions ...
  ResultGraph->maskIncompleteMarkers();
  ResultGraph->markIncompleteNodes(DSGraph::IgnoreFormalArgs | DSGraph::IgnoreGlobals);

  for (DenseSet<const DSNode*>::Iterator
       N = Incomplete.begin(); N != Incomplete.end(); ++N) {
    const_cast<DSNode*>(*N)->setIncompleteMarker();
  }

  //FIXME: Renau. This may be a good place to split a DSNode if it has
  //variables that may not alias. This could happen if the steensgrad pass was
  //not able to detect correctly a non alias and another alias pass did
  //a better job.
  //
  // If AliasAnalysis::alias(V1, V1Size, V2, V2Size) does not alias for V1 and
  // V2, and both V1 and V2 are in the same DSNode, we know that we can split
  // the DSNode.

  if(!doNonUnifSteensGaard) {
    // Having issues with non-unified function argument mapping
    ResultGraph->removeDeadNodes(DSGraph::KeepUnreachableGlobals);
  }

  // -- Mark external globals ...
  GlobalsGraph->removeTriviallyDeadNodes();
  GlobalsGraph->maskIncompleteMarkers();
  GlobalsGraph->markIncompleteNodes(DSGraph::IgnoreGlobals);
  formGlobalECs();

  // Clone the global nodes into this graph.
  ReachabilityCloner RC(ResultGraph, GlobalsGraph,
                        DSGraph::DontCloneCallNodes |
                        DSGraph::DontCloneAuxCallNodes);
  for (DSScalarMap::global_iterator
       I = GlobalsGraph->getScalarMap().global_begin();
       I != GlobalsGraph->getScalarMap().global_end(); ++I) {
    if (isa<GlobalVariable>(*I)) {
      RC.getClonedNH(GlobalsGraph->getNodeForValue(*I));
  
      /* if(getHandleForObject(*I).getNode())
        fprintf(stderr, "Added global value\n"); */
    } // add global value
  }
  return true;
}

// Recursively find the create/join call sites in functions
SccCreateJoinCalls *ThreadEscapeAnalysis::findAllFunctionsWithCreateOrJoin(Function *func, 
  std::map<const Function *, SccCreateJoinCalls *>& functionsToCreateJoins)
{
  std::map<const Function *, SccCreateJoinCalls *>::iterator itr = functionsToCreateJoins.find(func);
  if(itr != functionsToCreateJoins.end())
    return itr->second;


  SccCreateJoinCalls *retData = new SccCreateJoinCalls(func);
  functionsToCreateJoins[func] = retData;


  for(Function::iterator bb = func->begin(), E = func->end(); bb != E; ++bb) {
    for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
      Instruction *I = inst;
      if (!(isa<CallInst>(inst) || isa<InvokeInst>(inst))) continue;

      CallSite cs(inst);
      if(cs.getCalledFunction() != NULL) {

        CallInst *callInst = dyn_cast<CallInst>(I);
        InvokeInst *invokeInst = dyn_cast<InvokeInst>(I);

        // gather targets
        std::vector<Function*> callTargets;
        // if (cs.isDirectCall())
        callTargets.push_back(cs.getCalledFunction());
        /* else
          cs.getCalleeNode()->addFullFunctionList(callTargets); */

        for (unsigned c = 0; c != callTargets.size(); ++c) {
          Function *F = callTargets[c];
          if (F == ThreadCreate) {
            fprintf(stderr,"Found thread create call\n");
            retData->_createCalls.insert(callInst);
            retData->createOrJoin.insert(callInst);
          }
          else if(F == ThreadJoin) {
            fprintf(stderr,"Found thread join call\n");
            retData->_joinCalls.insert(callInst);
    	      retData->createOrJoin.insert(callInst);
          }
          else {
    	      SccCreateJoinCalls *d = findAllFunctionsWithCreateOrJoin(F, functionsToCreateJoins);
    	      for(set<CallInst *>::iterator itr = d->_createCalls.begin(); itr != d->_createCalls.end(); itr++) {
	            retData->_createCalls.insert(*itr);
            }
	          for(set<CallInst *>::iterator itr = d->_joinCalls.begin(); itr != d->_joinCalls.end(); itr++) {
	            retData->_joinCalls.insert(*itr);
	          }

	          if(!d->createOrJoin.empty()) 
	            retData->createOrJoin.insert(callInst);
	          }
        } // for each called function at this call site
      } // if called function
    }
  } // for each bb

  return retData;
}


// For non-recursive TSA, we used to do:
//   Do a DFS from main function, and populate threadSections. This is the top level function
//   to find threaded sections and parallel phases.
//
// Feb 7, 2013 (Madan): Recursive/hierarchical/general TSA
//    i) Find TSA in all functions.
//   ii) Recurse into thead start functions, and create edge to other TS if descendant TS found.
//  iii) 

// Input: hierTSA indicates whether to find TSAs that are descendants of other TSA, forming a hiearchy of TSAs.
void ThreadEscapeAnalysis::findAllThreadSections(Module& M, bool hierTSA)
{
  std::set<Function *> visitedFuncs;


  
  if(!hierTSA) {
    // TOP DOWN TSA: ORIGINAL PROPOSAL FOR FINIDING ONLY TOP LEVEL NON-HIERRCHICAL TSAS
    Function *mainFunc = M.getFunction(MAIN_FUNC_NAME);
    if(!mainFunc) {
      std::cerr << "*******************************************************************************\n";
      std::cerr << " ERROR: CAN'T FIND MAIN FUNCTION '" << MAIN_FUNC_NAME << "' NOT FOUND!! CAN'T DO THREAD SECTION ANALYSIS.\n";
      std::cerr << "YOU MUST FIX THIS ERROR!!!!\n";
      std::cerr << "*******************************************************************************\n";
      return;
    }

    // First, check if the create and join calls are distributed over multiple functions. In that case, we need
    // to treat the closure of all those as a TS.
    std::map<const Function *, SccCreateJoinCalls *> functionsToCreateJoins;
    SccCreateJoinCalls *mts = findAllFunctionsWithCreateOrJoin(mainFunc, functionsToCreateJoins);
 
    bool funcWithCreatesOnly = 0, funcWihJoinOnly = 0;
    for(std::map<const Function *, SccCreateJoinCalls *>::iterator itr = functionsToCreateJoins.begin();
	itr != functionsToCreateJoins.end(); itr++) {
      SccCreateJoinCalls *t = itr->second;
      const char *fname = (t->func->getName()).data();
      if(!t->_createCalls.empty() && t->_joinCalls.empty()) {
        funcWihJoinOnly++;
        std::cerr << "Function " << fname << " only has create calls!\n";
      }
      else if(!t->_joinCalls.empty() && t->_createCalls.empty()) {
        funcWithCreatesOnly++;
        std::cerr << "Function " << fname << " only has join calls!\n";
      }
    }


    // Recursively find threaded sections from main function
    findThreadSectionsInFunction2(mainFunc, visitedFuncs, true);
  }
  else {
    // ENHANCED TSA TO HANDLE HIERARCHICAL TSA

    // foreach function, find thread sections non-recursively
    for (Module::iterator F = M.begin(); F != M.end(); ++F) {
      if (!F->isDeclaration()) {
        findThreadSectionsInFunction2(F, visitedFuncs, false);
      }
    }

    // Now, Create edges from one TS to another to creae the DAG of TS sections.
  }

  // Now, do actual marking of DTS in ICFG
  // int numSections = markDisjointThreadSections();
  
  NumBarrieredSections = curDTSId;
  // std::cerr << "TSA: Found " << _threadSections.size() << " thread sections\n";
}

// Create a TS between these two points in root threads
bool ThreadEscapeAnalysis::createTS(SplicedCFG *topCFG, Function *topF, 
      const BasicBlock *nearestStartDominator, BasicBlock *nearestEndPostDom,
      Instruction *startOfTS, Instruction *endOfTS,
      std::vector<Instruction *>& threadStarts, std::vector<Instruction *>& threadJoins)
{
  if(!checkTSClosure(topF, threadStarts, threadJoins))
    return false; 

  Function *f = const_cast<Function *>(topF);
  BasicBlock *startBB = const_cast<BasicBlock *>(nearestStartDominator);
  BasicBlock *endBB = const_cast<BasicBlock *>(nearestEndPostDom);

  SccICFG icfg(f->getParent(), this, doBSA);
  icfg.addFunction(topF, startBB, startOfTS, endBB, endOfTS);
  markDTSInTS(&icfg);

  return true; // create a thread section
}

// Check closure of the looped thread_create and thread_join
bool ThreadEscapeAnalysis::checkTSClosure(Function *topF, 
  std::vector<Instruction *>& threadStarts, std::vector<Instruction *>& threadJoins)
{
  return true; // TBD
  if(threadStarts.size() != 1 && threadJoins.size() != 1)
    return false;

  LoopInfo& LI = getAnalysis<LoopInfo>(*topF);
  ScalarEvolution *SE = &getAnalysis<ScalarEvolution>(*topF);
  DominatorTree *DT = &(getAnalysis<DominatorTreeWrapperPass>(*topF).getDomTree());

  SccLoopHelper loopHelper(DT, SE);

  // Make array of start, end for using same code for both
  Instruction *calls[2] = { threadStarts[0], threadJoins[0] };

  Value *startVals[2] = { NULL, NULL};
  Value *exitVals[2] = { NULL, NULL};
  Value *tidArrays[2] = { NULL, NULL};

  Loop *loops[2] = { NULL, NULL};

  for(int i = 0; i < 2; i++) {
    Loop *loop = LI.getLoopFor(calls[i]->getParent());
    if(!loop) continue;
    loops[i] = loop;

    Value* tidValue = calls[i]->getOperand(0);
    GetElementPtrInst* Gep= dyn_cast<GetElementPtrInst>(tidValue);
    if (!Gep) continue;

    tidArrays[i] =  Gep->getPointerOperand();
    if(!tidArrays[i]) {
      fprintf(stderr, "   TID array not same in create and join loops.\n");
      continue;
    }

    // Check that base address doesn't change
    if(!loop->isLoopInvariant(tidArrays[i])) {
      fprintf(stderr, "   TID array not loop invariant for thread_join/thread_create!\n");
      continue; 
    }

    int numGepOperands = Gep->getNumOperands();
    Value *tidIndex = Gep->getOperand(numGepOperands-1); // get last operand
    /* if(numGepOperands != 2)  {
      fprintf(stderr, "   Skipping LILM for array as it is not single dimensional.\n");
      continue;
    } */

    BasicBlock *latchBlock = loop->getLoopLatch();
    BasicBlock *header = loop->getHeader();
    if(!(latchBlock && header)) {
      std::cerr << "  No latch! Skipping TS\n";
    }
    unsigned TripCount = 0, TripMultiple = 0;
    TripCount = SE->getSmallConstantTripCount(loop, latchBlock);
    TripMultiple = SE->getSmallConstantTripMultiple(loop, latchBlock);

    BranchInst *ExitBr = dyn_cast<BranchInst>(latchBlock->getTerminator());
    assert(ExitBr  && "Terminator of latch not branch instruction!"); 
    if(ExitBr->getNumSuccessors() != 2) {
      fprintf(stderr,"Skipping TS. ExitBr not conditional.\n");
      continue;
    }

    if (ExitBr->getSuccessor(0) != header &&
       ExitBr->getSuccessor(1) != header &&
       ExitBr->getParent() != header) 
    {
      // The simple checks failed, try climbing the unique predecessor chain up to the header.
      bool Ok = false;
      for (BasicBlock *BB = ExitBr->getParent(); BB; ) {
        BasicBlock *Pred = BB->getUniquePredecessor();
        if (!Pred) {
          std::cerr << "  Exit condition too difficult and no pred!\n";
          return false;
        }
        TerminatorInst *PredTerm = Pred->getTerminator();
        for (unsigned i = 0, e = PredTerm->getNumSuccessors(); i != e; ++i) {
          BasicBlock *PredSucc = PredTerm->getSuccessor(i);
          if (PredSucc == BB)
            continue;
          // If the predecessor has a successor that isn't BB and isn't
          // outside the loop, assume the worst.
         if (loop->contains(PredSucc)) {
            std::cerr << "  Exit condition too difficult and no pred!\n";
            return false;
          }
        }
        if (Pred == header) {
          Ok = true;
          break;
        }
        BB = Pred;
      }

      if (!Ok) {
        std::cerr << "  Exit condition too difficult!\n";
        return false;
      }
    }

    // If Icmp instr, get the value
    exitVals[i] = loopHelper.ComputeExitLimitFromCond(loop, ExitBr->getCondition(),
        ExitBr->getSuccessor(0), ExitBr->getSuccessor(1));
    if(!exitVals[i]) {
      fprintf(stderr, "Skipping: No loop invariant exit cond, or too complex exit cond.\n");
      continue;
    }

    // ICmpInst *ExitCondICmp = dyn_cast<ICmpInst>(ExitBr->getCondition());
    // Value *exitVal = ExitCondICmp->getOperand(1);

    // print some data
    int cmpInsts = 0, numInsts = 0;
    for (BasicBlock::iterator inst = latchBlock->begin(); inst != latchBlock->end(); ++inst) {
      numInsts++;
      if (isa<CmpInst>(inst)) cmpInsts++;

      // std::cerr << (*inst) << std::endl;
    }
    fprintf(stderr,"FOUND LATCH BLOCK : cmp insts %d totalInsts %d tripcnt %u tripMul %u\n", 
            cmpInsts, numInsts, TripCount, TripMultiple);


    PHINode *phiNode = loop->getCanonicalInductionVariable();
    if(phiNode) {
      // fprintf(stderr, "FOUND INDUCTION VARIABLE!\n");
    }

    // Get starting value, and also test that array index is loop iv
    startVals[i] = loopHelper.ComputeStart(loop, tidIndex);
    if(!startVals[i]) continue; 

    Type *addrTy = Gep->getPointerOperand()->getType();
    Type *elemType = NULL;
    if(isa<PointerType>(addrTy)) {
      elemType = cast<PointerType>(addrTy)->getElementType();
    }
  } // for both start and end

  if(loops[0] == NULL && loops[1] == NULL)
    return true; // non-looped

  if(!startVals[0] || startVals[0] != startVals[1]) {
    return false;
  }

  if(!exitVals[0] || exitVals[0] != exitVals[1])
    return false;

  if(tidArrays[0] == NULL || tidArrays[0] != tidArrays[1])
    return false;

  return true;
}

// Added by Madan
//
// Consider each threaded section.  Mark functions that are called.
// With phased thread section, first findThreadSection already 
// covers the pthread_create calls that are well formed as thread section.
// So, here we only cover the other pthread_create calls that couldn't
// be covered there, either because they were too complex or because
// that analysis was skipped

void ThreadEscapeAnalysis::identifyMultiThreadedFunctions(Module &M) 
{
  // DataStructures* DS = &getAnalysis<StdLibDataStructures>();

  const DataLayout *const_td =  &getAnalysis<DataLayoutPass>().getDataLayout();
  TD = const_cast<DataLayout *>(const_td);

  DEBUG(dbgs() << "Running identifyMultiThreadedFunctions\n");

  // For topo sorting to estimate function frequency
  _reverseTopoOrder.clear();

  // Find places where we call thread create
  //    call @SCCthread_create(id, attr, startfn, startarg)

  // assert(ThreadCreate);
  std::list<DSCallSite>& functionCalls = ResultGraph->getFunctionCalls();
  DEBUG(dbgs() << "thread create called " << functionCalls.size() << " places\n");
  std::vector<const Function*> callTargets;

  // For marking those parallel phases that are not in identifiable thread sections,
  // we are using a very high parallel phase
  uint32_t parallelPhase = (curDTSId <= 0) ? curDTSId : SccDontMarkTS;
  for (std::list<DSCallSite>::iterator CI = functionCalls.begin(); CI != functionCalls.end(); ) 
  {
    DSCallSite &call = *CI;
    CI++;

    const CallSite cs = call.getCallSite();
    CallInst *callInst = dyn_cast<CallInst>(cs.getInstruction());
    InvokeInst *invokeInst = dyn_cast<InvokeInst>(cs.getInstruction());

    if( callInst && _threadCreateCallsInTSA.find(callInst) != _threadCreateCallsInTSA.end())
      continue; // already covered in TS
    else if(invokeInst && _threadCreateCallsInTSA.find(callInst) != _threadCreateCallsInTSA.end())
      continue;

    // gather targets
    callTargets.clear();
    if (call.isDirectCall())
      callTargets.push_back(call.getCalleeFunc());
    else
      call.getCalleeNode()->addFullFunctionList(callTargets);

    for (unsigned c = 0; c != callTargets.size(); ++c) {
      const Function *F = callTargets[c];
      if (F == ThreadCreate || F == ThreadStart) {

        // add the calling function to thread-reachable also, as it might need
        // instrumentation in the parallel part
        const Function *parentFunc = callInst ? callInst->getParent()->getParent() : 
                                                invokeInst->getParent()->getParent();
        threadReachableFuncs[parentFunc]++;

        dfsThreadCreation(F, call, parallelPhase);
        assert(call.isDirectCall());
      }
    }
  }

#if 0
   // This was for profiling based passes
  // Now, update function frequencies using BlockFrequency analysis by travrsing in topological order
  for(long i = _reverseTopoOrder.size() -1; i >= 0; i--) {
     updateCallFreq(const_cast<Function *> (_reverseTopoOrder[i]));
  }
#endif
}

#if 0
// Update the call frequency of the function. Skip external functions here.
void ThreadEscapeAnalysis::updateCallFreq(Function *F)
{
  if(!F || F->isDeclaration())
    return;

  int myFreq = threadReachableFuncs[F];
  if(myFreq == 0) {
    threadReachableFuncs[F] = 1;
    myFreq = 1;
  }

  Function& func = (*F);
  BlockFrequencyInfo* blkFreqInfo = &getAnalysis<BlockFrequencyInfo>(func);
  /* if(blkFreqInfo) {
    std::string fname = F->getName();
    std::cerr << "GOT BlockFrequencyInfo in thread escape analysis for function " << fname << "\n";
  } */

  for(Function::const_iterator itr1 = F->begin(); itr1 != F->end(); itr1++) 
  {
    const BasicBlock *bb = itr1;
    int blockFreq = 1;
    if(blkFreqInfo) {
      const BlockFrequency& blkf = blkFreqInfo->getBlockFreq(bb);
      blockFreq = blkf.getFrequency();
      // std::cerr << "Got block freq. of " << blockFreq << std::endl;
    }

    for(BasicBlock::const_iterator itr = bb->begin(); itr != bb->end(); itr++) {
       const Instruction *I = itr;
       if(isa<CallInst>(I)) {
          const CallInst* CI = dyn_cast<CallInst>(I);
          if(CI) {
            const Function *CF = CI->getCalledFunction();
            if(CF) {
              DEBUG(dbgs() << "   Found CallInst with funciton " << CF->getName() << '\n');
              threadReachableFuncs[CF] += (blockFreq * myFreq);
            }
          }
      }
    }
  }
}
#endif

// Is this function called in multi-threaded mode? That is, did we do a DFS on this function
// for the current threaded section?
bool ThreadEscapeAnalysis::calledInMultiThreadedMode(const Function *f) const
{
  if(!doTSA) 
    return true; // assume called in mt mode

  if(threadReachableFuncs.size() == 0){
    // if we don't have thread regions, assume true
    return true;
  }
  
  return (threadReachableFuncs.count(f) > 0);
}

// Is it called in current thread section or phase? Assume yes if is not on.
bool ThreadEscapeAnalysis::isCalledFromThreadedSection(const Function *f) {
  if(!doTSA)
    return true;
  return (threadReachableFuncs.count(f) > 0);
}

// Add the dts meta data 
static void addDtsMetaData(const Instruction *I, uint32_t dtsId)
{
  Instruction *inst = const_cast<Instruction *>(I);
  char str[20];
  sprintf(str,"%d", dtsId);

  // fprintf(stderr,"Setting DTS ID %d\n", dtsId);

  LLVMContext& C = inst->getContext();
  /*  TBD: append meta data
  MDNode *oldmdNode = inst->getMetaData("DTS");
  if(oldmdNode) {
    int len;
    const char *oldv = oldmdNode->getStrng();
    if(oldv) {
      strcat(str, ":");
      strcat(str, oldv);
    }
  } */
  MDNode* mdnode = MDNode::get(C, MDString::get(C, str));
  inst->setMetadata("DTS", mdnode );
}


// Added by Madan
// Visit a store inst for TSA and phased mod/ref purpose
void ThreadEscapeAnalysis::visitTSAWriteValue(const Instruction *inst, 
          const Value *v, uint32_t parallelPhase)
{
  if(!(inst && v) ) return;
  // some stats for debug
  if(_dtsStCnts.count(parallelPhase))
      _dtsStCnts[parallelPhase]++;
  else
    _dtsStCnts[parallelPhase] = 1;
  addDtsMetaData(inst, parallelPhase);
  // const Value *v = store->getPointerOperand();
  if(v) {
    DSNodeHandle nodeHandle = getHandleForObject(v);
    if(!nodeHandle.isNull()) {
      DEBUG(dbgs() << "FOUND node handle\n");
    }
  }
  v = v ? getRealUnderlyingObject(v) : NULL;
  if(v) {
   vector<DSNodeHandle> nodes;
   getNodesForObject(v, nodes, inst);
   assert(nodes.size());
    
   for(size_t i = 0; i < nodes.size(); i++) {
    DSNode* N = nodes[i].getNode();

    // record this write access in phases
    addToAllAccesses(N, inst, parallelPhase);

    if (EscapingNodes.count(N) > 0) {
       N->setModifiedMarker();  // TBD: Make it field specific
       N->insertWriteLoc(inst, 1); // TBD: estimate frequency

       // Add this phase to list of phases this node is modified
       _nodeModifiedInPhases[N].insert(parallelPhase);
       _storeInstsInPhases[inst].insert(parallelPhase);

        _dtsAccessedNodes[parallelPhase].insert(N);

       DEBUG(dbgs() << " MARK MODIFIED NODE " << N << " Value " << v << '\n');
    }
   } // for each node
  }
  else
    fprintf(stderr, "CAN'T FIND UNDERLYING OBJECT FOR STORE!\n");
}

// Collect the set of phases ( including TSA ID) where this load instruction appears
void ThreadEscapeAnalysis::visitTSAReadValue(const Instruction *inst, const Value *v, uint32_t parallelPhase)
{

  if(!(inst && v)) return;
  // some stats for debug
  if(_dtsLdCnts.count(parallelPhase))
      _dtsLdCnts[parallelPhase]++;
  else
    _dtsLdCnts[parallelPhase] = 1;
  addDtsMetaData(inst, parallelPhase);
  // const Value *v = load->getPointerOperand();
  v = v ? getRealUnderlyingObject(v) : NULL;
  if(v) {
    vector<DSNodeHandle> nodes;
    getNodesForObject(v, nodes, inst);
    for(size_t i = 0; i < nodes.size(); i++) {
      DSNode *N = nodes[i].getNode();
      addToAllAccesses(N, inst, parallelPhase);
      _loadInstInPhases[inst].insert(parallelPhase); // this is most likely unused

      if (EscapingNodes.count(N) > 0) {
        N->setReadMarker();  // TBD: Make it field specific
        N->insertReadLoc(inst, 1); // TBD: estimate frequency
        _dtsAccessedNodes[parallelPhase].insert(N);
        DEBUG(dbgs() << " ESCAPING LOAD NODE " << N << " Value " << v << '\n' );
      }
      else if(!N) 
        DEBUG(dbgs() << " N is NULL" << '\n');
    } // for each node this variable can point to
  }
  else
    fprintf(stderr, "CAN'T FIND UNDERLYING OBJECT FOR LOAD!\n");
}

void ThreadEscapeAnalysis::visitTSAInstruction(const Instruction *I, 
        std::set<const Function *>& calledFunctions,
        uint32_t parallelPhase)
{
  const Function *func = I->getParent()->getParent();
  if(func && threadReachableFuncs.count(func) <= 0 ) {
    threadReachableFuncs[func] = parallelPhase;
  }
  
  if(const StoreInst *store = dyn_cast<StoreInst>(I)) {
    if(parallelPhase != SccDontMarkTS) visitTSAWriteValue(store, store->getPointerOperand(), parallelPhase);
  }
  else if(const LoadInst *load = dyn_cast<LoadInst>(I)) {
     if(parallelPhase != SccDontMarkTS) visitTSAReadValue(load, load->getPointerOperand(), parallelPhase);
  }
  else if(isa<CallInst>(I) || isa<InvokeInst>(I)) {
    Instruction *inst = const_cast<Instruction *>(I);
    const CallSite cs(inst);
    // const CallInst* CI = dyn_cast<CallInst>(I);
    const Function *CF = cs.getCalledFunction(); // CI->getCalledFunction();
      if(CF && CF->isDeclaration()) {
        string fname = CF->getName();
        if(cs.arg_size() > 0) {
          // Mark function arguments properly
          const char *argStr = argClassifier.getArgTypes(fname.c_str());
          if(argStr) {
            for(int pos = 0; argStr[pos]; pos++) {
              char ty = argStr[pos];
              if(ty == 'r' || ty == 'w' || ty == 'b') {
                const Value *v = cs.getArgument(pos);
                if(ty == 'r' || ty == 'b')
                  visitTSAReadValue(I, v, parallelPhase);
                if(ty == 'w' || ty == 'b')
                  visitTSAWriteValue(I, v, parallelPhase);
              }
              else if(ty != 'n') {
                fprintf(stderr,"SCC-ERROR: Invalid arg classifier %c for function %s\n", argStr[pos], fname.c_str());
              } // switch
            } // for each argument
          }
          else if(strncmp(fname.c_str(),"scc_", 4) != 0 && strncmp(fname.c_str(),"llvm",4) != 0 && 
              strncmp(fname.c_str(),"__tsan_",7) != 0)  {
            if(_flaggedFunctions.count(CF) == 0) {
              fprintf(stderr,"SCC-ERROR: Missing function argument classifier for  %s\n", fname.c_str());
              _flaggedFunctions.insert(CF);
            }
          }
        } // if more than 1 arg
    }
    else if(CF && calledFunctions.count(CF) <= 0) {
        DEBUG(dbgs() << "   Found CallInst with function " << CF->getName() << '\n');
        calledFunctions.insert(CF);
    }
  }
}

// Added by Madan
// Collect all functions that are called by this function
// TBD: Really go into functions calls.
// TBD: Mark task specific fields
// Input start and end: Only cover instructions sandwiched between start and end. 
// Return: Number of phases found when this is top level function for pthread_create
// Assume: Both start and end will never by NON-NULL

void ThreadEscapeAnalysis::doDFSOnFunction(SplicedCFG *spCfg, const Function *F, 
    const Instruction *start,  const Instruction *end, 
    uint32_t parallelPhase,  // a tuple of (ThreadSection, phase) where this belongs.
    std::set<const Function *>& visitedFuncs)
{
  if(!F) return;

  Function *nonconstF = const_cast<Function *>(F);
  if(nonconstF->isDeclaration())
    return;
  // if(!DT) DT = &getAnalysis<DominatorTree>(*nonconstF);
  // if(!PDT) PDT = &getAnalysis<PostDominatorTree>(*nonconstF);

  if(start && start == end) {
    std::cerr << " ERROR: ***** Both start and end cannot be same\n";
  }
  if(visitedFuncs.count(F) > 0)
      return;  // already visited

  visitedFuncs.insert(F);

  SplicedCFG *mySpCfg = (spCfg && spCfg->getFunction() == F) ?
              spCfg : new SplicedCFG(nonconstF);

  threadReachableFuncs[F] = 1;
  DEBUG(dbgs() << " Doing DFS in thread section for function " << F->getName() << '\n');

  std::set<const Function *> calledFunctions;

  if(start && end && start->getParent() == end->getParent()) {
    // only one basic block. visit all instruction from start to end, both inclusive
    const BasicBlock *bb = start->getParent();
    bool foundStart = false;
    for(BasicBlock::const_iterator itr = bb->begin(); itr != bb->end(); itr++) {
      const Instruction *I = itr;
      if(I == start)  {
        foundStart = true;
      }
      if(foundStart) {
        visitTSAInstruction(I, calledFunctions, parallelPhase);
      }
      if(I == end) break;
    }
  }
  else {
    for(Function::const_iterator itr1 = F->begin(); itr1 != F->end(); itr1++)
    {
      const BasicBlock *bb = itr1;
      if(start && start->getParent() == bb) {
        // The phase boundary is within a basic block. Cover the portion after start.
        for(BasicBlock::const_iterator itr = bb->begin(); itr != bb->end(); itr++) {
          const Instruction *I = itr;
          if(I != start && mySpCfg->dominates(start,I)) {
             visitTSAInstruction(I, calledFunctions, parallelPhase);
          }
        }
      }
      else if(end && end->getParent() == bb) {
        // The phase boundary is within a basic block. Cover the portion upto end.
        for(BasicBlock::const_iterator itr = bb->begin(); itr != bb->end(); itr++) {
          const Instruction *I = itr;
          if(I != end && mySpCfg->dominates(I,end)) {
             visitTSAInstruction(I, calledFunctions, parallelPhase);
          }
        }
      }
      else if((!start || mySpCfg->dominates(start,bb->getFirstNonPHI())) && 
                (!end || mySpCfg->postDominates(end, bb->getFirstNonPHI())))
      {
        // Entire block in this phase. Visit entire basic block
        for(BasicBlock::const_iterator itr = bb->begin(); itr != bb->end(); itr++) {
          const Instruction *I = itr;
          visitTSAInstruction(I,calledFunctions, parallelPhase);
        } // for each instruction in BB
      } // if BB is sandwiched
    } // for each basaic block in F
  } // if start and end in different BB

  // Now call other functions that are called from these
  for(std::set<const Function *>::iterator itr = calledFunctions.begin();
         itr != calledFunctions.end(); itr++)
  {
     const Function *F =  const_cast<const Function *>(*itr);
     assert(!F->isDeclaration() && "declaration function pushed to calledFunctions");
     doDFSOnFunction(mySpCfg, F, NULL, NULL, parallelPhase, visitedFuncs);
  }

  if(mySpCfg != spCfg) {
    delete mySpCfg;
    mySpCfg = NULL;
  }

  // When done, push in topoSortAray 
  _reverseTopoOrder.push_back(F);
}

// Get number of phases in this function
// No recursive descending into called functions
// Assume: instruction iterator iterates from beginning of function to end of function
// At present, can only do when single return or no return.
std::vector<Instruction *> ThreadEscapeAnalysis::GetNumPhases(Function& F)
{
  std::vector<Instruction *> scc_sync_thread_calls;
  if(!ThreadSync) return scc_sync_thread_calls;

  BasicBlock& entryBlock = F.getEntryBlock();
  Instruction *firstInstruction = entryBlock.getFirstNonPHI();
  // Instruction *end  = get last instruction in F.

  vector<Instruction *> returnInsts;

  std::string fname = F.getName();
  std::cerr  << "Find phases in " << fname << std::endl;
  bool foundStraySync = false;
  // Assume that the scc_sync_thread calls are arranged in order from entry.
  for(inst_iterator II = inst_begin(F), E = inst_end(F); II != E; ++II) {
    Instruction *I = &*II;
    if(isa<ReturnInst>(I)) {
      returnInsts.push_back(I);
    }
    else if (isa<CallInst>(I) || isa<InvokeInst>(I)) {
      const CallSite cs(I);
      Function *func = cs.getCalledFunction();
      /* fname = func->getName();
      std::cerr  << "   Called function " << fname << std::endl; */
      if (func != ThreadSync) continue;
      if(DT->dominates(I, firstInstruction) &&
        (scc_sync_thread_calls.empty() || DT->dominates(I, scc_sync_thread_calls.back())) )
        {
            scc_sync_thread_calls.push_back(I);
            // fprintf(stderr, "Size of scc_sync_thread_calls is %d\n", scc_sync_thread_calls.size());
        }
        else {
          foundStraySync = true;
        }
    } // for each instruction

    if(foundStraySync) {
      scc_sync_thread_calls.clear();
    }
  } // for each instruction

  // There are two types of these phases : (i) straight line, and (ii) looped.
  // In (i), first barrier post-dominates entry, and last sync dominates return at end.
  // In (ii), that is nto the case, So, merge code before first sync and after last sync to fomr
  // another default phase.
  bool dominatesReturns = true;
  if(scc_sync_thread_calls.size() > 0 && returnInsts.size() > 0) {
    Instruction *lastSyncCall = scc_sync_thread_calls.back();
    BasicBlock *lastSyncBlock = lastSyncCall->getParent();
    for(size_t i = 0; i < returnInsts.size(); i++) {
      if(!DT->dominates(lastSyncBlock, returnInsts[i]->getParent())) {
        dominatesReturns = false;
        break;
      }
    }
  }

  // check if first sync dominates entryBlock
  bool entryDominatesFirst = true;
  if(scc_sync_thread_calls.size() > 0 ) {
    BasicBlock *firstSyncBlk = scc_sync_thread_calls[0]->getParent();
    if((&entryBlock) != firstSyncBlk && !PDT->dominates(firstSyncBlk, &entryBlock)) {
      entryDominatesFirst = false;
    }
  }

  if(dominatesReturns && entryDominatesFirst) {
    // straightline case. do nothing
  }
  else {
    fprintf(stderr,"SCC-WARNING: NOT HANDLING LOOPED PHASES!\n");
    scc_sync_thread_calls.clear();
  }

  return scc_sync_thread_calls;
}


// Merge the actual argument with formal arguments for functions called through
// pthread_create. This is not supported by DSA yet.
void ThreadEscapeAnalysis::ResolveFunctionCall(const Function *F, 
                                               const DSCallSite &call) 
{
  std::string fname = F->getName();
  const CallSite cs = call.getCallSite();

  if(doNonUnifSteensGaard) {

    // Populate formals only if first time 
    Function *nonconstF = const_cast<Function *>(F);
    bool fillUpFormals = (_funcArgs.find(nonconstF) == _funcArgs.end());

    std::set<DSNodeHandle>& fArgs = _funcArgs[nonconstF];
    std::set<CallSiteArgMap *>& formalToActuals = _callSiteArgMaps[nonconstF]; 

    DSNodeHandle n1 = call.getRetVal();
    DSNodeHandle n2 = ResultGraph->getReturnNodeFor(*F);
    if(n2.getNode() && fillUpFormals) 
      fArgs.insert(n2);

    CallSiteArgMap *f2a = new CallSiteArgMap(cs); 
    NumNonUnifiedCallSiteArgMap++;
    formalToActuals.insert(f2a);

    if (n1.getNode() && n2.getNode()) {
      f2a->addMapping(n2, n1); 
      NumNonUnifiedArgMapping++;
      // n1.mergeWith(n2);
    }
  
    // Merge the arguments.
    Function::const_arg_iterator arg = F->arg_begin();
    for (unsigned i = 0; i < cs.arg_size() && i < F->arg_size(); ++i, ++arg) {
      n1 = getHandleForObject(cs.getArgument(i));
      n2 = getHandleForObject(arg);

      if(n2.getNode() && fillUpFormals)
         fArgs.insert(n2); 

      if (n1.getNode() && n2.getNode()) {
        f2a->addMapping(n2, n1);
        NumNonUnifiedArgMapping++;
      }    
    }    

    /* if(NumNonUnifiedArgMapping % 1000 < 7)
      std::cerr << "NumNonUnifiedArgMapping reaches " << NumNonUnifiedArgMapping << "\n"; */

    return;
  } // Non-unification based DSA
  else {   
     // unification based

    // Merge the return values.
    DSNodeHandle n1 = call.getRetVal();
    DSNodeHandle n2 = ResultGraph->getReturnNodeFor(*F);
    if (n1.getNode() && n2.getNode())
      n1.mergeWith(n2);

  
    // Merge the arguments.
    Function::const_arg_iterator arg = F->arg_begin();
    for (unsigned i = 0; i < cs.arg_size() && i < F->arg_size(); ++i, ++arg) {
      n1 = getHandleForObject(cs.getArgument(i));
      n2 = getHandleForObject(arg);
      if (n1.getNode() && n2.getNode()) {
        n1.mergeWith(n2);
  
        n2 = getHandleForObject(arg);
        /* std::cerr << "New node for argument:" << std::endl;
        n2.getNode()->dump(); */
      }
    }
  }
}

// Merge the argument within the function called in pthread_create, or thread)start for stamp benchmarks.
// Probably need a generic mechanism to define this.
void ThreadEscapeAnalysis::ResolveThreadCreation(const Function *ThreadCreateOrStart, const DSCallSite &call) 
{
  // Calling pthread_create().
  const CallSite cs = call.getCallSite();
  int fnPos = 2;
  int argPos = 3;
  if(ThreadCreateOrStart == ThreadStart) {
    // for stamp benchmarks
    fnPos = 0;
    argPos = 1;
  }

  vector<DSNodeHandle> nodes;
  getNodesForObject(cs.getArgument(fnPos), nodes, cs.getInstruction());
  assert(nodes.size());
  const DSNode* startfn = nodes[0].getNode();
  DSNodeHandle  startarg = getHandleForObject(cs.getArgument(argPos));

  assert(startfn);
  if (startarg.isNull())
    return;

  // Simulate all potential calls to 'startfn'.
  std::vector<const Function*> callTargets;
  startfn->addFullFunctionList(callTargets);

  DEBUG(dbgs() << "RESOLVING THREAD CREATE CALL:\n");
  for (size_t i = 0; i < callTargets.size(); ++i) {
    const Function* F = callTargets[i];
    std::string fname = F->getName();
    if (F->arg_empty())
      continue;
    DSNodeHandle arg = getHandleForObject(F->arg_begin());
    if (!arg.isNull()) {
      DEBUG(dbgs() << "THREAD START ARG IN THREAD FUNC:\n");
      // arg.getNode()->print(dbgs(), ResultGraph);
      DEBUG(dbgs() << "TOP LEVEL NODE FOR THREAD ARG:\n");
      // startarg.getNode()->print(dbgs(), ResultGraph);
      startarg.mergeWith(arg);
    } else {
      DEBUG(dbgs() << "NO NODE FOR: " << (*F->arg_begin()) << "\n");
    }
  }

  // Remember which functions are used to spawn threads.
  ThreadStartFunctions.insert(ThreadStartFunctions.end(),
                              callTargets.begin(), callTargets.end());
}

// Do context sensitive mod/ref tracking starting from thread create functions
// This is called only for those calls that are not in any identified thread section.
void ThreadEscapeAnalysis::dfsThreadCreation(const Function *ThreadCreateOrStart, 
    const DSCallSite &call, uint32_t parallelPhase) 
{
  // Calling pthread_create().
  const CallSite cs = call.getCallSite();
  int fnPos = (ThreadCreateOrStart == ThreadStart) ? 0 : 2;
  int argPos = (ThreadCreateOrStart == ThreadStart) ? 1 : 3;

  vector<DSNodeHandle> nodes;
  getNodesForObject(cs.getArgument(fnPos), nodes, cs.getInstruction());
  assert(nodes.size() == 1);
  const DSNode* startfn = nodes[0].getNode();
  assert(startfn);

  DSNodeHandle startarg = getHandleForObject(cs.getArgument(argPos));

  // Simulate all potential calls to 'startfn'.
  std::vector<const Function*> callTargets;
  startfn->addFullFunctionList(callTargets);

  std::set<const Function *> visitedFuncs; // to avoid recursions in function call tree
  for (size_t i = 0; i < callTargets.size(); ++i) {
    const Function* F = callTargets[i];
    DEBUG(dbgs() << "Track THREAD START FUNCTION: " << F->getName() << "\n");
    if (F->arg_empty()) {
      doDFSOnFunction(NULL, F, NULL, NULL, parallelPhase, visitedFuncs);
    }
    else {
      DSNodeHandle arg = getHandleForObject(F->arg_begin());
      if (!arg.isNull()) {
        DEBUG(dbgs() << "THREAD START ARG:\n");
        // arg.getNode()->print(dbgs(), ResultGraph);
        doDFSOnFunction(NULL, F, NULL, NULL, parallelPhase, visitedFuncs);
      // startarg.mergeWith(arg);
      } else {
        DEBUG(dbgs() << "NO NODE FOR: " << (*F->arg_begin()) << "\n");
      }
    }
  }

  // Remember which functions are used to spawn threads.
  ThreadStartFunctions.insert(ThreadStartFunctions.end(),
                              callTargets.begin(), callTargets.end());
}

//
// Escape
//

void ThreadEscapeAnalysis::doEscapeAnalysis(Module &M) 
{
  //
  // Build the set of all DSNodes that escape.
  //
  EscapingNodes.clear();
  std::vector<const Function*> callTargets;

  // DataStructures* DS = &getAnalysis<StdLibDataStructures>();

  //
  // -- 1 --
  // All nodes reachable from globals are escaping.
  //

  DEBUG(dbgs() << "[ThreadEscapeAnalysis] Marking Globals ...\n");

  for (DSGraph::node_const_iterator
       N = ResultGraph->node_begin(); N != ResultGraph->node_end(); ++N) {
    if (N->isGlobalNode()) {
      PRINTNODE
      // N->print(dbgs(), ResultGraph);
      N->markReachableNodes(EscapingNodes);
    }
  }

  //
  // -- 2 --
  // All nodes reachable from a ThreadStart function are escaping.
  //

  DEBUG(dbgs() << "[ThreadEscapeAnalysis] Marking ThreadStarts ...\n");

  for (size_t i = 0; i < ThreadStartFunctions.size(); ++i) {
    const Function* F = ThreadStartFunctions[i];
    if (!F->arg_empty()) {
      DSNodeHandle arg = getHandleForObject(F->arg_begin());
      if (!arg.isNull())
        arg.getNode()->markReachableNodes(EscapingNodes);
    }

#if 0
    // Madan: check if variable was modified by any thread function
    DSGraph *funcGraph = DS->getOrCreateGraph(F);
    if(funcGraph) {
      std::cerr << "Function " << F->getName() << " has DSgraph\n";
      DSGraph::ScalarMapTy &scalarMap = funcGraph->getScalarMap();
      std::cerr << " Global set size " << scalarMap.global_size() << " value map size " <<
              scalarMap.valuemap_size() << " Nodes " << funcGraph->node_size() << std::endl;
      for(DSGraph::node_iterator itr = funcGraph->node_begin(); itr != funcGraph->node_end(); itr++) {
        DSNode& n = *itr;
        std::cerr << "  value map node is rd: " << n.isReadNode() << "wr: " << n.isModifiedNode() << std::endl;
      }
    }
#endif
  }

  //
  // -- 3 --
  // All nodes reachable from the arguments or return values of external
  // functions are escaping.  NOTE: by this point we should have handled
  // all thread creation calls.
  //

  DEBUG(dbgs() << "[ThreadEscapeAnalysis] Marking External Calls ...\n");
  const std::list<DSCallSite>& Calls = ResultGraph->getFunctionCalls();

  for (std::list<DSCallSite>::const_iterator
      CI = Calls.begin(); CI != Calls.end(); ++CI) {
    const DSCallSite &call = *CI;

    callTargets.clear();
    if (!callMayBeExternal(call, callTargets))
      continue;

    // debug
    if (call.isDirectCall()) {
      DEBUG(dbgs() << "[ThreadEscapeAnalysis] External Call: "
           << call.getCalleeFunc()->getName() << "\n");
    } else {
      DEBUG(dbgs() << "[ThreadEscapeAnalysis] External Call (indirect)\n");
      // call.getCalleeNode()->print(dbgs(), ResultGraph);
    }

    // The args are escaping.
    for (unsigned i = 0; i < call.getNumPtrArgs(); ++i) {
      const DSNode* N = call.getPtrArg(i).getNode();
      if (N != NULL) {  // TODO: also use arg->hasNoCaptureAttr()?
        // N->print(dbgs(), ResultGraph);
        PRINTNODE
        N->markReachableNodes(EscapingNodes);
      }
    }
  }

  DEBUG(dbgs() << "[ThreadEscapeAnalysis] " << EscapingNodes.size()
       << " Escaping Nodes, " << (ResultGraph->getGraphSize() - EscapingNodes.size())
       << " Nonescaping Nodes\n");
}

//------------------------------------------------------------------------------
// Escape Queries
//------------------------------------------------------------------------------

// Is this stack-only node? Return false for conservative answer.
bool ThreadEscapeAnalysis::objectIsStackOnly(const Value* object, Instruction *inst) const
{
    // Check the DSGraph.
    if (isa<ConstantPointerNull>(object))
       return false;

    // The object might escape if it's unknown, incomplete, or global.
  vector<DSNodeHandle> nodes;
  if(!getNodesForObject(object, nodes, inst)) {
      DEBUG(dbgs() << "[Stack Access ] Value is not stack-only (not in DSGraph):\n" << (*object));
      return false;
  }

  bool stackOnly = true;
  for(size_t i = 0; stackOnly && i < nodes.size(); i++) {
    DSNode* N = nodes[i].getNode();
    if(!N) return false;
    // assert(N);

    if (N->isUnknownNode() || N->isIncompleteNode() || N->isExternalNode() 
      || N->isMTROMHeapNode() || N->isMTRWHeapNode()) 
    {
      DEBUG(dbgs() << "[Stack Access ] Value is unknown or incomplete or heap:\n" << (*object));
      // N->print(dbgs(), ResultGraph);
      PRINTNODE
      stackOnly = false;
    }

    if(N->isAllocaNode() && (!N->isGlobalNode())) {
      DEBUG(dbgs() << "[Stack Access ] Value is global or stack allocated. Skip tracking:\n" << (*object));
      // N->print(dbgs(), ResultGraph);
      PRINTNODE
      continue;
    }
    if(!(N->isGlobalNode() || N->isUnknownNode())) {
      continue;
    }

    DEBUG(dbgs() << "[Stack Access] Conservatively assume value is not stack only :\n" << (*object));
    // N->print(dbgs(), ResultGraph);
    PRINTNODE
    stackOnly = false;
  }

  return stackOnly;
}

// used to avoid infinite loops in PDT. Not sure why it happens. infinite loop in code might trigger this.
static bool isBBPresent(std::vector<BasicBlock *>& bbs, BasicBlock *bb)
{
  for(size_t i = 0, ie = bbs.size(); i < ie; i++) {
    if(bbs[i] == bb) return true;
  }

  return false;
}

// If an instruction is replaced with another, record it so when we refer to old instruction,
// we can reachits substitution instead
void ThreadEscapeAnalysis::recordReplacement(Instruction *origInst, Instruction *newInst) 
{
  replacements[origInst] = newInst;

  /* MDNode *oldmdNode = origInst->getMetadata("CRIT");
  if(oldmdNode) {
    const char *mdVal = "true";
    LLVMContext& C = origInst->getContext();
    MDNode* mdnode = MDNode::get(C, MDString::get(C, mdVal));
    newInst->setMetadata("CRIT", mdnode );

  }
  */
}

// Check if the memory node for both lock and unlock values are the same
// TBD: Extedn this to return (node, offset) and record that, and compare. Not just compare node.
NodeOffsetPair ThreadEscapeAnalysis::checkSameNodeForValues(const Value *value1, const Value *value2)
{
  if(!(value1 && value2))
    return NodeOffsetPair();

  const Value *uv1 = value1; // getRealUnderlyingObject(value1);
  const Value *uv2 = value2; // getRealUnderlyingObject(value2);
  if(!(uv1 && uv2))
    return NodeOffsetPair(); // empty

  vector<DSNodeHandle> nodes1, nodes2;
  getNodesForObject(uv1, nodes1, NULL);
  getNodesForObject(uv2, nodes2, NULL);

  if(nodes1.size() != 1 || nodes2.size() != 1)
    return NodeOffsetPair();

  unsigned offset1 = nodes1[0].getOffset();
  unsigned offset2 = nodes2[0].getOffset();
  DSNode* Node1 = nodes1[0].getNode();
  DSNode* Node2 = nodes2[0].getNode();
#if 0
  // FIXME: Renau the current offset check is not correct. It should be
  
      if (offset1 != offset2) {
        if (offset2 < offset1) {    // Ensure that O1 <= O2
          std::swap(V1, V2);
          std::swap(offset1, offset2);
          std::swap(V1Size, V2Size);
        }

        if (offset1+V1Size <= offset2)
          return NoAlias;
      }
#endif
  if (Node1 && Node2 && Node1 == Node2 && offset1 == offset2)
    return NodeOffsetPair(Node1, offset1);

  return NodeOffsetPair();
}

// Put the instruction in has table of protected instances
bool ThreadEscapeAnalysis::markInstProtected(Instruction *inst, Value *mutexLockValue)
{
  if(!(inst && mutexLockValue))
    return false;

//  if (isa<LoadInst>(inst) || isa<StoreInst>(inst)) {
    _mutexProtectInstrs[inst] = mutexLockValue;

    // Also attach protected metadata
    const char *mdVal = "true"; 
    LLVMContext& C = inst->getContext();
    MDNode* mdnode = MDNode::get(C, MDString::get(C, mdVal));
    inst->setMetadata("CRIT", mdnode );

    return true;
//  }
}


// Assume that every call to this func are protected by mutex. A hack for stamp TM_BEGIN/TM_END that sits outside function calls.
//TBD: Check that every call is indeed protected by same mutex_lock/mutex_unlock
void ThreadEscapeAnalysis::protectAllInsts(Function *F, set<Function *>& fullyProtected, 
    Value *mutexLockValue, int& lockedInstrCnt)
{
  if(!F || fullyProtected.count(F)) return;
  fullyProtected.insert(F);

  for(Function::iterator itr = F->begin(); itr != F->end(); itr++) {
      BasicBlock *bb = itr;
      for(BasicBlock::iterator inst = bb->begin(); inst != bb->end(); inst++) {
        // Instruction *I = inst;
        if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
          const CallSite cs(inst);
          Function *cf = cs.getCalledFunction();
          if(cf && !cf->isDeclaration()) {
            protectAllInsts(cf, fullyProtected, mutexLockValue, lockedInstrCnt);
          }
          else if(markInstProtected(inst,mutexLockValue)) {
            lockedInstrCnt++;
          }
        }
        else if(markInstProtected(inst,mutexLockValue)) {
          lockedInstrCnt++;
        }
      } // for each instruction
  }
}

// Record all instructions sandwiched between mutex_lock adn mutex_unlock
void ThreadEscapeAnalysis::findMutexProtectedInstructions(Module& M)
{
  if(!(SCCMutexLockFunc && SCCMutexUnlockFunc)) {
    fprintf(stderr,"SCC-WARNING: Skipping detecttion of mutex-protected access as lock/unlock function not found\n");
    return ;
  }

  set<Function *> fullyProtected; // if lock/unlock found outside

  for (Module::iterator F = M.begin(); F != M.end(); ++F) {
    std::string fname = F->getName();
    if(fname == "TMfreeList") {
      std::cerr  << "Processing " << fname << "\n";
    }
    if (F->isDeclaration()) continue;
    if(fullyProtected.count(F)) continue;

    int lockCnt = 0, unlockCnt = 0, lockedInstrcnt = 0;

    for(Function::iterator itr = F->begin(); itr != F->end(); itr++) {
      BasicBlock *bb = itr;
      Value *mutexLockValue = NULL;
      for(BasicBlock::iterator inst = bb->begin(); inst != bb->end(); inst++) {
        Instruction *I = inst;
        if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
          const CallSite cs(inst);
          Function *cf = cs.getCalledFunction();
          if(cf == SCCMutexLockFunc) {
            mutexLockValue = const_cast<Value *>(getRealUnderlyingObject(cs.getArgument(0)));
            DEBUG(dbgs() << "[SCC] mutexlockvalue:" << (*mutexLockValue) << "\n");
            lockCnt++;
          }
          else if(cf ==SCCMutexUnlockFunc) {
            mutexLockValue = NULL;
            unlockCnt++;
          }
          else if(mutexLockValue) {
            // _mutexProtectInstrs[I] = mutexLockValue;
            if(cf && !cf->isDeclaration())
               protectAllInsts(cf, fullyProtected, mutexLockValue, lockedInstrcnt);
            else if(markInstProtected(inst, mutexLockValue)) {
              lockedInstrcnt++;
            }
          }
        } // call inst
        else if(markInstProtected(I, mutexLockValue)) {
          lockedInstrcnt++;
        }
      } // for each instr in block

      // If the lock is not unlocked, traverse forward. Assume some unlock will be obtained.
      // Conservatively, release lock at  the first unlock instruction
      if(mutexLockValue) {
        // traverse forward till some unlock is found. Assume post-dominating unlock for now
        queue<BasicBlock *> Q;
        set<BasicBlock *> visited;

        const TerminatorInst *BBTerm = bb->getTerminator();
        if(!BBTerm) continue;
        for (unsigned i = 0, e = BBTerm->getNumSuccessors(); i < e; ++i) {
          BasicBlock *succ = BBTerm->getSuccessor(i);
          if(visited.count(succ) == 0)
            Q.push(succ);
        } // add to-from edge

        while(!Q.empty()) {
          BasicBlock *bb1 = Q.front();
          Q.pop();
          if(visited.count(bb1) > 0) continue;
          visited.insert(bb1);

          bool foundUnlock = false;
          for(BasicBlock::iterator inst = bb1->begin(); inst != bb1->end(); inst++ ) {
            Instruction *I = inst;
            if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
              CallSite cs(inst);
              Function *cf = cs.getCalledFunction();
              if(cf ==SCCMutexUnlockFunc) {
                foundUnlock = true;
                unlockCnt++;
                break;
              }
              else {
                if(cf && !cf->isDeclaration()) {
                  protectAllInsts(cf, fullyProtected, mutexLockValue, lockedInstrcnt);
                }
                else if(markInstProtected(inst, mutexLockValue)) {
                  lockedInstrcnt++;
                }
              }
                
            }
            if(markInstProtected(I, mutexLockValue)) {
              lockedInstrcnt++;
            }
          } // for each instruction

          if(!foundUnlock) {
            // Keep traversing
            const TerminatorInst *BBTerm = bb1->getTerminator();
            if(!BBTerm) continue;
            for (unsigned i = 0, e = BBTerm->getNumSuccessors(); i < e; ++i) {
              BasicBlock *succ = BBTerm->getSuccessor(i);
              if(visited.count(succ) == 0)
                Q.push(succ);
            } // add to-from edge
          }
        } // while Q is not empty
      } // if bb is not unlocked

    } // for each BB


    if(lockCnt)
      fprintf(stderr,"Function %s: lockCnt %d unlockCnt %d lockedInstrcnt %d\n",
            fname.c_str(), lockCnt, unlockCnt, lockedInstrcnt);
    
  } // for each function

}

// Return the mutex var if the instruction is sandwiched between mutex_lock and mutex_unlock of this variable
// First check within same BB. If not, go up dominator tree for lock, and down post-dominator tree
// for unlock. Check for the first lock and unlock in those BBs.
// TBD: This function is not robust enough when multiple locks are mixed up. But it answers conservatively
const Value *ThreadEscapeAnalysis::mutexProtected(Instruction *instr)
{
  if(!instr) 
    return NULL; // NodeOffsetPair();
#if 0
  if(!(SCCMutexLockFunc && SCCMutexUnlockFunc))
    return NULL; // NodeOffsetPair();

  BasicBlock *bb = instr->getParent();
  
  // Do a simple check in same BB only
  const Value *mutexLockValue = NULL;
  const Value *mutexUnlockValue = NULL; 
  Instruction *mutexLockInstr = NULL, *mutexUnlockInstr = NULL;


  // We need to find a lock before the instruction, without nay unlock
  // Similarly, an unlock of the same var after the ld/st

  // First find instruction position
  int instructionPos = 0; 
  int pos = 0;
  for(BasicBlock::iterator inst = bb->begin(); inst != bb->end(); inst++, pos++) {
    Instruction *I = inst;
    if( I == instr) {
      instructionPos = pos;
      break;
    }
  }

  // Then, find the first unlock after instruction pos. Also, an unlock before instruciton
  // invalidates the prior acquired lock
  int mutexLockPos = INT_MAX , mutexUnlockPos = INT_MIN;
  int unlockedBeforeInstr = false;
  pos = 0;
  for(BasicBlock::iterator inst = bb->begin(); inst != bb->end(); inst++, pos++) {
    Instruction *I = inst;
    if( I == instr) {
      continue;
    }
    if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
        const CallSite cs(inst);
        if (!cs.getCalledFunction()) continue;
        Function* F = cs.getCalledFunction();
        // std::string fname  = F->getName();

        if(F == SCCMutexLockFunc) {
            if(pos < instructionPos) {
              // ignore any lock after instruction!
              mutexLockValue = getRealUnderlyingObject(cs.getArgument(0));
              mutexLockPos = pos;
              mutexLockInstr = inst;
            }
        }
        else if(F == SCCMutexUnlockFunc) {
          if(pos < instructionPos) {
              unlockedBeforeInstr = true;
              if(pos > mutexLockPos) {
                // invalidates prior lock
                mutexLockValue = NULL;
                mutexLockPos = INT_MAX;
                mutexLockInstr = NULL;
              }
          }
          else if(pos > instructionPos && pos > mutexUnlockPos) {
              mutexUnlockValue = getRealUnderlyingObject(cs.getArgument(0));
              mutexUnlockPos = pos;
              mutexUnlockInstr = inst;
              break; // no point inspecting instruction after ld/st if we already found an unlock
          } // unlock func
        } // if called function
    }
  } // for each instr in block

  // check mutex lock and unlock var
  // The values also needs to be same
  // TBD: Also should check that there is no lock->unlock->var access etc. Ignoring this check for now
  bool foundLock   = (mutexLockInstr   && mutexLockPos < instructionPos);
  bool foundUnlock = (mutexUnlockInstr && mutexUnlockPos > instructionPos);
  
  if(foundLock && foundUnlock) {
#if 0
    NodeOffsetPair nodeNOffset = checkSameNodeForValues(mutexLockValue, mutexUnlockValue);
    if(!nodeNOffset.isNull()) {
      fprintf(stderr, "*** FOUND MUTEX PROTECTED ACCESS IN SAME BASICBLOCK\n");
      return nodeNOffset;
    }
#else
      if(mutexLockValue == mutexUnlockValue) {
        const GlobalVariable *GV = dyn_cast<GlobalVariable>(mutexLockValue);
        fprintf(stderr, "*** FOUND MUTEX PROTECTED ACCESS IN SAME BASICBLOCK. GlobalVar : %s\n",
                  GV ? "Yes" : "No");
        return mutexLockValue;
      }
      else 
        return NULL;
#endif
  }

  // Else, try dominator tree for lock, and post-dominator for unlock
  // if this BB already had an unlock before the ld/st, then any lock in dominator doesn't help
  if(!foundLock && DT && !unlockedBeforeInstr) {
    BasicBlock *dom = bb;
    vector<BasicBlock *> visitedBBs;
    do {
      visitedBBs.push_back(dom);
      pred_iterator PI = pred_begin(dom), E = pred_end(dom);
      if (PI == E) break;
      BasicBlock *newDom = *PI;
      ++PI;
      for ( ; newDom && PI != E; ++PI) {
        newDom = DT->findNearestCommonDominator(newDom, *PI);
      }

      // can we have same bb as dominator?
      if(isBBPresent(visitedBBs, newDom)) {
        break; // TBD: why do we have this kind? infinite loop without break:
      }
      else {
        dom = newDom;
      }

      int lockPos = 0, unlockPos = 0;
      if(dom && (mutexLockInstr = findCallInstInBB(SCCMutexLockFunc, dom, lockPos))) {
        // Also ensire that there is no lock in the same bb after lock!
        Instruction *unlockInstr = findCallInstInBB(SCCMutexUnlockFunc, dom, unlockPos);
        if(unlockInstr == NULL || unlockPos < lockPos) {
          const CallSite cs(mutexLockInstr);
          mutexLockValue = cs.getArgument(0);
          foundLock = true;
        }
        else {
          mutexLockInstr = NULL; // reset
        }
      } // mutex lock instruction

    } while(dom && !foundLock);
  } // find locking instruction

  // Find some post-dominator that has a mutex_unlock
  if(!foundUnlock && PDT) {
    BasicBlock *postDom = bb;
    vector<BasicBlock *> visitedBBs;
    do {
      visitedBBs.push_back(postDom);
      TerminatorInst *BBTerm = postDom->getTerminator();
      if(!BBTerm) break;
      unsigned numSucc = BBTerm->getNumSuccessors(); 
      if(numSucc == 0) break;

      // Else, find common dominator
      BasicBlock *newPostDom = BBTerm->getSuccessor(0);
      for (unsigned i = 1, e = BBTerm->getNumSuccessors(); i < e && newPostDom; ++i) {
        BasicBlock *succ = BBTerm->getSuccessor(i);
        newPostDom = PDT->findNearestCommonDominator(newPostDom, succ);
      }

      if(isBBPresent(visitedBBs, newPostDom)) {
        break; // TBD: why do we have this kind? infinite loop without break:
      }
      else
        postDom = newPostDom;

      int unlockPos;
      if(postDom && (mutexUnlockInstr = findCallInstInBB(SCCMutexUnlockFunc, postDom, unlockPos))) {
        const CallSite cs(mutexUnlockInstr);
        mutexUnlockValue = cs.getArgument(0);
        foundUnlock = true;
      }

    } while(postDom && !foundUnlock);  // go down post-dominator tree

  } // Try to find post-dominating unlock instruction

  if(foundLock && foundUnlock) {
#if 0
    NodeOffsetPair nodeNOffset = checkSameNodeForValues(mutexLockValue,  mutexUnlockValue);
    if(!nodeNOffset.isNull()) {
      fprintf(stderr, "*** FOUND MUTEX PROTECTED ACCESS THROUGH DOM & POST-DOM\n");
      return nodeNOffset;
    }
#else
    if(mutexLockValue == mutexUnlockValue) {
      fprintf(stderr, "*** FOUND MUTEX PROTECTED ACCESS THROUGH DOM & POST-DOM\n");
      return mutexLockValue;
    }
    else
      return NULL;
#endif
  }

  return NULL; // NodeOffsetPair();
#else
  // Find origInst
  Instruction *origInst = instr;
  DenseMap<Instruction*, Instruction*>::iterator itr = replacements.begin();
  for(; itr != replacements.end(); itr++) {
    if(itr->second == instr) {
      origInst = itr->first;
      break;
    }
  }
  if(_mutexProtectInstrs.find(origInst) != _mutexProtectInstrs.end())
    return _mutexProtectInstrs[origInst];
  return NULL;
#endif
}

// Does this write in critical region only?
bool ThreadEscapeAnalysis::accessedInCriticalRegionOnly(const DSNode *N, uint32_t phaseId, const Instruction *loadStoreInst)
{
  if(!N)
    return false; // can't tell anything

  // Check cached values. If not found, compute new value
  bool computedNow = false;
  if(_protectedWritePhases.find(N) == _protectedWritePhases.end()) {
    computedNow = true;
    std::cerr   << "[SCC] Checking mutex protection for node with NodeId " << N->getNodeId()  << ":...\n";

    // Else, we need to compute the proetcted phases by inspecting all phases
    map<const DSNode *, map<uint32_t, vector<Instruction *> > >::iterator itr;
    itr = _allAccessesInPhases.find(N);
    // for(; itr != _allAccessesInPhases.end(); itr++) {}
    if(itr != _allAccessesInPhases.end()) {
      map<uint32_t, vector<Instruction *> >& InstrsMap = itr->second;
      map<uint32_t, vector<Instruction *> >::iterator itr2 = InstrsMap.begin();
      for(; itr2 != InstrsMap.end(); itr2++) {
        // 
        uint32_t pid = itr2->first;
        vector<Instruction *>& instrs = itr2->second;
        bool protectedOnly = true, foundInst = false;
        const Value *mutexVal = NULL; // NodeOffsetPair mutexNode;
        for(size_t i = 0; i < instrs.size(); i++) {
          Instruction *I = instrs[i];
//#if 0
          // RRENAU: FIXME I AM NOT SURE ABOUT THIS
          if(replacements.count(I)) {
            instrs[i] = I = replacements[I];
          }
//#endif

          if(pid == phaseId && loadStoreInst == I)
            foundInst = true;

          const Value *thisMutexVal = mutexProtected(I);
          if (thisMutexVal) {
            DEBUG(dbgs()    << "[SCC] mutexProtected called : NodeId " << N->getNodeId() << " two mutex:\n" << 
                "thisMutex:"    << *thisMutexVal  << "\n" <<
                "mutex    :"    << *mutexVal      << "\n");
          }
          if(thisMutexVal && mutexVal == NULL) {
            mutexVal = thisMutexVal;
          }
          else if(!thisMutexVal || thisMutexVal != mutexVal)  {
            protectedOnly = false;
            DEBUG(dbgs()    << "[SCC] not protected : NodeId " << N->getNodeId() << " two mutex:\n" << 
                "thisMutex:"    << *thisMutexVal  << "\n" <<
                "mutex    :"    << *mutexVal      << "\n");

  
            #if 0
            Function *F = I->getParent()->getParent();
            std::string fname = F->getName();

            std::cerr << "[SCC] not protected : NodeId " << N->getNodeId()  << 
                (thisMutexVal ? " different mutex" : " no mutex" ) << " in " << fname << "\n";

            for(size_t j = 0; j < instrs.size(); j++) {
              Instruction *ii = instrs[j];
              Function *f = ii->getParent()->getParent();
              std::string ss = f->getName();
              std:;cerr << ss << ":";
              ii->dump();
            }
            #endif

            break;
          }
        } // for all instrs in this phase only

        if(protectedOnly) {
          NumCriticalRegionAccessNode++;
          _protectedWritePhases[N].push_back(pid); // record for future use

           DEBUG(dbgs() <<  "NodeId " <<  N->getNodeId() << " protected in phase " << pid << 
                            " with " << instrs.size() << " nstructions\n");
          /* if(phaseId == pid && !foundInst) {
            fprintf(stderr,"Error: NodeId %d protected in phase %d , but instruction it protects wasn't considered!\n", 
                N->getNodeId(), pid);
          }  */
        }  // if protected

      }  // for all phases where this node is accessed
    } // if found associated instructionf for this DSNode
  } // if no data present, update it first

  vector<uint32_t>& prPhases = _protectedWritePhases[N];
  for(size_t i = 0; i < prPhases.size(); i++) {
    if(prPhases[i] == phaseId) {
      DEBUG(dbgs()    << "[SCC] protected returning TRUE from accessedInCriticalRegionOnly for nodeId " << N->getNodeId() << 
          " pid " << phaseId << "\n");
      return true;
    }
  }

  return false;
}

// Check uf the fields of ld and st access in same array of structs overlap.
// Only do this for array accesses, whose indices are induction vars
bool ThreadEscapeAnalysis::fieldsOverlap(const LoadInst *ldInst, const StoreInst *stInst)
{
  if(!doFieldSensitiveModRef) 
    return true;

  if(!(ldInst && stInst))
    return true;

  // const DataLayout &td = getAnalysis<DataLayout>();
  // LLVMContext& context = ldInst->getContext();

  const GetElementPtrInst *geps[2] = {NULL, NULL};
  const Value *ldValue = ldInst->getPointerOperand();
  const Value *stValue = stInst->getPointerOperand();

  geps[0] = dyn_cast<GetElementPtrInst>(stValue);
  geps[1] = dyn_cast<GetElementPtrInst>(ldValue);

  if (!(geps[0] && geps[1]))
    return true; // conservative answer

  // Only do this for multi-dimensional array
  if(geps[0]->getNumOperands() != geps[1]->getNumOperands() || geps[0]->getNumOperands() < 2)
    return true;

  const Value *pv0 = geps[0]->getPointerOperand();
  const Value *pv1 = geps[1]->getPointerOperand();
  if(!(pv0 && pv1) || pv0->getType() != pv1->getType())
    return true; 

  gep_type_iterator GTI0 = gep_type_begin(geps[0]);
  gep_type_iterator GTI1 = gep_type_begin(geps[1]);
  uint64_t Offset0 = 0, Offset1 = 0;

  // bool allStructFields = true;

  // 0-th operand is pointer, so skip that
  uint64_t Size0 = 00, Size1 = 0;
  for (unsigned i = 1; i < geps[0]->getNumOperands(); ++i, ++GTI0, ++GTI1) {
    ConstantInt *C0 = dyn_cast<ConstantInt>(geps[0]->getOperand(i));
    ConstantInt *C1 = dyn_cast<ConstantInt>(geps[1]->getOperand(i));

    /* assert(C0 && "assumed constant GEP indices!");
    assert(C1 && "assumed constant GEP indices!"); */

    // Structs: add the field offset.
    StructType *Ty0 = dyn_cast<StructType>(*GTI0);
    StructType *Ty1 = dyn_cast<StructType>(*GTI1);

    if(Ty0 == NULL && Ty1 == NULL) {
      Size0 = TD->getTypeAllocSize(GTI0.getIndexedType());
      Size1 = TD->getTypeAllocSize(GTI1.getIndexedType());
    }


    if(!(C0 || C1))  {
      if((!Ty0 || Ty0 != Ty1) && Size0 != Size1)
        return true; 
      else
        continue; // assume same offset if same types
    }
    else if(!(C0 && C1))
      return true; // one of these is NULL

   /*  if (C0->isZero() && C1->isZero())
      continue; */

    if(Ty0 && Ty1) {
      Offset0 += TD->getStructLayout(Ty0)->getElementOffset(C0->getZExtValue());
      Offset1 += TD->getStructLayout(Ty1)->getElementOffset(C1->getZExtValue());
    } else {
       // Arrays: multiply by the element size.

      fprintf(stderr, "Size0 %lu Size1 is %lu\n", Size0, Size1);
       
      Offset0 += Size0 * C0->getSExtValue();
      Offset1 += Size1 * C1->getSExtValue();
    }
  }

  // Now, check overlaps with pointer operand sizes
  Type *ty0 = pv0->getType();
  Type *ty1 = pv1->getType();
  bool retval = true;
  if(ty0->isSized() && ty1->isSized()) {
    if(Offset0 <= Offset1)  {
      //Value *sizeV = ConstantInt::get(Type::getInt64Ty(context), TD->getTypeStoreSize(ty0));
      // return (Offset0 + sizeV->getZExtValue() >= Offset1);
      retval = (Offset0 + TD->getTypeStoreSize(ty0) >= Offset1);
    } else {
      // Value *sizeV = ConstantInt::get(Type::getInt64Ty(context), TD->getTypeStoreSize(ty1));
      // return (Offset1 + sizeV->getZExtValue() >= Offset0);
      retval = (Offset1 + TD->getTypeStoreSize(ty1) >= Offset0);
    }
  }

  if(!retval) {
    NumFieldSensitiveLoadRemoved++;
  }

  return retval;
}

// Assumption: 
// This function is called for a load instruction, with stores modifying the same DSNode.
// Check if the field modified by any store overlaps with the field read by this load
bool ThreadEscapeAnalysis::phasesOverlap(std::set<uint32_t>& rdPhases, std::set<uint32_t>& wrPhases)
{
  bool retval = false;
  for(std::set<uint32_t>::iterator itr = rdPhases.begin(); itr != rdPhases.end() && retval == false; itr++) {
    if(wrPhases.count(*itr))
        retval = true;
  }

  return retval;
}


// Note that the phase consists of (tS_id, phase_id), so it checks both.
// These are the conditions when it returns true"
//   1. (Same TSA R/W): Is node N written in same phase as where loadStoreInst is executed ?
//   2. (TBD) Different TSA in different hierarchy R/W: If written in different TSA, that are
//      not R/W. This is only fo rhierarchical TSA model.

bool ThreadEscapeAnalysis::writtenInSamePhase(const Instruction *loadStoreInst, const DSNode *N,
    bool checkProtectedWrites)
{
  if(!(N && loadStoreInst)) return true;
  if(isa<StoreInst>(loadStoreInst)) {
    return true;
  }

  if(!doTSA) return true;

  // Check for overlaps in parallel phases where N is modified, and where loadStoreInst is executed
  if (_nodeModifiedInPhases.find(N) == _nodeModifiedInPhases.end()) {
    // fprintf(stderr,"Escape ana: Load Node wasn't written in any phase.\n");
    return false;
  }

  if (_loadInstInPhases.find(loadStoreInst) == _loadInstInPhases.end()) {
    // fprintf(stderr,"Escape ana: Load instruction is not in any phase!\n");
    return false;
  }

  std::set<uint32_t>& modifiedPhases = _nodeModifiedInPhases[N];
  std::set<uint32_t>& execPhases = _loadInstInPhases[loadStoreInst];


  const LoadInst *ldInst = dyn_cast<LoadInst>(loadStoreInst);
  bool retval = false;
  for(std::set<uint32_t>::iterator itr = execPhases.begin(); retval == false && itr != execPhases.end(); itr++) {
    uint32_t phaseId = *itr;
    if(modifiedPhases.count(phaseId)) {
      // fprintf(stderr,"Escape ana: Load node modified in phase %d.\n", *itr);

      if(!checkProtectedWrites)
        retval = true;
      else if(!accessedInCriticalRegionOnly(N, phaseId, loadStoreInst))
        retval = true;
      else {
        fprintf(stderr,"NodeId %d is protected in phase %d\n", N->getNodeId(), phaseId);
        loadStoreInst->dump();
      }
    }
  }

  // If we decide that it is indeed modified, check if same field in an array of struct is modified
  if(retval && doFieldSensitiveModRef) {

    retval = false;
    // collect set of store insts that execute in  these phases, and also point to the same DSNode
    map<const Instruction *, std::set<uint32_t> >::iterator itr1 = _storeInstsInPhases.begin();
    vector<const StoreInst *> modifyingStores;
    for(; itr1 != _storeInstsInPhases.end() && retval == false; itr1++) {
      const StoreInst *storeInst = dyn_cast<StoreInst>(itr1->first);
      if(!storeInst) continue;
      storeInst = dyn_cast<StoreInst> (getReplacedInst(storeInst));
      if(!storeInst) continue;

      const Value *v = storeInst->getPointerOperand();
      v = v ? getRealUnderlyingObject(v) : NULL;

      vector<DSNodeHandle> retNodes;
      getNodesForObject(v, retNodes, storeInst); 

      bool modifiedSameNode = false;
      for(size_t i = 0; modifiedSameNode == false && i < retNodes.size(); i++) {
        // DSNode *n = retNodes[i].getNode();
        // if(n && n != N) continue; // known node, but different
    
        std::set<uint32_t>& phases = itr1->second;
        if(phasesOverlap(execPhases, phases))
          modifiedSameNode = true;
      } // for each pointed node 

      if(modifiedSameNode && fieldsOverlap(ldInst, storeInst))
        retval = true;
    } // fof each store Inst
  } // if field sensitive mod-ref is needed

  return retval;
}

scc_escaping_type ThreadEscapeAnalysis::objectMayEscape(const Value* object, const Instruction *loadStoreInst)
{
  // Trivial checks first.
  if (isa<ConstantPointerNull>(object))
    return SCC_NON_ESCAPING_NODE;

  // Check the DSGraph.
  // The object might escape if it's unknown, incomplete, or global.
 vector<DSNodeHandle> nodes;
 getNodesForObject(object, nodes, loadStoreInst);
 scc_escaping_type escType = SCC_NON_ESCAPING_NODE;
 for(size_t i = 0; i < nodes.size() && escType == SCC_NON_ESCAPING_NODE; i++) {
  DSNode* N = nodes[i].getNode();
  // assert(N);
  if (N == NULL) {
    return SCC_ESCAPING_NONODE;
  }

  if (N->isIncompleteNode()) {
    escType = TSNodeIsModified(object, N, loadStoreInst);
    continue;
    // return SCC_ESCAPING_INCOMPLETE_NODE;
  }
  else if(N->isUnknownNode()) {
     escType = TSNodeIsModified(object, N, loadStoreInst);
     if(escType != SCC_NON_ESCAPING_NODE) {
     } else {
       std::cerr << "Unknown and not modified\n";
     }
    continue;
  }

  if (EscapingNodes.count(N) > 0) {
    if(N->isMTROMHeapNode() && !N->isMTRWHeapNode()) {
      continue; // return SCC_NON_ESCAPING_NODE;
    }

    escType = TSNodeIsModified(object, N, loadStoreInst);
  }
  else if(N->isExternalNode()) {
    escType = TSNodeIsModified(object, N, loadStoreInst);
  }
 } // for each DSNode this object can point

  if(escType == SCC_NON_ESCAPING_NODE) {
    DEBUG(dbgs() << "[ThreadEscapeAnalysis] Value does not escape:\n" << (*object));
  }
  return escType;
}

// Is this access only to stack location?
bool ThreadEscapeAnalysis::valueIsStackOnly(Value* value, Instruction *inst) const 
{
  // Return true if the given value is only pointing to stack location.
  // Check also if any underlying objects are non-stack
  SmallPtrSet<Value*, 8> visited;
  std::vector<Value*> worklist;

  worklist.push_back(value);
  visited.insert(value);

  while (!worklist.empty()) {
    Value* v = worklist.back();
    Value* object = const_cast<Value *>(getRealUnderlyingObject(v));
    worklist.pop_back();

    if (!objectIsStackOnly(object, inst))
      return false;

    // Chase PHIs.
    if (PHINode* phi = dyn_cast<PHINode>(object)) {
      for (unsigned i = 0; i < phi->getNumIncomingValues(); ++i) {
        Value* in = phi->getIncomingValue(i);
        if (visited.insert(in))
          worklist.push_back(in);
      }
    }
  }

  // The value is stack only.
  return true;
}

// Does this escape to other threads?
scc_escaping_type ThreadEscapeAnalysis::valueMayEscape(Value* value, Instruction *loadStoreInst)
{
  // Return true iff the given value is a pointer that might escape.
  // Check if any underlying objects of this value might escape.
  SmallPtrSet<Value*, 8> visited;
  std::vector<Value*> worklist;

  worklist.push_back(value);
  visited.insert(value);

  while (!worklist.empty()) {
    Value* v = worklist.back();
    Value* object = const_cast<Value *>(getRealUnderlyingObject(v));
    worklist.pop_back();

    // Check the DSGraph.
    scc_escaping_type esctype = objectMayEscape(object, loadStoreInst);
    if (esctype != SCC_NON_ESCAPING_NODE)
      return esctype;

    // Chase PHIs.
    if (PHINode* phi = dyn_cast<PHINode>(object)) {
      for (unsigned i = 0; i < phi->getNumIncomingValues(); ++i) {
        Value* in = phi->getIncomingValue(i);
        if (visited.insert(in))
          worklist.push_back(in);
      }
    }
  }

  // The value does not escape.
  return SCC_NON_ESCAPING_NODE;
}

#if 0
// Is this value blacklisted?  It should be escaping, ABND mutex protected
// NOTE: THIS SHOULD ONLY BE USED FOR LESSER STORE, TO LOG ONLY.
bool ThreadEscapeAnalysis::valueIsBlackListed(Value* value) const
{
  // Return true iff the given value is a pointer that is black listed.
  // If it is non-escaping, it will return false. Only if it is escaping,
  // and blacklisted will it return true.

  SmallPtrSet<Value*, 8> visited;
  std::vector<Value*> worklist;

  worklist.push_back(value);
  visited.insert(value);

  while (!worklist.empty()) {
    Value* v = worklist.back();
    Value* object = const_cast<Value *>(getRealUnderlyingObject(v));
    worklist.pop_back();

    // Check the DSGraph.
    // Trivial checks first.
    if (isa<ConstantPointerNull>(object))
      return false;

    // Check the DSGraph.
    // The object might escape if it's unknown, incomplete, or global.
    unsigned offset;
    vector<DSNodeHandle> nodes;
    if(!getNodesForObject(object,nodes)) {
      DEBUG(dbgs() << "[ThreadEscapeAnalysis] Value escapes (not in DSGraph):\n" << (*object));
      return false;
    }

    bool mutexProtected = true;
    for(size_t i = 0; mutexProtected && i < nodes.size(); i++) {
      DSNode* N = nodes[i];
      if(!N)  
        mutexProtected = false;
      else if (EscapingNodes.count(N) > 0 && !N->isMutexProtected()) {
        mutexProtected = false;
      }
    }

    if(!mutexProtected)
      return false;

    // Chase PHIs.
    if (PHINode* phi = dyn_cast<PHINode>(object)) {
      for (unsigned i = 0; i < phi->getNumIncomingValues(); ++i) {
        Value* in = phi->getIncomingValue(i);
        if (visited.insert(in))
          worklist.push_back(in);
      }
    }
  } 

  // The value does not escape.
  return false;
}
#endif

// Is this instruction executed in any parallel phases of the program?
bool ThreadEscapeAnalysis::execInParallelMode(Instruction *inst)
{
 if(!doTSA)
    return true; // conservative

  if(isa<LoadInst>(inst))
    return (_loadInstInPhases.find(inst) != _loadInstInPhases.end());
  else if(isa<StoreInst>(inst))
    return (_storeInstsInPhases.find(inst) != _storeInstsInPhases.end());
  else
    return true; // conservative
}

// Is this node always accessed in mutex protected manner in all phases where this instruction is executed?
// If no TSA or mutex pass is run, conservatively returns false.
bool ThreadEscapeAnalysis::execMutexProtectedOnly(Value* value, Instruction *inst)
{
  if(!(doTSA && doCriticalRegionOpt))
    return false;

  std::set<uint32_t> phasesOfInst;
  if(isa<LoadInst>(inst)) {
    if(_loadInstInPhases.find(inst) != _loadInstInPhases.end())
      phasesOfInst = _loadInstInPhases[inst];
  }
  else if(isa<StoreInst>(inst)) {
    if(_storeInstsInPhases.find(inst) != _storeInstsInPhases.end())
      phasesOfInst = _storeInstsInPhases[inst];
  }
  else
    return false; // conservative; don't understand instruction

   Value* object = const_cast<Value *>(getRealUnderlyingObject(value));
   if (isa<ConstantPointerNull>(object))
     return false;

  // Check the DSGraph.
  // The object might escape if it's unknown, incomplete, or global.
  vector<DSNodeHandle> nodes;
  if(!getNodesForObject(object,nodes, inst))
    return false;

  for(size_t i = 0; i < nodes.size(); i++) {
    DSNode* N = nodes[i].getNode();
    if(!N) return false; // lost, can't get points-to set. be conservative.

    for(std::set<uint32_t>::iterator itr = phasesOfInst.begin(); itr != phasesOfInst.end(); itr++) {
      if(!accessedInCriticalRegionOnly(N, *itr, inst))
        return false;
    }
  }

  return true;  // all phases where this inst appears , access to all memory nods are mutex protected
}


// Return true iff the given call may target an external function.
// Direct call?
bool ThreadEscapeAnalysis::callSiteMayBeExternal(const CallSite& cs) const {
  if (Function *F = cs.getCalledFunction())
    return F->isDeclaration();

  // Indirect call.
  vector<DSNodeHandle> nodes;
  if(!getNodesForObject(getRealUnderlyingObject(cs.getCalledValue()), nodes, cs.getInstruction()))
    return true; // conservative

  for(size_t i = 0; i < nodes.size(); i++) {
    DSNode* N = nodes[i].getNode();
    if (!N)
      return true;
    if (N->isIncompleteNode() || N->isUnknownNode())
      return true;

    vector<const Function*> callTargets;
    N->addFullFunctionList(callTargets);
    for (size_t j = 0; j < callTargets.size(); ++j)
      if (callTargets[j]->isDeclaration())
        return true;
  } // for each possible points-to set

  return false;
}

// Get loop info for a function
LoopInfo *ThreadEscapeAnalysis::getLoopInfo(Function *func) {
  // return &getAnalysis<LoopInfo>(func) ;
  if(!func) {
    std::cerr << " getLoopInfo called with NULL function!\n";
  }
  // return getAnalysisIfAvailable<LoopInfo>();
  return &getAnalysis<LoopInfo>(*func);
  // TBD: program crashes, why? return &getAnalysis<LoopInfo>() ;
}


ModulePass *createThreadEscapeAnalysisPass() { 
  fprintf(stderr, "CREATING ThreadEscapeAnalysis\n");
  return new ThreadEscapeAnalysis(); 
}

//------------------------------------------------------------------------------
// Pass Registration
//------------------------------------------------------------------------------
/*
INITIALIZE_PASS_BEGIN(ThreadEscapeAnalysis, "threadescapeanalysis",
                "Thread-local escape analysis", true, false)
INITIALIZE_PASS_DEPENDENCY(DominatorTree)
INITIALIZE_PASS_DEPENDENCY(LoopInfo)
INITIALIZE_PASS_END(ThreadEscapeAnalysis, "threadescapeanalysis",
                "Thread-local escape analysis", true, false)
*/

// INITIALIZE_PASS_DEPENDENCY(LoopSimplify)

char ThreadEscapeAnalysis::ID = 0;
// char &llvm::ThreadEscapeAnalysisID = ThreadEscapeAnalysis::ID;

static RegisterPass<ThreadEscapeAnalysis> X("threadescapeanalysis",
                                            "Thread-local escape analysis");
