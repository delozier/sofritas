#include "AccessInfo.h"

namespace llvm{

int AccessInfo::classify(const DataLayout& TD, LLVMContext& context, Instruction* inst, AccessInfo all[2]) 
{
  AccessInfo& a = all[0];
  AccessInfo& b = all[1];
  a = AccessInfo();
  b = AccessInfo();

  // Cache for keeping statistics.
  static std::set<Instruction*> seen;

  // Load?
  if (LoadInst* load = dyn_cast<LoadInst>(inst)) {
    a = AccessInfo(Load, inst);
    a.addr = load->getPointerOperand();
    a.setSizeFromAddr(TD, context);
    a.setAlignmentFromAddr(TD);

    // fprintf(stderr, "Load : a.alignment %d load->alignment %d\n", a.alignment, load->getAlignment());
    // if (load->getAlignment() != 0) 
    if (load->getAlignment() != 0 && load->getAlignment() >= a.alignment) // Madan: 2011/12/9 : added 2nd cond 
      a.alignment = load->getAlignment();
    return 1;
  }

  // Store?
  if (StoreInst* store = dyn_cast<StoreInst>(inst)) {
    a = AccessInfo(Store, inst);
    a.addr = store->getPointerOperand();
    a.setSizeFromAddr(TD, context);
    a.setAlignmentFromAddr(TD);
    // fprintf(stderr, "Store : a.alignment %d store->alignment %d\n", a.alignment, store->getAlignment());
    // if (store->getAlignment() != 0)
    if (store->getAlignment() != 0 && store->getAlignment() >= a.alignment) // Madan: 2011/12/9 : added 2nd cond
      a.alignment = store->getAlignment();
    return 1;
  }

  // Intrinsics?
  if ( true /* doMemTrackIntrinsics */ ) {
    // Atomic op?
    if (IntrinsicInst* call = dyn_cast<IntrinsicInst>(inst)) {
      if (isAtomicOp(call)) {
        a = AccessInfo(Store, inst);
        a.addr = call->getOperand(1);
        a.setSizeFromAddr(TD,context);
        a.setAlignmentFromAddr(TD);
        return 1;
      }
    }

    // Memory op?
#if 0
    if (MemIntrinsic* call = dyn_cast<MemIntrinsic>(inst)) {
      if (seen.insert(call).second) {
        NumIntrinsicMems++;
        DEBUG(dbgs() << "[SCC] memory op: " << (*call) << "\n");
      }

      a = AccessInfo(Store, inst);
      a.addr = call->getDest();
      a.size = call->getLength();

      if (MemTransferInst* call = dyn_cast<MemTransferInst>(inst)) {
        b = AccessInfo(Load, inst);
        b.addr = call->getSource();
        b.size = call->getLength();
        return 2;
      }

      return 1;
    }
#endif
  }

  return 0;
}

bool AccessInfo::isAtomicOp(Instruction* inst) 
{
#if 0
  // Madan: changed for llvm3.0. Verify
  if (IntrinsicInst* call = dyn_cast<IntrinsicInst>(inst)) {
    return (call->getIntrinsicID() == Intrinsic::atomic_cmp_swap ||
            call->getIntrinsicID() == Intrinsic::atomic_load_add ||
            call->getIntrinsicID() == Intrinsic::atomic_load_and ||
            call->getIntrinsicID() == Intrinsic::atomic_load_max ||
            call->getIntrinsicID() == Intrinsic::atomic_load_min ||
            call->getIntrinsicID() == Intrinsic::atomic_load_nand ||
            call->getIntrinsicID() == Intrinsic::atomic_load_or ||
            call->getIntrinsicID() == Intrinsic::atomic_load_sub ||
            call->getIntrinsicID() == Intrinsic::atomic_load_xor ||
            call->getIntrinsicID() == Intrinsic::atomic_swap);
  }
  return false;
#else
  if (LoadInst *LI = dyn_cast<LoadInst>(inst))
    return LI->isAtomic();
  else if(StoreInst *SI = dyn_cast<StoreInst>(inst)) 
    return SI->isAtomic();
  return false;
#endif
}

void AccessInfo::setSizeFromAddr(const DataLayout& TD, LLVMContext& context) 
{
  assert(addr);
  Type* Ty = cast<PointerType>(addr->getType())->getElementType();
  if (Ty->isSized()) {
    size = ConstantInt::get(Type::getInt64Ty(context), TD.getTypeStoreSize(Ty));
    // size = ConstantInt::get(Type::getInt64Ty(getGlobalContext()), TD.getTypeStoreSize(Ty));
  }
}

void AccessInfo::setAlignmentFromAddr(const DataLayout& TD) 
{
  assert(addr);
  Type* Ty = cast<PointerType>(addr->getType())->getElementType();
  alignment = (unsigned)TD.getPrefTypeAlignment(Ty);
}

}
