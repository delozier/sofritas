// Thread-local escape analysis
// The Steensgaard points-to analysis is borrowed heavily from llvm-poolalloc,
// which is released under the University of Illinois Open Source License.
// AND under the University of Santa Cruz Open Source License.

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/DenseSet.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Support/Debug.h"
#include <map>
#include <queue>

#include "dsa/DataStructure.h"
#include "dsa/DSGraph.h"
#include "dsa/DSNode.h"
#include "identifyTS.h"
#include "SccICFG.h"

using std::vector;
using std::map;
using std::queue;

namespace llvm {

class LoopInfo;
class BlockFrequencyInfo;
class Loop;
class SccProfile;
class DominatorTree;
struct PostDominatorTree;
class DominanceFrontier;
}

class SCC_CreateJoinTSGroup;
class SplicedCFG;

// Types of scaping nodes. 0 for non-escaping ( and has node)
typedef enum
{
   SCC_NON_ESCAPING_NODE = 0,
   SCC_ESCAPING_NONODE = 1,
   SCC_ESCAPING_UNKNOWN_NODE = 2,
   SCC_ESCAPING_INCOMPLETE_NODE = 3,
   SCC_ESCAPING_EXTERNAL_NODE = 4,
   SCC_ESCAPING_KNOWN_NODE = 5
} scc_escaping_type;

class ThreadEscapeAnalysis;

using namespace llvm;

// Used in mutual exclusion og regions of code accessing some variable
class SCCAccessRegion 
{
  BasicBlock *_preheader;
  Instruction *_origInst; // starting instruction
  int _freq;
  bool _read;
  bool _write;
  std::vector<uint32_t> _tasks; // composite task ids
 public:
  SCCAccessRegion(Instruction *inst, bool write) {
      _origInst = inst; 
      _preheader = NULL;
      _write = write;
      _read = (!write);
      _freq = 1;
  }
  ~SCCAccessRegion() {}

  bool findTasks(SccProfile *sccProf);  // Find composite statis task ids that executes this instruction
  void expand(ThreadEscapeAnalysis *ea);
  void merge(SCCAccessRegion *a);
  int  estimateTripCnt(Loop *l);
  bool loopOKToInclude(Loop *l);

  void protectRegion(int lockId, Constant *readLockFunc, Constant *writeLockFunc,
        IntegerType *Int32Ty); // protect with a lock

  int freq() { return _freq; }
  bool isRead() { return _read; }
  bool isWrite() { return _write; }
  BasicBlock *getBlock() { return (_origInst ? _origInst->getParent() : NULL); }

  BasicBlock *getPreHeader() { return _preheader; }
  static bool cmpRegionByLoop(SCCAccessRegion *a, SCCAccessRegion *b);
  std::vector<uint32_t>& tasks() { return _tasks; }
};

// This class is used in creating mutually exclusive regions ( blacklisting). It contains tuples { beginning, end, frequency}
// for one DSNode 
class SCCMutualExclusionBlock
{
    DSNode *_node;
    int _writeFreq;
    int _readFreq;
    float _serialPenalty;
    std::vector<SCCAccessRegion *> _accesses;
  public:
   SCCMutualExclusionBlock(DSNode *node);
   ~SCCMutualExclusionBlock();

   DSNode *getDSNode() { return _node; }
   std::vector<SCCAccessRegion *>& getAccesses() { return _accesses; }
   int totalFreq() const { return (_readFreq + _writeFreq); }
   float serialPenalty() { return _serialPenalty; }

   void expandRegions(ThreadEscapeAnalysis *ea);
   void computeOuterLoops(ThreadEscapeAnalysis *ea);
   void computeTaskOverlapScore(SccProfile *sccProf); // for dynamic profile based blacklisting

   static bool cmpFreq(SCCMutualExclusionBlock *a, SCCMutualExclusionBlock *b) 
   { return (a->totalFreq() < b->totalFreq()); }

   // sorts n ascending penalty of overlap with other read/write blocks 
   static bool cmpSerialPenalty(SCCMutualExclusionBlock *a, SCCMutualExclusionBlock *b) 
   { return (a->serialPenalty() > b->serialPenalty()); }
};

// This class is used to detect regions already protected by previous protected regions
class SCCProtectingLocks
{
    std::vector<SCCAccessRegion *> _protectedRegions;
    std::vector<int> _protectingLocks;

  public:
    SCCProtectingLocks() {}
    ~SCCProtectingLocks() {}
    void updateCnts(bool isWrite, int& numRdLocks, int& numWrLocks, int& numRdToWr);
    void addRegion(SCCAccessRegion *r, int lockId) {
      _protectedRegions.push_back(r);
      _protectingLocks.push_back(lockId);
    }
};


// A class to contain all the tasks in a thread start routine
class TaskSections 
{
    const DSCallSite *_callsite; 
    const Function *_function;

    // parallel phase boundaries identified ost commonly mby barriers
    // If there are n barriers, then there are (n+1) parallel phases.
    // The first barrier has first instruction in function as start. The last one has
    // the last instruction of function as end point.
    // If _barriers are empty, single parallel phase for whole function

    std::vector<Instruction *> _barrierPoints; // these are all or none barriers

  public:
     TaskSections(const DSCallSite *cs, const Function *f) {
        _callsite = cs; _function = f; 
     }
     ~TaskSections() {}
};

// A section in code with all the thread starts call sites
// Assume common call stack till all functions in a thread section, so they are all 
class ThreadSection
{
    std::vector<TaskSections *> _tasks;
    const BasicBlock *_rootThreadStart;  // first instr in main thread
    const BasicBlock *_rootThreadEnd;    // last instr in main thread
    std::vector<CallInst *> _threadStarts;
    std::vector<CallInst *> _barriers;
  public:
    ThreadSection(const BasicBlock *start, const BasicBlock *end,
        std::vector<CallInst *>& threadStarts)
    {
        _rootThreadStart = start; _rootThreadEnd = end;
        _threadStarts = threadStarts;
    }
    ~ThreadSection() {}
};

// Remember functions that need instrumentation, based on whatever analysis
class ValuesToInstrumentForFunction
{
  public:
    Function *_function;
    std::set<const Value *> _valuesToInstrument;  // has underlying object
};

// Simple class to includ ebotj DSNode and offset. It is simpler versionof DSNodeHandle
class  NodeOffsetPair
{
  public:
    DSNode *node;
    int offset;

  NodeOffsetPair() { node = NULL; offset = -1; }
  NodeOffsetPair(DSNode *n, int off) { node = n; offset = off; }
  bool operator==(NodeOffsetPair& a) {
    return (node == a.node && offset == a.offset);
  }
  bool operator!=(NodeOffsetPair& a) {
    return (node != a.node || offset != a.offset);
  }

  bool isNull() { return (!node || offset < 0); }
};

struct scc_ltstr
{
  bool operator()(const char* s1, const char* s2) const
  {
    return strcmp(s1, s2) < 0;
  }
};


// This class tells whether a particular argument of an external function call is
// read (r), write(w), both(b) or none(n). Only the pointer args should have r/w/b
class SccFunctionArgClassifier
{
  char *_threadStartName;
  char *_threadJoinName;
  map<const char *, const char *, scc_ltstr> _funcArgMap;

  public:
   SccFunctionArgClassifier();
   ~SccFunctionArgClassifier() {
    if(_threadStartName) free(_threadStartName);
    if(_threadJoinName) free(_threadJoinName);
  }

  void setThreadFunctionNames(const char *start, const char *join);

  // returns a string, like rwbn.
  const char *getArgTypes(const char *fname);
};

// This struct is to support non-unification based Steensgaard algo. If a node reaches
// the argument of the function, then there is a pointer to actual node, instead of merging the actuals
class CallSiteArgMap
{
   CallSite _cs; // actual arg at this callsize
   vector<uint32_t> _phases; // phases in which it is called
   map<DSNodeHandle, DSNodeHandle> _formalToActualMap; // top node connected to this DSNode in this function body

 public:
   CallSiteArgMap(const CallSite& cs1) {
      _cs = cs1;
   }
   ~CallSiteArgMap() {}

   CallSite& getCallSite() { return _cs; }
   void addMapping(DSNodeHandle& formal, DSNodeHandle& actual) {
      _formalToActualMap[formal] = actual;
   }
   int different(const CallSiteArgMap *other) const ;
   const DSNodeHandle& getActualNode(const DSNodeHandle& formal) 
     { return _formalToActualMap[formal]; }

};

// Class to find number of create/join calls in a function, or its descendants
class SccCreateJoinCalls
{
  public:
    SccCreateJoinCalls(const Function *f) { func = f; }
    ~SccCreateJoinCalls() {}

    const Function *func;
    set<CallInst *> _createCalls;  // direct
    set<CallInst *> _joinCalls;

    set<CallInst *> createOrJoin; // either direct create or join, or calls with create or join within this function
};


// Main class for thread escaoe analysis
class ThreadEscapeAnalysis : public DataStructures 
{
public:
  static char ID;
  char *_THREAD_CREATE_FN_NAME;
  char *_THREAD_JOIN_FN_NAME;

  ThreadEscapeAnalysis();
  ~ThreadEscapeAnalysis() {}

  // Analysis API
  bool valueIsStackOnly(Value* ptr, Instruction *inst) const;
  bool hasThreadCreateCall() { return ThreadCreate; }
  bool hasThreadStartCall() { return ThreadStart; }
  scc_escaping_type valueMayEscape(Value* ptr, Instruction *inst);
  bool valueIsBlackListed(Value* value) const; // Only for calling a lesser store if it is definitively blacklisted
  bool execInParallelMode(Instruction *inst); // if tsa performed, and appears 
  bool execMutexProtectedOnly(Value* value, Instruction *inst);

  // update replaced instruction info
  void recordReplacement(Instruction *origInst, Instruction *newInst);
  Instruction *getReplacedInst(const Instruction *origInst)
  {
    if(!origInst) return NULL;
    Instruction *inst = const_cast<Instruction *>(origInst);
    if(replacements.count(inst))
      return replacements[inst];
    else
      return inst;
  }

  bool calledInMultiThreadedMode(const Function *f) const;
  bool callSiteMayBeExternal(const CallSite& cs) const;
  bool isCalledFromThreadedSection(const Function *f);

  const Value* getRealUnderlyingObject(const Value* v) const;
  // Pass API
  virtual bool runOnModule(Module &M);
  virtual void getAnalysisUsage(AnalysisUsage &AU) const;

  virtual void releaseMemory() {
    ResultGraph = 0;
    DataStructures::releaseMemory();
  }

  // DataStructures API
  virtual DSGraph *getDSGraph(const Function &F) const {
    return ResultGraph;
  }
	virtual bool hasDSGraph(const Function &F) const {
    return true;
  }

  // Should it be here?
  LoopInfo *getLoopInfo(Function *func);

  void attachDSNodeMetaData(Instruction *inst);

private:
  DominatorTree *DT;
  PostDominatorTree *PDT;
  DominanceFrontier *DF;

  DataLayout *TD;
  DSGraph* ResultGraph;
  SccProfile *sccProfPass;

  map<CallInst *, vector<Function *> > threadCreate2StartFns;

  SccFunctionArgClassifier argClassifier; // to classify functin arg of external func call
  set<const Function *> _flaggedFunctions; // avoid duplicate warnings about same function

  DenseSet<const DSNode*> EscapingNodes;
  std::vector<const Function*> ThreadStartFunctions;

  // These maps maintain DSNode modification in each phase of parallel programming
  map<const DSNode *, std::set<uint32_t> > _nodeModifiedInPhases;   // all phases where node is modified
  map<const Instruction *, std::set<uint32_t> > _loadInstInPhases;  // all phases where this load is executed
  map<const Instruction *, std::set<uint32_t> > _storeInstsInPhases;  // all phases where this store is executed

  // DSNode is accessed in these instructions in this phase
  map<const DSNode *, map<uint32_t, vector<Instruction *> > >  _allAccessesInPhases;  


  // These data structs are to maintain non-unification DSA
  std::map< Function *, std::set<DSNodeHandle> > _funcArgs; // map of function -> local arg nodes in F 
  std::map< Function *, std::set<CallSiteArgMap *> > _callSiteArgMaps; // map of formal -> actual for each F

  // The DSNode writes in protected (by mutex) mode only in these phases
  map<const DSNode *, vector<uint32_t> > _protectedWritePhases;

  // map of instructions protected by mutex
  map<Instruction *, Value *> _mutexProtectInstrs;

  DenseMap<Instruction*, Instruction*> replacements;

  // Added by Madan
  const Function *ThreadCreate;
  const Function *ThreadStart;
  const Function *ThreadJoin;
  const Function *ThreadSync;
  const Function *StartTSFn;
  const Function *EndTSFn;
  const Function *SCCMutexLockFunc;
  const Function *SCCMutexUnlockFunc;

  // For disjoint thread section marking
  int curDTSId;
  // For stat reporting and debugging
  map<uint32_t, int> _dtsLdCnts;
  map<uint32_t, int> _dtsStCnts;
  map<uint32_t, set<DSNode *> > _dtsAccessedNodes;

  // Following needed for mutual exclusion ( black listing) of access regions
  Constant *_readLockFunc; // start_rd, end_read, 
  Constant *_writeLockFunc; // start_write, end_write
  Type* VoidTy;
  IntegerType* Int32Ty;
  map<BasicBlock *, SCCProtectingLocks> _protectedLoops;
  map<BasicBlock *, SCCProtectingLocks> _protectedBlocks;
  std::set<CallInst *> _threadCreateCallsInTSA; // covered in findThreadSections

  std::vector<ThreadSection *> _threadSections;
  std::map<const Function *, int> threadReachableFuncs;
  std::vector<const Function *> _reverseTopoOrder;
  std::map<const Function *, ValuesToInstrumentForFunction *> _escapingValuesPerFunc;

  bool doSteensgaard(Module &M);
  void doEscapeAnalysis(Module &M);

  // Next few functions for performing thread section analysis
  SccCreateJoinCalls *findAllFunctionsWithCreateOrJoin(Function *func,
  		std::map<const Function *, SccCreateJoinCalls *>& functionsToCreateJoins);

  void findAllThreadSections(Module &M, bool hierTSA);
  void findThreadSectionsInFunction(Function *F, std::set<Function *>& visitedFuncs, bool recursive);

  // For finding load/store within critical sections
  void findMutexProtectedInstructions(Module& M);
  void protectAllInsts(Function *F,  set<Function *>& fullyProtected, Value *mutexLockValue, int& lockedInstrCnt);
  bool markInstProtected(Instruction *inst, Value *mutexLockValue);

  // (In works) : more robust version
  bool findThreadSectionsInFunction2(Function *F, std::set<Function *>& visitedFuncs, bool recursive);
  bool findThreadSectionsWithUserDirectives(Function *F, std::set<Function *>& visitedFuncs, bool recursive,
    std::map<Loop *, SCC_CreateJoinTSGroup>& callGroups, std::set<Function *> calledFuncs);
  bool findThreadSectionsWithoutUserDirectives(Function *F, std::set<Function *>& visitedFuncs, bool recursive,
    std::map<Loop *, SCC_CreateJoinTSGroup>& callGroups, std::set<Function *> calledFuncs);

  bool createThreadSectionsInGroup(Function *F, SCC_CreateJoinTSGroup *topGroup, bool recursive);
  bool createOneTS(vector<SCC_CallInstDomPostDom>& calls, size_t startIdx, size_t endIdx, SplicedCFG *spCfg);

  bool createTS(SplicedCFG *topCFG, Function *topF, const BasicBlock *nearestStartDominator, BasicBlock *nearestEndPostDom,
      Instruction *startOfTS, Instruction *endOfTS,
      std::vector<Instruction *>& threadStarts, std::vector<Instruction *>& threadJoins);
  bool checkTSClosure(Function *topF,
      std::vector<Instruction *>& threadStarts, std::vector<Instruction *>& threadJoins);

  std::vector<Instruction *> GetNumPhases(Function& F);

  void identifyMultiThreadedFunctions(Module &M);  // added by Madan
  void doDFSOnFunction(SplicedCFG *spCfg, const Function *F, 
        const Instruction *start, const Instruction *end,
        uint32_t parallelPhase, std::set<const Function *>& visitedFuncs);
  void updateCallFreq(Function *F);

  void checkReadOnlyInMultithreaded();

  int doBlackListing(); // call this after thread section analysis, after calling read/write locations
  bool protectRegions(SCCMutualExclusionBlock *node, int lockId);

  // Report static races
  int reportRaces(Module& M);
  void printLineInfo(const Instruction *I);

  void ResolveFunctionCall(const Function *F, const DSCallSite &call);
  void ResolveThreadCreation(const Function *ThreadCreateOrStart, const DSCallSite &call);
  void dfsThreadCreation(const Function *ThreadCreateOrStart, const DSCallSite &call, uint32_t parallelPhase);

  bool getNodesForObject(const Value* object, vector<DSNodeHandle>& retNodes, const Instruction *inst) const;
  bool getPointedNodeRecursive(Function *F, DSNodeHandle& nodeInFunc, uint32_t phase, 
              vector<DSNodeHandle>& retNodes, bool recursive, set<Function *>& visitedFuncs) const;
  bool getAllPointedNodes(const Value* object, const Instruction *inst,
        vector<DSNodeHandle>& retNodes, unsigned phase, bool recursive) const;
  void debugNonUnifiedFuncCalls() const;

  NodeOffsetPair checkSameNodeForValues(const Value *v1, const Value *v2);
  DSNodeHandle getHandleForObject(const Value* object) const;
  scc_escaping_type objectMayEscape(const Value* object, const Instruction *inst);

  bool writtenInSamePhase(const Instruction *loadStoreInst, const DSNode *N, bool checkProtectedWrites);

  // field sensitive mod-ref
  bool fieldsOverlap(const LoadInst *ldInst, const StoreInst *stInst);
  bool phasesOverlap(std::set<uint32_t>& rdPhases, std::set<uint32_t>& wrPhases);

  bool accessedInCriticalRegionOnly(const DSNode *N, uint32_t phaseId, const Instruction *loadStoreInst);

  // helper func for critical section access only
  void addToAllAccesses(DSNode *N, const Instruction *inst, uint32_t phaseId)
  {
    if(!(N && inst)) return;
    map<uint32_t, vector<Instruction *> >& accessesInPhase = _allAccessesInPhases[N];
    Instruction  *ii = const_cast<Instruction *>(inst);
    accessesInPhase[phaseId].push_back(ii);;
  }

  const Value *mutexProtected(Instruction *instr);

  scc_escaping_type TSNodeIsModified(const Value* object, DSNode *N, const Instruction *loadStoreInst);
  bool objectIsStackOnly(const Value* object, Instruction *inst) const;

  void visitTSAWriteValue(const Instruction *inst, const Value *value, uint32_t parallelPhase);
  void visitTSAReadValue(const Instruction *inst, const Value *value, uint32_t parallelPhase);
  void visitTSAInstruction(const Instruction *I, 
            std::set<const Function *>& calledFunctions,
            uint32_t parallelPhase);

  // Call this for each TS. Delete globaslICFG after this step., so new TS gets a new ICFG
  void markDTSInTS(SccICFG *icfg);
  void markOneDTS(SccICFGNode *start, const set<SccICFGNode *>& allBarriers, int dtsId);

};
