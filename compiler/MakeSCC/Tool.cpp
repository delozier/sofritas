#include "AccessInfo.h"
#include "llvm/IR/InlineAsm.h"
#include "llvm/IR/Function.h"
#include "Tool.h"

#include <iostream>

namespace llvm{

bool ORCATool::Init(Module &M){
  Mod = &M;

  debugNumber = 0;

  std::string ec;
  debugOut = new raw_fd_ostream("debug_symbols.txt",ec,llvm::sys::fs::OpenFlags::F_RW);
  
  Type **functionArgs = new Type*[2];
  functionArgs[0] = Type::getInt64Ty(M.getContext());
  functionArgs[1] = Type::getInt64Ty(M.getContext());

  ArrayRef<Type*> args(functionArgs,2);

  Constant *InstFuncConst =
    M.getOrInsertFunction("SOFRITAS_acquireLoad",
                          FunctionType::get(Type::getVoidTy(M.getContext()),
                                            args,false));
  assert(InstFuncConst != NULL);
  LoadHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(LoadHookFunction != NULL);

  InstFuncConst =
    M.getOrInsertFunction("SOFRITAS_acquireLoadAligned",
                          FunctionType::get(Type::getVoidTy(M.getContext()),
                                            args,false));
  assert(InstFuncConst != NULL);
  AlignedLoadHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(AlignedLoadHookFunction != NULL);

  InstFuncConst = 
    M.getOrInsertFunction("SOFRITAS_acquireStore",
			  FunctionType::get(Type::getVoidTy(M.getContext()),
					    args,false));
  assert(InstFuncConst != NULL);
  StoreHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(StoreHookFunction != NULL);

  InstFuncConst = 
    M.getOrInsertFunction("SOFRITAS_acquireStoreAligned",
			  FunctionType::get(Type::getVoidTy(M.getContext()),
					    args,false));
  assert(InstFuncConst != NULL);
  AlignedStoreHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(AlignedStoreHookFunction != NULL);

  InstFuncConst =
    M.getOrInsertFunction("SOFRITAS_acquireHeapLoad",
                          FunctionType::get(Type::getVoidTy(M.getContext()),
                                            args,false));
  assert(InstFuncConst != NULL);
  HeapLoadHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(HeapLoadHookFunction != NULL);

  InstFuncConst =
    M.getOrInsertFunction("SOFRITAS_acquireHeapLoadAligned",
                          FunctionType::get(Type::getVoidTy(M.getContext()),
                                            args,false));
  assert(InstFuncConst != NULL);
  HeapAlignedLoadHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(HeapAlignedLoadHookFunction != NULL);

  InstFuncConst = 
    M.getOrInsertFunction("SOFRITAS_acquireHeapStore",
			  FunctionType::get(Type::getVoidTy(M.getContext()),
					    args,false));
  assert(InstFuncConst != NULL);
  HeapStoreHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(HeapStoreHookFunction != NULL);

  InstFuncConst = 
    M.getOrInsertFunction("SOFRITAS_acquireHeapStoreAligned",
			  FunctionType::get(Type::getVoidTy(M.getContext()),
					    args,false));
  assert(InstFuncConst != NULL);
  HeapAlignedStoreHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(HeapAlignedStoreHookFunction != NULL);

  Function *PthreadCreateFn = M.getFunction("pthread_create");
  if(PthreadCreateFn != NULL){
    InstFuncConst = 
      M.getOrInsertFunction("SOFRITAS_threadCreate",
                            PthreadCreateFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAThreadCreateFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAThreadCreateFunction != NULL);
  }

  Function *PthreadMutexLockFn = M.getFunction("pthread_mutex_lock");
  if(PthreadMutexLockFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_mutexLock",
                            PthreadMutexLockFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAMutexLockFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAMutexLockFunction != NULL);
  }

  Function *PthreadMutexTrylockFn = M.getFunction("pthread_mutex_trylock");
  if(PthreadMutexTrylockFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_mutexTrylock",
                            PthreadMutexTrylockFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAMutexTrylockFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAMutexTrylockFunction != NULL);
  }

  Function *PthreadMutexUnlockFn = M.getFunction("pthread_mutex_unlock");
  if(PthreadMutexUnlockFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_mutexUnlock",
                            PthreadMutexUnlockFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAMutexUnlockFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAMutexUnlockFunction != NULL);
  }

  Function *PthreadCondWaitFn = M.getFunction("pthread_cond_wait");
  if(PthreadCondWaitFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_condWait",
                            PthreadCondWaitFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCACondWaitFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCACondWaitFunction != NULL);
  }

  Function *PthreadCondSignalFn = M.getFunction("pthread_cond_signal");
  if(PthreadCondSignalFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_condSignal",
                            PthreadCondSignalFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCACondSignalFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCACondSignalFunction != NULL);
  }

  Function *PthreadCondBroadcastFn = M.getFunction("pthread_cond_broadcast");
  if(PthreadCondBroadcastFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_condBroadcast",
                            PthreadCondBroadcastFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCACondBroadcastFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCACondBroadcastFunction != NULL);
  }

  Function *PthreadJoinFn = M.getFunction("pthread_join");
  if(PthreadJoinFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_join",
                            PthreadJoinFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAThreadJoinFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAThreadJoinFunction != NULL);
  }

  Function *PthreadBarrierWaitFn = M.getFunction("pthread_barrier_wait");
  if(PthreadBarrierWaitFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("SOFRITAS_barrierWait",
                            PthreadBarrierWaitFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCABarrierWaitFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCABarrierWaitFunction != NULL);
  }

  return true;
}

void ORCATool::Finalize()
{
  debugOut->close();
}
  
bool ORCATool::Access(Function &F, const AccessInfo &info,
		      uint64_t accessSize,bool isHeapValue){
  Value *addr = info.addr;

  accessSize = 8;

  unsigned long myDebug = debugNumber++;
  
  DebugLoc loc = info.inst->getDebugLoc();

  *debugOut << myDebug << ",";
  loc.print(Mod->getContext(),*debugOut);
  *debugOut << "," << info.addr->getName() << "\n";

  if(info.type == AccessInfo::AccessType::Load ||
     info.type == AccessInfo::AccessType::ExclusiveLoad){

    Value *castResult = 
      CastInst::CreatePointerCast(addr,
				  Type::getInt64Ty(Mod->getContext()),
				  "",info.inst);

    SmallVector<Value*,1> operands;
    operands.push_back(castResult);
    operands.push_back(ConstantInt::get(Type::getInt64Ty(Mod->getContext()),
					myDebug));
    
    if(accessSize == 4 || accessSize == 8){
      if(isHeapValue){
	CallInst::Create(HeapAlignedLoadHookFunction,operands,"",info.inst);
      }else{
	CallInst::Create(AlignedLoadHookFunction,operands,"",info.inst);
      }
    }else{
      if(isHeapValue){
	CallInst::Create(HeapLoadHookFunction,operands,"",info.inst);
      }else{
	CallInst::Create(LoadHookFunction,operands,"",info.inst);
      }
    }
  }else{
    Value *castResult = 
      CastInst::CreatePointerCast(addr,
				  Type::getInt64Ty(Mod->getContext()),
				  "",info.inst);

    SmallVector<Value*,1> operands;
    operands.push_back(castResult);
    operands.push_back(ConstantInt::get(Type::getInt64Ty(Mod->getContext()),
					myDebug));
    
    if(accessSize == 4 || accessSize == 8){
      if(isHeapValue){
	CallInst::Create(HeapAlignedStoreHookFunction,operands,"",info.inst);
      }else{
	CallInst::Create(AlignedStoreHookFunction,operands,"",info.inst);
      }
    }else{
      if(isHeapValue){
	CallInst::Create(HeapStoreHookFunction,operands,"",info.inst);
      }else{
	CallInst::Create(StoreHookFunction,operands,"",info.inst);
      }
    }
  }
  
  return true;
}

bool ORCATool::HandleInvokeInst(BasicBlock& bb, InvokeInst *inst){
  std::string calledFunctionName = inst->getCalledFunction()->getName().str();
  SmallVector<Value*,0> operands;
  if(calledFunctionName.compare("pthread_create") == 0){
    assert(ORCAThreadCreateFunction != NULL);
    inst->setCalledFunction(ORCAThreadCreateFunction);
  }else if(calledFunctionName.compare("pthread_join") == 0){
    assert(ORCAThreadJoinFunction != NULL);
    inst->setCalledFunction(ORCAThreadJoinFunction);
  }else if(calledFunctionName.compare("pthread_barrier_wait") == 0){
    assert(ORCABarrierWaitFunction != NULL);
    inst->setCalledFunction(ORCABarrierWaitFunction);
  }else if(calledFunctionName.compare("pthread_mutex_lock") == 0){
    assert(ORCAMutexLockFunction != NULL);
    inst->setCalledFunction(ORCAMutexLockFunction);
  }else if(calledFunctionName.compare("pthread_mutex_trylock") == 0){
    assert(ORCAMutexTrylockFunction != NULL);
    inst->setCalledFunction(ORCAMutexTrylockFunction);
  }else if(calledFunctionName.compare("pthread_mutex_unlock") == 0){
    assert(ORCAMutexUnlockFunction != NULL);
    inst->setCalledFunction(ORCAMutexUnlockFunction);
  }else if(calledFunctionName.compare("pthread_cond_wait") == 0){
    assert(ORCACondWaitFunction != NULL);
    inst->setCalledFunction(ORCACondWaitFunction);
  }else if(calledFunctionName.compare("pthread_cond_signal") == 0){
    assert(ORCACondSignalFunction != NULL);
    inst->setCalledFunction(ORCACondSignalFunction);
  }else if(calledFunctionName.compare("pthread_cond_broadcast") == 0){
    assert(ORCACondBroadcastFunction != NULL);
    inst->setCalledFunction(ORCACondBroadcastFunction);
  }
  return true;
}

bool ORCATool::HandleCallInst(BasicBlock& bb, CallInst *inst){
  std::string calledFunctionName = inst->getCalledFunction()->getName().str();
  SmallVector<Value*,0> operands;
  if(calledFunctionName.compare("pthread_create") == 0){
    assert(ORCAThreadCreateFunction != NULL);
    inst->setCalledFunction(ORCAThreadCreateFunction);
  }else if(calledFunctionName.compare("pthread_join") == 0){
    assert(ORCAThreadJoinFunction != NULL);
    inst->setCalledFunction(ORCAThreadJoinFunction);
  }else if(calledFunctionName.compare("pthread_barrier_wait") == 0){
    assert(ORCABarrierWaitFunction != NULL);
    inst->setCalledFunction(ORCABarrierWaitFunction);
  }else if(calledFunctionName.compare("pthread_mutex_lock") == 0){
    assert(ORCAMutexLockFunction != NULL);
    inst->setCalledFunction(ORCAMutexLockFunction);
    return false;
  }else if(calledFunctionName.compare("pthread_mutex_trylock") == 0){
    assert(ORCAMutexTrylockFunction != NULL);
    inst->setCalledFunction(ORCAMutexTrylockFunction);
    return false;
  }else if(calledFunctionName.compare("pthread_mutex_unlock") == 0){
    assert(ORCAMutexUnlockFunction != NULL);
    inst->setCalledFunction(ORCAMutexUnlockFunction);
  }else if(calledFunctionName.compare("pthread_cond_wait") == 0){
    assert(ORCACondWaitFunction != NULL);
    inst->setCalledFunction(ORCACondWaitFunction);
  }else if(calledFunctionName.compare("pthread_cond_signal") == 0){
    assert(ORCACondSignalFunction != NULL);
    inst->setCalledFunction(ORCACondSignalFunction);
  }else if(calledFunctionName.compare("pthread_cond_broadcast") == 0){
    assert(ORCACondBroadcastFunction != NULL);
    inst->setCalledFunction(ORCACondBroadcastFunction);
  }
  return true;
}

}
