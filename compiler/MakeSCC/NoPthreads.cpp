// (C) 2010 University of Washington.
// The software is subject to an academic license.
// Terms are contained in the LICENSE file.
//
// A pass to look for calls to pthreads_* functions

#include "llvm/Pass.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstVisitor.h"
#include <iostream>

using namespace llvm;

namespace {

  struct NoPthreadsVisitor : public InstVisitor<NoPthreadsVisitor> {
    NoPthreadsVisitor() {}

    void checkForPthreads(CallSite C) {
      if ( Function* fun = C.getCalledFunction() ) {
        if ( fun->getName().find( "pthread_" ) == 0 && 
             fun->getName().find( "pthread_mutexattr_" ) == std::string::npos &&
             fun->getName().find( "pthread_attr_" ) == std::string::npos
             ) {
          std::string location = C.getInstruction()->getParent()->getParent()->getName();
          std::cerr << "[NoPthreads] ERROR: calling " << fun->getName().data() << "() in " << location << "() \n" ;
          exit( 42 );
        }
      }
    }
  
    void visitCallInst(CallInst &I) { checkForPthreads( CallSite(&I) ); }
    void visitInvokeInst(InvokeInst &I) { checkForPthreads( CallSite(&I) ); }
  };

  struct NoPthreads : public BasicBlockPass {
    
    static char ID;
    NoPthreads() : BasicBlockPass(ID) {}

    virtual bool runOnBasicBlock(BasicBlock &BB) {
      NoPthreadsVisitor NPV;
      NPV.visit( BB );
      return false;
    }

  };

  char NoPthreads::ID = 0;
  RegisterPass<NoPthreads> X("no-pthreads", "Assert there are no pthread_* calls.");
}
