// Implements inter-procedural CFG (for disjoint thread section analysis)
// released under the University of Santa Cruz Open Source License

#ifndef _SCC_ICFG_H_
#define _SCC_ICFG_H_

#include "dsa/DataStructure.h"
#include "dsa/DSGraph.h"
#include "dsa/DSNode.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/DenseSet.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Support/Debug.h"
#include <map>
#include <vector>
#include <limits.h>

using std::vector;
using std::map;

class ThreadEscapeAnalysis;


// Node for ICFG
// No backedges ( predecessors)
class SccICFGNode
{
   BasicBlock *_bb;
   int instrStartPos;
   int instrEndPos;
   int _syncNodeId; // syncNodeId. -1 if not sync node.

   vector<SccICFGNode *> succs;
   int inEdgeCount;  // don't need predecessors, but need in edge count
   // vector<ICFGNode *> preds; // might not need this.

  public:
    SccICFGNode(BasicBlock *bb, int startPos = 0, int endPos = INT_MAX)  
    { 
      _bb = bb; 
      instrStartPos = startPos; instrEndPos = endPos; 
      _syncNodeId = -1;
      inEdgeCount = 0;
    }
    ~SccICFGNode() {}
    void addEdgeTo(SccICFGNode *to) { 
      succs.push_back(to); 
      to->incInCnt(); 
    }
    // void addEdgeFrom(SccICFGNode *from) { preds.push_back(to); }

    void setStartPos(int pos) { instrStartPos = pos; }
    void setEndPos(int pos) { instrEndPos = pos; }
    void setSyncNodeId(int id) { _syncNodeId = id; }
    int getSyncNodeId() { return _syncNodeId; }
    bool isSyncNode()  { return (_syncNodeId >= 0); }

    void incInCnt() { inEdgeCount++; }
    int  getInCnt() { return inEdgeCount; }
    BasicBlock *getBB() { return _bb; }
    int getStartPos() { return instrStartPos; }
    int getEndPos() { return instrEndPos; }

    vector<SccICFGNode *>& getSuccs() { return succs; }

};

// Info on which node is function entry etc. This is for demarcating functions 
// within the ICFG.
struct SccICFGFuncInfo
{
   SccICFGNode *entry;
   vector<SccICFGNode *> returnNodes;

   SccICFGFuncInfo(SccICFGNode *ent, vector<SccICFGNode *>& rets) 
   {
     entry = ent; 
     returnNodes = rets;
   }
};

class SccICFG
{
    ThreadEscapeAnalysis *EA;
    vector<SccICFGNode *> tsStarts;
    vector<SccICFGNode *> tsEnds;

    vector<SccICFGNode *> _nodes;

    // Populate this after creating icfg for this.DO NOT create entry for top TS
    map<const Function *, SccICFGFuncInfo *> icfgInfoForFunction;

    // Always map sync_all calls to these global nodes
    map<int, SccICFGNode *> _syncAllNodes;

    // This is used in disjoint thread section identification within a TS
    map<const Function *, bool> _functionHasSyncAlls; // either F or its descendant has sync_all
    map<const Function *, SplicedCFG *> _functionIcfgs; // hybrid icfg for function 

    bool hasSyncAll(Function* func, set<Function *>& visitedFuncs); // visitedFuncs to avoid recursions
    bool hasSyncAll(Function* func) {
      set<Function *> visitedFuncs;
      return hasSyncAll(func, visitedFuncs);
    }

    void markFunctionsWithSyncAlls(Module& M);

    typedef enum { NO_SPLIT = 0, THREAD_CREATE_CALL = 1, FUNC_CALL = 2, SYNC_CALL = 3} funcCallType;

    funcCallType classifyInstr(Instruction *inst, vector<Function *>& calledFuncs);
    int getInstrPos(const BasicBlock *bb, const Instruction *inst);
    SccICFGNode *getNewNode(BasicBlock *bb, int startPos, int endPos);
    SccICFGNode *getSyncNode(Instruction *inst);

    Function *_syncAllFunc; // to determine if  called function is a sync_all
    Function *_threadCreateFunc;
    Function *_threadStartFunc; // for stamp benchmarks
    Module *_module;

   // To link the final nodes, after processign each block at a time
   map<BasicBlock *, SccICFGNode *> _firstNode;
   map<BasicBlock *, SccICFGNode *> _lastNode;


  public:
    SccICFG(Module *M, ThreadEscapeAnalysis *ea, bool doBSA);
    ~SccICFG();

    void setModule(Module *m) { _module = m; }

    vector<SccICFGNode *>& getTsStarts() { return  tsStarts; }
    vector<SccICFGNode *>& getTsEnds() { return  tsEnds; }
    map<int, SccICFGNode *>& getSyncAllNodes() { return  _syncAllNodes; }

    // Add portion of func between start and end to ICFG
    SccICFGFuncInfo *addFunction(Function *func, BasicBlock *start, Instruction *startInstr,
           BasicBlock *end, Instruction *endInstr);

    // Collect the terminal vertices when forward traversing from the start node provided.
    void collectTerminalNodes(SccICFGNode *start, SccICFGNode *endTsNode, 
        set<SccICFGNode *>& terminals);

    void clear(); // clear all nodes and data, put back to empty state

};

#endif
