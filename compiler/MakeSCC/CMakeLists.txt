add_llvm_loadable_module( LLVMMakeSCC
  MakeSCC.cpp
  OptTsan.cpp
  NoPthreads.cpp
  ThreadEscapeAnalysis.cpp
  SccProfile.cpp
  identifyTS.cpp
  LoopHelper.cpp
  AccessInfo.cpp
  Tool.cpp
  )
