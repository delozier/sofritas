import sys,os,subprocess

args = sys.argv[1:]

LLVM_ROOT= os.environ["LLVM_ROOT"]
LLVM_BIN = LLVM_ROOT + os.environ["LLVM_BIN"]

output_file = ""
output_index = -1

linking = False

realArgs = []
output_files = []
libs = []
checkNext = False
output_file = ""
source_output_file = ""
annotations_object = ""

for arg in args:
    if checkNext:
        checkNext = False
        if not arg.endswith(".o"):
            linking = True
            output_file = arg

    if arg == "-o":
        checkNext = True

checkNext = False
hadO = False
if linking:
    for arg in args:
        if arg.startswith("-l"):
            libs.append(arg)
            continue

        if arg.startswith("-L"):
            libs.append(arg)
            continue

        if arg.endswith("annotations.o"):
            annotations_object = arg
            continue

        if arg.endswith(".o"):
            output_files.append(arg.replace(".o","_instr.bc"))
            continue

        if arg == "-o":
            checkNext = True
            continue

        if checkNext:
            checkNext = False
            continue

        realArgs.append(arg)
else:
    for arg in args:
        if arg == "-o":
            hadO = True
            continue

        if arg.endswith(".o"):
            output_files.append(arg.replace(".o",".bc"))
        else:
            realArgs.append(arg)

        if arg.endswith(".c"):
            source_output_file = arg.replace(".c",".bc")
        
        if arg.endswith(".cpp"):
            source_output_file = arg.replace(".cpp",".bc")

if linking:
    # Link mode, produce final executable

    if link_lib == "":
        command = [LLVM_BIN + "llvm-link","-o","instrumented.bc",SOFRITAS_ROOT + "/lib/sofritas/sofritas.bc"]
    else:
        command = [LLVM_ROOT + "llvm-link","-o","instrumented.bc"]

    if len(output_files) > 0:
        command.extend(output_files)
    print ' '.join(command)
    subprocess.call(command)

    file_in = "instrumented.bc"
    if link_lib == "":
        command = LLVM_BIN + "opt -inline < instrumented.bc > opt.bc"
        print command
        os.system(command)
        file_in = "opt.bc"

    command = [LLVM_BIN + "llc",file_in,"-o","instrumented.s"]
    print ' '.join(command)
    subprocess.call(command)

    command = [LLVM_BIN +  "clang++","instrumented.s","-ldl","-pthread","-lpthread",SOFRITAS_ROOT + "/lib/gperftools/libtcmalloc_minimal.a",link_lib,"-o",]
    command.append(output_file)
    command.extend(libs)
    print ' '.join(command)
    subprocess.call(command)
else:
    # Compile mode
    command = [LLVM_BIN + compiler,"-emit-llvm","-I" + SOFRITAS_ROOT + "/include/"]
    command.extend(realArgs)

    if hadO:
        command.append("-o")
        command.extend(output_files)
    else:
        command.append("-o")
        command.append(source_output_file)

    print ' '.join(command)
    subprocess.call(command)

    output_file = ""
    output_file_orig = ""
    if hadO:
        output_file_orig = output_files[0]
        output_file = output_file_orig.replace(".bc","_instr.bc")
    else:
        output_file = ""
        for arg in realArgs:
            if arg.endswith(".cpp"):
                output_file_orig = arg.replace(".cpp",".bc")
                output_file = arg.replace(".cpp","_instr.bc")
            elif arg.endswith(".cc"):
                output_file_orig = arg.replace(".cc",".bc")
                output_file = arg.replace(".cc","_instr.bc")
            elif arg.endswith(".c"):
                output_file_orig = arg.replace(".c",".bc")
                output_file = arg.replace(".c","_instr.bc")

    command = LLVM_BIN + "opt -load " + LLVM_BIN + "../lib/LLVMDataStructure.so -load " + LLVM_BIN + "../lib/LLVMMakeSCC.so -basicaa -libcall-aa -enable-pie -makescc < "
    command = command + output_file_orig
    command = command + " > " + output_file
    print command
    os.system(command)
