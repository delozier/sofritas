#!/usr/bin/python

import os

SOFRITAS_ROOT = os.environ["SOFRITAS_ROOT"]
compiler = "clang++"
link_lib = SOFRITAS_ROOT + "/lib/libsofritas.so"
execfile(SOFRITAS_ROOT + "/scripts/compile.py")
