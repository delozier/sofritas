#pragma once

#include <mutex>

#include <sofritas/config.hpp>
#include <sofritas/types.hpp>


#define IN_SOFRITAS_HEAP(a) (a >= sofritas::Heap::_slabStart && a < sofritas::Heap::_slabEnd)

namespace sofritas{

class Heap{
  static bool _initialized;
  static char* _slabPos;
  static char* _mmapPos;
  static std::mutex _slabLock;

public:
  static address _slabStart;
  static address _slabEnd;

  static std::mutex _hugePagesLock;
  static uint64_t _remainingHugePages;

  static bool reserveHugePages(uint64_t size);

  static void InitializeHeap(memory_size size);

  static void* HandleSBRK(size_t size);

  // NOTE: addr, flags, fd, and offset will most likely be ignored
  // because we can't adjust these things within our monolithic
  // allocated heap
  static void* HandleMMAP(void *addr, size_t size, int prot,
			  int flags, int fd, off_t offset);

  static int HandleMUNMAP(void *addr, size_t size);
};

}
