#pragma once

#include <sofritas/types.hpp>
#include <atomic>

#include <sys/mman.h>

namespace sofritas{

class ShadowSpace{
  std::atomic<char *> _base;
  address _memoryBase;
  size_t _size;
  size_t _shadowSize;
  size_t _shadowPerByte;
  size_t _shift;
  
  address getIndex(address addr);

public:
  void Initialize(address base, size_t size, size_t shadowSize, 
		  size_t shadowPerByte);
  char* getMetadata(address addr);
  uint64_t getBase();
  void Clear();
  void ClearSFR();
  void HandleReallocate(void *p, size_t size);
};

}
