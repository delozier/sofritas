#pragma once

#include <pthread.h>

#include <atomic>
#include <mutex>
#include <unordered_map>
#include <queue>
#include <vector>

#include <sofritas/emulated_cond_var.hpp>
#include <sofritas/types.hpp>
#include <sofritas/thread.hpp>
#include <sofritas/thread_state.hpp>

#define INTERNAL_HEAP_SIZE (1048576 * 32)
#define THREAD_HEAP_SIZE (HEAP_SIZE / 4)

namespace sofritas{

class Runtime{
  Runtime() = delete;
private:
  static TID _nextTid;
  static std::queue<TID> _reuseTids;
  static std::mutex _tidLock;

  static std::unordered_map<pthread_cond_t*,emulated_cond_var*> _condition_variables;
  static std::mutex _cond_var_lock;

  static bool _initialized;

  static emulated_cond_var* getCondVar(pthread_cond_t *cond);
  static void initShadowMem();

public:
  static void handleMalloc(void *p, size_t size);
  static void setThreadStatus(ThreadState state);

  static void *get_internal_memory(size_t size);

  static TID getTidBit(TID tid);

  static std::atomic<lock_state_type>* getLockState(uint64_t addr);
  static char* getThreadLock(uint64_t addr);
  static char* getSpecificThreadLock(uint64_t addr, TID tid);
  static void waitOnThreadLock(uint64_t addr,TID tid, bool isWrite);

  static void init();
  static void cleanup();
  
  static bool acquireLock(size_t addr, bool isWrite);
  
  static void releaseAll(bool isLock = false);
  static bool releaseLock(size_t addr);
  static void releaseObject(size_t addr, size_t size);
  static void releaseArray(size_t addr, size_t size, size_t num);

  static void clearLock(size_t addr);

  static void requireMutex(size_t addr);

  static void disableSOFRITAS();
  static void enableSOFRITAS();

  static void threadStart();
  static void threadExit();

  static void beforeJoin();
  static void afterJoin();

  static void beforeBarrier();
  static void afterBarrier();

  static void condWait(pthread_cond_t *cond, pthread_mutex_t *mutex);
  static void condSignal(pthread_cond_t *cond);
  static void condBroadcast(pthread_cond_t *cond);
};

}
