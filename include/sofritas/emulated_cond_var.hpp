#pragma once

#include <queue>
#include <atomic>
#include <mutex>
#include <chrono>
#include <thread>
#include <iostream>

#include <sofritas/lock.hpp>

class cond_var_node{
  std::atomic<bool> _awake;

public:
  cond_var_node() : _awake(false) {
  }

  void wakeup(){
    _awake = true;
  }

  bool isAwake(){
    return _awake;
  }
};

class emulated_cond_var{
  std::queue<cond_var_node*> _waiters;
  std::mutex _lock;

public:
  cond_var_node* init_wait(cond_var_node *node){
    _lock.lock();
    _waiters.push(node);
    _lock.unlock();
    return node;
  }

  void wait(cond_var_node *node){
    while(!node->isAwake()){
      //std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
  }

  void signal(){
    cond_var_node *node = nullptr;

    _lock.lock();
    if(!_waiters.empty()){
      node = _waiters.front();
    }    
    if(node != nullptr){
      _waiters.pop();
    }
    _lock.unlock();

    if(node != nullptr){
      node->wakeup();
    }
  }

  void broadcast(){
    cond_var_node *node = nullptr;

    _lock.lock();
    while(!_waiters.empty()){
      node = _waiters.front();
      _waiters.pop();
      node->wakeup();
    }
    _lock.unlock();
  }
};
