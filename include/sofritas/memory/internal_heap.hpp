#pragma once

#include <sofritas/config.hpp>
#include <sofritas/types.hpp>

namespace sofritas{

class InternalHeap{
  static bool _initialized;
  static char* _slabPos;

public:
  static address _slabStart;
  static address _slabEnd;

  static void Initialize(memory_size size);

  static void* internal_malloc(size_t size);
  static void internal_free(void *p);
};

}
