#pragma once

#include <stdlib.h>

void SOFRITAS_begin_lib_read(void *addr) __attribute__((noinline));
void SOFRITAS_end_lib_read(void *addr) __attribute__((noinline));

void SOFRITAS_begin_lib_write(void *addr) __attribute__((noinline));
void SOFRITAS_end_lib_write(void *addr) __attribute__((noinline));
 
void SOFRITAS_print_lock(void *addr) __attribute__((noinline));

void SOFRITAS_set_stage(int stage) __attribute__((noinline));

void SOFRITAS_print_debug(const char *message) __attribute__((noinline));

/*
 * User annotation for releasing the lock on the base
 * address of an object
 *
 * Parameters: Address of the object to be released
 * Returns: int
 *   true - Lock was successfully released
 *   false - Lock was not found for the given object
 */

void SOFRITAS_END_OFR() __attribute__((noinline));

int SOFRITAS_release(void *addr) __attribute__((noinline));

int SOFRITAS_clear(void *addr) __attribute__((noinline));

/*
 * User annotation for releasing the lock on all
 * fields of an object
 *
 * Parameters: Address of the object to be released
 * Returns: int
 *   true - Locks were successfully released
 *   false - Locks were not found for the given object
 */

int SOFRITAS_release_object(void * addr, size_t size) __attribute__((noinline));

/*
 * User annotation for releasing the lock on all elements
 * of an array
 *
 * Parameter: Address of the array to be released
 * Parameter: Number of elements in the array
 *
 * Returns: int
 *   true - Locks were successfully released
 *   false - Locks were not found for the given object
 */

int SOFRITAS_release_array(void * addr, size_t size, size_t num) __attribute__((noinline));

/*
 * User annotation to specify that a location must use a 
 * mutex lock instead of a reader-writer lock
 *
 * This annotation is required for some variables when the variable
 * needs 
 *
 * Parameter: Address of the object
 */

int SOFRITAS_require_mutex_lock(void * addr) __attribute__((noinline));

void SOFRITAS_allocate(void *addr, size_t size) __attribute__((noinline));

void SOFRITAS_disable() __attribute__((noinline));

void SOFRITAS_enable() __attribute__((noinline));

int SOFRITAS_barrier_no_release(pthread_barrier_t *barrier);
