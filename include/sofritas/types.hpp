#pragma once

#include <cstdint>
#include <cstdlib>

typedef unsigned int TID;
typedef uint64_t address;
typedef uint64_t memory_size;

typedef uint16_t lock_state_type;
typedef unsigned char thread_state_type;
