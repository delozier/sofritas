#pragma once

#include <cassert>

#include <atomic>
#include <chrono>
#include <list>
#include <mutex>
#include <set>
#include <thread>
#include <iostream>
#include <cstdint>

#include <sofritas/thread_state.hpp>
#include <sofritas/lock.hpp>

namespace sofritas{

class AtomicThread{
  unsigned int _tid;
  ThreadState _state;
  int _disabled;

public:
  uint64_t _currentAddr;
  bool _currentIsWrite;

  std::list<void*> _usedPages;

public:
  AtomicThread(int tid) : 
    _tid(tid)
    ,_state(RUNNING)
    ,_disabled(0)
    ,_currentAddr(0)
    ,_currentIsWrite(false)
  {
  }

  void disable()
  {
    ++_disabled;
  }

  void enable()
  {
    --_disabled;
  }

  bool isDisabled(){
    return _disabled;
  }

  void setState(ThreadState state){
    _state = state;
  }

  unsigned int getTid(){
    return _tid;
  }

  std::string stateToString(){
    if(_state == RUNNING){
      return "RUNNING";
    }else if(_state == BLOCKED){
      return "BLOCKED";
    }else if(_state == JOINED){
      return "JOINED";
    }else if(_state == EXITED){
      return "EXITED";
    }else if(_state == WAITING){
      return "WAITING";
    }else if(_state == BARRIER){
      return "BARRIER";
    }else if(_state == CAS){
      return "CAS";
    }else if(_state == UPDATING){
      return "UPDATING";
    }else{
      return "OTHER";
    }
  }

  void printState(std::ostream &out){
    out << "Thread " << _tid << ": [";
    out << "State = " << stateToString() << ", ";

    // TODO: Add information about current source location
  }

  bool isBlocked(){
    return _state == BLOCKED;
  }

  bool inWaitingState(){
    return _state == JOINED ||
      _state == EXITED || 
      _state == WAITING ||
      _state == BARRIER;
  }
};

}
