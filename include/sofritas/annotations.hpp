#pragma once

#include <pthread.h>

/*
 * Objects for STL code that insert calls on construction and destruction
 * to disable and enable SOFRITAS and grab locks for the type of STL function
 */
 
extern "C" void SOFRITAS_begin_lib_read(void *addr) __attribute__((noinline));
extern "C" void SOFRITAS_end_lib_read(void *addr) __attribute__((noinline));

extern "C" void SOFRITAS_begin_lib_write(void *addr) __attribute__((noinline));
extern "C" void SOFRITAS_end_lib_write(void *addr) __attribute__((noinline));

extern "C" void SOFRITAS_set_stage(int stage) __attribute__((noinline));

extern "C" void SOFRITAS_print_debug(const char *message) __attribute__((noinline));

template<typename unused>
class SOFRITAS_read_inserter{
  void *_addr;
public:
  SOFRITAS_read_inserter(void *addr) : _addr(addr)
  {
    SOFRITAS_begin_lib_read(_addr);
  }

  ~SOFRITAS_read_inserter()
  {
    SOFRITAS_end_lib_read(_addr);
  }
};

template<class unused>
class SOFRITAS_write_inserter{
  void *_addr;
public:
  SOFRITAS_write_inserter(void *addr) : _addr(addr)
  {
    SOFRITAS_begin_lib_write(_addr);
  }

  ~SOFRITAS_write_inserter()
  {
    SOFRITAS_end_lib_write(_addr);
  }
};

extern "C" void SOFRITAS_print_lock(void *addr);

/*
 * User annotation for releasing the lock on the base
 * address of an object
 *
 * Parameters: Address of the object to be released
 * Returns: int
 *   true - Lock was successfully released
 *   false - Lock was not found for the given object
 */

extern "C" void SOFRITAS_END_OFR();

extern "C" int SOFRITAS_release(void *addr);

extern "C" void SOFRITAS_clear(void *addr);

/*
 * User annotation for releasing the lock on all
 * fields of an object
 *
 * Parameters: Address of the object to be released
 * Returns: int
 *   true - Locks were successfully released
 *   false - Locks were not found for the given object
 */

extern "C" void SOFRITAS_release_object(void * addr, unsigned long size);

/*
 * User annotation for releasing the lock on all elements
 * of an array
 *
 * Parameter: Address of the array to be released
 * Parameter: Number of elements in the array
 *
 * Returns: int
 *   true - Locks were successfully released
 *   false - Locks were not found for the given object
 */

extern "C" void SOFRITAS_release_array(void * addr, unsigned long size, unsigned long num);

/*
 * User annotation to specify that a location must use a 
 * mutex lock instead of a reader-writer lock
 *
 * This annotation is required for some variables when the variable
 * needs 
 *
 * Parameter: Address of the object
 */

extern "C" int SOFRITAS_require_mutex_lock(void * addr);

extern "C" void SOFRITAS_allocate(void *addr, unsigned long size);

extern "C" void SOFRITAS_disable();

extern "C" void SOFRITAS_enable();

extern "C" int SOFRITAS_barrier_no_release(pthread_barrier_t *barrier);
