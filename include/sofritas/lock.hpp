#pragma once

#include <atomic>
#include <cstdint>
#include <iostream>

#include <mutex>

#include <sofritas/types.hpp>

#define LOCK_ORDER std::memory_order::memory_order_seq_cst

// Assumes 4 state bits for the lock, adjust if this changes
#define SOFRITAS_LOCK_OWNER(l) l >> 4;

namespace sofritas{

class Lock{
  static std::mutex g_lock;

  static void waitOnThreadLock(uint64_t addr, TID tid, bool isWrite);

  static thread_state_type getThreadLockMask(uint64_t addr);
  static void acquireThreadLock(thread_state_type *threadLock, 
				uint64_t addr,bool isWrite);
  static void releaseThreadLock(thread_state_type *threadLock, 
				uint64_t addr);
  static bool checkThreadLock(thread_state_type *threadLock, 
			      uint64_t addr,bool isWrite);
public:
  static constexpr lock_state_type UPDATING = 0x1;
  static constexpr lock_state_type MAYBE_HELD = 0x2;
  static constexpr lock_state_type MUTEX = 0x4;
  static constexpr lock_state_type WRITE = 0x8;

#ifdef SOFRITAS_LOCK_STATS
  static std::atomic<unsigned long> loadChecks;
  static std::atomic<unsigned long> storeChecks;

  static std::atomic<unsigned long> loadAcquires;
  static std::atomic<unsigned long> storeAcquires;

  static std::atomic<unsigned long> unlockedAcquires;
  static std::atomic<unsigned long> exclusiveReadAcquires;
  static std::atomic<unsigned long> sharedReadAcquires;
  static std::atomic<unsigned long> writeAcquires;

  static std::atomic<unsigned long> reacquires;

  static std::atomic<unsigned long> releases;
  static std::atomic<unsigned long> batchReleases;
#endif

  static constexpr thread_state_type THREAD_LOCK_MASKS[4] = 
    {0x03, 0x0C, 0x30, 0xC0};

  static constexpr int MAX_THREADS = 128;

  static std::atomic<lock_state_type>* getLockState(address addr);
  static thread_state_type* getThreadLock(address addr, TID tid);

  // WARNING: Must be called before any acquires have occurred
  static void requireMutex(uint64_t addr);
  static bool acquire(uint64_t addr, TID tid, bool isWrite);
  static bool acquireHeapLock(uint64_t addr, TID tid, bool isWrite);
  static bool acquireGlobalLock(uint64_t addr, TID tid, bool isWrite); 
  static bool isOwner(uint64_t addr, TID tid, bool isWrite);
  static bool release(uint64_t addr, TID tid);
  static void clear(uint64_t addr);

  static void validateState(address addr);
 
  static void printState(uint64_t addr, std::ostream &out);
};
  
}
