#pragma once

namespace sofritas{

enum ThreadState{
  RUNNING,
  BLOCKED,
  JOINED,
  EXITED,
  WAITING,
  BARRIER,
  CAS,
  UPDATING
};

}
