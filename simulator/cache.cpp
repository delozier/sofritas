/** $lic$
 * Copyright (C) 2012-2014 by Massachusetts Institute of Technology
 * Copyright (C) 2010-2013 by The Board of Trustees of Stanford University
 *
 * This file is part of zsim.
 *
 * zsim is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, version 2.
 *
 * If you use this software in your research, we request that you reference
 * the zsim paper ("ZSim: Fast and Accurate Microarchitectural Simulation of
 * Thousand-Core Systems", Sanchez and Kozyrakis, ISCA-40, June 2013) as the
 * source of the simulator in any publications that use this software, and that
 * you send us a citation of your work.
 *
 * zsim is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "cache.h"
#include "hash.h"

Cache::Cache(uint32_t _numLines, CC* _cc, CacheArray* _array, ReplPolicy* _rp, uint32_t _accLat, uint32_t _invLat, const std::string& _name)
  : cc(_cc), array(_array), rp(_rp), numLines(_numLines), accLat(_accLat), invLat(_invLat), name(_name), 
    m_cycles(0), m_instructions(0), m_hits(0), m_misses(0), 
    m_l1_hits(0), m_l1_misses(0), m_l2_hits(0), m_l2_misses(0),
    m_l3_hits(0), m_l3_misses(0){}

const char* Cache::getName() {
    return name.c_str();
}

void Cache::setParents(uint32_t childId, std::vector<MemObject*>& parents, Network* network) {
    cc->setParents(childId, parents, network);
}

void Cache::setChildren(std::vector<BaseCache*>& children, Network* network) {
    cc->setChildren(children, network);
}

uint64_t Cache::access(MemReq& req) {
    uint64_t respCycle = req.cycle;
    bool skipAccess = cc->startAccess(req); //may need to skip access due to races (NOTE: may change req.type!)
    if (!skipAccess) {
        bool updateReplacement = (req.type == GETS) || (req.type == GETX);
        int32_t lineId = array->lookup(req.lineAddr, &req, updateReplacement);

        respCycle += accLat;

        if (lineId == -1 && cc->shouldAllocate(req)) {
          m_misses++;
            //Make space for new line
            Address wbLineAddr;
            lineId = array->preinsert(req.lineAddr, &req, &wbLineAddr); //find the lineId to replace

            //Evictions are not in the critical path in any sane implementation -- we do not include their delays
            //NOTE: We might be "evicting" an invalid line for all we know. Coherence controllers will know what to do
            cc->processEviction(req, wbLineAddr, lineId, respCycle); //1. if needed, send invalidates/downgrades to lower level
            array->postinsert(req.lineAddr, &req, lineId); //do the actual insertion. NOTE: Now we must split insert into a 2-phase thing because cc unlocks us.
        }else{
          m_hits++;
        }

        respCycle = cc->processAccess(req, lineId, respCycle);
    }
    cc->endAccess(req);

    switch(respCycle){
    case 1:
      m_l1_hits++;
      break;
    case 10:
      m_l1_misses++;
      m_l2_hits++;
      break;
    case 16:
      m_l1_misses++;
      break;
    case 25:
      m_l1_misses++;
      m_l2_misses++;
      break;
    case 35:
      m_l1_misses++;
      m_l2_misses++;
      m_l3_hits++;
      break;
    case 120:
      m_l1_misses++;
      m_l2_misses++;
      m_l3_misses++;
      break;
    default:
      break;
    }

    return respCycle;
}

uint64_t Cache::invalidate(Address lineAddr, InvType type, bool* reqWriteback, uint64_t reqCycle, uint32_t srcId) {
    cc->startInv(); //note we don't grab tcc; tcc serializes multiple up accesses, down accesses don't see it

    int32_t lineId = array->lookup(lineAddr, NULL, false);
    uint64_t respCycle = reqCycle + invLat;
    respCycle = cc->processInv(lineAddr, lineId, type, reqWriteback, respCycle, srcId); //send invalidates or downgrades to children, and adjust our own state

    return respCycle;
}

