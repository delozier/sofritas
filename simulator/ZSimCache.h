#pragma once

#include <iostream>
#include <vector>

#include "mem_ctrls.h"
#include "cache.h"
#include "hash.h"

class ZSimCache{
  const int L1_HIT_LATENCY = 1;
  const int REMOTE_HIT_LATENCY = 15;
  const int L2_HIT_LATENCY = 9;
  const int L3_HIT_LATENCY = 25;
  const int MEM_LATENCY = 85;

  unsigned long _numCaches;

  unsigned long _L1size;
  unsigned long _L1blockSize;
  unsigned long _L1assoc;

  unsigned long _L2size;
  unsigned long _L2blockSize;
  unsigned long _L2assoc;

  unsigned long _L3size;
  unsigned long _L3blockSize;
  unsigned long _L3assoc;

  std::vector<Cache*> L1Caches;
  std::vector<Cache*> L2Caches;
  Cache *L3Cache;
  MemObject *mem;
public:
  
  ZSimCache(unsigned long numCaches, 
            unsigned long L1size, unsigned long L1blockSize, unsigned long L1assoc,
            unsigned long L2size, unsigned long L2blockSize, unsigned long L2assoc,
            unsigned long L3size, unsigned long L3blockSize, unsigned long L3assoc) :
  _numCaches(numCaches),
    _L1size(L1size), _L1blockSize(L1blockSize), _L1assoc(L1assoc),
    _L2size(L2size), _L2blockSize(L2blockSize), _L2assoc(L2assoc),
    _L3size(L3size), _L3blockSize(L3blockSize), _L3assoc(L3assoc)
  {
    unsigned long l1Lines = L1size / L1blockSize;
    unsigned long l2Lines = L2size / L2blockSize;
    unsigned long l3Lines = L3size / L3blockSize;

    std::string memstring = "mem";

    mem = new SimpleMemory(MEM_LATENCY,memstring);

    std::string l1string = "L1";
    std::string l2string = "L2";
    std::string l3string = "L3";

    LRUReplPolicy<true> *l3ReplPolicy = new LRUReplPolicy<true>(l3Lines);
    MESICC *l3CC = new MESICC(l3Lines,false,l3string);
    l3ReplPolicy->setCC(l3CC);

    L3Cache = new Cache(l3Lines,l3CC,
                        new SetAssocArray(l3Lines,_L3assoc,l3ReplPolicy,
                                          new IdHashFamily()),
                        l3ReplPolicy,L3_HIT_LATENCY,0,l3string);

    std::vector<MemObject*> l3Parents;
    std::vector<BaseCache*> l3Children;

    l3Parents.push_back(mem);

    for(unsigned long c = 0; c < numCaches; c++){
      LRUReplPolicy<true> *replPolicy = new LRUReplPolicy<true>(l1Lines);
      MESITerminalCC *l1CC = new MESITerminalCC(l1Lines,l1string);
      replPolicy->setCC(l1CC);
      
      Cache *cache = new Cache(l1Lines,l1CC,
                               new SetAssocArray(l1Lines,_L1assoc,replPolicy,
                                                 new IdHashFamily()),
                               replPolicy,L1_HIT_LATENCY,REMOTE_HIT_LATENCY,l1string);
      L1Caches.push_back(cache);

      std::vector<MemObject*> l1Parents;

      LRUReplPolicy<true> *l2ReplPolicy = new LRUReplPolicy<true>(l2Lines);
      MESICC *l2CC = new MESICC(l2Lines,false,l2string);
      l2ReplPolicy->setCC(l2CC);

      Cache *l2cache = new Cache(l2Lines,l2CC,
                                 new SetAssocArray(l2Lines,_L2assoc,l2ReplPolicy,
                                                   new IdHashFamily()),
                                 l2ReplPolicy,L2_HIT_LATENCY,REMOTE_HIT_LATENCY,l2string);

      L2Caches.push_back(cache);

      std::vector<MemObject*> l2Parents;
      std::vector<BaseCache*> l2Children;

      l1Parents.push_back(l2cache);
      l2Children.push_back(cache);

      l2Parents.push_back(L3Cache);
      l3Children.push_back(l2cache);

      cache->setParents(0,l1Parents,NULL);
      
      l2cache->setParents(c,l2Parents,NULL);
      l2cache->setChildren(l2Children,NULL);
    }

    L3Cache->setParents(0,l3Parents,NULL);
    L3Cache->setChildren(l3Children,NULL);
  }

  int readLine(int tid, size_t inst, size_t addr)
  {
    int core = TidToCore(tid);

    MESIState state = I;
    MemReq req = {addr,GETS,core,&state,0,state,core,0};

    uint64_t cycles = L1Caches[core]->access(req);
    return cycles;
  }

  int writeLine(int tid, size_t inst, size_t addr)
  {
    int core = TidToCore(tid);

    MESIState state = I;
    MemReq req = {addr,GETX,core,&state,0,state,core,0};

    uint64_t cycles = L1Caches[core]->access(req);
    return cycles;
  }

  void updateCycles(int tid, uint64_t cycles, bool isLock = false, bool isBlocked = false, bool isNonMem = false)
  {
    int core = TidToCore(tid);
    L1Caches[core]->m_cycles += cycles;

    if(isLock){
      L1Caches[core]->m_lock_cycles += cycles;
    }

    if(isBlocked){
      L1Caches[core]->m_blocked_cycles += cycles;
    }

    if(isNonMem){
      L1Caches[core]->m_nonmem_cycles += cycles;
    }
  }

  void incrementInstructions(int tid)
  {
    int core = TidToCore(tid);
    L1Caches[core]->m_instructions++;
  }

  uint64_t getCycles(int tid)
  {
    int core = TidToCore(tid);
    return L1Caches[core]->m_cycles;
  }

  void setCycles(int tid, uint64_t cycles)
  {
    int core = TidToCore(tid);
    L1Caches[core]->m_cycles = cycles;
  }

  void printStats(std::ostream *outFile)
  {
      *outFile << "cpus = []\n\n";
      *outFile << "class CPU:\n";
      *outFile << "  def __init__(self):\n";
      *outFile << "    self.id = 0\n";
      *outFile << "    self.instructions = 0\n";
      *outFile << "    self.read_hits = 0\n";
      *outFile << "    self.read_misses = 0\n";
      *outFile << "    self.read_on_invalid_misses = 0\n";
      *outFile << "    self.read_requests_sent = 0\n";
      *outFile << "    self.read_misses_serviced_remotely = 0\n";
      *outFile << "    self.read_misses_serviced_by_shared = 0\n";
      *outFile << "    self.read_misses_serviced_by_modified = 0\n";
      *outFile << "    self.write_hits = 0\n";
      *outFile << "    self.write_misses = 0\n"; 
      *outFile << "    self.write_on_shared_misses = 0\n";
      *outFile << "    self.write_on_invalid_misses = 0\n";
      *outFile << "    self.invalidates_sent = 0\n";
      *outFile << "    self.l2_hits = 0\n";
      *outFile << "    self.l2_misses = 0\n";
      *outFile << "    self.l3_hits = 0\n";
      *outFile << "    self.l3_misses = 0\n";
      *outFile << "    self.cycles = 0\n";
      *outFile << "\n\n";
      
      *outFile << "lock_caches = []\n\n";
      *outFile << "class LockCache:\n";
      *outFile << "  def __init__(self):\n";
      *outFile << "    self.id = 0\n";
      *outFile << "    self.sector_hits = 0\n";
      *outFile << "    self.sector_misses = 0\n";
      *outFile << "    self.sector_fills = 0\n";
      *outFile << "    self.sector_updates = 0\n";
      *outFile << "    self.remote_invalidates = 0\n";
      *outFile << "\n\n";

    for(unsigned long c = 0; c < _numCaches; c++){
      *outFile << "cpu = CPU()\n";
      *outFile << "cpu.id = " << c << "\n";
      
      //*outFile << "cpu.read_hits = " << L1Caches[c]->m_hits << "\n";
      //*outFile << "cpu.read_misses = " << L1Caches[c]->m_misses << "\n";

      *outFile << "cpu.read_hits = " << L1Caches[c]->m_l1_hits << "\n";
      *outFile << "cpu.read_misses = " << L1Caches[c]->m_l1_misses << "\n";
      
      *outFile << "cpu.write_hits = " << 0 << "\n";
      *outFile << "cpu.write_misses = " << 0 << "\n";
      
      //*outFile << "cpu.l2_hits = " << L2Caches[c]->m_hits << "\n";
      //*outFile << "cpu.l2_misses = " << L2Caches[c]->m_misses << "\n";

      *outFile << "cpu.l2_hits = " << L1Caches[c]->m_l2_hits << "\n";
      *outFile << "cpu.l2_misses = " << L1Caches[c]->m_l2_misses << "\n";
      
      if(c == 0){
        //*outFile << "cpu.l3_hits = " << L3Cache->m_hits << "\n";
        //*outFile << "cpu.l3_misses = " << L3Cache->m_misses << "\n";
        *outFile << "cpu.l3_hits = " << L1Caches[c]->m_l3_hits << "\n";
        *outFile << "cpu.l3_misses = " << L1Caches[c]->m_l3_misses << "\n";
      }else{
        *outFile << "cpu.l3_hits = " << 0 << "\n";
        *outFile << "cpu.l3_misses = " << 0 << "\n";
      }
      
      *outFile << "cpu.cycles = " << L1Caches[c]->m_cycles << "\n";
      *outFile << "cpu.instructions = " << L1Caches[c]->m_instructions << "\n";
      *outFile << "cpu.lock_cycles = " << L1Caches[c]->m_lock_cycles << "\n";
      *outFile << "cpu.blocked_cycles = " << L1Caches[c]->m_blocked_cycles << "\n";
      *outFile << "cpu.nonmem_cycles = " << L1Caches[c]->m_nonmem_cycles << "\n";
      
      *outFile << "cpus.append(cpu)\n\n";
    }
  }

  int TidToCore(int tid){
    return tid % _numCaches;
  }
};
