#include "MultiSectorCacheSim.h"

SectorCache * MultiSectorCacheSim:: findSectorCacheByTid(int tid)
{
    int CPUid=tidToCPUId(tid);
    std::vector<SectorCache *>::iterator cacheIter = sectorCaches.begin();
    std::vector<SectorCache *>::iterator cacheEndIter = sectorCaches.end();
    for(; cacheIter != cacheEndIter; cacheIter++){
      if((*cacheIter)->CPUId == CPUid){
        return (*cacheIter);
      }
    }
    return nullptr;
	
}

void MultiSectorCacheSim::invalidateRemote(int tid, size_t addr)
{
  int CPUid = tidToCPUId(tid);
    std::vector<SectorCache *>::iterator cacheIter = sectorCaches.begin();
    std::vector<SectorCache *>::iterator cacheEndIter = sectorCaches.end();
    for(; cacheIter != cacheEndIter; cacheIter++){
      if((*cacheIter)->CPUId != CPUid){
        (*cacheIter)->remoteInvalidateLine(addr,tid);
      }
    }
}

void MultiSectorCacheSim::printStats(std::ostream &out)
{
  std::vector<SectorCache *>::iterator cacheIter = sectorCaches.begin();
  std::vector<SectorCache *>::iterator cacheEndIter = sectorCaches.end();
  for(; cacheIter != cacheEndIter; cacheIter++){
    (*cacheIter)->printStats(out);
  }
}

int MultiSectorCacheSim::tidToCPUId(int tid)
{
	return tid%num_caches;
}
