#ifndef __SMPCACHE_H_
#define __SMPCACHE_H_
#include "CacheCore.h"

#include <vector>

#include <iostream>
#include <fstream>
#include "SectorCache.h"
class SMPCache{

public:
  unsigned long CPUId;
  
  //The actual SESC cache object
  CacheGeneric<StateGeneric<> > *L1Cache;

  //A vector of all the caches in the multicachesim
  std::vector<SMPCache * > *allCaches;

  //Stats about the events the cache saw during execution
  long numReadHits;
  long numReadMisses;

  long numReadOnInvalidMisses;
  long numReadRequestsSent;
  long numReadMissesServicedByOthers;
  long numReadMissesServicedByShared;
  long numReadMissesServicedByModified;

  long numWriteHits;
  long numWriteMisses;

  long numWriteOnSharedMisses;
  long numWriteOnInvalidMisses;
  long numInvalidatesSent;

  long numL2Hits;
  long numL2Misses;

  long numL3Hits;
  long numL3Misses;
  
  unsigned long long _cycles;
  unsigned long long _instructions;

  SMPCache(int cpuid, std::vector<SMPCache * > * cacheVector);

  virtual ~SMPCache(){}

  int getCPUId();
  std::vector<SMPCache * > *getCacheVector();
  //Readline performs a read, and uses readRemoteAction to 
  //check for data in other caches
  virtual int readLine(uint64_t rdPC, uint64_t addr) = 0;

  //Writeline performs a write, and uses writeRemoteAction
  //to check for data in other caches
  virtual int writeLine(uint64_t wrPC, uint64_t addr) = 0;
 
  //Fill line touches cache state, bringing addr's block in, and setting its state to mesi_state 
  virtual int fillLine(uint64_t addr, uint64_t mesi_state) = 0;

  unsigned long long getCycles(){
    return _cycles;
  }
  
  void setCycles(unsigned long long cycles){
    _cycles = cycles;
  }

  void updateCycles(unsigned long long cycles){
    _cycles += cycles;
  }

  unsigned long long getInstructions(){
    return _instructions;
  }

  void incrementInstructions(){
    ++_instructions;
  }

  virtual char *Identify() = 0;

  //Dump the stats for this cache to outFile
  virtual void dumpStatsToFile(std::ofstream *outFile);
  virtual void conciseDumpStatsToFile(std::ofstream *outFile);
  
  int getStateAsInt(unsigned long addr);

};

typedef SMPCache *(*CacheFactory)(int, std::vector<SMPCache*> *, int, int, int, int, const char *, bool);
#endif
