#include <pin.H>

#include <MultiSectorCacheSim.h>
#include <ZSimCache.h>

#include <string>
#include <set>
#include <iostream>
#include <fstream>
#include <unordered_map>

#define ORCA 1

ADDRINT g_lower_bound;
ADDRINT g_upper_bound;

unsigned long csize = 32768;
unsigned long bsize = 64;
unsigned long assoc = 8;
unsigned long num = 16;

unsigned long L2csize = 262144;
unsigned long L2bsize = 64;
unsigned long L2assoc = 8;

unsigned long L3csize = 8388608;
unsigned long L3bsize = 64;
unsigned long L3assoc = 16;

unsigned long sectorCacheSize = 16384;
unsigned long sectorCacheLocksPerLine = 64;
unsigned long sectorCacheAssoc = 1;

#define MAX_THREADS 16
#define MAX_SPACES 128

unsigned long long instructions[MAX_THREADS];
unsigned long long cycles[MAX_THREADS];
unsigned long long stores = 0;
unsigned long long loads = 0;
unsigned long long acquireLoads = 0;
unsigned long long acquireStores = 0;

std::unordered_map<unsigned long,char> shadow[MAX_SPACES];

ADDRINT slabBase;
ADDRINT memBase;
ADDRINT threadMemBase[MAX_SPACES];

ZSimCache *Cache;
MultiSectorCacheSim *LockCache;

PIN_MUTEX CacheLock;

VOID I_NonMem(UINT32 tid) {
  cycles[tid%MAX_THREADS]++;
  instructions[tid%MAX_THREADS]++;
}

int ReadCache(ADDRINT tid, ADDRINT addr){
  PIN_MutexLock(&CacheLock);
  int cycles = Cache->readLine(tid%MAX_THREADS,0,addr);
  PIN_MutexUnlock(&CacheLock);
  return cycles;
}

int WriteCache(ADDRINT tid, ADDRINT addr){
  PIN_MutexLock(&CacheLock);
  int cycles = Cache->writeLine(tid%MAX_THREADS,0,addr);
  PIN_MutexUnlock(&CacheLock);
  return cycles;
}

ADDRINT getAddressOffset(ADDRINT addr){
  return addr - slabBase; 
}

ADDRINT getGlobalLockAddress(ADDRINT addr){
  return getAddressOffset(addr) + memBase;
}

ADDRINT getThreadLockAddress(ADDRINT tid, ADDRINT addr){
  return getAddressOffset(addr) + threadMemBase[tid];
}

VOID AcquireLockUpdates(ADDRINT tid, ADDRINT addr){
  // Procedure is the same for reads and writes
  // first, read the lock in main memory because we have to fully check (lock cache missed)
  cycles[tid%MAX_THREADS] += ReadCache(tid,getThreadLockAddress(tid,addr));
  // Next, read the global metadata state to determine what to do
  cycles[tid%MAX_THREADS] += ReadCache(tid,getGlobalLockAddress(addr));
  // Next, update the global metadata state
  // TODO: Might be some inefficiency here in the main SOFRITAS code (updates require read then write)
  cycles[tid%MAX_THREADS] += WriteCache(tid,getGlobalLockAddress(addr));
  // Update the thread local state
  cycles[tid%MAX_THREADS] += WriteCache(tid,getThreadLockAddress(tid,addr));
  // finally, unset the atomic flag in global state
  cycles[tid%MAX_THREADS] += WriteCache(tid,getGlobalLockAddress(addr));
}

VOID LoadToLockCache(ADDRINT tid, ADDRINT addr){
  SectorCache *sectorCache = LockCache->findSectorCacheByTid(tid);
  if(sectorCache->isInCache(addr,tid)){
    // Do nothing if it's already in the lock cache, no additional cycles
    return;
  }
  sectorCache->fillLine(tid,addr);
  for(ADDRINT c = 0; c < 64; c+=8){
    // Record cache accesses for loading each of the 8 lines into the lock cache
    cycles[tid%MAX_THREADS] += WriteCache(tid,getThreadLockAddress(tid,addr+c));
  }
}

VOID AcquireLoad(ADDRINT tid, ADDRINT addr){
  cycles[tid%MAX_THREADS] += ReadCache(tid,addr);
  instructions[tid%MAX_THREADS]++;
  acquireLoads++;

  if(shadow[tid].count(addr) == 0){
    shadow[tid][addr] = 0;
  }

  if(shadow[tid][addr] == 0){
    AcquireLockUpdates(tid,addr);
    shadow[tid][addr] = 1;
  }else{
    LoadToLockCache(tid,addr);
  }
}

VOID AcquireStore(ADDRINT tid, ADDRINT addr){
  acquireStores++;
  cycles[tid%MAX_THREADS]++;
  instructions[tid%MAX_THREADS]++;

  if(shadow[tid].count(addr) == 0){
    shadow[tid][addr] = 0;
  }

  if(shadow[tid][addr] <= 1){
    AcquireLockUpdates(tid,addr);
    shadow[tid][addr] = 2;
    LockCache->invalidateRemote(tid,addr);
  }else{
    LoadToLockCache(tid,addr);
  }
}

VOID PassRegion(ADDRINT tid, ADDRINT sBase, ADDRINT regionBase){
  std::cerr << "Passed slab base of " << (void*)slabBase << "\n";
  std::cerr << "Passed region base of " << (void*)regionBase << "\n";
  slabBase = sBase;
  memBase = regionBase;
}

VOID PassThreadRegion(ADDRINT tid, ADDRINT regionBase){
  std::cerr << "Passed region base of " << (void*)regionBase << " for thread " << tid << "\n";
  threadMemBase[tid] = regionBase;
}

VOID I_MemRead(UINT32 tid, void * addr) {
  int cyc = ReadCache(tid,(ADDRINT)addr);
  if(cyc > 1){
    ReadCache(tid,(size_t)addr+64);
    ReadCache(tid,(size_t)addr+128);
  }
  cycles[tid%MAX_THREADS] += cyc;
  instructions[tid%MAX_THREADS]++;
  loads++;
}

VOID I_MemWrite(UINT32 tid, void * addr) {
  int cyc = WriteCache(tid,(ADDRINT)addr);
  if(cyc > 1){
    WriteCache(tid,(size_t)addr+64);
    WriteCache(tid,(size_t)addr+128);
  }
  cycles[tid%MAX_THREADS] += cyc;
  instructions[tid%MAX_THREADS]++;
  stores++;
}

VOID EndOFR(UINT32 tid) {
  shadow[tid].clear();
}

VOID MatchMaxCycles(UINT32 tid) {
  unsigned long long maxCycles = cycles[tid % MAX_THREADS];
  for(int c = 0; c < MAX_THREADS; c++){
    if(cycles[c] > maxCycles){
      maxCycles = cycles[c];
    }
  }
  cycles[tid % MAX_THREADS] = maxCycles;
}

VOID Join(UINT32 tid) {
  EndOFR(tid);
  MatchMaxCycles(tid);
}

VOID Barrier(UINT32 tid) {
  EndOFR(tid);
  MatchMaxCycles(tid);
}

VOID BarrierNoRel(UINT32 tid) {
  MatchMaxCycles(tid);
}

VOID CondWait(UINT32 tid) {
  EndOFR(tid);
  MatchMaxCycles(tid);
}

VOID CondSignal(UINT32 tid) {
  EndOFR(tid);
}

VOID CondBroadcast(UINT32 tid) {
  EndOFR(tid);
}

VOID Release(UINT32 tid, void *addr) {
  shadow[tid][(unsigned long)addr] = 0;
}

VOID ReleaseObject(UINT32 tid, void *addr, unsigned long size) {
  unsigned long current = (unsigned long)addr;
  unsigned long maxAddr = current + size;

  while(current < maxAddr){
    Release(tid, (void*)(current++));
  }
}

VOID ReleaseArray(UINT32 tid, void *addr, unsigned long size,
		  unsigned long num) {
  unsigned long a = (unsigned long)addr;
  for(unsigned long c = 0; c < num; c++){
    Release(tid,(void*)(a + (size * c)));
  }
}

VOID ImageLoad(IMG img, VOID *v) {
  if(IMG_IsMainExecutable(img)){
    g_lower_bound = IMG_LowAddress(img);
    g_upper_bound = IMG_HighAddress(img);
  }

  RTN acquireRtn = RTN_FindByName(img, "SOFRITAS_acquireLoadAligned");
  if (RTN_Valid(acquireRtn)) {
    RTN_Open(acquireRtn);
    RTN_InsertCall( acquireRtn, IPOINT_BEFORE, (AFUNPTR)AcquireLoad,
                    IARG_THREAD_ID, IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                    IARG_END);
    RTN_Close(acquireRtn);
  }

  acquireRtn = RTN_FindByName(img, "SOFRITAS_acquireLoad");
  if (RTN_Valid(acquireRtn)) {
    RTN_Open(acquireRtn);
    RTN_InsertCall( acquireRtn, IPOINT_BEFORE, (AFUNPTR)AcquireLoad,
                    IARG_THREAD_ID, IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                    IARG_END);
    RTN_Close(acquireRtn);
  }

  acquireRtn = RTN_FindByName(img, "SOFRITAS_acquireStoreAligned");
  if (RTN_Valid(acquireRtn)) {
    RTN_Open(acquireRtn);
    RTN_InsertCall( acquireRtn, IPOINT_BEFORE, (AFUNPTR)AcquireStore,
                    IARG_THREAD_ID, IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                    IARG_END);
    RTN_Close(acquireRtn);
  }

  acquireRtn = RTN_FindByName(img, "SOFRITAS_acquireStore");
  if (RTN_Valid(acquireRtn)) {
    RTN_Open(acquireRtn);
    RTN_InsertCall( acquireRtn, IPOINT_BEFORE, (AFUNPTR)AcquireStore,
                    IARG_THREAD_ID, IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                    IARG_END);
    RTN_Close(acquireRtn);
  }

  RTN passRegionRtn = RTN_FindByName(img, "SOFRITAS_passMapRegion");
  if(RTN_Valid(passRegionRtn)){
    RTN_Open(passRegionRtn);
    RTN_InsertCall( passRegionRtn, IPOINT_BEFORE, (AFUNPTR)PassRegion,
		    IARG_THREAD_ID, IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
		    IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
		    IARG_END);
    RTN_Close(passRegionRtn);
  }

  passRegionRtn = RTN_FindByName(img, "SOFRITAS_passThreadRegion");
  if(RTN_Valid(passRegionRtn)){
    RTN_Open(passRegionRtn);
    RTN_InsertCall( passRegionRtn, IPOINT_BEFORE, (AFUNPTR)PassThreadRegion,
		    IARG_THREAD_ID, IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
		    IARG_END);
    RTN_Close(passRegionRtn);
  }

  RTN joinRtn = RTN_FindByName(img, "SOFRITAS_join");
  if (RTN_Valid(joinRtn)) {
    RTN_Open(joinRtn);
    RTN_InsertCall(joinRtn, IPOINT_BEFORE, (AFUNPTR)Join,
                   IARG_THREAD_ID,
                   IARG_END);
    RTN_Close(joinRtn);
  }
  
  RTN barrierRtn = RTN_FindByName(img, "SOFRITAS_barrierWait");
  if (RTN_Valid(barrierRtn)) {
    RTN_Open(barrierRtn);
    RTN_InsertCall(barrierRtn, IPOINT_BEFORE, (AFUNPTR)Barrier,
                   IARG_THREAD_ID,IARG_END);
    RTN_Close(barrierRtn);
  }

  barrierRtn = RTN_FindByName(img, "SOFRITAS_barrier_no_release");
  if (RTN_Valid(barrierRtn)) {
    RTN_Open(barrierRtn);
    RTN_InsertCall(barrierRtn, IPOINT_BEFORE, (AFUNPTR)BarrierNoRel,
                   IARG_THREAD_ID,IARG_END);
    RTN_Close(barrierRtn);
  }

  RTN waitRtn = RTN_FindByName(img, "SOFRITAS_condWait");
  if (RTN_Valid(waitRtn)) {
    RTN_Open(waitRtn);
    RTN_InsertCall(waitRtn, IPOINT_BEFORE, (AFUNPTR)CondWait,
		   IARG_THREAD_ID, IARG_END);
    RTN_Close(waitRtn);
  }
  
  RTN signalRtn = RTN_FindByName(img, "SOFRITAS_condSignal");
  if (RTN_Valid(signalRtn)) {
    RTN_Open(signalRtn);
    RTN_InsertCall(signalRtn, IPOINT_BEFORE, (AFUNPTR)CondSignal,
		   IARG_THREAD_ID, IARG_END);
    RTN_Close(signalRtn);
  }
  
  RTN broadcastRtn = RTN_FindByName(img, "SOFRITAS_condBroadcast");
  if (RTN_Valid(broadcastRtn)) {
    RTN_Open(broadcastRtn);
    RTN_InsertCall(broadcastRtn, IPOINT_BEFORE, (AFUNPTR)CondBroadcast,
		   IARG_THREAD_ID, IARG_END);
    RTN_Close(broadcastRtn);
  }

  RTN releaseRtn = RTN_FindByName(img, "SOFRITAS_release");
  if (RTN_Valid(releaseRtn)) {
    RTN_Open(releaseRtn);
    RTN_InsertCall(releaseRtn, IPOINT_BEFORE, (AFUNPTR)Release,
		   IARG_THREAD_ID,
		   IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
		   IARG_END);
    RTN_Close(releaseRtn);
  }

  releaseRtn = RTN_FindByName(img, "SOFRITAS_release_object");
  if (RTN_Valid(releaseRtn)) {
    RTN_Open(releaseRtn);
    RTN_InsertCall(releaseRtn, IPOINT_BEFORE, (AFUNPTR)ReleaseObject,
		   IARG_THREAD_ID,
		   IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
		   IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
		   IARG_END);
    RTN_Close(releaseRtn);
  }

  releaseRtn = RTN_FindByName(img, "SOFRITAS_release_array");
  if (RTN_Valid(releaseRtn)) {
    RTN_Open(releaseRtn);
    RTN_InsertCall(releaseRtn, IPOINT_BEFORE, (AFUNPTR)ReleaseArray,
		   IARG_THREAD_ID,
		   IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
		   IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
		   IARG_FUNCARG_ENTRYPOINT_VALUE, 2,
		   IARG_END);
    RTN_Close(releaseRtn);
  }

  releaseRtn = RTN_FindByName(img, "SOFRITAS_clear");
  if (RTN_Valid(releaseRtn)) {
    RTN_Open(releaseRtn);
    RTN_InsertCall(releaseRtn, IPOINT_BEFORE, (AFUNPTR)Release,
		   IARG_THREAD_ID,
		   IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
		   IARG_END);
    RTN_Close(releaseRtn);
  }

  RTN endRtn = RTN_FindByName(img, "SOFRITAS_END_OFR");
  if(RTN_Valid(endRtn)){
    RTN_Open(endRtn);
    RTN_InsertCall(endRtn, IPOINT_BEFORE, (AFUNPTR)EndOFR,
		   IARG_THREAD_ID, IARG_END);
    RTN_Close(endRtn);
  }
}

VOID Instruction(INS ins, VOID *v) {
  UINT32 memOperands = INS_MemoryOperandCount(ins);
  ADDRINT ins_addr = INS_Address(ins);

#ifdef ORCA
  if(RTN_FindNameByAddress(INS_Address(ins)).find("SOFRITAS") != std::string::npos){
    return;
  }
#endif
  
  bool do_instrument = (ins_addr >= g_lower_bound) &&
    (ins_addr <= g_upper_bound);

  if(do_instrument){
    if(memOperands == 0){
      INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)I_NonMem,
                     IARG_THREAD_ID, IARG_END);
    }else{
      for (UINT32 memOp = 0; memOp < memOperands; memOp++) {
	bool isStack = false;
	if (INS_IsStackRead(ins) || INS_IsStackWrite(ins)){
	  isStack = true;
	}
	
	if (INS_MemoryOperandIsRead(ins, memOp)) {
	  INS_InsertPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR)I_MemRead,
				   IARG_THREAD_ID,
				   IARG_MEMORYOP_EA, memOp,
				   IARG_ADDRINT,INS_Address(ins),
				   IARG_CONST_CONTEXT,
				   IARG_BOOL,isStack,
				   IARG_END);
	}
	if (INS_MemoryOperandIsWritten(ins, memOp)) {
	  INS_InsertPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR)I_MemWrite,
				   IARG_THREAD_ID,
				   IARG_MEMORYOP_EA, memOp,
				   IARG_ADDRINT,INS_Address(ins),
				   IARG_CONST_CONTEXT,
				   IARG_BOOL,isStack,
				   IARG_END);
	}
      }
    }
  }
}

VOID ProgramExit(INT32 code, VOID *v) {
  unsigned long long cyc = 0;
  unsigned long long inst = 0;
  for(int c = 0; c < MAX_THREADS; c++){
    if(cycles[c] > cyc){
      cyc = cycles[c];
    }

    if(instructions[c] > inst){
      inst = instructions[c];
    }
  }
  std::cout << cyc << "\n";
}

int main(int argc, char *argv[])
{
  // Initialize PIN library. Print help message if -h(elp) is specified
  // in the command line or the command line is invalid 
  if( PIN_Init(argc,argv) ) {
    return 0;
  }

  PIN_InitSymbols();

  PIN_MutexInit(&CacheLock);
  
  IMG_AddInstrumentFunction(ImageLoad, NULL);
  INS_AddInstrumentFunction(Instruction, 0);
  PIN_AddFiniFunction(ProgramExit, 0);

  Cache = new ZSimCache(num,csize,bsize,assoc,L2csize,L2bsize,L2assoc,L3csize,L3bsize,L3assoc);
  LockCache =new MultiSectorCacheSim(num,sectorCacheSize,sectorCacheLocksPerLine,sectorCacheAssoc);
  
  PIN_StartProgram();
  
  return 0;
}
