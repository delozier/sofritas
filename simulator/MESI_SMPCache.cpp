#include "MESI_SMPCache.h"

MESI_SMPCache::MESI_SMPCache(int cpuid, std::vector<SMPCache * > * cacheVector,
                             int csize, int cassoc, int cbsize, int caddressable, const char * repPol, bool cskew) : 
  SMPCache(cpuid,cacheVector){
  //fprintf(stderr,"Making a MESI cache with cpuid %d\n",cpuid);
  CacheGeneric<MESI_SMPCacheState> *c = CacheGeneric<MESI_SMPCacheState>::create(csize, cassoc, cbsize, caddressable, repPol, cskew);
  L1Cache = (CacheGeneric<StateGeneric<> >*)c; 
}

int MESI_SMPCache::fillLine(uint64_t addr, uint64_t mesi_state){
  std::vector<SMPCache * >::iterator cacheIter;
  std::vector<SMPCache * >::iterator lastCacheIter;
  MESI_SMPCacheState *st = (MESI_SMPCacheState *)L1Cache->findLine2Replace(addr); //this gets the contents of whateverline this would go into
  if(st == nullptr){
    return MEMORY_ACCESS_LATENCY;
  }

  int latency = MEMORY_ACCESS_LATENCY;

  st->setTag(L1Cache->calcTag(addr));
  st->_addr = addr;
  st->changeStateTo((MESIState_t)mesi_state);

  return latency;
}
  

MESI_SMPCache::RemoteReadService MESI_SMPCache::readRemoteAction(uint64_t addr){

  std::vector<SMPCache * >::iterator cacheIter;
  std::vector<SMPCache * >::iterator lastCacheIter;
  for(cacheIter = this->getCacheVector()->begin(), lastCacheIter = this->getCacheVector()->end(); cacheIter != lastCacheIter; cacheIter++){
    MESI_SMPCache *otherCache = (MESI_SMPCache*)*cacheIter; 

    if(otherCache->getCPUId() == this->getCPUId()){
      continue;
    }
      
    MESI_SMPCacheState* otherState = (MESI_SMPCacheState *)otherCache->L1Cache->findLine(addr);

    if(otherState){
      if(otherState->getState() == MESI_MODIFIED){

        otherState->changeStateTo(MESI_SHARED);
        return MESI_SMPCache::RemoteReadService(false,true);

      }else if(otherState->getState() == MESI_EXCLUSIVE){

        otherState->changeStateTo(MESI_SHARED); 
        return MESI_SMPCache::RemoteReadService(false,true);

      }else if(otherState->getState() == MESI_SHARED){  //doesn't matter except that someone's got it
        return MESI_SMPCache::RemoteReadService(true,true);

      }else if(otherState->getState() == MESI_INVALID){ //doesn't matter at all.

      }
    }
  }//done with other caches

  //fprintf(stderr,"Done with all caches\n");
  //This happens if everyone was MESI_INVALID
  return MESI_SMPCache::RemoteReadService(false,false);
}

int MESI_SMPCache::readLine(uint64_t rdPC, uint64_t addr){

  MESI_SMPCacheState *st = (MESI_SMPCacheState *)L1Cache->findLine(addr);    
  //fprintf(stderr,"In MESI ReadLine\n");
  if(!st || (st && !(st->isValid())) ){//Read Miss -- i need to look in other peoples' caches for this data
    
    numReadMisses++;

    if(st){
      numReadOnInvalidMisses++;
    }

    //Query the other caches and get a remote read service object.
    MESI_SMPCache::RemoteReadService rrs = readRemoteAction(addr);
    numReadRequestsSent++;
      
    MESIState_t newMesiState = MESI_INVALID;
  
    if(rrs.providedData){
   
      numReadMissesServicedByOthers++;

      if(rrs.isShared){
 
        numReadMissesServicedByShared++;
         
      }else{ 
      
        numReadMissesServicedByModified++;
      } 

      newMesiState = MESI_SHARED;
      fillLine(addr,newMesiState);

      return REMOTE_L1_HIT_LATENCY;
    }else{

      newMesiState = MESI_EXCLUSIVE;
      return fillLine(addr,newMesiState);      
    }
  }else{ //Read Hit

    numReadHits++; 
    return L1_HIT_LATENCY;
  }

}

MESI_SMPCache::InvalidateReply MESI_SMPCache::writeRemoteAction(uint64_t addr){
    
  bool empty = true;
    std::vector<SMPCache * >::iterator cacheIter;
    std::vector<SMPCache * >::iterator lastCacheIter;
    for(cacheIter = this->getCacheVector()->begin(), lastCacheIter = this->getCacheVector()->end(); cacheIter != lastCacheIter; cacheIter++){
      MESI_SMPCache *otherCache = (MESI_SMPCache*)*cacheIter; 
      if(otherCache->getCPUId() == this->getCPUId()){
        continue;
      }

      //Get the line from the current other cache 
      MESI_SMPCacheState* otherState = (MESI_SMPCacheState *)otherCache->L1Cache->findLine(addr);

      //if it was actually in the other cache:
      if(otherState && otherState->isValid()){
          /*Invalidate the line, cause we're writing*/
          otherState->invalidate();
          empty = false;
      }

    }//done with other caches

    //Empty=true indicates that no other cache 
    //had the line or there were no other caches
    //
    //This data in this object is not used as is, 
    //but it might be useful if you plan to extend 
    //this simulator, so i left it in.
    return MESI_SMPCache::InvalidateReply(empty);
}


int MESI_SMPCache::writeLine(uint64_t wrPC, uint64_t addr){

  MESI_SMPCacheState * st = (MESI_SMPCacheState *)L1Cache->findLine(addr);    
    
  if(!st || (st && !(st->isValid())) ){ //Write Miss
    numWriteMisses++;

    if(st){
      numWriteOnInvalidMisses++;
    }
  
    MESI_SMPCache::InvalidateReply inv_ack = writeRemoteAction(addr);
    numInvalidatesSent++;

    //Fill the line with the new write
    return fillLine(addr,MESI_MODIFIED);
  }else if(st->getState() == MESI_SHARED ||
           st->getState() == MESI_EXCLUSIVE){ //Coherence Miss
    
    numWriteMisses++;
    numWriteOnSharedMisses++;
      
    MESI_SMPCache::InvalidateReply inv_ack = writeRemoteAction(addr);
    numInvalidatesSent++;

    st->changeStateTo(MESI_MODIFIED);

    return REMOTE_L1_HIT_LATENCY;
  }else{ //Write Hit
    numWriteHits++;
    return L1_HIT_LATENCY;
  }

}

MESI_SMPCache::~MESI_SMPCache(){

}

char *MESI_SMPCache::Identify(){
  return (char *)"MESI Cache Coherence";
}
