
#include "CacheInterface.h"
#include "SMPCache.h"

#include "MESI_SMPCache.h"
#include "MSI_SMPCache.h"

#include <iostream>
#include <fstream>

#ifndef PIN
#include <pthread.h>
#else
#include "pin.H"
#endif

class MultiSectorCacheSim;  
class MultiCacheSim : public CacheInterface{

public:


  //FIELDS
  //Number of Caches in the multicachesim
  int num_caches;

  //The vector that contains the caches
  std::vector<SMPCache * > allCaches;

  //The lock that protects the vector so it isn't corrupted by concurrent updates
  pthread_mutex_t allCachesLock;

  //Cache Parameters
  int cache_size;
  int cache_assoc;
  int cache_bsize;
  
  //The output file to dump stats to at the end
  std::ofstream *_cacheLog;

  CacheFactory cacheFactory;
  //METHODS
  //Constructor
  //MultiCacheSim(FILE *cachestats,int size, int assoc, int bsize);
  MultiCacheSim(std::ofstream *cacheLog, int size, int assoc, int bsize, CacheFactory c);

  //Adds a cache to the multicachesim
  void createNewCache();
 
  //These three functions implement the CacheInterface interface 
  int readLine(unsigned long tid, unsigned long rdPC, unsigned long addr);
  int writeLine(unsigned long tid, unsigned long wrPC, unsigned long addr);

  unsigned long long getCycles(unsigned long tid);
  void setCycles(unsigned long tid, unsigned long long cycles);
  void updateCycles(unsigned long tid, unsigned long long cycles);

  unsigned long long getInstructions(unsigned long tid);
  void incrementInstructions(unsigned long tid);

  void dumpStatsForAllCaches(bool concise);

  //Utility Function to get the cache object that has the specified CPUid
  SMPCache *findCacheByCPUId(unsigned int CPUid);

  //Translate from program threadID to multicachesim CPUId
  int tidToCPUId(int tid);

  char *Identify();
  int getStateAsInt(unsigned long tid, unsigned long addr);

  //Destructor
  ~MultiCacheSim();

};
