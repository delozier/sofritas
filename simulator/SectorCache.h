#pragma once

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>

#include <unordered_set>

class SectorCacheEntry{
public:
  size_t _tag;
  int _tid;

 SectorCacheEntry() : _tag(0), _tid(-1){}
};

class SectorCache{
  size_t _size; // in bytes
  size_t _locksPerLine;
  size_t _assoc;
  SectorCacheEntry **_cache;
  size_t _entries;
  size_t _indexBits;
  size_t _offsetBits;

  size_t baseAddress(size_t addr)
  {
    addr = addr >> _offsetBits;
    addr = addr << _offsetBits;
    return addr;
  }

  size_t tag(size_t addr)
  {
    return (addr >> (_offsetBits + _indexBits));
  }

  size_t index(size_t addr, int tid)
  {
    if(_entries == 1){
      return 0;
    }
    size_t i = addr >> _offsetBits;
    size_t j = i;
    j = j >> _indexBits;
    j = j << _indexBits;
    return (i - j) ^ tid;
  }

  size_t offset(size_t addr)
  {
    size_t i = addr >> _offsetBits;
    i = i << _offsetBits;
    return addr - i;
  }

public:
  // Stats
  unsigned long _hits;
  unsigned long _misses;
  unsigned long _coldMisses;
  unsigned long _fills;
  unsigned long _updates;
  unsigned long _remoteInvalidates;

  unsigned int CPUId;

 SectorCache(unsigned int cpuId, size_t size, size_t locksPerLine, size_t assoc) : _size(size), _locksPerLine(locksPerLine), 
    _assoc(assoc), _cache(nullptr), _entries(0), _hits(0), _misses(0), _coldMisses(0), _fills(0), _updates(0), _remoteInvalidates(0)
  {
    CPUId=cpuId;
    _entries = (_size * 8) / (_locksPerLine * 2);
    _entries = _entries / _assoc;
    _cache = new SectorCacheEntry*[_entries];

    for(size_t c = 0; c < _entries; c++){
      _cache[c] = new SectorCacheEntry[_assoc];
      for(size_t d = 0; d < _assoc; d++){
        _cache[c][d]._tid=-1;
      }
    }
    _indexBits = (size_t)log2((double)(_entries));
    _offsetBits = (size_t)log2((double)_locksPerLine);
    std::cout << "Creating lock cache with " << _entries << " entries and " << _assoc << " associativity\n";
  }

  /*
   * Need to iterate over number of entries to put the 
   * entry into the sector cache
   */ 
  void fillLine(int tid, size_t addr);

  void invalidateLine(size_t addr, int tid);

  void remoteInvalidateLine(size_t addr, int tid);

  /*
   * Check if the address is in the sector cache
   * Return whether the line was already in the cache or not
   */
  bool isInCache(size_t addr, int tid, bool recordStats = true, bool isRemote = false)
  {
    size_t i = index(addr,tid);
    size_t t = tag(addr);

    bool found = false;

    for(size_t c = 0; c < _assoc; c++){
      if(_cache[i][c]._tag == t){
        // If we're querying from a remote cache, we don't care who owns this line, 
        // just that the tag matches
        if(isRemote == true || _cache[i][c]._tid == tid){
          found = true;
          moveToFront(i,t,c);
          break;
        }
      }
    }

    return found;
  }

  void moveToFront(size_t index, size_t tag, size_t pos)
  {
    SectorCacheEntry newFirst = _cache[index][pos];

    for(size_t c = 0; c < pos; c++){
      _cache[index][c+1] = _cache[index][c];
    }

    _cache[index][0] = newFirst;
  }

  SectorCacheEntry* findLineToFill(int tid, size_t addr)
  {
    size_t i = index(addr,tid);
    SectorCacheEntry *line = &_cache[i][_assoc-1];
    return line;
  }

  void printStats(std::ostream &out){
    out << "lock_cache = LockCache()\n";
    out << "lock_cache.id = " << CPUId << "\n";
    out << "lock_cache.sector_misses = " << _misses << "\n";
    out << "lock_cache.sector_cold_misses = " << _coldMisses << "\n";
    out << "lock_cache.sector_hits = " << _hits << "\n";
    out << "lock_cache.sector_fills = " << _fills << "\n";
    out << "lock_cache.sector_updates = " << _updates << "\n";
    out << "lock_cache.remote_invalidates = " << _remoteInvalidates << "\n";
    out << "lock_caches.append(lock_cache)\n";
    out << "\n";
  }
};
