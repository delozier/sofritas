#include "SMPCache.h"

SMPCache::SMPCache(int cpuid, std::vector<SMPCache * > * cacheVector){

  CPUId = cpuid;
  allCaches = cacheVector;

  numReadHits = 0;
  numReadMisses = 0;
  numReadOnInvalidMisses = 0;
  numReadRequestsSent = 0;
  numReadMissesServicedByOthers = 0;
  numReadMissesServicedByShared = 0;
  numReadMissesServicedByModified = 0;

  numWriteHits = 0;
  numWriteMisses = 0;
  numWriteOnSharedMisses = 0;
  numWriteOnInvalidMisses = 0;
  numInvalidatesSent = 0;

  _cycles = 0;
  _instructions = 0;
}

void SMPCache::conciseDumpStatsToFile(std::ofstream *outFile){
  /* TODO: Fix later
  fprintf(outFile,"%lu,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
                  CPUId,
                  numReadHits,
                  numReadMisses,
                  numReadOnInvalidMisses,
                  numReadRequestsSent,
                  numReadMissesServicedByOthers,
                  numReadMissesServicedByShared,
                  numReadMissesServicedByModified,
                  numWriteHits,
                  numWriteMisses,
                  numWriteOnSharedMisses,
                  numWriteOnInvalidMisses,
                  numInvalidatesSent);
  */ 
}

void SMPCache::dumpStatsToFile(std::ofstream *outFile){
  *outFile << "cpu = CPU()\n";
  *outFile << "cpu.id = " << CPUId << "\n";

  *outFile << "cpu.read_hits = " << numReadHits << "\n";
  *outFile << "cpu.read_misses = " << numReadMisses << "\n";
  *outFile << "cpu.read_on_invalid_misses = " << numReadOnInvalidMisses << "\n";
  *outFile << "cpu.read_requests_sent = " << numReadRequestsSent << "\n";
  *outFile << "cpu.read_misses_serviced_remotely = " << numReadMissesServicedByOthers << "\n";
  *outFile << "cpu.read_misses_serviced_by_shared = " << numReadMissesServicedByShared << "\n";
  *outFile << "cpu.read_misses_serviced_by_modified = " << numReadMissesServicedByModified << "\n";

  *outFile << "cpu.write_hits = " << numWriteHits << "\n";
  *outFile << "cpu.write_misses = " << numWriteMisses << "\n";
  *outFile << "cpu.write_on_shared_misses = " << numWriteOnSharedMisses << "\n";
  *outFile << "cpu.write_on_invalid_misses = " << numWriteOnInvalidMisses << "\n";
  *outFile << "cpu.invalidates_sent = " << numInvalidatesSent << "\n";

  *outFile << "cpu.l2_hits = " << numL2Hits << "\n";
  *outFile << "cpu.l2_misses = " << numL2Misses << "\n";

  *outFile << "cpu.l3_hits = " << numL3Hits << "\n";
  *outFile << "cpu.l3_misses = " << numL3Misses << "\n";
  
  *outFile << "cpu.cycles = " << _cycles << "\n";
  *outFile << "cpu.instructions = " << _instructions << "\n";

  *outFile << "cpus.append(cpu)\n\n";
}

int SMPCache::getCPUId(){
  return CPUId;
}

int SMPCache::getStateAsInt(unsigned long addr){
  return (int)this->L1Cache->findLine(addr)->getState();
}

std::vector<SMPCache * > *SMPCache::getCacheVector(){
  return allCaches;
}
