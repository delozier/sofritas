#include "CacheCore.h"
#include "SMPCache.h"
#include "MESI_SMPCacheState.h"
#include <vector>


class MESI_SMPCache : public SMPCache{

public:

  /*First we define a couple of helper classes that 
   * carry data between methods in our main class*/
  class RemoteReadService{
  public:
    bool isShared;
    bool providedData;
  
    RemoteReadService(bool shrd, bool prov){
      isShared = shrd;
      providedData = prov;
    }
  };
  
  class InvalidateReply{
  /*This class isn't used, but i left it here so you
   * could add to it if you need to communicate
   * between caches in Invalidate Replies*/
  public:
    uint64_t empty;
  
    InvalidateReply(bool EMPTY){
      empty = EMPTY;
    }
  };

  //FIELDS
  //This cache's ID
  int CPUId;

  static const int L1_HIT_LATENCY = 1;
  static const int REMOTE_L1_HIT_LATENCY = 15;
  static const int MEMORY_ACCESS_LATENCY = 120;

  //METHODS
  //Constructor
  MESI_SMPCache(int cpuid, std::vector<SMPCache * > * cacheVector, int csize, int cassoc, int cbsize, int caddressable, const char * repPol, bool cskew);

  //Readline performs a read, and uses readRemoteAction to 
  //check for data in other caches
  virtual int readLine(uint64_t rdPC, uint64_t addr);//SMPCache Interface Function
  virtual MESI_SMPCache::RemoteReadService readRemoteAction(uint64_t addr);

  //Writeline performs a write, and uses writeRemoteAction
  //to check for data in other caches
  virtual int writeLine(uint64_t wrPC, uint64_t addr);//SMPCache Interface Function
  virtual MESI_SMPCache::InvalidateReply writeRemoteAction(uint64_t addr);
 
  //Fill line touches cache state, bringing addr's block in, and setting its state to mesi_state 
  virtual int fillLine(uint64_t addr, uint64_t mesi_state);//SMPCache Interface Function

  virtual char *Identify();
 
  //Dump the stats for this cache to outFile
  //void dumpStatsToFile(FILE* outFile);

  static SMPCache *Create(int num, std::vector<SMPCache*> *cvec, int csize, int casso, int bs, 
                          int addrble, const char *repl, bool skw){
    return new MESI_SMPCache(num,cvec,csize,casso,bs,addrble,repl,skw);
  }
  
  //Destructor
  ~MESI_SMPCache();
};


