/** $lic$
 * Copyright (C) 2012-2014 by Massachusetts Institute of Technology
 * Copyright (C) 2010-2013 by The Board of Trustees of Stanford University
 *
 * This file is part of zsim.
 *
 * zsim is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, version 2.
 *
 * If you use this software in your research, we request that you reference
 * the zsim paper ("ZSim: Fast and Accurate Microarchitectural Simulation of
 * Thousand-Core Systems", Sanchez and Kozyrakis, ISCA-40, June 2013) as the
 * source of the simulator in any publications that use this software, and that
 * you send us a citation of your work.
 *
 * zsim is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CACHE_H_
#define CACHE_H_

#include <string>
#include <vector>

#include "cache_arrays.h"
#include "coherence_ctrls.h"
#include "memory_hierarchy.h"
#include "repl_policies.h"

class Network;

/* General coherent modular cache. The replacement policy and cache array are
 * pretty much mix and match. The coherence controller interfaces are general
 * too, but to avoid virtual function call overheads we work with MESI
 * controllers, since for now we only have MESI controllers
 */
class Cache : public BaseCache {
    protected:
  CC* cc; // MESITerminalCC for L1, MESICC for L2 and L3
  CacheArray* array; // SetAssocArray
  ReplPolicy* rp; // LRUReplPolicy

        uint32_t numLines;

        //Latencies
        uint32_t accLat; //latency of a normal access (could split in get/put, probably not needed)
        uint32_t invLat; //latency of an invalidation

        std::string name;

 public:
        uint64_t m_cycles;
        uint64_t m_lock_cycles;
        uint64_t m_blocked_cycles;
        uint64_t m_nonmem_cycles;
        uint64_t m_instructions;
        uint64_t m_hits;
        uint64_t m_misses;

        uint64_t m_l1_hits;
        uint64_t m_l1_misses;
        uint64_t m_l2_hits;
        uint64_t m_l2_misses;
        uint64_t m_l3_hits;
        uint64_t m_l3_misses;

        // For top level, add a special cache that always returns access time of MAIN_MEMORY_CYCLES (120)

    public:
        Cache(uint32_t _numLines, CC* _cc, CacheArray* _array, ReplPolicy* _rp, uint32_t _accLat, uint32_t _invLat, const std::string& _name);

        const char* getName();
        void setParents(uint32_t _childId, std::vector<MemObject*>& parents, Network* network);
        void setChildren(std::vector<BaseCache*>& children, Network* network);

        virtual uint64_t access(MemReq& req);

        //NOTE: reqWriteback is pulled up to true, but not pulled down to false.
        virtual uint64_t invalidate(Address lineAddr, InvType type, bool* reqWriteback, uint64_t reqCycle, uint32_t srcId);
};

class MemCache : public BaseCache{
  uint32_t accLat;
  BaseCache* child;

public:
  MemCache(uint32_t _accLat) : accLat(_accLat){}
  
  const char* getName()
  {
    return "MainMemory";
  }

  void setParents(uint32_t _childId, std::vector<MemObject*>& parents, Network* network)
  {
    // No parents in Mem cache, just a hack to avoid main memory stuff from Zsim
  }

  void setChildren(std::vector<BaseCache*>& children, Network* network)
  {
    // Only supporting single child right now, again a hack for submission
    child = children[0];
  }
  
  virtual uint64_t access(MemReq& req)
  {
    switch (req.type) {
        case PUTS:
        case PUTX:
            *req.state = I;
            break;
        case GETS:
            *req.state = req.is(MemReq::NOEXCL)? S : E;
            break;
        case GETX:
            *req.state = M;
            break;
        default:
          break;
    }
    return accLat;
  }

  virtual uint64_t invalidate(Address lineAddr, InvType type, bool* reqWriteback, uint64_t reqCycle, uint32_t srcId)
  {
    return 0;
  }
};

#endif  // CACHE_H_
