#ifndef _CACHEINTERFACE_H_
#define _CACHEINTERFACE_H_
#include "MultiSectorCacheSim.h"
class CacheInterface{
public:
  virtual int readLine(unsigned long tid, unsigned long rdPC, unsigned long addr) = 0;
  virtual int writeLine(unsigned long tid, unsigned long rdPC, unsigned long addr) = 0;
  virtual void dumpStatsForAllCaches(bool concise) = 0;
};
#endif
