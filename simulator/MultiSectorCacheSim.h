#pragma once
#include "SectorCache.h"
#include <iostream>
#include <fstream>

#include <vector>
#include <pthread.h>
  
class SectorCache;
class MultiSectorCacheSim {

public:

  MultiSectorCacheSim(int num,int _sectorCacheSize,int _sectorCacheLocksPerLine, int _sectorCacheAssoc)
   {
	num_caches=num;
	sectorCacheSize=_sectorCacheSize;
	sectorCacheLocksPerLine= _sectorCacheLocksPerLine;
	for(int i=0;i<num;i++)
	{
          sectorCaches.push_back(new SectorCache(i,sectorCacheSize,sectorCacheLocksPerLine,_sectorCacheAssoc));
	}
   }	
  //FIELDS
  //Number of Caches in the multicachesim
  int num_caches;

  //The vector that contains the caches
  std::vector<SectorCache *> sectorCaches;

  //The lock that protects the vector so it isn't corrupted by concurrent updates
  pthread_mutex_t allCachesLock;

  //Cache Parameters
  int sectorCacheSize;
  int sectorCacheLocksPerLine;

  void printStats(std::ostream &out);

  SectorCache * findSectorCacheByTid(int tid);
  
  void invalidateRemote(int tid, size_t addr);

  //Translate from program threadID to multicachesim CPUId
  int tidToCPUId(int tid);

};
