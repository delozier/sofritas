/** $lic$
 * Copyright (C) 2012-2014 by Massachusetts Institute of Technology
 * Copyright (C) 2010-2013 by The Board of Trustees of Stanford University
 *
 * This file is part of zsim.
 *
 * zsim is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, version 2.
 *
 * If you use this software in your research, we request that you reference
 * the zsim paper ("ZSim: Fast and Accurate Microarchitectural Simulation of
 * Thousand-Core Systems", Sanchez and Kozyrakis, ISCA-40, June 2013) as the
 * source of the simulator in any publications that use this software, and that
 * you send us a citation of your work.
 *
 * zsim is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEM_CTRLS_H_
#define MEM_CTRLS_H_

#include <string>
#include "memory_hierarchy.h"

/* Simple memory (or memory bank), has a fixed latency */
class SimpleMemory : public MemObject {
private:
  std::string name;
  uint32_t latency;

public:
  uint64_t access(MemReq& req);
  
  const char* getName() {return name.c_str();}
  
  SimpleMemory(uint32_t _latency, std::string& _name) : name(_name), latency(_latency) {}
};

#endif  // MEM_CTRLS_H_
