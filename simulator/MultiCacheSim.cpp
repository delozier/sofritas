#include "MultiCacheSim.h"

MultiCacheSim::MultiCacheSim(std::ofstream *cacheLog, int size, int assoc, int bsize, 
                             CacheFactory c){

  cacheFactory = c;
  _cacheLog = cacheLog;
  num_caches = 0;
  cache_size = size;
  cache_assoc = assoc;
  cache_bsize = bsize; 

  #ifndef PIN
  pthread_mutex_init(&allCachesLock, NULL);
  #else
  PIN_InitLock(&allCachesLock);
  #endif

}

SMPCache *MultiCacheSim::findCacheByCPUId(unsigned int CPUid){
    std::vector<SMPCache *>::iterator cacheIter = allCaches.begin();
    std::vector<SMPCache *>::iterator cacheEndIter = allCaches.end();
    for(; cacheIter != cacheEndIter; cacheIter++){
      if((*cacheIter)->CPUId == CPUid){
        return (*cacheIter);
      }
    }
    return NULL;
} 
  
void MultiCacheSim::dumpStatsForAllCaches(bool concise){
   
  *_cacheLog << "cpus = []\n\n";
  *_cacheLog << "class CPU:\n";
  *_cacheLog << "  def __init__(self):\n";
  *_cacheLog << "    self.id = 0\n";
  *_cacheLog << "    self.instructions = 0\n";
  *_cacheLog << "    self.read_hits = 0\n";
  *_cacheLog << "    self.read_misses = 0\n";
  *_cacheLog << "    self.read_on_invalid_misses = 0\n";
  *_cacheLog << "    self.read_requests_sent = 0\n";
  *_cacheLog << "    self.read_misses_serviced_remotely = 0\n";
  *_cacheLog << "    self.read_misses_serviced_by_shared = 0\n";
  *_cacheLog << "    self.read_misses_serviced_by_modified = 0\n";
  *_cacheLog << "    self.write_hits = 0\n";
  *_cacheLog << "    self.write_misses = 0\n"; 
  *_cacheLog << "    self.write_on_shared_misses = 0\n";
  *_cacheLog << "    self.write_on_invalid_misses = 0\n";
  *_cacheLog << "    self.invalidates_sent = 0\n";
  *_cacheLog << "    self.l2_hits = 0\n";
  *_cacheLog << "    self.l2_misses = 0\n";
  *_cacheLog << "    self.l3_hits = 0\n";
  *_cacheLog << "    self.l3_misses = 0\n";
  *_cacheLog << "    self.cycles = 0\n";
  *_cacheLog << "\n\n";

  *_cacheLog << "lock_caches = []\n\n";
  *_cacheLog << "class LockCache:\n";
  *_cacheLog << "  def __init__(self):\n";
  *_cacheLog << "    self.id = 0\n";
  *_cacheLog << "    self.sector_hits = 0\n";
  *_cacheLog << "    self.sector_misses = 0\n";
  *_cacheLog << "    self.sector_fills = 0\n";
  *_cacheLog << "    self.sector_updates = 0\n";
  *_cacheLog << "    self.remote_invalidates = 0\n";
  *_cacheLog << "\n\n";

    std::vector<SMPCache *>::iterator cacheIter = allCaches.begin();
    std::vector<SMPCache *>::iterator cacheEndIter = allCaches.end();
    int i=0;
    for(; cacheIter != cacheEndIter; cacheIter++){
      	
      if(!concise){
        (*cacheIter)->dumpStatsToFile(_cacheLog);
      }else{
        (*cacheIter)->conciseDumpStatsToFile(_cacheLog);
      }
	i++;
    }
}

void MultiCacheSim::createNewCache(){

    #ifndef PIN
    pthread_mutex_lock(&allCachesLock);
    #else
    PIN_GetLock(&allCachesLock,1); 
    #endif

    SMPCache * newcache;
    newcache = this->cacheFactory(num_caches++, &allCaches, cache_size, 
                                  cache_assoc, cache_bsize, 1, "LRU", false);
    allCaches.push_back(newcache);


    #ifndef PIN
    pthread_mutex_unlock(&allCachesLock);
    #else
    PIN_ReleaseLock(&allCachesLock); 
    #endif
}

int MultiCacheSim::readLine(unsigned long tid, unsigned long rdPC, unsigned long addr){
    #ifndef PIN
    pthread_mutex_lock(&allCachesLock);
    #else
    PIN_GetLock(&allCachesLock,1); 
    #endif


    SMPCache * cacheToRead = findCacheByCPUId(tidToCPUId(tid));
    if(!cacheToRead){
      return 0;
    }
    int cycles = cacheToRead->readLine(rdPC,addr);


    #ifndef PIN
    pthread_mutex_unlock(&allCachesLock);
    #else
    PIN_ReleaseLock(&allCachesLock); 
    #endif
    return cycles;
}
 
/*
 * Both get and set cycles assume outer synchronization
 */
 
unsigned long long MultiCacheSim::getCycles(unsigned long tid){
  SMPCache *cacheToGet = findCacheByCPUId(tidToCPUId(tid));
  return cacheToGet->getCycles();
}

void MultiCacheSim::setCycles(unsigned long tid, unsigned long long cycles){
  SMPCache *cacheToSet = findCacheByCPUId(tidToCPUId(tid));
  cacheToSet->setCycles(cycles);
}

void MultiCacheSim::updateCycles(unsigned long tid, unsigned long long cycles){
  SMPCache *cacheToUpdate = findCacheByCPUId(tidToCPUId(tid));
  cacheToUpdate->updateCycles(cycles);
}

unsigned long long MultiCacheSim::getInstructions(unsigned long tid){
  SMPCache *cacheToGet = findCacheByCPUId(tidToCPUId(tid));
  return cacheToGet->getInstructions();
}

void MultiCacheSim::incrementInstructions(unsigned long tid){
  SMPCache *cacheToIncrement = findCacheByCPUId(tidToCPUId(tid));
  cacheToIncrement->incrementInstructions();
}

int MultiCacheSim::writeLine(unsigned long tid, unsigned long wrPC, unsigned long addr){
    #ifndef PIN
    pthread_mutex_lock(&allCachesLock);
    #else
    PIN_GetLock(&allCachesLock,1); 
    #endif


    SMPCache * cacheToWrite = findCacheByCPUId(tidToCPUId(tid));
    if(!cacheToWrite){
      return 0;
    }
    int cycles = cacheToWrite->writeLine(wrPC,addr);


    #ifndef PIN
    pthread_mutex_unlock(&allCachesLock);
    #else
    PIN_ReleaseLock(&allCachesLock); 
    #endif
    return cycles;
}

int MultiCacheSim::getStateAsInt(unsigned long tid, unsigned long addr){

  SMPCache * cacheToWrite = findCacheByCPUId(tidToCPUId(tid));
  if(!cacheToWrite){
    return -1;
  }
  return cacheToWrite->getStateAsInt(addr);

}

int MultiCacheSim::tidToCPUId(int tid){
    //simple for now, perhaps we want to be fancier
    return tid % num_caches; 
}

char *MultiCacheSim::Identify(){
  SMPCache *c = findCacheByCPUId(0);
  if(c != NULL){
    return c->Identify();
  }
  return 0;
}
  
MultiCacheSim::~MultiCacheSim(){
    std::vector<SMPCache *>::iterator cacheIter = allCaches.begin();
    std::vector<SMPCache *>::iterator cacheEndIter = allCaches.end();
    for(; cacheIter != cacheEndIter; cacheIter++){
      delete (*cacheIter);
    }
}
