#include "SectorCache.h"

/*
 * Need to iterate over number of entries to put the 
 * entry into the sector cache
 */
void SectorCache::fillLine(int tid, size_t addr)
{
  size_t i = index(addr,tid);
  size_t t = tag(addr);
  
  SectorCacheEntry *line = findLineToFill(tid,addr);
  
  line->_tag = t;
  line->_tid = tid;
}

void SectorCache::invalidateLine(size_t addr, int tid)
{
  if(!isInCache(addr,tid,false)){
    return;
  }
  size_t i = index(addr,tid);
  size_t t = tag(addr);
  
  for(size_t d = 0; d < _assoc; d++){
    if(_cache[i][d]._tag == t){
      _cache[i][d]._tag = 0;
      _cache[i][d]._tid = -1;
    }
  }
}

void SectorCache::remoteInvalidateLine(size_t addr,int tid)
{
  if(!isInCache(addr,0,false,true)){
    return;
  }
  
  ++_remoteInvalidates;
  
  size_t i = index(addr,tid);
  size_t t = tag(addr);
  
  for(size_t d = 0; d < _assoc; d++){
    if(_cache[i][d]._tag == t){
      _cache[i][d]._tag = 0;
      _cache[i][d]._tid = -1;
    }
  }
}

