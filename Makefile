all: compiler sofritas gperf

compiler: compiler/MakeSCC.cpp compiler/Tool.cpp compiler/Tool.h
	cd $(LLVM_ROOT)/build
	make
	cd $(SOFRITAS_ROOT)

sofritas:
	cd lib/sofritas && make
	cd ../../

gperf:
	cd libs/gperftools
	make
	cd ../../
