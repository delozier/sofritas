#ifndef __NO_SCC_H_
#define __NO_SCC_H_

#define main scc_wrapped_main

#ifndef TSAN
/* Used to make some scc calls as empty in code */

#define mmap_mtrom mmap
#define malloc_mtrom malloc
#define calloc_mtrom calloc
#define free_mtrom free
#define munmap_mtrom munmap
#define scc_assure_mtrom(a)

#define scc_start_ts() 
#define scc_end_ts() 
#define scc_sync_threads(mutexVar, ID)

#else

#ifdef __cplusplus
extern "C" {
#endif

/* To help the TSA and Barrier sensitive analysis */
void scc_sync_threads(int *mutexVar, int ID);   /* global barrier for phased tsa. Not implemented yet in sccstm.  */
void scc_start_ts();  /* start threaded section - user specified */
void scc_end_ts();   /* end threaded section - user specified */

void scc_start_ts_auto();  /* start threaded section - scc pass inserted for verification */
void scc_end_ts_auto();   /* end threaded section - scc pass inserted for verification */

/* for tracking ld/st counts per func */
void scc_begin_func(int funcId);

/* For support of multi-threaded read-only heap */
#ifdef SCC_MTROM
#include <sys/mman.h>
  void *malloc_mtrom(size_t size);
  void *calloc_mtrom(size_t numObjects, size_t size);
  void free_mtrom(void *ptr);
  void *mmap_mtrom(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
  int munmap_mtrom(void *addr, size_t length);
  void scc_assure_mtrom(void *ptr);
#else
#define mmap_mtrom mmap
#define malloc_mtrom malloc
#define calloc_mtrom calloc
#define free_mtrom free
#define munmap_mtrom munmap
#define scc_assure_mtrom(a)
#endif

#ifdef __cplusplus
}
#endif

#endif // SCC_H_



#endif

