/*
 * File:
 *  scc.h
 * Author(s):
 *  Gabriel Southern
 * Description:
 *  Redefine pthread functions for SCC.
 *  Should be included in every file that includes pthread.h
 *  except for files that are internal to sccstm library
 */

#ifndef SCC_H_
#define SCC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "scc-threads.h"

#include "scc-redefine.h"

/* To help the TSA and Barrier sensitive analysis */
void scc_sync_threads(int *mutexVar, int ID);   /* global barrier for phased tsa. Not implemented yet in sccstm.  */
void scc_start_ts();  /* start threaded section - user specified */
void scc_end_ts();   /* end threaded section - user specified */

void scc_start_ts_auto();  /* start threaded section - scc pass inserted for verification */
void scc_end_ts_auto();   /* end threaded section - scc pass inserted for verification */

/* for tracking ld/st counts per func */
void scc_begin_func(int funcId);

/* For support of multi-threaded read-only heap */
#ifdef SCC_MTROM
#include <sys/mman.h>
  void *malloc_mtrom(size_t size);
  void *calloc_mtrom(size_t numObjects, size_t size);
  void free_mtrom(void *ptr);
  void *mmap_mtrom(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
  int munmap_mtrom(void *addr, size_t length);
  void scc_assure_mtrom(void *ptr);
#else
#define mmap_mtrom mmap
#define malloc_mtrom malloc
#define calloc_mtrom calloc
#define free_mtrom free
#define munmap_mtrom munmap
#define scc_assure_mtrom(a)
#endif

/* For profiling and loading profiles */
void PROF_set_static_tsak_id(int32_t staticTaskId);
void PROF_sample(int bb_id);
void *PROF_shim_start_routine(void *wrapped_arg);
void PROF_init_module();
void PROF_postblock();
void PROF_preblock();

/* These functions replace their standard counterparts */
char *scc_strcpy(char *dest, const char *src);
char *scc_strncpy(char *dest, const char *src, size_t  spec_len);
int scc_strcmp(const char *a, const char *b);
int scc_strncmp(const char *a, const char *b, size_t len);
size_t scc_strlen(const char *s);

// For debugging user code
void scc_debug_printf(const char *format, ...);

__attribute__((always_inline)) static inline uint64_t scc_debug_get_rsp() {
  uint64_t rsp;
  asm("movq %%rsp, %0" : "=r" (rsp));
  return rsp;
}

__attribute__((always_inline)) static inline uint64_t scc_debug_get_rbp() {
  uint64_t rbp;
  asm("movq %%rbp, %0" : "=r" (rbp));
  return rbp;
}


#ifdef __cplusplus
}
#endif

#endif // SCC_H_

