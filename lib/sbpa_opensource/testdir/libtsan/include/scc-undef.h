#ifdef SCC_REDEFINE_H_
#undef SCC_REDEFINE_H_

// Redefine pthread directives
#undef pthread_create          
#undef pthread_join            
#undef pthread_exit           
#undef pthread_mutex_lock    
#undef pthread_mutex_unlock 
#undef pthread_setcancelstate 
#undef pthread_setcanceltype 

// Condition variable 
// #undef pthread_cond_init    
// #undef pthread_cond_wait   
// #undef pthread_cond_signal
// #undef pthread_cond_broadcast
// #undef pthread_cond_t       
// #undef PTHREAD_COND_INITALIZER 

// Barrier
#undef pthread_barrier_t      
#undef pthread_barrier_init  
#undef pthread_barrier_destroy
#undef pthread_barrier_wait  

// Redefine other miscellaneous functions 
#undef exit                 

#endif // SCC_REDEFINE_H_ 

