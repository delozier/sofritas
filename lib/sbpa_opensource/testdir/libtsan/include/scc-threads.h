/*
 * File:
 *  scc-threads.h
 * Author(s):
 *  Gabriel Southern
 * Description:
 *  Wrapper for pthread directives for scc
 */

#ifndef SCCTHREADS_H_
#define SCCTHREADS_H_

#include <pthread.h>
#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif
#include <stdint.h>
//#include <stdatomic.h>

#define SCC_BARRIER_SPEC 

/* ##########################################################
 * Macros
 * ########################################################## */
#define SCC_THREAD_COND_INITIALIZER PTHREAD_COND_INITIALIZER
#define scc_thread_cond_t pthread_cond_t


/* ##########################################################
 * Globals
 * ########################################################## */
#ifdef __cplusplus
extern "C" {
#endif
  // -----------------------------------------------------------
  // Types
  // -----------------------------------------------------------

  //Barrier 
  typedef struct {
    uint64_t init;
    volatile uint64_t arrival_cnt;
    volatile uint64_t departure_cnt;
    volatile int arrival_phase;
    volatile int arrival_wait;
    volatile int departure_wait;
      
  } scc_thread_barrier_t __attribute__ ((aligned (32)));

#if 0
  typedef struct {
    uint64_t init;
     wait; // signed
    std::atomic<int64_t> release; // signed
  } scc_thread_barrier_t;
#endif

  // ----------------------------------------------------------
  // Functions
  // ----------------------------------------------------------
  __attribute__((noinline)) void scc_split_task();
  extern int scc_wrapped_main(int argc, char **argv);
  
  int scc_thread_create(pthread_t *thread, __const pthread_attr_t *attr,
      void *(*start_routine)(void*), void *start_arg);
 
  int scc_thread_join(pthread_t thread, void **value_ptr);
  void scc_thread_exit(void *value_ptr);

  void scc_exit(int status)  __THROW __attribute__ ((__noreturn__));
  
  // Mutex

  // Condition variable 
  int scc_thread_setcancelstate(int state, int *oldstate);
  int scc_thread_setcanceltype(int type, int *oldtype);

  // Barrier
  int scc_thread_barrier_destroy(scc_thread_barrier_t *barrier);
  int scc_thread_barrier_init(scc_thread_barrier_t * __restrict__ barrier,
      const pthread_barrierattr_t * __restrict__ attr, unsigned count);
  int scc_thread_barrier_wait(scc_thread_barrier_t *barrier);


#ifdef __cplusplus
}
#endif

#endif // SCCTHREADS_H_

