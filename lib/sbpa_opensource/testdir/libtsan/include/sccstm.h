/*
 * File:
 *    scc.h
 * Author(s):
 *    Gabriel Southern
 *    Jose Renau
 * Description:
 *    Implement SCC in software using STM like functionality
 */

#ifndef SCCSTM_H_
#define SCCSTM_H_

// This is needed before stdint.h is included
#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif
#include <stdint.h>

#include <string.h>
#include <setjmp.h>
#include <signal.h>

/* ##########################################################
 * Functions
 * ########################################################## */

int scc_spawn_thread(pthread_t *thread, __const pthread_attr_t *attr,
    void *(*start_routine)(void *),void *args); 

extern "C" void __attribute__((always_inline)) scc_split_task();

// Thread wait until it is safe before continuing
extern "C" void scc_sync_safe();

extern "C" {

  void SCCloadReplaceContained(void* addr, size_t size, void* outbuffer);
  void SCCstoreReplaceContained(void* addr, size_t size, void* inbuffer);

  // Note that Int8* can never be an uncontained access.
  uint8_t  SCCloadReplaceContainedInt8  (uint8_t*  addr);
  uint16_t SCCloadReplaceContainedInt16 (uint16_t* addr);
  uint32_t SCCloadReplaceContainedInt32 (uint32_t* addr);
  uint64_t SCCloadReplaceContainedInt64 (uint64_t* addr);
  float    SCCloadReplaceContainedFloat (float*    addr);
  double   SCCloadReplaceContainedDouble(double*   addr);
  void*    SCCloadReplaceContainedPtr   (void**    addr);

  uint16_t SCCloadReplaceRangeInt16 (uint16_t* addr);
  uint32_t SCCloadReplaceRangeInt32 (uint32_t* addr);
  uint64_t SCCloadReplaceRangeInt64 (uint64_t* addr);
  float    SCCloadReplaceRangeFloat (float*    addr);
  double   SCCloadReplaceRangeDouble(double*   addr);
  void*    SCCloadReplaceRangePtr   (void**    addr);

  void SCCstoreReplaceContainedInt8  (uint8_t*  addr, uint8_t  value);
  void SCCstoreReplaceContainedInt16 (uint16_t* addr, uint16_t value);
  void SCCstoreReplaceContainedInt32 (uint32_t* addr, uint32_t value);
  void SCCstoreReplaceContainedInt64 (uint64_t* addr, uint64_t value);
  void SCCstoreReplaceContainedFloat (float*    addr, float    value);
  void SCCstoreReplaceContainedDouble(double*   addr, double   value);
  void SCCstoreReplaceContainedPtr   (void**    addr, void*    value);

  /* These are used only for restart. there should be no conflict  check done for these addresses. */
  /* In systems without restarts ( like coredet), there is no need to intrument these */
  void SCCstoreForRestartOnlyInt8  (uint8_t*  addr, uint8_t  value);
  void SCCstoreForRestartOnlyInt16 (uint16_t* addr, uint16_t value);
  void SCCstoreForRestartOnlyInt32 (uint32_t* addr, uint32_t value);
  void SCCstoreForRestartOnlyInt64 (uint64_t* addr, uint64_t value);
  void SCCstoreForRestartOnlyFloat (float*    addr, float    value);
  void SCCstoreForRestartOnlyDouble(double*   addr, double   value);
  void SCCstoreForRestartOnlyPtr   (void**    addr, void*    value);

  /* For loop invariant store lgging motion of single address */
  void SCCstoreLogInt8  (uint8_t*  addr);
  void SCCstoreLogInt16 (uint16_t* addr);
  void SCCstoreLogInt32 (uint32_t* addr);
  void SCCstoreLogInt64 (uint64_t* addr);
  void SCCstoreLogFloat (float*    addr);
  void SCCstoreLogDouble(double*   addr);
  void SCCstoreLogPtr   (void**    addr);

  void SCCstoreReplaceRangeInt16 (uint16_t* addr, uint16_t value);
  void SCCstoreReplaceRangeInt32 (uint32_t* addr, uint32_t value);
  void SCCstoreReplaceRangeInt64 (uint64_t* addr, uint64_t value);
  void SCCstoreReplaceRangeFloat (float*    addr, float    value);
  void SCCstoreReplaceRangeDouble(double*   addr, double   value);
  void SCCstoreReplaceRangePtr   (void**    addr, void*    value);

  void SCCmemcpy(void *dest, void *src, size_t size);
  void SCCmemset(void *s, int c, size_t size);

  /* following 4 functions for black listing ( read/write exclusions) */
  void scc_init_all_blacklist_locks();
  void scc_clear_all_blacklist_locks(int threadId);
  void scc_start_write_phase(int id);
  void scc_start_read_phase(int id);
  /* void scc_set_logonly_store(int32_t isLogOnly); */

  /* For LILM: array elem accesses in loop */
  void SCCstoreArrayInt64(uint64_t *addr, int64_t start, int64_t end);
  void SCCloadArrayInt64(uint64_t *addr, int64_t start, int64_t end);
  void SCCstoreArrayInt32(uint32_t *addr, int64_t start, int64_t end);
  void SCCloadArrayInt32(uint32_t *addr, int64_t start, int64_t end);
  void SCCstoreArrayFloat(float *addr, int64_t start, int64_t end);
  void SCCloadArrayFloat(float *addr, int64_t start, int64_t end);
  void SCCstoreArrayDouble(double *addr, int64_t start, int64_t end);
  void SCCloadArrayDouble(double *addr, int64_t start, int64_t end);
}


extern "C" {
  int scc_wrapped_main(int argc, char **argv);
}

//void scc_thread_exit(void *value_ptr);

// Debugging support
#if 0
extern "C" {
  void scc_debug_printf(const char *format, ...);
  void scc_debug_stdout(const char *format, ...);
  uint64_t scc_debug_rdtscp();
}
#endif

#endif /* SCCSTM_H_ */

