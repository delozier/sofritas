#include "scc-threads.h"


#define SCC_REDEFINE_H_


// Redefine pthread directives
#define pthread_create          scc_thread_create
#define pthread_join            scc_thread_join
#define pthread_exit            scc_thread_exit

// #define pthread_mutex_lock      scc_thread_mutex_lock
// #define pthread_mutex_unlock    scc_thread_mutex_unlock
// #define pthread_setcancelstate  scc_thread_setcancelstate
// #define pthread_setcanceltype   scc_thread_setcanceltype

// Condition variable 
// #define pthread_cond_init       scc_thread_cond_init
// #define pthread_cond_wait       scc_thread_cond_wait
// #define pthread_cond_signal     scc_thread_cond_signal 
// #define pthread_cond_broadcast  scc_thread_cond_broadcast
// #define pthread_cond_t          scc_thread_cond_t
// #define pthread_cond_destroy    scc_thread_cond_destroy
// #define PTHREAD_COND_INITALIZER SCC_THREAD_COND_INITALIZER

// Barrier
// #define pthread_barrier_t       scc_thread_barrier_t
// #define pthread_barrier_init    scc_thread_barrier_init
// #define pthread_barrier_destroy scc_thread_barrier_destroy
// #define pthread_barrier_wait    scc_thread_barrier_wait

// Redefine other miscellaneous functions 
#define exit                    scc_exit


//#endif // SCC_REDEFINE_H_ 

