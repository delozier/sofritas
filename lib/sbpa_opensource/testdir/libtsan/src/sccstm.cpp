/*
 * File:
 *  sccstm.c
 * Author(s):
 *  Gabriel Southern
 *  Jose Renau
 * Description:
 *  Implementation for sccstm library
 */

#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdarg.h>

#include <sys/syscall.h>
#include <sys/types.h>
#include <linux/unistd.h>
#include <errno.h>

#include <stdint.h>
#include <setjmp.h>
#include <pthread.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <atomic>

#include "sccstm.h"
//#include "sccstm-internal.h"
#include "scc-threads.h"
#include "nanassert.h"

/* ###########################################################
 * Constants
 * ########################################################### */
#define SCC_CACHE_LINE_SIZE 64
#define SCC_MAX_NUM_THREADS 64
#define SCC_MAX_CHECKPOINTS 8
#define SCC_UINT64_MAX      0xFFFFFFFFFFFFFFFF
#define SCC_MAX_VALID_TASK  SCC_UINT64_MAX 
#define SCC_TAG_INVALID     0xFFFFFFFFFFFFFFFF
#define SCC_TAG_MASK        0xFFFFFFFFFFFFFFE0
#define SCC_TAG_ALIGN       6
#define SCC_SIGNUM          SIGUSR1
#define SCC_VERSION_BITS    63
#define SCC_THREAD_BITS     8
#define SCC_FLAG_BITS       3
#define SCC_TASKVER_BITS    (SCC_VERSION_BITS - SCC_THREAD_BITS - SCC_FLAG_BITS)
#define SCC_TASKVER_MASK    (SCC_UINT64_MAX << (SCC_THREAD_BITS + SCC_FLAG_BITS))
#define SCC_MAX_TASK_VER    (SCC_UINT64_MAX >> SCC_THREAD_BITS)
#define SCC_MAX_THREAD_VER  (SCC_UINT64_MAX >> SCC_TASKVER_BITS)
#define SCC_MAX_THREAD_SPAWN_PER_TASK 255
#define SCC_MAX_VERSION_NODES 64
#define SCC_THREAD_VC_SIZE  1000

#define SCC_CACHE_LINE_MASK 0xFFFFFFFFFFFFFF00


#define SCC_DIRTY_BIT       0x1
#define SCC_MR_BIT          0x2
#define SCC_VALID_BIT       0x4

#define SCC_ENTRY_FLAG_MASK     0xFFFFFFFFFFFFFFF8
#define SCC_ENTRY_THREAD_MATCH  0x07F8

#define SCC_PAGE_SIZE       4096
#define SCC_PAGE_MASK       0xFFFFFFFFFFFFF000

#define SCC_STACK_SIZE      (100*SCC_PAGE_SIZE)

#define SCC_HTABLE_SIZE        0x10000
#define SCC_HTABLE_MASK         0xFFFF
//#define SCC_VERSION_FROM_FUTURE 0xFFFFFFFF
#define SCC_HASH_CONFLICT        (-1)

// Need to check on what first safe version is
#define SCC_FIRST_SAFE_VERSION (1 << (SCC_THREAD_BITS + SCC_FLAG_BITS))

#define SCC_BARRIER_TASK_INC (1 << (SCC_THREAD_BITS + SCC_FLAG_BITS))

#define SYSTEM_CACHE_LINE_SIZE 64

//#define NO_RDTSCP
/* ########################################################## 
 * MACROS 
 * ########################################################## */
#define ERROR(a...) SCC_PRINT_STDERR(a)

#ifdef DEBUG2
#define DEBUG_PRINT(a...) SCC_PRINT(a)
#define TRACE(a...)       SCC_PRINT(a)
#define LOG(a...)         SCC_PRINT(a)
#else
#define DEBUG_PRINT(a...) 
#define TRACE(a...)       
#define LOG(a...)         
#endif

#ifdef DEBUG2
#define DEBUG_LINE_MSG(a) SCC_PRINT("%s occured in %s at %d\n",a,__FILE__,__LINE__)
#else
#define DEBUG_LINE_MSG(a)
#endif

#define SCC_STRING_CONCAT(str1,str2) str1 " " str2

#define SCC_PRINT_STDERR(format, a...) {\
  char buf[256]; \
  snprintf(buf, 256, format, ##a); \
  int len = strlen(buf); \
  write(STDERR_FILENO,&buf,len); \
}

#define SCC_GET_RSP(rsp) asm("movq %%rsp, %0" : "=r" (rsp))
#define SCC_GET_RBP(rbp) asm("movq %%rbp, %0" : "=r" (rbp))

#define HASH1(a) ((((a)^((a)>>7)) & SCC_HTABLE_MASK) ) 

//Not using other hash functions right now
//#define HASH2(a) ((((a)^((a)>>9)+a) & SCC_HTABLE_MASK) + 1)
//#define HASH1(a) ((((a)^((a)>>7)) & SCC_HTABLE_MASK) + 1)
//#define HASH2(a) ((((a)^((a>>3))) & SCC_HTABLE_MASK) + 1) 

// Statistics
#ifdef SCC_STAT_ENABLE
  #define SCC_STAT_INC(stat) (stats[threadId].stat)++; 
#else
  #define SCC_STAT_INC(stat)
#endif 

//#define FORCE_SCC_INLINE 1
#ifdef FORCE_SCC_INLINE
  #define ALWAYS_INLINE __attribute__((always_inline)) 
#else
  #define ALWAYS_INLINE
#endif

/* ###########################################################
 * Enums
 * ########################################################### */
enum scc_task_state {
  TASK_SPEC_RUN,
  TASK_SPEC_DONE,
  TASK_SAFE,
  TASK_SYNC_WAIT,
  TASK_FINAL,
  TASK_UNUSED,
  TASK_INVALID,
};


/* ###########################################################
 * Types
 * ########################################################### */
typedef int64_t scc_version_t;
typedef uint64_t scc_addr_t;

typedef struct {
  unsigned char data[SCC_CACHE_LINE_SIZE];
} scc_data_entry_t; 

 
typedef struct scc_thread_args {
  scc_version_t task_ver;
  uint8_t       thread_index;
  size_t        stack_size;
  void *(*wrapped_function)(void *);
  void *wrapped_arg;
} scc_thread_args_t;


typedef struct {
  int argc;
  char **argv;
  int (*main_func)(int,char **);
} scc_main_args_t; 

typedef struct scc_thread_entry {
  scc_version_t version;
  scc_version_t thread_index;
  int32_t threads_spawned;
  int32_t locks_held;
  int32_t restart_disabled;
  pthread_t pthread_id;
  pthread_attr_t attr;
  scc_thread_args_t arg;
  stack_t signal_stack;
  void *stack_mem;
  //size_t stack_log_limit;
  size_t stack_size;
  size_t stack_min_addr;
  size_t stack_max_addr;
  uint64_t detached;
  int cp_index;
  int ver_node_index;
  int vc_index;
  int logfile;
} scc_thread_entry_t;


scc_thread_entry_t scc_threads[SCC_MAX_NUM_THREADS+1];


/* #########################################################
 * Internal functions
 * ######################################################### */

template<typename loadType>
loadType scc_inline_load(loadType *addr);

template<typename storeType>
void scc_inline_store(storeType *addr,storeType value);


void sync_safe_internal();
static void segv_handler(int sig, siginfo_t *si, void *unused);
static void *wrapper(void *arg);
void scc_stat_print();
void scc_add_thread_stats(int id);
static void scc_init();
void inline scc_inline_mark_load(scc_addr_t addr);
void inline scc_inline_mark_store(scc_addr_t addr);

// profiling related
extern "C" {
  void PROF_init_thread(uint32_t threadId);
  void PROF_end_task();
  int scc_wrapped_main(int argc, char **argv);
}

// #define SCC_BB_PROFILING

/* ###################################################################
 * DEFINES (possibly place in sccstm-internal)
 * ################################################################### */
#ifdef SCC_MTROM
// Note that the address listed is an arbitrary high address we should check to make
// sure there are no conflict in case a program wants to use this address
#define SCC_MMAP_MTROM_START_ADDR 0x80000000000000

// Note that 20000 is an arbitrary large value
#define SCC_MMAP_FILESIZE (400000 * SCC_PAGE_SIZE)
#endif //SCC_MTROM

//extern "C" int native_pthread_create(pthread_t  *thread,  __const pthread_attr_t *attr, void * (*start_routine)(void *), void * arg);

/* ###################################################################
 * Global variables
 * ################################################################### */
#ifdef SCC_MTROM
size_t mtrdCurPtr = 0;
int mtrdFd = 0;
void *mtrdMap = NULL;     /* mmapped array of int's */
void *mtrdMapEnd = NULL;  /* mmapped array of int's */
size_t mtrdMappedSize = 0;
#endif //SCC_MTROM

static std::atomic<int> active_threads;

//TODO: evaluate whether there is a problem with false sharing for this variable.
// Same problem could exist for others variables such as scc_safe_version
//std::atomic<scc_version_t> task_list_lock;
std::atomic_flag task_list_lock = ATOMIC_FLAG_INIT;

scc_main_args_t scc_main_args;
pthread_t main_thread;

/* ###################################################################
 * SCC_STATS variables  
 * ################################################################### */

#ifdef SCC_STAT_ENABLE
////////////// Global stats /////////////////////////////
uint64_t main_start;

///////////// Per thread stats //////////////////////////

// MT readonly memory
class per_thread_stat
{
 public:
  uint64_t scc_stat_mtrdonly_ld = 0;

  // Total loads and stores
  uint64_t scc_stat_ld_total = 0;
  uint64_t scc_stat_rst_total = 0;
  uint64_t scc_stat_lst_total = 0;
  
  // Totals by type
  uint64_t scc_stat_st_ld_total = 0;
  uint64_t scc_stat_st_rst_total = 0;
  uint64_t scc_stat_st_lst_total = 0;
  uint64_t scc_stat_mt_ld_total = 0;
  uint64_t scc_stat_mt_rst_total = 0;
  uint64_t scc_stat_mt_lst_total = 0;

// Safe load paths
  uint64_t scc_stat_safe_ld_new = 0;
  uint64_t scc_stat_safe_ld_cur = 0;
  uint64_t scc_stat_safe_ld_stale = 0;
  uint64_t scc_stat_safe_ld_dirty = 0;
  uint64_t scc_stat_safe_ld_mark_mr = 0;
  uint64_t scc_stat_safe_ld_clean = 0;
  uint64_t scc_stat_safe_ld_mr = 0;

  // Spec load paths
  uint64_t scc_stat_spec_inline_ld;
  uint64_t scc_stat_spec_ld_cur;
  uint64_t scc_stat_spec_ld_new;
  uint64_t scc_stat_spec_ld_dirty;
  uint64_t scc_stat_spec_ld_mark_mr;
  uint64_t scc_stat_spec_ld_mr;

  // Safe store paths
  uint64_t scc_stat_safe_st_new;
  uint64_t scc_stat_safe_st_cur;
  uint64_t scc_stat_safe_st_stale;
  uint64_t scc_stat_safe_st_mr;
  uint64_t scc_stat_safe_st_dirty;
  uint64_t scc_stat_safe_st_rw;
  uint64_t scc_stat_safe_st_clean;

  // Spec store paths
  uint64_t scc_stat_spec_inline_st;
  uint64_t scc_stat_spec_st_cur;
  uint64_t scc_stat_spec_st_rw;
  uint64_t scc_stat_spec_st_new;
  uint64_t scc_stat_spec_st_dirty;
  uint64_t scc_stat_spec_st_mr;

  // Misc. stats
  uint64_t scc_stat_atomic_retry;
  uint64_t scc_stat_hash_conflict;

  // Sync safes
  uint64_t scc_stat_sync_safe;
  uint64_t scc_stat_sync_safe_internal;
  uint64_t scc_stat_sync_safe_thread;
  uint64_t scc_stat_set_next_safe;

  // Thread directives
  uint64_t scc_stat_split_task;
  uint64_t scc_stat_mutex_lock;
  uint64_t scc_stat_mutex_unlock;
  uint64_t scc_stat_barrier_wait;

#ifdef SCC_STAT_STACK_CNT
  uint64_t scc_stat_stack_ld;
  uint64_t scc_stat_stack_st;
#endif

#ifdef PROFILE_SPINLOCK
  uint64_t scc_stat_spinlock_cycles;
#endif

  uint64_t scc_stat_all_cycles ;
  uint64_t scc_stat_barrier_cycles ;
  uint64_t scc_stat_sync_safe_internal_cycles;

  public:
     per_thread_stat() {
	memset(this, sizeof(per_thread_stat), 0);
        scc_stat_all_cycles = 1;
     }
     ~per_thread_stat() {}

}; // per thread stats

namespace {
per_thread_stat stats[SCC_MAX_NUM_THREADS]; // upto 32 threads
int globalThreadId = 0;
}

__thread int threadId = 0;

///////////// Total stats //////////////////////

// MT readonly memory ( MTROM, and earlier name of MT-RDONLY few places)
uint64_t scc_stat_total_mtrdonly_ld = 0;

// Total loads and stores
uint64_t scc_stat_total_ld_total = 0;
uint64_t scc_stat_total_rst_total = 0;
uint64_t scc_stat_total_lst_total = 0;

// Totals by type
uint64_t scc_stat_total_st_ld_total = 0;
uint64_t scc_stat_total_st_rst_total = 0;
uint64_t scc_stat_total_st_lst_total = 0;
uint64_t scc_stat_total_mt_ld_total = 0;
uint64_t scc_stat_total_mt_rst_total = 0;
uint64_t scc_stat_total_mt_lst_total = 0;

// Safe load paths
uint64_t scc_stat_total_safe_ld_new = 0;
uint64_t scc_stat_total_safe_ld_cur = 0;
uint64_t scc_stat_total_safe_ld_stale = 0;
uint64_t scc_stat_total_safe_ld_dirty = 0;
uint64_t scc_stat_total_safe_ld_mark_mr = 0;
uint64_t scc_stat_total_safe_ld_mr = 0;

// Spec load paths
uint64_t scc_stat_total_spec_inline_ld = 0;
uint64_t scc_stat_total_spec_ld_cur = 0;
uint64_t scc_stat_total_spec_ld_new = 0;
uint64_t scc_stat_total_spec_ld_dirty = 0;
uint64_t scc_stat_total_spec_ld_mark_mr = 0;
uint64_t scc_stat_total_spec_ld_mr = 0;

// Safe store paths
uint64_t scc_stat_total_safe_st_new = 0;
uint64_t scc_stat_total_safe_st_stale= 0;
uint64_t scc_stat_total_safe_st_mr = 0;
uint64_t scc_stat_total_safe_st_dirty = 0;
uint64_t scc_stat_total_safe_st_clean = 0;
uint64_t scc_stat_total_safe_st_rw = 0;
uint64_t scc_stat_total_safe_st_cur = 0;

// Spec store paths
uint64_t scc_stat_total_spec_inline_st = 0;
uint64_t scc_stat_total_spec_st_cur = 0;
uint64_t scc_stat_total_spec_st_rw = 0;
uint64_t scc_stat_total_spec_st_new = 0;
uint64_t scc_stat_total_spec_st_dirty = 0;
uint64_t scc_stat_total_spec_st_mr = 0;

// Misc. stats
uint64_t scc_stat_total_atomic_retry = 0;
uint64_t scc_stat_total_hash_conflict = 0;

// Restarts
uint64_t scc_stat_total_restart_single = 0;
uint64_t scc_stat_total_restart_all = 0;
uint64_t scc_stat_total_restart_masked = 0;

// safe
uint64_t scc_stat_total_sync_safe = 0;
uint64_t scc_stat_total_sync_safe_internal = 0;
uint64_t scc_stat_total_sync_safe_thread = 0;
uint64_t scc_stat_total_set_next_safe = 0;

// Thread directives
uint64_t scc_stat_total_split_task = 0;
uint64_t scc_stat_total_mutex_lock = 0;
uint64_t scc_stat_total_mutex_unlock = 0;
uint64_t scc_stat_total_barrier_wait = 0;

#ifdef SCC_STAT_STACK_CNT
uint64_t scc_stat_total_stack_ld = 0;
uint64_t scc_stat_total_stack_st = 0;
#endif
   
uint64_t scc_stat_total_all_cycles = 0;
uint64_t scc_stat_total_barrier_cycles = 0;
uint64_t scc_stat_total_sync_safe_internal_cycles = 0;

#endif // SCC_STAT_ENABLE

/* for per-function stat, added by compiler, so must be unconditional */ 
__thread int sccGlobalFuncId = 0;
uint64_t scc_ld_cnt_per_func[1000];
uint64_t scc_st_cnt_per_func[1000];



/* ###################################################################
 * MTROM variables
 * ################################################################### */
int sccNumMtromMmaps = 0;
void *scc_mtrom_mmap_starts[10];  // start and end of mmap that is multi-thread rom
void *scc_mtrom_mmap_ends[10];  // start and end of mmap that is multi-thread rom

 
/* ###################################################################
 * SCC_STATS functions 
 * ################################################################### */
#ifdef SCC_STAT_ENABLE

void scc_reset_thread_stats(int id)
{
  // MT readonly memory
  stats[id].scc_stat_mtrdonly_ld = 0;

  // Total loads and stores
  stats[id].scc_stat_ld_total = 0;
  stats[id].scc_stat_rst_total = 0;
  stats[id].scc_stat_lst_total = 0;

  // Totals by type
  stats[id].scc_stat_st_ld_total = 0;
  stats[id].scc_stat_st_rst_total = 0;
  stats[id].scc_stat_st_lst_total = 0;
  stats[id].scc_stat_mt_ld_total = 0;
  stats[id].scc_stat_mt_rst_total = 0;
  stats[id].scc_stat_mt_lst_total = 0;

  // Safe load paths
  stats[id].scc_stat_safe_ld_new = 0;
  stats[id].scc_stat_safe_ld_stale = 0;
  stats[id].scc_stat_safe_ld_dirty = 0;
  stats[id].scc_stat_safe_ld_clean = 0;
  stats[id].scc_stat_safe_ld_mr = 0;

  // Spec load paths
  stats[id].scc_stat_spec_inline_ld = 0;
  stats[id].scc_stat_spec_ld_cur = 0;
  stats[id].scc_stat_spec_ld_new = 0;
  stats[id].scc_stat_spec_ld_dirty = 0;
  stats[id].scc_stat_spec_ld_mark_mr = 0;
  stats[id].scc_stat_spec_ld_mr = 0;

  // Safe store paths
  stats[id].scc_stat_safe_st_new = 0;
  stats[id].scc_stat_safe_st_stale = 0;
  stats[id].scc_stat_safe_st_mr = 0;
  stats[id].scc_stat_safe_st_dirty = 0;
  stats[id].scc_stat_safe_st_clean = 0;
  stats[id].scc_stat_safe_st_cur = 0;
  stats[id].scc_stat_safe_st_rw = 0;

  // Spec store paths
  stats[id].scc_stat_spec_inline_st = 0;
  stats[id].scc_stat_spec_st_cur = 0;
  stats[id].scc_stat_spec_st_rw = 0;
  stats[id].scc_stat_spec_st_new = 0;
  stats[id].scc_stat_spec_st_dirty = 0;
  stats[id].scc_stat_spec_st_mr = 0;

  // Misc. stats
  stats[id].scc_stat_atomic_retry = 0;
  stats[id].scc_stat_hash_conflict = 0;
  
  // Sync safe
  stats[id].scc_stat_sync_safe = 0;
  stats[id].scc_stat_sync_safe_internal = 0;
  stats[id].scc_stat_sync_safe_thread = 0;
  stats[id].scc_stat_set_next_safe = 0;

  // Thread directives
  stats[id].scc_stat_split_task = 0;
  stats[id].scc_stat_mutex_lock = 0;
  stats[id].scc_stat_mutex_unlock = 0;
  stats[id].scc_stat_barrier_wait = 0;

#ifdef SCC_STAT_STACK_CNT
  stats[id].scc_stat_stack_ld = 0;
  stats[id].scc_stat_stack_st = 0;
#endif

  // It has the last time ; scc_stat_all_cycles = 0;
  stats[id].scc_stat_barrier_cycles = 0;
  stats[id].scc_stat_sync_safe_internal_cycles = 0;
}


// Add thread stats to total
void scc_add_thread_stats(int id)
{
  // MT readonly memory
  scc_stat_total_mtrdonly_ld += stats[id].scc_stat_mtrdonly_ld;

  // Total loads and stores
  scc_stat_total_ld_total += stats[id].scc_stat_ld_total;
  scc_stat_total_rst_total += stats[id].scc_stat_rst_total;
  scc_stat_total_lst_total += stats[id].scc_stat_lst_total;

  // Totals by type
  scc_stat_total_st_ld_total  += stats[id].scc_stat_st_ld_total;
  scc_stat_total_st_rst_total += stats[id].scc_stat_st_rst_total;
  scc_stat_total_st_lst_total += stats[id].scc_stat_st_lst_total;
  scc_stat_total_mt_ld_total  += stats[id].scc_stat_mt_ld_total;
  scc_stat_total_mt_rst_total += stats[id].scc_stat_mt_rst_total;
  scc_stat_total_mt_lst_total += stats[id].scc_stat_mt_lst_total;

  //Loads
  scc_stat_total_safe_ld_new += stats[id].scc_stat_safe_ld_new;
  scc_stat_total_safe_ld_cur += stats[id].scc_stat_safe_ld_cur;
  scc_stat_total_safe_ld_stale += stats[id].scc_stat_safe_ld_stale;
  scc_stat_total_safe_ld_dirty += stats[id].scc_stat_safe_ld_dirty;
  scc_stat_total_safe_ld_mark_mr += stats[id].scc_stat_safe_ld_mark_mr;
  scc_stat_total_safe_ld_mr += stats[id].scc_stat_safe_ld_mr;
  
  scc_stat_total_spec_inline_ld += stats[id].scc_stat_spec_inline_ld;
  scc_stat_total_spec_ld_cur += stats[id].scc_stat_spec_ld_cur;
  scc_stat_total_spec_ld_new += stats[id].scc_stat_spec_ld_new;
  scc_stat_total_spec_ld_dirty += stats[id].scc_stat_spec_ld_dirty;
  scc_stat_total_spec_ld_mark_mr += stats[id].scc_stat_spec_ld_mark_mr;
  scc_stat_total_spec_ld_mr += stats[id].scc_stat_spec_ld_mr;

  //Stores
  scc_stat_total_safe_st_new += stats[id].scc_stat_safe_st_new;
  scc_stat_total_safe_st_stale += stats[id].scc_stat_safe_st_stale;
  scc_stat_total_safe_st_mr += stats[id].scc_stat_safe_st_mr;
  scc_stat_total_safe_st_dirty += stats[id].scc_stat_safe_st_dirty;
  scc_stat_total_safe_st_clean += stats[id].scc_stat_safe_st_clean;
  scc_stat_total_safe_st_cur += stats[id].scc_stat_safe_st_cur;
  scc_stat_total_safe_st_rw += stats[id].scc_stat_safe_st_rw ;

  scc_stat_total_spec_inline_st += stats[id].scc_stat_spec_inline_st;
  scc_stat_total_spec_st_cur += stats[id].scc_stat_spec_st_cur;
  scc_stat_total_spec_st_rw += stats[id].scc_stat_spec_st_rw ;
  scc_stat_total_spec_st_new += stats[id].scc_stat_spec_st_new;
  scc_stat_total_spec_st_dirty += stats[id].scc_stat_spec_st_dirty;
  scc_stat_total_spec_st_mr = stats[id].scc_stat_spec_st_mr;
 
  //Misc.
  scc_stat_total_atomic_retry += stats[id].scc_stat_atomic_retry;
  scc_stat_total_hash_conflict += stats[id].scc_stat_hash_conflict;

  //Thread directives
  scc_stat_total_split_task += stats[id].scc_stat_split_task;
  scc_stat_total_mutex_lock += stats[id].scc_stat_mutex_lock ;
  scc_stat_total_mutex_unlock += stats[id].scc_stat_mutex_unlock;
  scc_stat_total_barrier_wait += stats[id].scc_stat_barrier_wait;

  scc_stat_total_sync_safe += stats[id].scc_stat_sync_safe;
  scc_stat_total_sync_safe_internal += stats[id].scc_stat_sync_safe_internal;
  scc_stat_total_sync_safe_thread +=  stats[id].scc_stat_sync_safe_thread;
  scc_stat_total_set_next_safe = stats[id].scc_stat_set_next_safe;

#ifdef SCC_STAT_STACK_CNT
  scc_stat_total_stack_ld += stats[id].scc_stat_stack_ld;
  scc_stat_total_stack_st += stats[id].scc_stat_stack_st;
#endif

#ifdef PROFILE_SPINLOCK
  scc_stat_total_spinlock_cycles += stats[id].scc_stat_spinlock_cycles;
#endif

  scc_reset_thread_stats(id);
}

// Print stats
void scc_stat_print()
{
  fprintf(stderr,"Printing Stats\n");

#define STAT_SPACING 25
  FILE *scc_stats_file;
  scc_stats_file = fopen("sccstats.txt","w");
  fprintf(scc_stats_file,"===================================================\n");
  fprintf(scc_stats_file,"SCC STATS BEGIN\n");
  fprintf(scc_stats_file,"---------------------------------------------------\n");
  fprintf(scc_stats_file,"SAFE thread ops:\n");
  fprintf(scc_stats_file,"safe ld unused:           %*lu\n", STAT_SPACING, scc_stat_total_safe_ld_new);
  fprintf(scc_stats_file,"safe ld stale:            %*lu\n", STAT_SPACING, scc_stat_total_safe_ld_stale);
  fprintf(scc_stats_file,"safe ld dirty (restart):  %*lu\n", STAT_SPACING, scc_stat_total_safe_ld_dirty);

  fprintf(scc_stats_file,"safe st unused:           %*lu\n", STAT_SPACING, scc_stat_total_safe_st_new);
  fprintf(scc_stats_file,"safe st stale:            %*lu\n", STAT_SPACING, scc_stat_total_safe_st_stale);
  fprintf(scc_stats_file,"safe st mr (restart all): %*lu\n", STAT_SPACING, scc_stat_total_safe_st_mr);
  fprintf(scc_stats_file,"safe st dirty (restart):  %*lu\n", STAT_SPACING, scc_stat_total_safe_st_dirty);
  //fprintf(scc_stats_file,"total safe:               %*lu\n", STAT_SPACING, total_safe_ops);                        
  fprintf(scc_stats_file,"\n");
  fprintf(scc_stats_file,"SPEC thread ops\n");
  fprintf(scc_stats_file,"spec ld inline :          %*lu\n", STAT_SPACING, scc_stat_total_spec_inline_ld);
  fprintf(scc_stats_file,"spec ld current:          %*lu\n", STAT_SPACING, scc_stat_total_spec_ld_cur);
  fprintf(scc_stats_file,"spec ld new:              %*lu\n", STAT_SPACING, scc_stat_total_spec_ld_new);
  fprintf(scc_stats_file,"spec ld dirty (SS):       %*lu\n", STAT_SPACING, scc_stat_total_spec_ld_dirty);
  fprintf(scc_stats_file,"spec ld mark mr:          %*lu\n", STAT_SPACING, scc_stat_total_spec_ld_mark_mr);
  fprintf(scc_stats_file,"spec ld mr:               %*lu\n", STAT_SPACING, scc_stat_total_spec_ld_mr);

  fprintf(scc_stats_file,"spec st inline:           %*lu\n", STAT_SPACING, scc_stat_total_spec_inline_st);
  fprintf(scc_stats_file,"spec st current:          %*lu\n", STAT_SPACING, scc_stat_total_spec_st_cur);
  fprintf(scc_stats_file,"spec st set rw:           %*lu\n", STAT_SPACING, scc_stat_total_spec_st_rw);
  fprintf(scc_stats_file,"spec st new:              %*lu\n", STAT_SPACING, scc_stat_total_spec_st_new);
  fprintf(scc_stats_file,"spec st dirty (SS):       %*lu\n", STAT_SPACING, scc_stat_total_spec_st_dirty);
  fprintf(scc_stats_file,"spec st mr (SS):          %*lu\n", STAT_SPACING, scc_stat_total_spec_st_mr);
  //fprintf(scc_stats_file,"total spec:               %*lu\n", STAT_SPACING, total_spec_ops);
  fprintf(scc_stats_file,"\n");

  fprintf(scc_stats_file,"Total OPS:\n");
  fprintf(scc_stats_file,"ld_total:                 %*lu\n", STAT_SPACING, scc_stat_total_ld_total);
  fprintf(scc_stats_file,"st_full_total:            %*lu\n", STAT_SPACING, scc_stat_total_lst_total);
  fprintf(scc_stats_file,"st_logonly_total:         %*lu\n", STAT_SPACING, scc_stat_total_rst_total);

  uint64_t scc_stat_total_st_total = scc_stat_total_lst_total + scc_stat_total_rst_total;
  fprintf(scc_stats_file,"st_total:                 %*lu\n", STAT_SPACING, scc_stat_total_st_total);
 
  fprintf(scc_stats_file,"\n");
  fprintf(scc_stats_file,"total ld: %lu\t full st: %lu\t restart only st: %lu\t st_ld: %lu\t mt_ld: %lu st_rst: %lu\t mt_rst %lu\t st_lst: %lu\t mt_lst: %lu\n",
      scc_stat_total_ld_total, scc_stat_total_lst_total, scc_stat_total_rst_total,
      scc_stat_total_st_ld_total, scc_stat_total_mt_ld_total,
      scc_stat_total_st_rst_total, scc_stat_total_mt_rst_total,
      scc_stat_total_st_lst_total, scc_stat_total_mt_lst_total);

  fprintf(scc_stats_file,"\n");
  
  fprintf(scc_stats_file,"Other stats:\n");
  fprintf(scc_stats_file,"multithreaded readonly ld %*lu\n", STAT_SPACING,scc_stat_total_mtrdonly_ld);
#ifdef SCC_STAT_STACK_CNT
  fprintf(scc_stats_file,"stack ld:                 %*lu\n", STAT_SPACING, scc_stat_total_stack_ld);
  fprintf(scc_stats_file,"stack st:                 %*lu\n", STAT_SPACING, scc_stat_total_stack_st);
#endif
  fprintf(scc_stats_file,"retry cmpxchg:            %*lu\n", STAT_SPACING, scc_stat_total_atomic_retry);
  fprintf(scc_stats_file,"hash conflicts:           %*lu\n", STAT_SPACING, scc_stat_total_hash_conflict);
  fprintf(scc_stats_file,"\n");
  fprintf(scc_stats_file,"restart single thread:    %*lu\n", STAT_SPACING, scc_stat_total_restart_single);
  fprintf(scc_stats_file,"restart thread masked:    %*lu\n", STAT_SPACING, scc_stat_total_restart_masked);
  fprintf(scc_stats_file,"actual restarts:          %*lu\n", STAT_SPACING, scc_stat_total_restart_single - scc_stat_total_restart_masked);
  fprintf(scc_stats_file,"restart all threads:      %*lu\n", STAT_SPACING, scc_stat_total_restart_all);
  fprintf(scc_stats_file,"\n");
  fprintf(scc_stats_file,"sync_safe interal:        %*lu\n", STAT_SPACING, scc_stat_total_sync_safe_internal);
  fprintf(scc_stats_file,"sync_safe external:       %*lu\n", STAT_SPACING, scc_stat_total_sync_safe); 
  fprintf(scc_stats_file,"sync_safe thread:         %*lu\n", STAT_SPACING, scc_stat_total_sync_safe_thread);
  fprintf(scc_stats_file,"set_next_safe:            %*lu\n", STAT_SPACING, scc_stat_total_set_next_safe);
  fprintf(scc_stats_file,"\n");
  fprintf(scc_stats_file,"split task:               %*lu\n", STAT_SPACING, scc_stat_total_split_task);
  fprintf(scc_stats_file,"mutex lock                %*lu\n", STAT_SPACING, scc_stat_total_mutex_lock);
  fprintf(scc_stats_file,"mutex unlock              %*lu\n", STAT_SPACING, scc_stat_total_mutex_unlock);
  fprintf(scc_stats_file,"barrier wait:             %*lu\n", STAT_SPACING, scc_stat_total_barrier_wait);
  fprintf(scc_stats_file,"\n");
  fprintf(scc_stats_file,"total time                %*lu\n", STAT_SPACING, scc_stat_total_all_cycles);
  fprintf(scc_stats_file,"barrier time              %*lu\n", STAT_SPACING, scc_stat_total_barrier_cycles);
  fprintf(scc_stats_file,"syncsafe internal time    %*lu\n", STAT_SPACING, scc_stat_total_sync_safe_internal_cycles);
  fprintf(scc_stats_file,"---------------------------------------------------\n");


  // Sep 2013: per function ld/st. helps pinpoint which functions benefitted most, or are main ld.st hogs */
  fprintf(scc_stats_file,"\nPER FUNCTION LD/ST. SEE compile output to map ID to function name.\n");

  // First find total. Ignore the functions that are less than 1% of total
  uint64_t scc_ld_cnt_per_func_total = 0, scc_st_cnt_per_func_total = 0;
  for(int i = 1; i < 1000; i++) {
    scc_ld_cnt_per_func_total += scc_ld_cnt_per_func[i];
    scc_st_cnt_per_func_total += scc_st_cnt_per_func[i];
  }
  
  for(int i = 1; i < 1000; i++) {
    uint64_t ld = scc_ld_cnt_per_func[i];
    uint64_t st = scc_st_cnt_per_func[i];
    if(ld || st) {
      // ignore if less than 2%
      if(ld * 50 > scc_ld_cnt_per_func_total || st * 50 > scc_st_cnt_per_func_total) {
         fprintf(scc_stats_file,"FUNCTION ID: %4d LD %24lu ST %24lu\n", 
            i, scc_ld_cnt_per_func[i], scc_st_cnt_per_func[i]);
      }
    }
  }
  
  fprintf(scc_stats_file,"SCC STATS END\n");
  fprintf(scc_stats_file,"===================================================\n");
  fclose(scc_stats_file);
  fflush(stdout);
}

#else  // Not keeping stats
#endif // SCC_STAT_ENABLE

/* ####################################################################
 * Functions that must have declaration before template
 * #################################################################### */
// The MT_RDONLY functions that were added, possibly reorganized location later

#ifdef SCC_MTROM
// This is presumed to be multi-thread read only memory
extern "C" {
  void *malloc_mtrom(size_t size)
  {
    if(mtrdCurPtr == 0) {
      fprintf(stderr," Returning NULL from malloc_mtrom. mtrdCurPtr is NULL.\n");
      return NULL;
    }

    size_t end = (size_t) mtrdMap + mtrdMappedSize;
    if(mtrdCurPtr + size > end) {
      // TBD: Auto-reallocate more memory
      fprintf(stderr," Out of MTROM memory! Please initialize with bigger initial size.\n");
      return NULL;
    }

    static const size_t alignBytes = sizeof(void *);
    void *retval = reinterpret_cast<void *>(mtrdCurPtr);
    size += (size%alignBytes);
    mtrdCurPtr += size;
    DEBUG_PRINT("Alloc %zd bytes. Return %p New free pointer list %zd\n", size, retval, mtrdCurPtr);
    return retval;
  }

  void *calloc_mtrom(size_t numObjects, size_t size)
  {
    size_t totbytes = numObjects * size;
    void *ptr = malloc_mtrom(totbytes);
    if(ptr) memset(ptr, 0, totbytes);
    return ptr;
  }

  // Check that it is really mtrom space
  void scc_assure_mtrom(void *addr)
  {
    if(addr >= mtrdMap && addr < mtrdMapEnd)
      return *addr;

    for(int i = 0; i < sccNumMtromMmaps; i++) {
      if(addr >= scc_mtrom_mmap_starts[i] && addr < scc_mtrom_mmap_ends[i])
        return;
    }
    ERROR("FATAL ERROR: scc_assure_mtrom failed! Exiting.\n");
    exit(1);
  }

  // Free up the mt read-only memory
  void free_mtrom(void *ptr)
  {
    // TBD: Madan (Mar 3, 2013): Can't call free. Need to implement own free list here, to manage mtrom malloc/free
    // printf(" Free mt_rdonly to be done\n");
  }

  // multi-thread rom mmap
  void *mmap_mtrom(void *addr, size_t length, int prot, int flags,
      int fd, off_t offset)
  {
    void *start_addr = mmap(addr, length, prot, flags, fd, offset);
    void *end_addr = reinterpret_cast<void *>((reinterpret_cast<size_t>(start_addr) + length));
    scc_mtrom_mmap_starts[sccNumMtromMmaps] = start_addr;
    scc_mtrom_mmap_ends[sccNumMtromMmaps] = end_addr;
    sccNumMtromMmaps++;
    return start_addr;
  }

  int munmap_mtrom(void *addr, size_t length)
  {
    // tbd: no check for length, unmap entire map
    // assume exactly same address specified

    // remove the entry we created in mmap_mtrom
    for(int i = 0; i < sccNumMtromMmaps; i++) {
      if(scc_mtrom_mmap_starts[i] == addr) {
        scc_mtrom_mmap_starts[i] = scc_mtrom_mmap_starts[sccNumMtromMmaps-1];  // assign the last one to this position
        sccNumMtromMmaps--;
        break;
      }
    }

    return munmap(addr,length);
  }

} 

// XXX: Commented out because of warning saying function is unused.  Check if this is correct or not.
#if 0
static void close_mtrd_only_mem()
{

  DEBUG_PRINT(" Closing MT-RDONLY memory\n");
  /* Don't forget to free the mmapped memory */
  if (munmap(mtrdMap, mtrdMappedSize) == -1) {
    perror("Error un-mmapping the file");
    /* Decide here whether to close(fd) and exit() or not. Depends... */
  }

  mtrdCurPtr = 0;
}
#endif

static void open_mtrd_only_mem(size_t memSize)
{
  /* Now the file is ready to be mmapped.  */
  mtrdMap = mmap((void *) SCC_MMAP_MTROM_START_ADDR, memSize, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  if (mtrdMap == MAP_FAILED) {
    close(mtrdFd);
    perror("Error mmapping the file");
    exit(EXIT_FAILURE);
  }
  mtrdMappedSize = memSize;
  mtrdCurPtr = reinterpret_cast<size_t>(mtrdMap);
  size_t end = mtrdCurPtr + memSize;
  mtrdMapEnd = (void *) end;
}

static void set_mtmem_rdonly() 
{
  mprotect(mtrdMap,mtrdMappedSize, PROT_READ); 
}

static void set_mtmem_rw()
{
  mprotect(mtrdMap,mtrdMappedSize, PROT_READ | PROT_WRITE); 
}
#endif

/* ###################################################################
 * SCC API Functions
 * ################################################################### */
ALWAYS_INLINE void scc_inline_mark_load(scc_addr_t addr) 
{ 
  SCC_STAT_INC(scc_stat_safe_ld_cur);
}

template<typename loadType> ALWAYS_INLINE loadType scc_inline_load(loadType *addr) 
{
  scc_ld_cnt_per_func[sccGlobalFuncId]++; // Sep 2013 : for per func ld/st
  SCC_STAT_INC(scc_stat_ld_total);
  if(active_threads.load(std::memory_order_relaxed) > 1) {
    SCC_STAT_INC(scc_stat_mt_ld_total);
  } else {
    SCC_STAT_INC(scc_stat_st_ld_total);
  }

  return *addr;
}

ALWAYS_INLINE void inline scc_inline_mark_store(scc_addr_t addr)
{
  SCC_STAT_INC(scc_stat_spec_inline_st); 
  return;
}

template<typename storeType> void inline scc_inline_store(storeType *addr, storeType value) 
{
  scc_inline_mark_store(reinterpret_cast<scc_addr_t>(addr));
  *addr = value;
}

template<typename storeType> void inline scc_inline_logonly_store(storeType *addr, storeType value)
{
  SCC_STAT_INC(scc_stat_rst_total);
  if(active_threads.load(std::memory_order_relaxed) > 1) {
    SCC_STAT_INC(scc_stat_mt_rst_total);
  } else {
    SCC_STAT_INC(scc_stat_st_rst_total);
  }
  TRACE("scc_inline_longonly_store()\n");
  scc_inline_store(addr, value);
}

template<typename storeType> void inline scc_inline_full_store(storeType *addr, storeType value)
{
  scc_st_cnt_per_func[sccGlobalFuncId]++; // Sep 2013 : for per func ld/st
  TRACE("scc_inline_full_store(): %p\n", addr);
  SCC_STAT_INC(scc_stat_lst_total);
  if(active_threads.load(std::memory_order_relaxed) > 1) {
    SCC_STAT_INC(scc_stat_mt_lst_total);
  } else {
    SCC_STAT_INC(scc_stat_st_lst_total);
  }
  scc_inline_store(addr, value);
}

static void scc_init() 
{
#ifdef SCC_MTROM
  open_mtrd_only_mem(SCC_MMAP_FILESIZE);
#endif

  //Use 0 to mark as invalid/unused thread entries
  for(int i = 0; i < SCC_MAX_NUM_THREADS; i++) {
    scc_threads[i].pthread_id = 0;
  }
 
  active_threads.store(0, std::memory_order_relaxed);

  // for per-func stats
  for(int i = 0; i < 1000; i++) {
    scc_ld_cnt_per_func[i] = scc_st_cnt_per_func[i] = 0;
  }
  fprintf(stderr,"Finished scc_init\n");
}


// TODO: Optimize this function
void scc_load_range(void *addr, int size, void *outbuf)
{

  SCC_PRINT("scc_load_range called\n");
  //TODO: Test and debug this fuction. For now trigger an assertion if the function is called
  return;
}


// Probably buggy ...
void scc_store_range(void *addr, int size, void *inbuf)
{
   uint8_t *out = reinterpret_cast<uint8_t *>(addr);
   uint8_t *in = reinterpret_cast<uint8_t *>(inbuf);

   while(size > 0) {
     scc_st_cnt_per_func[sccGlobalFuncId]++;
     SCC_STAT_INC(scc_stat_lst_total);
     if(active_threads.load(std::memory_order_relaxed) > 1) {
       SCC_STAT_INC(scc_stat_mt_lst_total);
     } else {
       SCC_STAT_INC(scc_stat_st_lst_total);
     }
     scc_inline_store<uint8_t>(out, *in);
     out++;
     in++;
     size--;
   }
}

static void scc_swap_ints(int64_t& a, int64_t& b)
{
  int64_t tmp = a;
  a = b;
  b = tmp;
}

  extern "C" {
    //TODO: Probably buggy
    void SCCmemcpy(void *dest, void *src, size_t size)
    {
      for(uint64_t ptr = (uint64_t)src & SCC_CACHE_LINE_MASK; ptr < (uint64_t)src + size; ptr += SCC_CACHE_LINE_SIZE) {
        scc_inline_mark_load(ptr);
      } 

      for(uint64_t ptr = (uint64_t)dest & SCC_CACHE_LINE_MASK; ptr < (uint64_t)dest + size; ptr += SCC_CACHE_LINE_SIZE) {
        scc_inline_mark_store(ptr);
      }  

      memcpy(dest, src, size);

    }

    // Jump by cache-line size, so we store each cache line that's touched. We need to set n bytes
    // TODO: Probably buggy ....
    void SCCmemset(void *s, int c, size_t n)
    {
      DEBUG_PRINT("Memset called with %p %d %zu\n", s, c, n);
      uint8_t temp = c;
      uint8_t *out = (uint8_t *) s;
      uint8_t *last = out + n;
      last--;
      *last = temp; // scc_inline_store<uint8_t>(last, temp);

      while(out < last) {
        *out = temp; // scc_inline_store<uint8_t>(out, temp);
        out += SCC_CACHE_LINE_SIZE;
      }
      // make sure to write the last address, so each touched cache line is stored once

      // actual memset
      memset(s,c,n);
      DEBUG_PRINT("Exiting memset\n");
    }

  }

// Instantiate functions needed by llvm pass with function names
#define DEFINE_LLVM_PASS_FUNCS_CONTAINED(T, TNAME)\
 ALWAYS_INLINE extern "C" T SCCloadReplaceContained ## TNAME(T* addr) {\
    return scc_inline_load<T>(addr);\
  }\
 ALWAYS_INLINE extern "C" void SCCstoreReplaceContained ## TNAME(T* addr, T value) {\
    scc_inline_full_store<T>(addr, value);\
  } \
  extern "C" void SCCstoreForRestartOnly ## TNAME(T* addr, T value) {\
    scc_inline_logonly_store<T>(addr, value);\
  } \
  extern "C" void SCCstoreLog ## TNAME(T* addr) {\
    scc_inline_full_store<T>(addr, *addr);\
  } \
  extern "C" T SCCloadReplaceRange ## TNAME(T* addr) {\
    T tmp; \
    scc_load_range(addr, sizeof(T), &tmp);\
    return tmp; \
  }\
  extern "C" void SCCstoreReplaceRange ## TNAME(T* addr, T value) {\
    T tmp = value; \
    scc_store_range(addr, sizeof(T), &tmp);\
    return; \
  }

DEFINE_LLVM_PASS_FUNCS_CONTAINED(uint8_t,  Int8   )
DEFINE_LLVM_PASS_FUNCS_CONTAINED(uint16_t, Int16  )
DEFINE_LLVM_PASS_FUNCS_CONTAINED(uint32_t, Int32  )
DEFINE_LLVM_PASS_FUNCS_CONTAINED(uint64_t, Int64  )
DEFINE_LLVM_PASS_FUNCS_CONTAINED(float,    Float  )
DEFINE_LLVM_PASS_FUNCS_CONTAINED(double,   Double )
DEFINE_LLVM_PASS_FUNCS_CONTAINED(void*,    Ptr    )

/* Define arrayed versions of store/load for LILM, that stores same thing as address */
//TODO: Fix these are probably buggy too ....
#define DEFINE_ARRAY_LILM_FUNCS(T, TNAME, STRIDE)\
  extern "C" void SCCstoreArray ## TNAME(T *addr, int64_t start, int64_t end) \
  { \
      if(start > end) scc_swap_ints(start, end); \
      for(int64_t i = start; i < end; i += STRIDE) \
        SCCstoreReplaceContained ## TNAME (addr+i, *(addr+i)); \
  } \
  extern "C" void SCCloadArray ## TNAME(T *addr, int64_t start, int64_t end) \
  { \
      if(start > end) scc_swap_ints(start, end); \
      for(int64_t i = start; i < end; i += STRIDE) \
        SCCloadReplaceContained ## TNAME(addr+i); \
  }

DEFINE_ARRAY_LILM_FUNCS(uint8_t, Int8, 32)
DEFINE_ARRAY_LILM_FUNCS(uint16_t, Int16, 16)
DEFINE_ARRAY_LILM_FUNCS(uint32_t, Int32, 8)
DEFINE_ARRAY_LILM_FUNCS(uint64_t, Int64, 4)
DEFINE_ARRAY_LILM_FUNCS(float, Float, 8)
DEFINE_ARRAY_LILM_FUNCS(double, Double, 4)
DEFINE_ARRAY_LILM_FUNCS(void *, Ptr, 4)


// Following 2 are for arbitrary size?
extern "C" void SCCloadReplaceContained(void* addr, size_t size, void* outbuffer) 
{
  scc_load_range(addr,size,outbuffer); 
}

extern "C" void SCCstoreReplaceContained(void* addr, size_t size, void* inbuffer) 
{
   scc_store_range(addr,size,inbuffer);
}

/* Define library replacement functions */
extern "C" {
    
  // TODO: Improve unit test
  int scc_strcmp(const char *a, const char *b) {
    TRACE("scc_strcmp()\n");
    const char *ptrs[2] = {a,b};
    for(int i = 0; i < 2; i++) {
      int len = strlen(ptrs[i]);
      uint64_t str = reinterpret_cast<uint64_t>(ptrs[i]) & SCC_CACHE_LINE_MASK;
      uint64_t ptr = reinterpret_cast<uint64_t>(ptrs[i]);
      for(uint64_t strptr = str; strptr < ptr + len; strptr += SCC_CACHE_LINE_SIZE) {
        scc_inline_mark_load(strptr);
      }
    }
    return strcmp(a,b);
  }

  // TODO: Write unit test
  int scc_strncmp(const char *a, const char *b, size_t len) {
    const char *ptrs[2] = {a,b};
    TRACE("scc_strncmp()\n");
    for(int i = 0; i < 2; i++) {
      size_t l = strlen(ptrs[i]);
      if(len < l) {
        l = len;
      }
      uint64_t str = reinterpret_cast<uint64_t>(ptrs[i]) & SCC_CACHE_LINE_MASK;
      uint64_t ptr = reinterpret_cast<uint64_t>(ptrs[i]);
      for(uint64_t strptr = str; strptr < ptr + l; strptr += SCC_CACHE_LINE_SIZE) {
        scc_inline_mark_load(strptr);
      }
    }

    return strncmp(a,b, len);
  }

  // TODO: Write unit test
  char *scc_strcpy(char *dest, const char *src) {
    TRACE("scc_strcpy\n");
    uint64_t len = strlen(src);
   
    for(uint64_t ptr = reinterpret_cast<uint64_t>(src) & SCC_CACHE_LINE_MASK; ptr < reinterpret_cast<uint64_t>(src) + len; ptr += SCC_CACHE_LINE_SIZE) {
      scc_inline_mark_load(ptr);
    } 
    
    for(uint64_t ptr = reinterpret_cast<uint64_t>(dest) & SCC_CACHE_LINE_MASK; ptr < reinterpret_cast<uint64_t>(dest) + len; ptr += SCC_CACHE_LINE_SIZE) {
      scc_inline_mark_store(ptr);
    }  

    return strcpy(dest, src);
  }

  // TODO: Write unit test
  char *scc_strncpy(char *dest, const char *src, size_t spec_len) {
    TRACE("scc_strncpy\n");
    size_t len = strlen(src);
    if(len < spec_len) {
      len = spec_len;
    }

    for(uint64_t ptr = reinterpret_cast<uint64_t>(src) & SCC_CACHE_LINE_MASK; ptr < reinterpret_cast<uint64_t>(src) + len; ptr += SCC_CACHE_LINE_SIZE) {
      scc_inline_mark_load(ptr);
    } 

    for(uint64_t ptr = reinterpret_cast<uint64_t>(dest) & SCC_CACHE_LINE_MASK; ptr < reinterpret_cast<uint64_t>(dest) + len; ptr += SCC_CACHE_LINE_SIZE) {
      scc_inline_mark_store(ptr);
    }  

    return strncpy(dest, src, spec_len);
  }

  // TODO: Write unit test
  size_t scc_strlen(const char *s) {
    TRACE("scc_strlen\n");
    size_t len = strlen(s);
    I(len >= 0);
    //SCCloadArrayInt8((uint8_t *) s, 0, len);
    for(uint64_t ptr = reinterpret_cast<uint64_t>(s) & SCC_CACHE_LINE_MASK; ptr < reinterpret_cast<uint64_t>(s) + len; ptr += SCC_CACHE_LINE_SIZE) {
      scc_inline_mark_load(ptr);
    } 
    return len;
  }
  // TBD: strtol, strtoll etc.
}


/* ################################################
 * Internal Function
 * ################################################ */

/**
 * Wrapper function for running user function
 */
static void *wrapper(void *arg)
{
  scc_thread_args_t *targs = (scc_thread_args_t *) arg;
  threadId = targs->thread_index;
  // fprintf(stderr,"In wrapper: thread index %d\n", threadId);
#ifdef SCC_BB_PROFILING
  // PROF_init_thread(targs->thread_index); // for scc profiling
#endif

  /* fprintf(stderr,"wrapper: thread id %d fn %p wrapped_arg %p\n",  
		targs->thread_index, targs->wrapped_function, targs->wrapped_arg); */
  return targs->wrapped_function(targs->wrapped_arg);
  // free(targs); : can't gree
}

extern "C" void scc_sync_safe() 
{
}

void sync_safe_internal() 
{
  TRACE("sync_safe_internal()\n");

  SCC_STAT_INC(scc_stat_sync_safe_internal);
}

// Start a new scc_thread
// Note: Use same parameters as pthread_create
int scc_spawn_thread(pthread_t *thread, __const pthread_attr_t *attr, void *(*start_routine)(void *),void *args) 
{
  TRACE("scc_spawn_thread()\n");
#ifdef SCC_BB_PROFILING
   // PROF_end_task();
#endif

  //Only safe can spawn a new thread
/*  if(!is_self_safe()) {
    sync_safe_internal();
  } */

  // Switching between multithreaded and single threaded mode
#ifdef SCC_MTROM
  //TODO: Not sure if this is correct
  if(active_threads == 1) {
    set_mtmem_rdonly();
  }
#endif

  globalThreadId++;
  scc_thread_args_t *wrapped_arg = (scc_thread_args_t *) malloc(sizeof(scc_thread_args_t));
  wrapped_arg->thread_index = globalThreadId;
  wrapped_arg->wrapped_function = start_routine;
  wrapped_arg->wrapped_arg = args;
  
  active_threads++;
  //return native_pthread_create(thread,0,wrapper,&scc_threads[thread_index].arg);
  fprintf(stderr,"Creating thread  %d in countstm\n", globalThreadId);
  int rc = pthread_create(thread, attr, wrapper, wrapped_arg);
  fprintf(stderr,"Created thread %d\n", globalThreadId);
  scc_threads[globalThreadId].pthread_id = *thread;
  return rc;
}

static void segv_handler(int sig, siginfo_t *si, void *unused)
{
  //TODO: Add checks for MTROM memory

  // comment out unused variable for now
  //uint64_t page_addr = (uint64_t)(si->si_addr);

#ifdef SCC_STAT_ENABLE
  for(int i = 0; i <= globalThreadId; i++) {
    scc_add_thread_stats(i);
  }
  scc_stat_print();
#endif
  abort();
}

////// Code for replacing main function in instrumented application
extern "C" {
  // Right now, these 3 functions do nothing, only help compiler to 
  // demarcate TS begin, TS end and TD sync points.
  void scc_sync_threads(int *mutexVar, int ID) {}
  /* start threaded section - user specified*/
  void scc_start_ts() {}
  void scc_start_ts_auto() {} /* inserted by scc pass */

  __attribute__((noinline)) void scc_begin_func(int funcId) { sccGlobalFuncId = funcId; }

  /* end threaded section */
  void scc_end_ts() {}
  void scc_end_ts_auto() {} /* inserted by scc pass */
}

/////////////////////////////////////////////////////////////////////////////////////
// Merge in SCC threads functions
///////////////////////////////////////////////////////////////////////////////////

extern "C" {
  int scc_thread_mutex_lock(pthread_mutex_t *mutex)
  {
    pthread_mutex_lock(mutex);
    SCC_STAT_INC(scc_stat_mutex_lock);
    return 0;
  }

  int scc_thread_mutex_unlock(pthread_mutex_t *mutex)
  {
    pthread_mutex_unlock(mutex);
    SCC_STAT_INC(scc_stat_mutex_unlock);
    return 0;
  }

  __attribute__ ((__noreturn__)) void scc_exit(int status)  __THROW  
  {
    SCC_PRINT("SCC_EXIT\n");
#ifdef SCC_STAT_ENABLE
    scc_add_thread_stats(threadId);
    scc_stat_print();
#endif
    exit(status);
  }

// Function used for debugging in user code that should not be
// instrumented

void scc_debug_printf(const char *format, ...) {
    char buf[256];
    va_list arglist;
    va_start(arglist, format);
    vsnprintf(buf, 256, format, arglist );
    va_end(arglist);
    int len = strlen(buf);
    write(STDERR_FILENO,&buf,len);
  }

  void __attribute__((noinline)) scc_split_task()
  {
	return;
  }

  int main(int argc, char *argv[])
  {
#ifdef SCC_MTROM
  open_mtrd_only_mem(SCC_MMAP_FILESIZE);
#endif
    fprintf(stderr, "Running main in libtsan..\n");
    return scc_wrapped_main(argc, argv);
  }


} // extern "C"

