# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /soe/madandas/SCC/libsccstm/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /soe/madandas/SCC/libsccstm/src

# Include any dependencies generated for this target.
include CMakeFiles/sccstm.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/sccstm.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/sccstm.dir/flags.make

CMakeFiles/sccstm.dir/scc-unittest.o: CMakeFiles/sccstm.dir/flags.make
CMakeFiles/sccstm.dir/scc-unittest.o: scc-unittest.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /soe/madandas/SCC/libsccstm/src/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/sccstm.dir/scc-unittest.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/sccstm.dir/scc-unittest.o -c /soe/madandas/SCC/libsccstm/src/scc-unittest.cpp

CMakeFiles/sccstm.dir/scc-unittest.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/sccstm.dir/scc-unittest.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /soe/madandas/SCC/libsccstm/src/scc-unittest.cpp > CMakeFiles/sccstm.dir/scc-unittest.i

CMakeFiles/sccstm.dir/scc-unittest.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/sccstm.dir/scc-unittest.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /soe/madandas/SCC/libsccstm/src/scc-unittest.cpp -o CMakeFiles/sccstm.dir/scc-unittest.s

CMakeFiles/sccstm.dir/scc-unittest.o.requires:
.PHONY : CMakeFiles/sccstm.dir/scc-unittest.o.requires

CMakeFiles/sccstm.dir/scc-unittest.o.provides: CMakeFiles/sccstm.dir/scc-unittest.o.requires
	$(MAKE) -f CMakeFiles/sccstm.dir/build.make CMakeFiles/sccstm.dir/scc-unittest.o.provides.build
.PHONY : CMakeFiles/sccstm.dir/scc-unittest.o.provides

CMakeFiles/sccstm.dir/scc-unittest.o.provides.build: CMakeFiles/sccstm.dir/scc-unittest.o

CMakeFiles/sccstm.dir/sccstm.o: CMakeFiles/sccstm.dir/flags.make
CMakeFiles/sccstm.dir/sccstm.o: sccstm.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /soe/madandas/SCC/libsccstm/src/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/sccstm.dir/sccstm.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/sccstm.dir/sccstm.o -c /soe/madandas/SCC/libsccstm/src/sccstm.cpp

CMakeFiles/sccstm.dir/sccstm.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/sccstm.dir/sccstm.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /soe/madandas/SCC/libsccstm/src/sccstm.cpp > CMakeFiles/sccstm.dir/sccstm.i

CMakeFiles/sccstm.dir/sccstm.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/sccstm.dir/sccstm.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /soe/madandas/SCC/libsccstm/src/sccstm.cpp -o CMakeFiles/sccstm.dir/sccstm.s

CMakeFiles/sccstm.dir/sccstm.o.requires:
.PHONY : CMakeFiles/sccstm.dir/sccstm.o.requires

CMakeFiles/sccstm.dir/sccstm.o.provides: CMakeFiles/sccstm.dir/sccstm.o.requires
	$(MAKE) -f CMakeFiles/sccstm.dir/build.make CMakeFiles/sccstm.dir/sccstm.o.provides.build
.PHONY : CMakeFiles/sccstm.dir/sccstm.o.provides

CMakeFiles/sccstm.dir/sccstm.o.provides.build: CMakeFiles/sccstm.dir/sccstm.o

CMakeFiles/sccstm.dir/scc-profile.o: CMakeFiles/sccstm.dir/flags.make
CMakeFiles/sccstm.dir/scc-profile.o: scc-profile.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /soe/madandas/SCC/libsccstm/src/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/sccstm.dir/scc-profile.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/sccstm.dir/scc-profile.o -c /soe/madandas/SCC/libsccstm/src/scc-profile.cpp

CMakeFiles/sccstm.dir/scc-profile.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/sccstm.dir/scc-profile.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /soe/madandas/SCC/libsccstm/src/scc-profile.cpp > CMakeFiles/sccstm.dir/scc-profile.i

CMakeFiles/sccstm.dir/scc-profile.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/sccstm.dir/scc-profile.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /soe/madandas/SCC/libsccstm/src/scc-profile.cpp -o CMakeFiles/sccstm.dir/scc-profile.s

CMakeFiles/sccstm.dir/scc-profile.o.requires:
.PHONY : CMakeFiles/sccstm.dir/scc-profile.o.requires

CMakeFiles/sccstm.dir/scc-profile.o.provides: CMakeFiles/sccstm.dir/scc-profile.o.requires
	$(MAKE) -f CMakeFiles/sccstm.dir/build.make CMakeFiles/sccstm.dir/scc-profile.o.provides.build
.PHONY : CMakeFiles/sccstm.dir/scc-profile.o.provides

CMakeFiles/sccstm.dir/scc-profile.o.provides.build: CMakeFiles/sccstm.dir/scc-profile.o

CMakeFiles/sccstm.dir/nanassert.o: CMakeFiles/sccstm.dir/flags.make
CMakeFiles/sccstm.dir/nanassert.o: nanassert.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /soe/madandas/SCC/libsccstm/src/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/sccstm.dir/nanassert.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/sccstm.dir/nanassert.o -c /soe/madandas/SCC/libsccstm/src/nanassert.cpp

CMakeFiles/sccstm.dir/nanassert.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/sccstm.dir/nanassert.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /soe/madandas/SCC/libsccstm/src/nanassert.cpp > CMakeFiles/sccstm.dir/nanassert.i

CMakeFiles/sccstm.dir/nanassert.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/sccstm.dir/nanassert.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /soe/madandas/SCC/libsccstm/src/nanassert.cpp -o CMakeFiles/sccstm.dir/nanassert.s

CMakeFiles/sccstm.dir/nanassert.o.requires:
.PHONY : CMakeFiles/sccstm.dir/nanassert.o.requires

CMakeFiles/sccstm.dir/nanassert.o.provides: CMakeFiles/sccstm.dir/nanassert.o.requires
	$(MAKE) -f CMakeFiles/sccstm.dir/build.make CMakeFiles/sccstm.dir/nanassert.o.provides.build
.PHONY : CMakeFiles/sccstm.dir/nanassert.o.provides

CMakeFiles/sccstm.dir/nanassert.o.provides.build: CMakeFiles/sccstm.dir/nanassert.o

# Object files for target sccstm
sccstm_OBJECTS = \
"CMakeFiles/sccstm.dir/scc-unittest.o" \
"CMakeFiles/sccstm.dir/sccstm.o" \
"CMakeFiles/sccstm.dir/scc-profile.o" \
"CMakeFiles/sccstm.dir/nanassert.o"

# External object files for target sccstm
sccstm_EXTERNAL_OBJECTS =

libsccstm.a: CMakeFiles/sccstm.dir/scc-unittest.o
libsccstm.a: CMakeFiles/sccstm.dir/sccstm.o
libsccstm.a: CMakeFiles/sccstm.dir/scc-profile.o
libsccstm.a: CMakeFiles/sccstm.dir/nanassert.o
libsccstm.a: CMakeFiles/sccstm.dir/build.make
libsccstm.a: CMakeFiles/sccstm.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX static library libsccstm.a"
	$(CMAKE_COMMAND) -P CMakeFiles/sccstm.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/sccstm.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/sccstm.dir/build: libsccstm.a
.PHONY : CMakeFiles/sccstm.dir/build

CMakeFiles/sccstm.dir/requires: CMakeFiles/sccstm.dir/scc-unittest.o.requires
CMakeFiles/sccstm.dir/requires: CMakeFiles/sccstm.dir/sccstm.o.requires
CMakeFiles/sccstm.dir/requires: CMakeFiles/sccstm.dir/scc-profile.o.requires
CMakeFiles/sccstm.dir/requires: CMakeFiles/sccstm.dir/nanassert.o.requires
.PHONY : CMakeFiles/sccstm.dir/requires

CMakeFiles/sccstm.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/sccstm.dir/cmake_clean.cmake
.PHONY : CMakeFiles/sccstm.dir/clean

CMakeFiles/sccstm.dir/depend:
	cd /soe/madandas/SCC/libsccstm/src && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /soe/madandas/SCC/libsccstm/src /soe/madandas/SCC/libsccstm/src /soe/madandas/SCC/libsccstm/src /soe/madandas/SCC/libsccstm/src /soe/madandas/SCC/libsccstm/src/CMakeFiles/sccstm.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/sccstm.dir/depend

