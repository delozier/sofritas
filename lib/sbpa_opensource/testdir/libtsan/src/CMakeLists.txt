project(sccstm)

file(GLOB sccstm_source *.cpp)
file(GLOB sccstm_header *.h)

include_directories(${sccstm_SOURCE_DIR})

add_library(sccstm ${sccstm_source} ${sccstm_source} ${sccstm_header})
#test
