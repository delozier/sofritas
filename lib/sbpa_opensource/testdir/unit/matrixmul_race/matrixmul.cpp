#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifdef SCC_PASS
#include "scc.h"
#else
#include "noscc.h"
#endif

#include <pthread.h>
// #define COUNT_CYCLES 

#define START_VER (1 << 8) 
#define SEED 1024

#define READ_CYCLE 0

void init_rand_matrix(uint64_t ***matrix,int size) {
  int i,j; 
  *matrix = (uint64_t **)calloc(size,sizeof(uint64_t **));
  for(i = 0; i < size; i++) {
    (*matrix)[i] = (uint64_t *)calloc(size,sizeof(uint64_t));
  }

  for(i = 0; i < size; i++) {
    for(j = 0; j < size; j++) {
      (*matrix)[i][j] = rand() % 100;
    }
  }
}

void init_zero_matrix(uint64_t ***matrix,int size) {
  *matrix = (uint64_t **)calloc(size,sizeof(uint64_t **));
  for(int i = 0; i < size; i++) {
    (*matrix)[i] = (uint64_t *)calloc(size,sizeof(uint64_t));
  }
}

// Multiply matricies 
void matrixmul(uint64_t **A, uint64_t **B, uint64_t **C, int size, int startRow, int endRow) 
{
  for(int i = startRow; i < endRow; ++i) {
    for(int j = 0; j < size; ++j) {
      uint64_t c = 0; // C[i][j]);
      for(int k = 0; k < size; ++k) {
        uint64_t a = A[i][k];
        uint64_t b = B[k][j];
        c += a * b;
      }
      C[i][j] = c;
    }
  }
}

struct thread_args
{
    uint64_t **A;
    uint64_t **B; 
    uint64_t **C;
    int startRow;
    int endRow;
    int size;
};

struct sptr
{
  thread_args *ptr;
};


uint64_t **globalA = NULL;
uint64_t **globalB = NULL;
uint64_t **globalC = NULL;

void *thread_mul(void *ptr);


//Return matrix total used as a simple checksum
uint64_t sum_matrix_val(uint64_t ***matrix, int size) {
  uint64_t total = 0;
  for(int i = 0; i < size; i++) {
    for(int j = 0; j < size; j++) {
        uint64_t val = ((*matrix)[i][j]);
        total+=val;
    } 
  }
  return total;
}

//Matricies 
uint64_t **m1,**m2,**m3;

#ifdef SCC_PASS
int scc_wrapped_main(int argc, char** argv)
#else
extern "C" int main(int argc, char** argv)
#endif
{
  // SCC_INIT(0);
  if(argc != 2) {
    fprintf(stderr,"Usage: matrixmul <size of n x n matrix> \n");
    exit(1);
  }

  printf("Debug output enabled\n");

  int msize = atoi(argv[1]);
  if(msize < 1 ) {
    fprintf(stderr,"Error: invalid matrix size %d\n",msize);
    exit(1);
  }
  printf("size is: %d\n",msize);

  // Used a fixed value for rand seeding for now
  srand(SEED);
  init_rand_matrix(&m1,msize);
  init_rand_matrix(&m2,msize);
  init_zero_matrix(&m3,msize);

  uint64_t start_cycles = READ_CYCLE;  

  pthread_t thread1, thread2;
  thread_args args, args2;;
  args.A = args2.A = globalA = m1;
  args.B = args2.B = globalB = m2;
  args.C = args2.C = globalC = m3;
  args.size = args2.size = msize;

  args.startRow = 0;
  args.endRow = msize/2 +1; // msize/2;

  args2.startRow = msize/2;
  args2.endRow = msize;

  sptr s1, s2;
  s1.ptr = &args; 
  s2.ptr = &args2; 
  pthread_create(&thread1, NULL, thread_mul, (void *) (&s1));
  pthread_create(&thread2, NULL, thread_mul, (void *) (&s2));

  // Now join threads
  pthread_join( thread1, NULL);
  pthread_join( thread2, NULL);
  // pthread_join( thread2, NULL);

  uint64_t end_cycles = READ_CYCLE;
  uint64_t total = sum_matrix_val(&m3,msize);
  printf("matrix_mul total is: %lu\n",total);

  if(end_cycles > start_cycles) {
    printf("Start cycles: %lu\n",start_cycles);
    printf("End cycles:   %lu\n",end_cycles);
    printf("Diff cycles:  %lu\n",end_cycles - start_cycles);
  }

   return 0;
}

void *thread_mul(void *ptr)
{
  sptr *p = (sptr *) ptr;
  thread_args *args = p->ptr;
  uint64_t **A = args->A;
  uint64_t **B = args->B;
  uint64_t **C = args->C;
  int size = args->size;
  int startRow = args->startRow;
  int endRow = args->endRow;

  for(int i = startRow; i < endRow; ++i) {
    uint64_t *c_row = C[i];
    uint64_t *a_row = A[i];
    for(int j = 0; j < size; ++j) {
      uint64_t c = 0; // C[i][j]);
      for(int k = 0; k < size; ++k) {
        uint64_t a = a_row[k];
        uint64_t b = B[k][j];
        c += a * b;
      }
      c_row[j] = c;
    }
  }

  return NULL;
}
