// (C) 2010 University of Washington.
// (C) 2011 University of California, Santa Cruz.
// (C) 2016 University of Pennsylvania
// The software is subject to an academic license.
// Terms are contained in the LICENSE file.
//
// ORCA atomicity pass (UPenn) based on the SCC compiler pass (Washington)
// using additional optimizations (Santa Cruz)

#include <algorithm>
#include <deque>
#include <queue>
#include <map>
#include <stack>
#include <string>

#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>

#include <memory>
#include <cxxabi.h>

#include "llvm/IR/Constants.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/DerivedTypes.h" 
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/CaptureTracking.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/CFG.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/ConstantRange.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/GetElementPtrTypeIterator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/DebugInfo.h"
#include "ThreadEscapeAnalysis.h"
#include "identifyTS.h"
#include "SccProfile.h"
#include "LoopHelper.h"

#include "AccessInfo.h"
#include "Tool.h"

#undef DEBUG_TYPE 
#define DEBUG_TYPE "scc"

namespace llvm {
namespace {

typedef ilist<AccessInfo> AccessInfoList;

cl::opt<bool> doSccDbg("scc-dbg-extra", cl::init(false), cl::desc("Extra debugging"));

// Skip stack access optimization
cl::opt<bool> doStackOpt("scc-skip-stack", cl::init(false),
              cl::desc("Skip stack access instrumentation"));

// Memory access optimizations. includes mod-ref info usage.
cl::opt<bool> doEscapeOpt("scc-do-escapeopt", cl::init(false),
              cl::desc("Perform escape-based optimization"));

//cl::opt<bool> doOnlyThreadSections("scc-instrument-threadsections-only", cl::init(false),
//              cl::desc("Track load stores in threaded sections only"));

cl::opt<bool> doReplaceLoadStores("scc-replace-load-stores", cl::init(true),
              cl::desc("Replace load/stores"));

cl::opt<bool> doMemTrackLdLdOpt("scc-do-ldldsao", cl::init(true),
              cl::desc("Perform SAO optimization for ld-ld"));

cl::opt<bool> doMemTrackLdStOpt("scc-do-ldstsao", cl::init(true),
              cl::desc("Perform SAO optimization for ld-st (this promotes the ld)"));

cl::opt<bool> doMemTrackStLdOpt("scc-do-stldsao", cl::init(true),
              cl::desc("Perform SAO optimization for st-ld"));

cl::opt<bool> doMemTrackStStOpt("scc-do-ststsao", cl::init(true),
              cl::desc("Perform SAO optimization for st-st"));

cl::opt<bool> doMemCoalesceOpt("scc-do-memcoalesceopt", cl::init(false),
              cl::desc("Perform ACO optimization"));

cl::opt<bool> doMemTrackPreOpt("scc-do-pre-memtrackopt", cl::init(false),
              cl::desc("Perform PRE+SAO"));

// This compressed multiple array element access to single logging of range
cl::opt<bool> doLoopInvariantArrayMotion("scc-vlilm", cl::init(false),
              cl::desc("Loop Invariant load/store log motion for consecutive array elements"));

// This hoists multiple read/write of same address to loop pre-header
cl::opt<bool> doLoopInvariantAddrMotion("scc-lilm", cl::init(false),
              cl::desc("Loop Invariant load/store log motion for same address"));

cl::opt<bool> doMemCoalescePreOpt("scc-do-pre-memcoalesceopt", cl::init(false),
              cl::desc("Perform PRE+ACO"));

cl::opt<bool> doMemTrackTypesSpecOpt("scc-do-memtrack-type-specialize", cl::init(true),   /* Madan: changed default to false */
              cl::desc("Specialize memtracking by types."));

cl::opt<bool> doMemTrackParallelMalloc("scc-do-memtrack-parallel-malloc", cl::init(false),
              cl::desc("Don't serialize malloc/free calls."));

cl::opt<bool> doMemTrackHoardMalloc("scc-do-memtrack-hoard-malloc", cl::init(false),
              cl::desc("For linking with the SCC version of libhoard."));

// Memory access tracking.
cl::opt<bool> doLoadTrack("scc-instrument-all-load", cl::init(true),
              cl::desc("Call SCC Library on Load"));
 
cl::opt<bool> doStoreTrack("scc-instrument-all-store", cl::init(true),
              cl::desc("Call SCC Library on Store"));

cl::opt<bool> doTraceEndTask("scc-do-trace-endtask", cl::init(true),
              cl::desc("Call SCC Library end task on Trace Exit"));

cl::opt<bool> sccSafeOnly("scc-safe-only", cl::init(true),
              cl::desc("Run SAFE_ONLY mode"));

typedef SmallPtrSet<BasicBlock*, 8> BasicBlockSet;
typedef SmallPtrSet<Instruction*, 8> InstructionSet;

//------------------------------------------------------------------------------
// The Pass
//------------------------------------------------------------------------------

class MakeSCC : public FunctionPass 
{
  ORCATool *_tool;

  LLVMContext *_context;

  // Some handy types.
  PointerType* VoidPtrTy;     // like void*
  PointerType* Int64PtrTy;     // like void*
  PointerType* VoidPtrPtrTy;  // like void**
  Type* SizeTy;               // like size_t

  Type* VoidTy;
  Type* FloatTy;
  Type* DoubleTy;
  IntegerType* Int8Ty;
  IntegerType* Int16Ty;
  IntegerType* Int32Ty;
  IntegerType* Int64Ty;

  // Instrumenting these can cuase issues in runtime.
  std::vector<std::string> dontInstrumentFuncs;
  std::vector<std::string> dontInstrumentMangled;
  std::vector<std::string> dontInstrumentDemangled;

  std::vector<std::string> callInstrument;

  // Instructions replaced during memtracking.
  DenseMap<Value *, Value *> replacements;

  void recordReplacement(Instruction *origInst, Instruction *newInst)
  {
    ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
    EA.recordReplacement(origInst, newInst);
    replacements[origInst] = newInst;
  }

public:
  MakeSCC();
  virtual void getAnalysisUsage(AnalysisUsage &AU) const;
  virtual bool doInitialization(Module &M);
  virtual void init(Module &M);
  virtual bool runOnFunction(Function &F);

  bool isSkipInstrumentFunction(const std::string &funcName);

  // Store/Load instrumentation
  void identifyAccesses(Function &F, AccessInfoList &accesses);
  void trackLoadStores(Function &F, const AccessInfoList &accesses);

  Value* getAccessInfoAddr(const AccessInfo &info);

  void runStackOpt(Function &F, AccessInfoList &accesses);   // remove stack access tracking
  // Thread escape ( pointer) analysis
  void runEscapeOpt(Function &F, AccessInfoList &accesses);

  bool accessIsEscaping(Value *value, Instruction *inst = NULL);
  bool accessIsBlackListed(Value *value);
  bool accessIsStackOnly(Value *value, Instruction *inst);
  bool calledInMultiThreadedMode(Function *f); // is it called in multi-threaded mode?

  // If in same task, remove subsequent access if redundant
  void runSAO(Function &F, AccessInfoList &accesses);
  AccessPairInfo analyzeAccessPair(AccessInfo &A, AccessInfo &B);
  bool accessIsRedundantWith(AccessPairInfo &info);

  // Function to print line information on load/store
  void printLineInfo(Instruction *I) {
    if (MDNode *N = I->getMetadata("dbg")) {  
      DILocation Loc(N);                      // DILocation is in DebugInfo.h
      unsigned Line = Loc.getLineNumber();
      StringRef File = Loc.getFilename();
      // StringRef Dir = Loc.getDirectory();

      DEBUG(dbgs() << I << " File " << File << ", Line " << Line << "\n");
    }
    else {
      DEBUG(dbgs() << I << " File: NA Line: NA\n");
    }
  }

  // Coalesce multiple accesses if possible
  void runACO(Function &F, AccessInfoList &accesses);
  bool coalesceAccesses(AccessPairInfo &info);

  bool isValueAvailableAt(Value* v, Instruction *at);
  void makeValueAvailableAt(Value* v, Instruction *at);
  void computePREJoin(AccessPairInfo &info);

  // Pass identification (replacement for typeid).
  static char ID;
};

char MakeSCC::ID = 0;

MakeSCC::MakeSCC() : FunctionPass(ID) {
  // don't instrument within these functions
  dontInstrumentFuncs.push_back("_GLOBAL__I_a");
  dontInstrumentFuncs.push_back("malloc");
  dontInstrumentFuncs.push_back("free");
  dontInstrumentFuncs.push_back("fprintf");
  dontInstrumentFuncs.push_back("dlopen");
  dontInstrumentFuncs.push_back("dlsym");
  dontInstrumentFuncs.push_back("printf");
  dontInstrumentFuncs.push_back("dlerror");
  dontInstrumentFuncs.push_back("fflush");
  dontInstrumentFuncs.push_back("strcmp");
  dontInstrumentFuncs.push_back("pthread_create");
  dontInstrumentFuncs.push_back("pthread_join");
  dontInstrumentFuncs.push_back("strtol");
  dontInstrumentFuncs.push_back("puts");
  dontInstrumentFuncs.push_back("llvm.memset.p0i8.i64");
  dontInstrumentFuncs.push_back("MAMA_acquireLock");
  dontInstrumentFuncs.push_back("MAMA_init");

  dontInstrumentMangled.push_back("__atomic_base");
  dontInstrumentMangled.push_back("MAMA");
  dontInstrumentMangled.push_back("mama");
  dontInstrumentMangled.push_back("allocator_traits");

  dontInstrumentDemangled.push_back("std::");
  dontInstrumentDemangled.push_back("boost::");
  dontInstrumentDemangled.push_back("__gnu_cxx::");
  dontInstrumentDemangled.push_back("emulated_cond_var");
  dontInstrumentDemangled.push_back("__gthread");
  dontInstrumentDemangled.push_back("cond_var_node");

  callInstrument.push_back("pthread_create");
  callInstrument.push_back("pthread_join");
  callInstrument.push_back("pthread_mutex_lock");
  callInstrument.push_back("pthread_mutex_trylock");
  callInstrument.push_back("pthread_mutex_unlock");
  callInstrument.push_back("pthread_barrier_wait");
  callInstrument.push_back("pthread_cond_wait");
  callInstrument.push_back("pthread_cond_signal");
  callInstrument.push_back("pthread_cond_broadcast");
}

void MakeSCC::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<AliasAnalysis>();
  AU.addRequired<DataLayoutPass>();
  AU.addRequired<DominatorTreeWrapperPass>();
  AU.addRequired<ScalarEvolution>();
  AU.addRequired<LoopInfo>();
  AU.addRequired<ThreadEscapeAnalysis>();
}

bool MakeSCC::doInitialization(Module &M) {
  _tool = new ORCATool();
  _tool->Init(M);

  LLVMContext& context = M.getContext();
  _context = &context;

  // Some handy types.
  VoidTy   = Type::getVoidTy(context );
  FloatTy  = Type::getFloatTy(context);
  DoubleTy = Type::getDoubleTy(context);
  Int8Ty   = Type::getInt8Ty(M.getContext());
  Int16Ty  = Type::getInt16Ty(M.getContext());
  Int32Ty  = Type::getInt32Ty(M.getContext());
  Int64Ty  = Type::getInt64Ty(M.getContext());
  Int64PtrTy  = Type::getInt64PtrTy(M.getContext());

  VoidPtrTy    = Type::getInt32PtrTy(context);
  VoidPtrPtrTy = Type::getInt32PtrTy(context)->getPointerTo(0);

  // Enumerate all functions whose address is taken (other than in a direct call),
  // and place them in a NULL-terminated list.
  std::vector<Constant*> L;
  for (Module::iterator F = M.begin(); F != M.end(); ++F) {
    if (F->isDeclaration())
      continue;
    for (Function::use_iterator U = F->use_begin(); U != F->use_end(); ++U) {
      // Skip direct calls.
      if (CallInst* call = dyn_cast<CallInst>(*U)) {
        if (call->getCalledFunction() == F)
          continue;
      }
      L.push_back(ConstantExpr::getPointerCast(F, VoidPtrTy));
      break;
    }
  }

  // null-terminate
  L.push_back(ConstantPointerNull::get(VoidPtrTy));

  return true;
}

void MakeSCC::init(Module &M) 
{
  const DataLayout &TD = (getAnalysis<DataLayoutPass>()).getDataLayout();
}

bool MakeSCC::runOnFunction(Function &F) {
  bool rc = false;

  // FIXME: Create a list of such functions and insert in it
  std::string fname = F.getName();

  if(isSkipInstrumentFunction(fname)) {
    return false;
  }

  Module *M = F.getParent();
  init(*M);

  if (doSccDbg)
    DEBUG(dbgs() << F);

  // Attach DSNode info to instructions. Also, after each call instruction.
  // reinstate the current function id for ld/st tracking
  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  for (inst_iterator II = inst_begin(F), E = inst_end(F); II != E; ++II) {
    Instruction *inst = &*II;
    EA.attachDSNodeMetaData(inst);
  }

  if (doLoadTrack || doStoreTrack) {
    DEBUG(dbgs() << "[SCC] MemTrack(" << fname << ")\n");
    AccessInfoList accesses;

    // The order here is important!
    identifyAccesses(F, accesses);
    trackLoadStores(F, accesses);
  }

  if(fname.compare("main") == 0){
    Constant *InstFuncConst =
      M->getOrInsertFunction("MAMA_init",
                             FunctionType::get(Type::getVoidTy(M->getContext()),
                                               false));
    assert(InstFuncConst != NULL);
    Function *InitHookFunction = dyn_cast<Function>(InstFuncConst);
    assert(InitHookFunction != NULL);

    auto BB = F.begin();
    auto BI = BB->begin();
    IRBuilder<> Builder(M->getContext());
    Builder.SetInsertPoint(BB,BI);
    Builder.CreateCall(InitHookFunction);
  }

  return rc;
}

// Is this some function that shouldn't be instrumented?
bool MakeSCC::isSkipInstrumentFunction(const std::string &fname) {
  // Get demangled name and check for trusted C++ library prefixes
  int status = -1;
  std::unique_ptr<char, void(*)(void*)> res { abi::__cxa_demangle(fname.c_str(), NULL, NULL, &status), std::free };
  std::string result;

  if (status == 0){
    result = res.get();

    for (size_t i = 0; i < dontInstrumentDemangled.size(); ++i){
      const std::string &p = dontInstrumentDemangled[i];
      if (result.find(p) != std::string::npos){
        return true;
      }
    }
  }

  for (size_t i = 0; i < dontInstrumentMangled.size(); ++i){
    const std::string &p = dontInstrumentMangled[i];
    if (fname.find(p) != std::string::npos){
      return true;
    }
  }

  for (size_t i = 0; i < dontInstrumentFuncs.size(); ++i) {
    const std::string &p = dontInstrumentFuncs[i];
    if (strncmp(fname.c_str(), p.c_str(), p.size()) == 0) {
      DEBUG(dbgs() << "SCC: check safe : " << fname << "\n");
      return true;
    }
  }

  return false;
}

// Classify and optimize each memory access.

void MakeSCC::identifyAccesses(Function &F, AccessInfoList &accesses) 
{
  accesses.clear();
  AccessInfo infos[2];

  time_t t1 = time(NULL);

  DominatorTree& DT = getAnalysis<DominatorTreeWrapperPass>().getDomTree();
  // AliasAnalysis &AA = getAnalysis<AliasAnalysis>();
  const DataLayout &TD = (getAnalysis<DataLayoutPass>()).getDataLayout();

  // Access Identification.
  // Do a simple pass to gather all accesses.  Make this a depth-first
  // traversal over the dominator tree so that each instruction on the
  // access list appears before all instructions it dominates.
  std::vector<DomTreeNode*> stack;
  stack.push_back(DT.getRootNode());

  while (!stack.empty()) {
    DomTreeNode* node = stack.back();
    stack.pop_back();

    // Check for Loads and Stores and do some trivial optimizations.
    BasicBlock* bb = node->getBlock();
    for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
      for (int i = 0, n = AccessInfo::classify(TD, *_context, inst, infos); i < n; ++i) {
        AccessInfo &A = infos[i];
        assert(A.inst);
        assert(A.addr);
        assert(A.type != AccessInfo::None);

        const GlobalVariable *GV = dyn_cast<GlobalVariable>(GetUnderlyingObject(A.addr, &TD, 0));
        // const GlobalVariable *GV = dyn_cast<GlobalVariable>((A.addr));
        if(GV && GV->isConstant()) {
          continue;
        }

        accesses.push_back(new AccessInfo(A));
      }
    }

    // Recurse.
    for (DomTreeNode::iterator iter = node->begin(); iter != node->end(); ++iter)
      stack.push_back(*iter);
  }

  // Optimizations
  runStackOpt(F, accesses);
  runEscapeOpt(F, accesses);
  runSAO(F, accesses);
  runACO(F, accesses);
}

//------------------------------------------------------------------------------
// Optimizations
//------------------------------------------------------------------------------

static AccessInfoList::iterator eraseAccessAndAdvance(AccessInfoList& accesses,
                                                      AccessInfoList::iterator A) 
{
  AccessInfoList::iterator next = AccessInfoList::getNext(A);
  accesses.erase(A);
  return next;
}

//
// Basic analysis about a pair of accesses.
//

static bool accessesMustAlias(AccessInfo &A, AccessInfo &B, Pass &pass) 
{
  // Returns true iff 'A' must alias 'B'.
  AliasAnalysis& aa = pass.getAnalysis<AliasAnalysis>();

  // They must both use a constant size, else alias analysis can't be applied.
  if (A.addr == B.addr && A.size == B.size)
    return true;
  if (!A.isConstantSize() || !B.isConstantSize())
    return false;
  return aa.alias(A.addr, A.constantSize(), B.addr, B.constantSize())
         == AliasAnalysis::MustAlias;
}

// Must alias with Value instead of AccessInfo
static bool valuesMustAliasForMtrom(const Value *v1, const Value *v2, Pass &pass)
{
  if(!(isa<PointerType>(v1->getType()) && isa<PointerType>(v1->getType())))
    return false;

  // They must both use a constant size, else alias analysis can't be applied.
  if(v1 == v2)   // no size check?
    return true;
  /* if (!A.isConstantSize() || !B.isConstantSize())
    return false; */

  AliasAnalysis& aa = pass.getAnalysis<AliasAnalysis>();
  return aa.alias(v1, 4, v2, 4) == AliasAnalysis::MustAlias;
}


AccessPairInfo MakeSCC::analyzeAccessPair(AccessInfo &A, AccessInfo &B) 
{
  // Assumes that 'A' may dominate 'B' but 'B' doesn't dominate 'A'.
  const DominatorTree& dt = getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  AccessPairInfo info(A,B);
  info.dominates = dt.dominates(A.inst, B.inst);
  info.mustAlias = accessesMustAlias(A, B, *this);
  info.join = NULL;
  computePREJoin(info);

  return info;
}

// Is the DSNode ( points-to set) of the Value accessible from another thread?
// Also consider phase of original instruction.
bool MakeSCC::accessIsEscaping(Value *value, Instruction *inst) 
{
  if (!doEscapeOpt)
    return true;

  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  return (EA.valueMayEscape(value, inst) != SCC_NON_ESCAPING_NODE);
}

// Is the DSNode of this Value mutex-protected (blacklisted) ?
bool MakeSCC::accessIsBlackListed(Value *value) 
{
  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  return EA.valueIsBlackListed(value);
}

// Is this access only accessing stack location? false return value is conservative.
bool MakeSCC::accessIsStackOnly(Value *value, Instruction *inst) 
{
  if (!doStackOpt)
    return false;

  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  return EA.valueIsStackOnly(value, inst);
}

// Is this function called in multi-threaded mode
bool MakeSCC::calledInMultiThreadedMode(Function *func)
{
  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  return EA.calledInMultiThreadedMode(func);
}

// Eliminate thread local memory accesses
void MakeSCC::runEscapeOpt(Function &F, AccessInfoList &accesses) 
{
  if (!doEscapeOpt)
    return;

  bool calledInMultiThreaded = calledInMultiThreadedMode(&F);

  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();

  DEBUG(dbgs() << "[SCC] ... runEscapeOpt ...\n");
  // std::cerr << "runEscapeOpt for function " << F.getName().data() << "\n";

  // Do a quick pass to remove all safely nonescaping accesses.
  AccessInfoList::iterator A = accesses.begin();
  while (A != accesses.end()) {
    /* GetElementPtrInst* Gep = dyn_cast<GetElementPtrInst>(A->addr);
      fprintf(stderr, "Checking %s access with %s GEP\n", A->isLoad() ? "load" : "store",
           Gep ? "" : "NO"); */
    if(!calledInMultiThreaded) {
      // Whole function is never called in MT mode. But may be, it creates threads.
      DEBUG(dbgs() << "[SCC] Removing non-escaping access in function " << F.getName().data() << "\n" << (*A->inst) << "\n");
      A = eraseAccessAndAdvance(accesses, A);
      continue;
    }

    A->escaping =  accessIsEscaping(A->addr, A->original); // escaping stores will be dealt with later

    if (A->isLoad() && (!A->escaping)) {
      // Due to rollback, we can only skip loads that don't escape
      DEBUG(dbgs() << "[SCC] Removing non-escaping access in function " << F.getName().data() << "\n" << (*A->inst) << "\n");
      A = eraseAccessAndAdvance(accesses, A);
      continue;
    } 

    // Also check if these are mutex protected, or is in ST mode within a function that creates TS.
    // If whole function is never called in any TS, calledInMultiThreaded above would have handled it.
    A->stModeOnly = !EA.execInParallelMode(A->original);
    if(!A->stModeOnly)
      A->mutexProtected = EA.execMutexProtectedOnly(A->addr, A->original);

    if(A->isLoad() && (A->stModeOnly || A->mutexProtected)) {
      const char *reason = A->stModeOnly ? "ST-mode" : "mutex protected";
      DEBUG(dbgs() << "[SCC] Removing " << reason << " access in function " << F.getName().data() << "\n" << (*A->inst) << "\n");
      A = eraseAccessAndAdvance(accesses, A);
      continue;
    }

    ++A;
  }
}

// Eliminate stack accesses , as they are saved in STM runtime model
void MakeSCC::runStackOpt(Function &F, AccessInfoList &accesses) 
{
  if (!doStackOpt)
    return;

  // bool calledInMultiThreaded = calledInMultiThreadedMode(&F);

  DEBUG(dbgs() << "[SCC] ... runStackOpt ...\n");
  // std::cerr << "runStackOpt for function " << F.getName().data() << "\n";

  // Do a quick pass to remove all stack-only accesses.
  AccessInfoList::iterator A = accesses.begin();
  while (A != accesses.end()) {
    // GetElementPtrInst* Gep = dyn_cast<GetElementPtrInst>(A->addr);
    if (accessIsStackOnly(A->addr, A->inst)) { 
      DEBUG(dbgs() << "[SCC] Removing stack access in function " << F.getName().data() << "\n" << (*A->inst) << "\n");
      A = eraseAccessAndAdvance(accesses, A);
    } else {
      ++A;
    }
  }
}

//
// SAO (Subsequent Access Optimization)
//

bool MakeSCC::accessIsRedundantWith(AccessPairInfo &info) 
{
  AccessInfo &A = info.A;
  AccessInfo &B = info.B;

  // Returns true if 'B' should be optimized due to the presence of 'A'.
  // Assumes that 'A' may dominate 'B' but 'B' doesn't dominate 'A'.

  // Check the type.
  if (A.isLoad() && B.isLoad()) {
    if (!doMemTrackLdLdOpt) return false;
  }

  if (A.isLoad() && B.isStore()) {
    if (!doMemTrackLdStOpt) return false;
  }

  if (A.isStore() && B.isLoad()) {
    if (!doMemTrackStLdOpt) return false;
  }

  if (A.isStore() && B.isStore()) {
    if (!doMemTrackStStOpt) return false;
  }

  // Check the conditions for SAO.
  if (!info.mustAlias)
    return false;
  if (!info.dominates && (!doMemTrackPreOpt || !info.join))
    return false;

  // The accesses are redundant, so 'B' can be removed.
  // Upgrade 'A' to an exclusive load if necessary.
  if (A.isLoad() && B.type != AccessInfo::Load) {
    A.type = AccessInfo::ExclusiveLoad;
  }

  // If PRE was applied, make sure 'A' is available at the join point.
  if (info.join) {
    makeValueAvailableAt(A.addr, info.join);
    makeValueAvailableAt(A.size, info.join);
    A.inst = info.join;
  }

  if (info.join) {
    DEBUG(dbgs() << "*** SAO+PRE ***********************************************\n");
  } else {
    DEBUG(dbgs() << "*** SAO ***************************************************\n");
  }
  DEBUG(dbgs() << (*A.original));
  DEBUG(dbgs() << (*B.original));

  return true;
}

void MakeSCC::runSAO(Function &F, AccessInfoList &accesses) 
{
  DEBUG(dbgs() << "[SCC] ... runSAO ...\n");

  // Iterate over pairs of accesses.
  // Note that that dominators appear before all their dominees.
  for (AccessInfoList::iterator A = accesses.begin(); A != accesses.end(); ++A) {
    AccessInfoList::iterator B = A;
    ++B;

    while (B != accesses.end()) {
      AccessPairInfo info = analyzeAccessPair(*A, *B);
      if (!accessIsRedundantWith(info)) {
        ++B;
        continue;
      }
      B = eraseAccessAndAdvance(accesses, B);
    }
  }
}

//
// ACO (Access Coalescing Optimization)
// partly inspired by lib/Transforms/Scalar/MemCpyOptimizer.cpp
//

static bool GepHasVariableIndexAfter(GetElementPtrInst* GEP, unsigned i) 
{
  // Returns true if 'GEP' has any variable indices after or including 'i'.
  for (; i < GEP->getNumOperands(); ++i) {
    if (!isa<ConstantInt>(GEP->getOperand(i)))
      return true;
  }
  return false;
}

int64_t GetOffsetFromIndex(GetElementPtrInst* GEP, unsigned Start,
                            const DataLayout &TD) 
{
  // Skip over the first indices.
  gep_type_iterator GTI = gep_type_begin(GEP);
  for (unsigned i = 1; i < Start; ++i, ++GTI)
    /*skip along*/;
  
  // Compute the offset implied by the rest of the indices.
  // Technically, this could be negative (think array indices in C).
  int64_t Offset = 0;
  for (unsigned i = Start; i < GEP->getNumOperands(); ++i, ++GTI) {
    ConstantInt *C = dyn_cast<ConstantInt>(GEP->getOperand(i));
    assert(C && "assumed constant GEP indices!");

    if (C->isZero())
      continue;

    // Structs: add the field offset.
    if (StructType *Ty = dyn_cast<StructType>(*GTI)) {
      Offset += TD.getStructLayout(Ty)->getElementOffset(C->getZExtValue());

    // Arrays: multiply by the element size.
    } else {
      uint64_t Size = TD.getTypeAllocSize(GTI.getIndexedType());
      Offset += Size * C->getSExtValue();
    }
  }

  return Offset;
}

bool MakeSCC::coalesceAccesses(AccessPairInfo &info) 
{
  AccessInfo &A = info.A;
  AccessInfo &B = info.B;

  // If 'B' can be coalesced into 'A', merge 'A' into 'B' and return true.
  // Assumes that 'A' may dominate 'B' but 'B' doesn't dominate 'A'.
  if (!doMemCoalesceOpt)
    return false;

  const DataLayout &td = getAnalysis<DataLayoutPass>().getDataLayout();

  // Check the conditions for ACO.
  if (!info.dominates && (!doMemCoalescePreOpt || !info.join))
    return false;

  // This case should have been covered by SAO.
  if (info.mustAlias)
    return false;

  // Check that 'A' and 'B' are offsets from the same base pointer.
  // TODO: what cases does this miss?
  GetElementPtrInst* GepA = dyn_cast<GetElementPtrInst>(A.addr);
  GetElementPtrInst* GepB = dyn_cast<GetElementPtrInst>(B.addr);
  if (!GepA || !GepB)
    return false;

  AccessInfo BaseA;
  BaseA.addr = GepA->getPointerOperand();
  BaseA.setSizeFromAddr(td, *_context);

  AccessInfo BaseB;
  BaseB.addr = GepB->getPointerOperand();
  BaseB.setSizeFromAddr(td, *_context);

  if (!accessesMustAlias(BaseA, BaseB, *this))
    return false;

  // Skip over the shared indices.
  unsigned i = 1;
  for (; i < GepA->getNumOperands() && i < GepB->getNumOperands(); ++i) {
    if (GepA->getOperand(i) != GepB->getOperand(i))
      break;
  }

  // Fail if any remaining indices are variable: offsets must be constant.
  if (GepHasVariableIndexAfter(GepA, i) || GepHasVariableIndexAfter(GepB, i))
    return false;

  // Compute the offset of 'A' and 'B' from their shared base address.
  const int64_t OffsetA = GetOffsetFromIndex(GepA, i, td);
  const int64_t OffsetB = GetOffsetFromIndex(GepB, i, td);

  // Constant sizes are currently required for simplicity.
  if (!A.isConstantSize() || !B.isConstantSize()) {
    DEBUG(dbgs() << "*** ACO (skip nonconst) *****************************\n");
    DEBUG(dbgs() << (*A.original));
    DEBUG(dbgs() << (*B.original));
    return false;
  }

  // Profitability check: the accesses should be within one MOT block.
  const int64_t delta  = (OffsetA < OffsetB) ? OffsetB - OffsetA : OffsetA - OffsetB;
  const int64_t bottom = (OffsetA < OffsetB) ? A.constantSize()  : B.constantSize();
  const int64_t gap    = delta - bottom;

  // Adjust 'A.size' to merge with 'B'.
  // If 'A' is not first, we also have to adjust its address.
  if (OffsetA <= OffsetB) {
    A.size = ConstantInt::get(Int64Ty,
                              std::max(A.constantSize(), delta + B.constantSize()));
  } else {
    if (BaseA.addr->getType() != BaseB.addr->getType())
      return false;
    SmallVector<Value*, 8> Indices(GepB->idx_begin(), GepB->idx_end());
    Twine tw("ACOaddr");
    A.addr = GetElementPtrInst::Create(GepA->getPointerOperand(),
                                       Indices, tw, A.inst);
    A.size = ConstantInt::get(Int64Ty,
                              std::max(B.constantSize(), delta + A.constantSize()));
  }

  // The alignment of the merged accesses is unknown (can we do better?)
  A.alignment = 1;

  // The accesses have been merged, so 'B' can be removed.
  // Upgrade 'A' to an exclusive load if necessary.
  if (A.isLoad() && B.type != AccessInfo::Load) {
    A.type = AccessInfo::ExclusiveLoad;
  }

  // If PRE was applied, make sure 'A' is available at the join point.
  // NOTE: 'A' changed since 'info.join' was computed, but that's OK because:
  // -- If 'A.addr' changed, its GEP.pointer hasn't changed
  // -- If 'A.size' changed, it is a constant
  if (info.join) {
    makeValueAvailableAt(A.addr, info.join);
    makeValueAvailableAt(A.size, info.join);
    A.inst = info.join;
  }

  if (info.join) {
    DEBUG(dbgs() << "*** ACO+PRE [delta=" << delta << "] *******************************\n");
  } else {
    DEBUG(dbgs() << "*** ACO [delta=" << delta << "] ***********************************\n");
  }
  DEBUG(dbgs() << (*A.original));
  DEBUG(dbgs() << (*B.original));
  DEBUG(dbgs() << (*A.addr));
  DEBUG(dbgs() << "        [" << (*A.size) << "]\n");

  return true;
}

void MakeSCC::runACO(Function &F, AccessInfoList &accesses) 
{
  if (!doMemCoalesceOpt)
    return;

  DEBUG(dbgs() << "[SCC] ... runACO ...\n");

  // Run this pass repeatedly until it has no effect: it's possible that
  // accesses (a,c) could not be merged by ACO, but they could be merged
  // after ACO has performed a merge like (b,c).
  bool changed = true;
  while (changed) {
    changed = false;

    // Iterate over pairs of accesses.
    // Note that that dominators appear before all their dominees.
    for (AccessInfoList::iterator A = accesses.begin(); A != accesses.end(); ++A) {
      AccessInfoList::iterator B = A;
      ++B;

      while (B != accesses.end()) {
        AccessPairInfo info = analyzeAccessPair(*A, *B);

        if (!coalesceAccesses(info)) {
          ++B;
          continue;
        }

        // Remove this 'B' and keep going.
        changed = true;
        B = eraseAccessAndAdvance(accesses, B);
      }
    }
  }
}

//
// PRE (Partial Redundancy Elimination)
//

bool MakeSCC::isValueAvailableAt(Value* v, Instruction *at) 
{
  DominatorTree& dt =  getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  // Constants and Arguments are always available.
  if (isa<Constant>(v) || isa<Argument>(v))
    return true;

  // Instructions must dominate 'at' or be copyable to 'at'.
  // TODO: lots of ways this can be improved
  if (Instruction* inst = dyn_cast<Instruction>(v)) {
    if (dt.dominates(inst, at))
      return true;
    // GEPs are copyable if the the base ptr is available and indices are const.
    if (GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(inst)) {
      if (!isValueAvailableAt(gep->getPointerOperand(), at))
        return false;
      if (GepHasVariableIndexAfter(gep, 0))
        return false;
      return true;
    }
  }

  return false;
}

void MakeSCC::makeValueAvailableAt(Value* v, Instruction *at) 
{
  DominatorTree& dt = getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  // Assumes 'isValueAvailableAt(v, at)'.
  // In most cases, we don't need to do anything.
  // In some cases, we need to move the instruction to before 'at'.
  if (GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(v)) {
    if (!dt.dominates(gep, at))
      // TODO: clone instead? (would need to update 'v')
      //  gep->clone()->insertBefore(at);
      gep->moveBefore(at);
  }
}

void MakeSCC::computePREJoin(AccessPairInfo &info) 
{
  DominatorTree& dt =  getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  AccessInfo &A = info.A;
  AccessInfo &B = info.B;

  if (!doMemTrackPreOpt && !doMemCoalescePreOpt)
    return;

  // Don't bother with this if 'A' dominates 'B'.
  if (info.dominates)
    return;

  // Find the first basic block which dominates both 'A' and 'B'.
  // This might not exist if, for example, 'A' or 'B' is dead.
  BasicBlock* common = dt.findNearestCommonDominator(A.inst->getParent(),
                                                     B.inst->getParent());
  if (!common) return;
  assert(common != A.inst->getParent());  // already checked this ('info.dominates')
  assert(common != B.inst->getParent());  // 'B' never dominates 'A'

  // We'll join at the end of the common dominator.
  Instruction* join = &(common->back());

  // Make sure the 'addr' and 'size' of each access is available at 'join'.
  if (!isValueAvailableAt(A.addr, join) || !isValueAvailableAt(A.size, join) ||
      !isValueAvailableAt(B.addr, join) || !isValueAvailableAt(B.size, join))
    return;

  info.join = join;
}

Value* MakeSCC::getAccessInfoAddr(const AccessInfo &info) 
{
  Value* addr = info.addr;
  if (replacements.count(addr)) {
    addr = replacements[addr];
  }
  return addr;
}


// EIter replace load/stores ( for buffered loging) or just instrument them (fi unbuffered)
void MakeSCC::trackLoadStores(Function &F, const AccessInfoList &accesses) 
{
  time_t t1 = time(NULL);
  bool doMemTrackDebugCodeLoc = false;
  replacements.clear();

  DEBUG(dbgs() << "[SCC] Instrument function " << F.getName().data() << "\n");

  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  if(!EA.isCalledFromThreadedSection(&F)) {
    DEBUG(dbgs() << " Skipped " << F.getName().data() << " as not reachable from threaded functions\n");
    return;
  }

  // First get all mtrom accesses
  Module *M = F.getParent();
  Function *markMtrom = M->getFunction("scc_assure_mtrom");
  std::set<const Value *> mtromValues;
  for(Function::iterator bb = F.begin(); bb != F.end(); bb++) {
    for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
      if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
        const CallSite cs(inst);

	if(cs.getCalledFunction() != NULL){
	  for (std::string& s : callInstrument) {
	    if(s.compare(cs.getCalledFunction()->getName().str()) == 0){
	      if(isa<CallInst>(inst)){
		_tool->HandleCallInst(*bb,dyn_cast<CallInst>(inst));
	      }else{
		_tool->HandleInvokeInst(*bb,dyn_cast<InvokeInst>(inst));
	      }
	    }
	  }
	}

        if (markMtrom && cs.getCalledFunction() == markMtrom) {
          const CallInst* CI = dyn_cast<CallInst>(inst);
          const Value *value = CI->getOperand(0);
          if(value) {
            mtromValues.insert(value);
            value = EA.getRealUnderlyingObject(value);
            if(value) mtromValues.insert(value);
          }
        }
      } // if callInst
    } // for each instruction in BB
  } // for each basic block

  for (AccessInfoList::const_iterator A = accesses.begin(); A != accesses.end(); ++A) {
    const AccessInfo &info = *A;

    // Skip?
    if (info.type == AccessInfo::None)
      continue;
    if (info.type == AccessInfo::Load && !doLoadTrack)
      continue;
    if (info.type == AccessInfo::ExclusiveLoad && !doLoadTrack && !doStoreTrack)
      continue;
    if (info.type == AccessInfo::Store && !doStoreTrack)
      continue;

    // Access size (or 0 if not constant).
    uint64_t size = 0;
    if (ConstantInt* iSize = dyn_cast<ConstantInt>(info.size))
      size = iSize->getZExtValue();

    Value *addr = getAccessInfoAddr(info);
    const Value *underlyingValue = EA.getRealUnderlyingObject(addr);
    bool mtRomPtr = (mtromValues.count(underlyingValue) > 0 || mtromValues.count(addr) > 0);
    // Check must aliases
    for(std::set<const Value *>::const_iterator itr = mtromValues.begin(); 
          !mtRomPtr && itr != mtromValues.end(); itr++) {
      Value *mtromAddr = const_cast<Value *>(*itr);
      if(replacements.count(mtromAddr))
        mtromAddr = replacements[mtromAddr];

      if(valuesMustAliasForMtrom(addr, mtromAddr, *this) || valuesMustAliasForMtrom(underlyingValue, mtromAddr, *this))
      {
          mtRomPtr = true;
          break;
      }
    } // mtRom Ptr

    if(mtRomPtr) {
      continue;
    }

    _tool->Access(F,info,size);
  }
}

}  // namespace

//------------------------------------------------------------------------------
// Pass Registration
//------------------------------------------------------------------------------

static RegisterPass<MakeSCC> X("makescc", "MakeSCCPass");

FunctionPass *createMakeSCCPass() 
{ 
    return new MakeSCC(); 
}

}  // namespace llvm
