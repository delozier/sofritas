// Helper graphs to identify threaded sections in program
// Originally copied from LLVM source code and modified for SBPA pass.
// released under the University of Santa Cruz Open Source License, and LLVM license

#ifndef _SCC_LOOP_HELPER_H_
#define _SCC_LOOP_HELPER_H_

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/DenseSet.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Support/Debug.h"
#include <map>
#include <set>
#include <algorithm>

using std::vector;
using std::map;
using std::set;

namespace llvm {

// class LoopInfo;
class Loop;
class DominatorTree;
class ScalarEvolution;
class DominanceFrontier;
}

using namespace llvm;
using namespace std;

// A helper class to find loop start, ends, induction variables etc.
class SccLoopHelper
{
  const DominatorTree *DT;
  ScalarEvolution *SE;

  public:
    SccLoopHelper(const DominatorTree *dt, ScalarEvolution *se) { 
        DT = dt; 
        SE = se;
    }
    ~SccLoopHelper() {}


    Value *ComputeExitLimitFromICmp(const Loop *L,
                      ICmpInst *ExitCond,
                      BasicBlock *TBB,
                      BasicBlock *FBB);
    bool isLoopInvariant(Value *V, const Loop *L);
    PHINode *getLoopPhiForCounter(Value *IncV, Loop *L);

    Value *ComputeStart(Loop *L, Value *arrayIndex);
    Value *ComputeExitLimitFromCond(const Loop *L, Value *ExitCond,
                      BasicBlock *TBB, BasicBlock *FBB);
};

#endif
