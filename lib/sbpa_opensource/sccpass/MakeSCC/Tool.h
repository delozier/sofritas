#include "AccessInfo.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/DerivedTypes.h" 
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/CaptureTracking.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/CFG.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/ConstantRange.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/GetElementPtrTypeIterator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/DebugInfo.h"

namespace llvm{

class ORCATool{
  Module *Mod;

  GlobalVariable *tlsTid;
  GlobalVariable *mapRegionBase;

  Function *LoadHookFunction;
  Function *StoreHookFunction;

  Function *InitHookFunction;

  Function *ORCAThreadCreateFunction;
  Function *ORCAThreadJoinFunction;
  Function *ORCABarrierWaitFunction;
  Function *ORCAMutexLockFunction;
  Function *ORCAMutexTrylockFunction;
  Function *ORCAMutexUnlockFunction;
  Function *ORCACondWaitFunction;
  Function *ORCACondSignalFunction;
  Function *ORCACondBroadcastFunction;
  

public:
 bool Init(Module &M);
 bool Access(Function &F, const AccessInfo &info, 
                     const uint64_t accessSize);
 bool HandleInvokeInst(BasicBlock& bb, InvokeInst *inst);
 bool HandleCallInst(BasicBlock& bb, CallInst *inst);
};

}

