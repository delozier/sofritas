//===- Hello.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source License.
// This file is distributed under the University of Santa Cruz Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This pass adds instrumentation code when using SCC pass, so that the dynamic profile 
// can later be loaded for profile guided SCC optimizations.
//
// This pass was initially inspired by the PtProfile pass (harmony) and edge profiling
// pass in llvm, but needed to be adapted for SCC integration and specific needs.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "sccprofile"
#include <iostream>
#include "llvm/ADT/Statistic.h"
// #include "llvm/Analysis/Verifier.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
// #include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "SccProfile.h"

#include <vector>
#include <algorithm>
#include <cstdio>

using namespace llvm;

namespace llvm {

namespace {
cl::opt<bool> doSccProfile("scc-profile", cl::init(false),
              cl::desc("Add instrumentation code for profiling for SCC passes."));
cl::opt<bool> useSccProfile("scc-use-profile", cl::init(false),
              cl::desc("Use previous runtime profiles for scc pass."));

static int32_t profNumTasks = 0;
} 

char SccProfile::ID = 0;
static RegisterPass<SccProfile> X("sccprofile",
                                            "Profile for SCC optimizations");
ModulePass *createSccProfilePass() { 
  // fprintf(stderr, "CREATING SCC Profile PASS\n");
  return new SccProfile(); 
}

// This is inverse of write_histogram function in sccstm/scc_profile.cpp
// Assume fixed filename sccprof.out
// However, this is for use during compiler optimziation for black listing

// histogram format (binary)
// assume machine endianess
// Bytes | Data Type | Name
// -------------------------
// 0-3   | uint32    | nbbs
// 4-7   | uint32    | nthreads
// 8-11  | uint32    | type of profile (0 = active; 1 = working)
// 12-n  | uint32[]  | data, where n = 12 + 4*PER_THREAD_N
// n+1-  | char      | basic block names; each string is null-terminated

bool SccProfile::readHistogram()
{
  const char *name = "scc.prof";

  _n_threads = 0;
  _n_bbs = 0;
  _n_tasks = 0;
  _n_composite_tasks = 0;

  FILE *fp = fopen(name,"rb");
  if(!fp) return false;

  size_t n = fread(&_n_bbs, 4, 1, fp);
  n = fread(&_n_composite_tasks, 4, 1, fp);
  n = fread(&_n_tasks, 4, 1, fp);
  n = fread(&_n_threads,  4, 1, fp);

  size_t taskTaskEntries = _n_composite_tasks * _n_composite_tasks;
  _task_task_overlap = (float *) calloc(taskTaskEntries,  sizeof(float));
  n = fread(_task_task_overlap, 4, taskTaskEntries, fp);

  unsigned bbTaskEntries = _n_bbs * _n_composite_tasks;
  _bb_task_map = (uint32_t *) calloc(bbTaskEntries, sizeof(uint32_t));
  n = fread(_bb_task_map, 4, bbTaskEntries, fp);

#if 0
  // skip reading basic block names
  char **p = PROF_bbmap;
  while (*p) {
    PUTS(*p, f);
    PUTC(0, f); // reinsert a null termination
    ++p;
  }
#endif

  fclose(fp);
  return true;
}

// Print histogram when needed for debug
void SccProfile::printHistogram()
{
  fprintf(stderr,"***** HISTOGRAM PRINTING NOT DONE YET ***** !\n");
#if 0
  if(_n_bbs == 0 || _profile_hist == NULL) {
    fprintf(stderr," NO HISTOGRAM TO PRINT!\n");
  }
  else {
    fprintf(stderr,"BB with  1    2    3    4    5    6    7    8 threads\n");
    fprintf(stderr,"==========================================================\n");
    for(uint32_t i = 0; i < _n_bbs; i++)  {
      fprintf(stderr,"%5d ", i);
      for(int j = 0; j < 8; j++)  {
         int n_offset = j * _n_bbs;
         fprintf(stderr,"%4d ", _profile_hist[n_offset + i]);
      }
      fprintf(stderr,"\n");
    }
  }
#endif
}



} // end llvm namespace


// INITIALIZE_PASS(SccProfile, "insert-ptprofile", "Instrument pthread block profiling", false, false)
namespace {

static Module *pM;

// some types
static IntegerType *int32Ty;
static IntegerType *int8Ty;
static FunctionType *funcVoidTy;
static PointerType *ptrFuncVoidTy;
static StructType *globalCDtorElemTy;
static FunctionType *startRoutineTy;
static PointerType *ptrStartRoutineTy;
static PointerType *voidPtrTy;
static StructType *shimWrapperTy;
static PointerType *ptrShimWrapperTy;
static Type* voidTy;
static FunctionType *sampleCallTy;

static void initializeTyConstants(Module &M) {
  LLVMContext &ctx = M.getContext();
  int32Ty = Type::getInt32Ty(ctx);
  int8Ty = Type::getInt8Ty(ctx);
  funcVoidTy = FunctionType::get(Type::getVoidTy(ctx), false);
  ptrFuncVoidTy = static_cast<Type *>(funcVoidTy)->getPointerTo();
  globalCDtorElemTy = StructType::get((Type *) int32Ty, (Type *) ptrFuncVoidTy, NULL);
  voidPtrTy = Type::getInt8Ty(ctx)->getPointerTo();

  startRoutineTy = FunctionType::get(voidPtrTy, 
      ArrayRef<Type*>(voidPtrTy), /*isVarArg*/ false);
  ptrStartRoutineTy = static_cast<Type *>(startRoutineTy)->getPointerTo();

  Type *wrapper_tmp[] = { ptrStartRoutineTy, voidPtrTy };

  shimWrapperTy = StructType::create(ArrayRef<Type*>(wrapper_tmp, 2), 
    "PROF_shim_wrapper_t");
  ptrShimWrapperTy = shimWrapperTy->getPointerTo();
  voidTy = Type::getVoidTy(ctx);

  sampleCallTy = FunctionType::get(voidTy, 
      ArrayRef<Type*>(int32Ty), false);
}
} // anonymous

namespace llvm {
void SccProfile::insertModuleInit(Module &M) 
{
  std::vector<Constant *> ctors;
 
  // If llvm.global_ctors already exists, copy contents into ctors and destroy.
  // Assume priorities will not overflow, and minimum priority is zero.
  if (GlobalVariable *globalCtors = M.getNamedGlobal("llvm.global_ctors")) {
    if (ConstantArray *initList = 
        dyn_cast<ConstantArray>(globalCtors->getInitializer())) {
      for (unsigned i = 0, e = initList->getType()->getNumElements();
           i != e; ++i) {
        Constant *entry = cast<Constant>(initList->getOperand(i));
        ConstantInt *priority = cast<ConstantInt>(entry->getOperand(0));
        DEBUG(dbgs() << "Constructor " << *entry << " priority is " << *priority << "\n");

        APInt p = priority->getValue();
        assert(!priority->isMaxValue(false));
        p++;

        ctors.push_back(
            ConstantStruct::get(globalCDtorElemTy,
              ConstantInt::get(M.getContext(), p),
              cast<Constant>(entry->getOperand(1)),
              NULL));
      }
    }
    globalCtors->eraseFromParent();
  }

  // Get the module initializer function and add it as the lowest priority
  Constant *moduleInitFn = M.getOrInsertFunction("PROF_init_module", funcVoidTy);
  ctors.push_back(ConstantStruct::get(
        globalCDtorElemTy,
        ConstantInt::get(int32Ty, 0),
        ConstantExpr::getBitCast(moduleInitFn, ptrFuncVoidTy),
        NULL));

  // Reconstruct globalCtors using the ctors vector
  GlobalVariable *globalCtors = new GlobalVariable(
      M, ArrayType::get(globalCDtorElemTy, ctors.size()), /*isConstant*/ false,
      GlobalValue::AppendingLinkage, /*Initializer*/ NULL,
      "llvm.global_ctors"); 
  globalCtors->setInitializer(ConstantArray::get(
    cast<ArrayType>(globalCtors->getType()->getElementType()), ctors));
  DEBUG(dbgs() << "globalCtors is " << *globalCtors << "\n");
}

void SccProfile::insertSampleCall(int bb_id, BasicBlock *B) 
{
  BasicBlock::iterator insertPoint = B->getFirstInsertionPt();
  ConstantInt *constBBId = ConstantInt::get(int32Ty, bb_id);
  CallInst::Create(sampleCall, ArrayRef<Value*>(constBBId), "", insertPoint);
}

void SccProfile::insertBBMap(const std::vector<BasicBlock*> &bbs) 
{
  // inserting this equivalent C code:
  // char *PROF_bbmap[] = { "alpha", "beta" };
  //
  // which is this in LLVM:
  // @.str = private unnamed_addr constant [6 x i8] c"alpha\00", align 1
  // @.str1 = private unnamed_addr constant [5 x i8] c"beta\00", align 1
  // @PROF_bbmap = global [2 x i8*] [i8* getelementptr inbounds ([6 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str1, i32 0, i32 0)], align 16
  //
  // which is the following in C++:
  std::vector<Constant*> gepIndices;
  ConstantInt *zero = ConstantInt::get(int32Ty, 0);
  gepIndices.push_back(zero);
  gepIndices.push_back(zero);

  std::vector<Constant*> bbNames;

  // No need to insert all these if 
  // Set PROF_nbbs to 1, so runtime knows that there is no need to write out profile file.
  // That is an indication that profiling wasn't done.
  if(doSccProfile) {
    char bb_n_str[64];
    for (int i = 0, sz = bbs.size(); i < sz; ++i) {
      snprintf(bb_n_str, 63, "%d", i);
      Twine tName = bbs[i]->getParent()->getName() + ":" + bbs[i]->getName();
      std::string srName(tName.str());
      ArrayType *ty = ArrayType::get(int8Ty, srName.size() + 1);
      Constant *StrConst = ConstantDataArray::getString(pM->getContext(), srName);

      GlobalVariable *v = new GlobalVariable(/*Module=*/*pM,
        /*Type=*/ty,
        /*isConstant=*/true,
        /*Linkage*/GlobalValue::PrivateLinkage,
  //      /*Initializer=*/ ConstantArray::get(pM->getContext(), srName), // c string shorthand
        /*Initializer=*/ StrConst, // c string shorthand
        /*Name=*/ std::string("PROF_bb_id_") + bb_n_str);
      v->setAlignment(1);
      Constant *const_ptr_v = ConstantExpr::getGetElementPtr(v, gepIndices);
      bbNames.push_back(const_ptr_v);
    }
  } // if we need to profile this binary

  // Null-terminate this list
  PointerType *ptrInt8Ty = PointerType::get(int8Ty, 0);
  bbNames.push_back(llvm::ConstantPointerNull::get(ptrInt8Ty));
  ArrayType *BBMapTy = ArrayType::get(ptrInt8Ty, bbNames.size());
  Constant *constBBMap = ConstantArray::get(BBMapTy, bbNames);
  /* GlobalVariable *gvBBMap = */ new GlobalVariable(*pM,
      BBMapTy,
      true,
      GlobalValue::ExternalLinkage,
      constBBMap,
      "PROF_bbmap");

  // Also add a counter. Initialize it
  /* GlobalVariable *gvNbbs =*/ new GlobalVariable(*pM,
      int32Ty,
      true,
      GlobalValue::ExternalLinkage,
      ConstantInt::get(int32Ty, bbNames.size()),
      "PROF_nbbs");

   // Create variable for number of static tasks
   /* GlobalVariable *gvNtasks =*/ new GlobalVariable(*pM,
      int32Ty,
      true,
      GlobalValue::ExternalLinkage,
      ConstantInt::get(int32Ty,profNumTasks+1),
      "PROF_ntasks");
   if(bbNames.size() > 1)
     fprintf(stderr, "SccProfile: Profiling with PROF_nbbs %lu PROF_ntasks %d\n",  bbNames.size(), profNumTasks+1);
   else
     fprintf(stderr, "SccProfile: Not inserting profiling code. but defining PROF_nbbs as 1 for linking.\n");
}
} // llvm

namespace {
  class PthreadCreateVisitor : public InstVisitor<PthreadCreateVisitor> {
  private:
    int ci;
    IRBuilder<> builder;
    Constant *threadInitFn;

  public:
    PthreadCreateVisitor() : ci(0), builder(pM->getContext()),
      threadInitFn(pM->getOrInsertFunction("PROF_init_thread", funcVoidTy)) { }
    void visitCallInst(CallInst &call) {
      Function *f = call.getCalledFunction();
      if (f == 0) {
        DEBUG(dbgs() << "PthreadCreateVisitor::visitCallInst saw indirect call: "
                     << call << " -- make sure this is not pthread_create.\n");
        return;
      }
      if (f->getName() == "pthread_create") {
        fprintf(stderr,"FOUND PTHREAD_CREATE FOR profiling\n");
        DEBUG(dbgs() << "PthreadCreateVisitor: " << call << "Was a call to pthread_create\n");
        ci++;

        builder.SetInsertPoint(&call);
  
        Instruction *wrapper = CallInst::CreateMalloc(/*InsertBefore*/ &call,
            /*IntPtrTy*/ int32Ty,
            /*AllocTy*/ shimWrapperTy,
            /*AllocSize*/ builder.getInt32(1));
        Value *wrapper_start_routine = builder.CreateConstGEP2_32(
            wrapper, 0, 0);
        Value *wrapper_arg = builder.CreateConstGEP2_32(
            wrapper, 0, 1);
        
        DEBUG(dbgs() << "wrapper_t = " << *shimWrapperTy);
        DEBUG(dbgs() << "wrapper = " << *wrapper << "\n");
        DEBUG(dbgs() << "wrapper_start_routine = " << *wrapper_start_routine
                     << " and " << "wrapper_arg = " << *wrapper_arg << "\n");
        Value *start_routine = call.getArgOperand(2);
        Value *arg           = call.getArgOperand(3);
        builder.CreateStore(start_routine, wrapper_start_routine);
        builder.CreateStore(arg, wrapper_arg);
        
        // call pthread_create with shim instead of start_routine
        Constant *shim = pM->getOrInsertFunction("PROF_shim_start_routine",
            startRoutineTy);
        Value *newArg = builder.CreateBitCast(wrapper, voidPtrTy);
        Value *newArgs[] = {call.getArgOperand(0), 
                            call.getArgOperand(1),
                            shim,
                            newArg};
        CallInst *newCall = CallInst::Create(call.getCalledValue(),
                                             ArrayRef<Value*>(newArgs, 4),
                                             Twine("PROF_patched_call"),
                                             &call);
        DEBUG(dbgs() << "old call was " << call << "\n");
        DEBUG(dbgs() << "newCall is   " << *newCall << "\n");
        call.replaceAllUsesWith(newCall);
        call.eraseFromParent();
        DEBUG(dbgs() << "erased from parent" << "\n");
        // TODO: make the shim
      }
    }
  };

  class BlockingCallVisitor : public InstVisitor<BlockingCallVisitor> {
  private:
    std::vector<StringRef> blockingNames;
    Constant *preblockFn;
    Constant *postblockFn;
    inline bool isBlocking(Function *f) {
      if (f == 0) { return false; }
      std::vector<StringRef>::iterator r = 
        std::find(blockingNames.begin(), blockingNames.end(), f->getName());
      return r != blockingNames.end();
    }

  public:
    BlockingCallVisitor() {
      preblockFn = pM->getOrInsertFunction("PROF_postblock", funcVoidTy);
      postblockFn = pM->getOrInsertFunction("PROF_postblock", funcVoidTy);
      blockingNames.push_back("pthread_mutex_lock");
      blockingNames.push_back("pthread_rwlock_wrlock");
      blockingNames.push_back("pthread_rwlock_rdlock"); 
      blockingNames.push_back("pthread_cond_timedwait");
      blockingNames.push_back("pthread_cond_wait");
      blockingNames.push_back("pthread_join");
      blockingNames.push_back("pthread_barrier_wait");
    }
    void visitCallInst(CallInst &call) {
      Function *f = call.getCalledFunction();
      if (f == 0) {
        DEBUG(dbgs() << "BlockingCallVisitor::visitCallInst saw indirect call: "
                     << call << " -- make sure this is not a blocking call.\n");
        return;
      }
      if (isBlocking(f)) {
        DEBUG(dbgs() << "Got a blocking call " << *f << "\n");
        CallInst::Create(preblockFn)->insertBefore(&call);
        CallInst::Create(postblockFn)->insertAfter(&call);
      }
    }
  };

  // Create a function call to set static task id after each call
  class SccTaskCreateCallVisitor : public InstVisitor<SccTaskCreateCallVisitor> {
  private:
    std::vector<StringRef> taskCreateNames;
    Constant *setTaskIdFn ;
    bool isBlocking(Function *f) {
      if (f == 0) { return false; }
      #if 0
      std::vector<StringRef>::iterator r = 
        std::find(taskCreateNames.begin(), taskCreateNames.end(), f->getName());
      return r != taskCreateNames.end();
      #else
      StringRef fname = f->getName();
      // fprintf(stderr,"Isblocking called for %s\n", fname.data()); 
      for(size_t i = 0, ie = taskCreateNames.size(); i < ie; i++) {
        if(fname.find(taskCreateNames[i]) != StringRef::npos)
          return true;
      }
      return false;
      #endif
    }

  public:
    SccTaskCreateCallVisitor() {
      setTaskIdFn = pM->getOrInsertFunction("PROF_set_static_task_id", sampleCallTy);
      profNumTasks = 0; // reserve 0 id for beginning of program. Inc this before use.
      if(!setTaskIdFn) {
        DEBUG(dbgs() << "SccTaskCreateCallVisitor:: CAN'T FIND FUNCTION PROF_set_static_task_id\n");
      }

      taskCreateNames.push_back("scc_thread_create");
      taskCreateNames.push_back("scc_thread_mutex_lock");
      taskCreateNames.push_back("scc_thread_rwlock_wrlock");
      taskCreateNames.push_back("scc_thread_rwlock_rdlock"); 
      taskCreateNames.push_back("scc_thread_cond_timedwait");
      taskCreateNames.push_back("scc_thread_cond_wait");
      taskCreateNames.push_back("scc_thread_join");
      taskCreateNames.push_back("scc_thread_barrier_wait");

      // these too
      taskCreateNames.push_back("scc_split_task");
      taskCreateNames.push_back("scc_sync_safe");
    }

    void visitCallInst(CallInst &call) {
      Function *f = call.getCalledFunction();
      if (f == 0) {
        DEBUG(dbgs() << "SccTaskCreateCallVisitor::visitCallInst saw indirect call: "
                     << call << " -- make sure this is not a task create call.\n");
        return;
      }
      if (isBlocking(f)) {
        DEBUG(dbgs() << "Got a blocking call " << *f << "\n");
        std::string fname = f->getName();
        std::cerr << "Got a blocking call " << fname << "\n";
        // CallInst::Create(preblockFn)->insertBefore(&call);
        profNumTasks++;
        ConstantInt *constTaskId = ConstantInt::get(int32Ty, profNumTasks);
        CallInst::Create(setTaskIdFn, ArrayRef<Value*>(constTaskId))->insertAfter(&call);
      }
    }
  };


} // anonymous namespace

namespace llvm {
bool SccProfile::runOnModule(Module &M) 
{
  /* if(!(doSccProfile || useSccProfile))
    return true; */

  // Initialize some globals. Should really factor into class stuff.
  pM = &M;
  initializeTyConstants(M);

  fprintf(stderr, "INITIALIZING SCC PROFILE PASS\n");

  // Do instrumentation
  insertModuleInit(M);

  // Intercept calls to pthread_create.
#if 0
  PthreadCreateVisitor PtCV;
  PtCV.visit(M);
#endif

  // Insert call to set static task id after each task create call.
  if(doSccProfile) {
    SccTaskCreateCallVisitor taskCreateCV;
    taskCreateCV.visit(M);
  }

  // Now gather all of our basic blocks
  std::vector<BasicBlock*> bbs;
  for (Module::iterator F = M.begin(), FE = M.end(); F != FE; ++F) {
    for (Function::iterator B = F->begin(), BE = F->end(); B != BE; ++B) {
      BasicBlock &pB = *B;
      bbs.push_back(&pB);
    }
  }

  // create a binary that will generate profile data. Now, always doing this
  insertBBMap(bbs);

  // Store how basic block IDs map to names
  if(doSccProfile) {

    // Insert sampling call into each basic block
    sampleCall = pM->getOrInsertFunction("PROF_sample", sampleCallTy);
    for (int i = 0, sz = bbs.size(); i < sz; ++i) {
      insertSampleCall(i, bbs[i]);
    }
  }
  else if(useSccProfile) {
    // consume previously generated dynamic profiling data for compiler optimizations.
    fprintf(stderr, "*** READING IN scc.prof TO USE IN BLACKLISTING.\n");
    if(readHistogram()) {
      // TBD: print histogram data
      // TBD: create interfaces to access such data in compilation
      if(_n_bbs != bbs.size() +1) // 1 added, since we add a dummy block at end when writing
        fprintf(stderr,"ERROR: Number of basic block read (%d) != number of basic blocks in current program (%lu)!\n",
              _n_bbs, bbs.size() +1);
      else
        fprintf(stderr, "Matched number of basic block read (%d).\n", _n_bbs);

      // Create map of basic block to id 
      _bbIds.clear();
      for(size_t i = 0, ie = bbs.size(); i < ie; i++) {
        _bbIds[bbs[i]] = i;
      }

      printHistogram();
    } // read histogram
    else 
        fprintf(stderr," COULDN'T READ PROFILE FILE scc.prof FOR USING IN COMPILATION!\n");
  } // if use profile data

  return true;

#if 0
  // Intercept blocking calls   ( TBD: Is this needed?)
  BlockingCallVisitor BCV;
  BCV.visit(M);
#endif

  return true;
}

// Get all the composite task ids that execute this bb. Only if we have loaded
// scc.prof
bool SccProfile::getTasks(BasicBlock *bb, std::vector<uint32_t>& taskIds)
{
  if(!useSccProfile || bb == NULL) 
    return false;

  uint32_t bbid = _bbIds[bb];
  if(bbid > _n_bbs) return false;

  for(uint32_t col = 0; col < _n_composite_tasks; col++)
    if(_bb_task_map[_n_bbs *col + bbid] > 0)
      taskIds.push_back(col);

  return true;
}

// Did these composite task ids overlap in execution?
float SccProfile::tasksOverlap(int t1, int t2)
{
   if(t1 >= (int) _n_composite_tasks || t2 >= (int) _n_composite_tasks)
    return false;

  float v1 =  _task_task_overlap[t1 * _n_composite_tasks + t2];
  float v2 =  _task_task_overlap[t2 * _n_composite_tasks + t1];
  float diff = (v1 > v2) ? v1 - v2: v2 -v1;
  if(diff > 0.05) { // accept floaint pt. inaccuracies, add epsilon in comparison
    fprintf(stderr,"Error: Task-task overlap asymmetric!!!\n");
  }

  return (v1 > v2 ? v1 : v2);
}

} // llvm

