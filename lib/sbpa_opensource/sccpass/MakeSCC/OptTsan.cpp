// (C) 2013 University of California, Santa Cruz.
// The software is subject to an academic license.
// Terms are contained in the LICENSE file.

#include <deque>
#include <queue>
#include <map>
#include <stack>
#include <string>

#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>

#include "llvm/IR/Constants.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/DerivedTypes.h" 
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/Pass.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SetOperations.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/CaptureTracking.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
// #include "llvm/Assembly/Writer.h"
#include "llvm/IR/CFG.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/ConstantRange.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/GetElementPtrTypeIterator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/InstIterator.h"
// #include "llvm/Support/Streams.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/DebugInfo.h"
#include "ThreadEscapeAnalysis.h"
#include "identifyTS.h"
#include "SccProfile.h"
#include "LoopHelper.h"

#undef DEBUG_TYPE 
#define DEBUG_TYPE "scc"

namespace llvm {
namespace {


cl::opt<bool> SccRemoveAll("scc-removeall", cl::init(false), cl::desc("SCC Remove all thread sanitizer instrumentations"));


// Mem-tracking stats.
STATISTIC(NumLoadsRemoved, "Loads Removed");
STATISTIC(NumStoresRemoved, "Stores Removed");
STATISTIC(NumLoadsKept, "Loads Kept");
STATISTIC(NumStoresKept, "Stores Kept");

// Escape node stats
STATISTIC(NumNonEscapeNode, "NumNonEscapeNode");
STATISTIC(NumEscapeNoNode, "NumEscapeNoNode");
STATISTIC(NumEscapeUnknown, "NumEscapeUnknown");
STATISTIC(NumEscapeIncomplete, "NumEscapeIncomplete");
STATISTIC(NumEscapeExternal, "NumEscapeExternal");
STATISTIC(NumEscapeKnown, "NumEscapeKnown");
STATISTIC(NumMTROM, "NumMTROM");
STATISTIC(NumSAO, "NumSAO");

cl::opt<bool> doBaseOpt("scc-do-base-opt", cl::init(false),
              cl::desc("Perform Base Optimizations ( SAO, ACO)"));
cl::opt<bool> doTsanStackOpt("scc-tsan-stack-opt", cl::init(false),
              cl::desc("Perform stack opt for thread sanitizer"));

double idAccessTimeInSec = 0.0; // bulk of time? below 4 are sub-components of this.
double escapeOptTimeInSec = 0.0;
double saoOptTimeInSec = 0.0;
double acoOptTimeInSec = 0.0;
double stackOptTimeInSec = 0.0;

struct AccessInfo;
struct AccessPairInfo;
typedef ilist<AccessInfo> AccessInfoList;
typedef SmallPtrSet<BasicBlock*, 8> BasicBlockSet;
typedef SmallPtrSet<Instruction*, 8> InstructionSet;


//------------------------------------------------------------------------------
// The Pass
//------------------------------------------------------------------------------

class OptTsan : public FunctionPass 
{
  LLVMContext *_context;
  // SCC runtime functions.
  Constant* SCCendtask;
  Constant* SCCprepareForExternalCall;
  Constant* SCCcheckStack;
  Constant* SCCprepareForIndirectCall;
  Constant* SCCmembarrier;
  Constant* SCCsetCodeLocation;
  // SCC runtime functions: libC
  Constant* SCCmemset;
  Constant* SCCmemcpy;
  Constant* SCCmemmove;
  Constant *SCCLogOnlyStore;

  // SCC runtime functions: SCC uses buffering for speculative writes
  Constant* SCCloadReplaceContained;
  Constant* SCCloadReplaceRange;
  Constant* SCCstoreReplaceContained;
  Constant* SCCstoreReplaceRange;
  Constant* SCCremoveReplaceRange;
  std::map<const Type*, Constant*> SCCloadReplaceContainedTyped;
  std::map<const Type*, Constant*> SCCloadReplaceRangeTyped;
  std::map<const Type*, Constant*> SCCstoreReplaceContainedTyped;
  std::map<const Type*, Constant*> SCCstoreLogTyped; // Used in LILM, where same value is written back, 
  std::map<const Type*, Constant*> SCCstoreForRestartOnly; // These stores shouldn't be used to check conflicts. 
                                                      // In systems like coredet, these won't need instrumentation
  std::map<const Type*, Constant*> SCCstoreReplaceRangeTyped;

  // for li log motion of array elems
  std::map<const Type*, Constant*> SCCloadArrayElemTyped;
  std::map<const Type*, Constant*> SCCstoreArrayElemTyped;

  // SCC load/store instrumentation functions when no buffering. In this case, these functions 
  // just instrument, and are inserted before actual load/store
  Constant* SCCloadContained;
  Constant* SCCloadRange;
  Constant* SCCstoreContained;
  Constant* SCCstoreRange;

  Constant *SCCRangeStoreInt64;
  Constant *SCCRangeLoadInt64;

  Constant *funcBeginMarker;

  // Some handy types.
  PointerType* VoidPtrTy;     // like void*
  PointerType* Int64PtrTy;     // like void*
  PointerType* VoidPtrPtrTy;  // like void**
  Type* SizeTy;               // like size_t

  Type* VoidTy;
  Type* FloatTy;
  Type* DoubleTy;
  IntegerType* Int8Ty;
  IntegerType* Int16Ty;
  IntegerType* Int32Ty;
  IntegerType* Int64Ty;

  // External functions which do not touch uninstrumented shared memory.
  std::set<std::string> safeExternalCalls;
  std::vector<std::string> safeExternalCallPrefixes;

  // replacement function calls, like strcmp, strcpy etc.
  std::map<std::string, Constant *> replacementLibFuncs; 

  // (Madan: feb 20, 2013): These are global functions that get called before main is called.
  // Instrumenting these can cuase issues in runtime.
  std::vector<std::string> dontInstrumentFuncs;

  // TODO: make these local to runFunction !

  // SCC stack scratch space.
  AllocaInst* SCCscratchspace;

  // Instructions replaced during memtracking.
  DenseMap<Value *, Value *> replacements;

  void recordReplacement(Instruction *origInst, Instruction *newInst)
  {
    ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
    EA.recordReplacement(origInst, newInst);
    replacements[origInst] = newInst;
  }

  typedef enum { TSAN_NONE = 0, TSAN_READ = 1, TSAN_WRITE = 2} tsan_rdwr_type;
  tsan_rdwr_type tsanReadWrite(Instruction *I);

public:
  OptTsan();
  virtual void getAnalysisUsage(AnalysisUsage &AU) const;
  virtual bool doInitialization(Module &M);
  virtual void init(Module &M);
  virtual bool runOnFunction(Function &F);

  bool LoopInvariantLSMotion(Function *F, const AccessInfo& A, const bool isContained);
  bool HoistLoggingToPreheader(Function *F, const AccessInfo &A, Loop *loop, const bool isContained);

  void findRedundantAccesses(Function &F, set<Instruction *>& redundant);
  Value* getAccessInfoAddr(const AccessInfo &info);

  bool accessIsEscaping(Value *value, Instruction *inst = NULL);
  bool accessIsStackOnly(Value *value, Instruction *inst);
  bool calledInMultiThreadedMode(Function *f); // is it called in multi-threaded mode?

  // If in same task, remove subsequent access if redundant
  bool isEndTask(BasicBlock* bb) { return false; }
  bool instrsInSameTask(Instruction* I1, Instruction* I2);
  bool existsPathInSameTask(Instruction* I1, Instruction* I2);

  void runSAO(Function &F, AccessInfoList &accesses, set<Instruction *>& redundant);
  AccessPairInfo analyzeAccessPair(AccessInfo &A, AccessInfo &B);
  bool accessIsRedundantWith(AccessPairInfo &info);

  // Function to print line information on load/store
  void printLineInfo(Instruction *I) {
    if (MDNode *N = I->getMetadata("dbg")) {  
      DILocation Loc(N);                      // DILocation is in DebugInfo.h
      unsigned Line = Loc.getLineNumber();
      StringRef File = Loc.getFilename();
      // StringRef Dir = Loc.getDirectory();

      DEBUG(dbgs() << I << " File " << File << ", Line " << Line << "\n");
    }
    else {
      DEBUG(dbgs() << I << " File: NA Line: NA\n");
    }
  }

  bool isValueAvailableAt(Value* v, Instruction *at);
  void makeValueAvailableAt(Value* v, Instruction *at);
  void computePREJoin(AccessPairInfo &info);

  // Pass identification (replacement for typeid).
  static char ID;
};

char OptTsan::ID = 0;

OptTsan::OptTsan() : FunctionPass(ID) {
  if (true /* doKnownFunctionOpt */) {

    // ALl functions for which we have replacement functions in scc library,
    // and we chose not to replace the function in call, are safe. Ensure that these are really
    // covered in replacementLibFuncs
    safeExternalCalls.insert("strtol");  // FIXME: Add library function for this.
    safeExternalCalls.insert("strlen");  // FIXME: Add library function for this.
    safeExternalCalls.insert("strcmp"); 
    safeExternalCalls.insert("strncmp"); 
    safeExternalCalls.insert("strcpy"); 
    safeExternalCalls.insert("strncpy"); 
    // Math functions (these are 'readnone').
    safeExternalCalls.insert("alloca"); // local stack memory allocation
    safeExternalCalls.insert("lrand48"); 
    safeExternalCalls.insert("rand"); 
    safeExternalCalls.insert("srand48");
    safeExternalCalls.insert("srand");
    safeExternalCalls.insert("drand48");
    safeExternalCalls.insert("exp");
    safeExternalCalls.insert("expf");
    safeExternalCalls.insert("exp2");
    safeExternalCalls.insert("exp2f");
    safeExternalCalls.insert("pow");
    safeExternalCalls.insert("log");
    safeExternalCalls.insert("powf");
    safeExternalCalls.insert("sqrt");
    safeExternalCalls.insert("sqrtf");
    safeExternalCalls.insert("sin");
    safeExternalCalls.insert("sinf");
    safeExternalCalls.insert("cos");
    safeExternalCalls.insert("cosf");
    safeExternalCalls.insert("tan");
    safeExternalCalls.insert("tanf");
    safeExternalCalls.insert("asin");
    safeExternalCalls.insert("asinf");
    safeExternalCalls.insert("acos");
    safeExternalCalls.insert("acosf");
    safeExternalCalls.insert("atan");
    safeExternalCalls.insert("atanf");
    safeExternalCalls.insert("floor");
    safeExternalCalls.insert("floorf");
    safeExternalCalls.insert("ceil");
    safeExternalCalls.insert("ceilf");
    safeExternalCalls.insert("modf");
    safeExternalCalls.insert("modff");
    safeExternalCalls.insert("abs");
    safeExternalCalls.insert("fabs");
    safeExternalCalls.insert("fabsf");
    safeExternalCalls.insert("frexp");

    // Optimization:
    //
    // 1-Functions like atoi can not be added because the input string is not
    // protected for read. We could scc_load the input string, and then call
    // without needing a sync_safe
    //
    // 2-malloc/free require a sync safe, but it may be good to have a per
    // thread prtivate allocator (or just compile the hoard with the scc pass)

#if 0
    // If you just want to have VERY few sync_safes with non-deterministic outout in screen :)
    safeExternalCalls.insert("fprintf");
    safeExternalCalls.insert("printf");
    safeExternalCalls.insert("puts");
    safeExternalCalls.insert("gettimeofday");
    safeExternalCalls.insert("putchar");
    safeExternalCalls.insert("getopt");
#endif

    safeExternalCallPrefixes.push_back("SCC"); // Must be not instrumented or everything is serialized to death
    safeExternalCallPrefixes.push_back("scc_"); // Must be not instrumented or everything is serialized to death
    safeExternalCallPrefixes.push_back("PROF_"); // profiling calls inserted by scc pass. 
    safeExternalCallPrefixes.push_back("pthread_"); // Must be not instrumented or everything is serialized to death

    safeExternalCallPrefixes.push_back("llvm.bswap.");
    safeExternalCallPrefixes.push_back("llvm.cos.");
    safeExternalCallPrefixes.push_back("llvm.sin.");
    safeExternalCallPrefixes.push_back("llvm.sqrt.");
    safeExternalCallPrefixes.push_back("llvm.log.");
    safeExternalCallPrefixes.push_back("llvm.exp.");
    safeExternalCallPrefixes.push_back("llvm.pow.");
    safeExternalCallPrefixes.push_back("llvm.powi.");
    safeExternalCallPrefixes.push_back("llvm.dbg.");
    safeExternalCallPrefixes.push_back("llvm.lifetime.");
    // Exception handling: these are pure functions.
    safeExternalCalls.insert("llvm.eh.exception");
    safeExternalCallPrefixes.push_back("llvm.eh.selector.");
    safeExternalCallPrefixes.push_back("llvm.eh.typeid.for.");
    // Allocator calls are safe when using a our customized libhoard.
    // We wrap these specially.
    safeExternalCallPrefixes.push_back("llvm.memset.");
    safeExternalCallPrefixes.push_back("llvm.memcpy.");
    if (true) {
      safeExternalCallPrefixes.push_back("llvm.atomic.");
      safeExternalCallPrefixes.push_back("llvm.memmove.");
    }
    // Nondeterministic output, and essentially readnone.
    safeExternalCalls.insert("time");
    // Almost surely safe, though not necessarily ...
    safeExternalCalls.insert("llvm.va_start");
    safeExternalCalls.insert("llvm.va_end");
    // Do not instrument sleed or usleep to allow debug (no side effect after all)
    safeExternalCalls.insert("sleep");
    safeExternalCalls.insert("usleep");
  }

  // don't instrument within these functions, which are sometimes called before main
  dontInstrumentFuncs.push_back("_GLOBAL__I_a");
}

void OptTsan::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<AliasAnalysis>();
  AU.addRequired<DataLayoutPass>();
  AU.addRequired<DominatorTreeWrapperPass>();
  AU.addRequired<ScalarEvolution>();

  // ScProfile needed for profiling. Seems there is some issue in PROF_INIT_module
  // scc_sync_safe added to this function. If you uncomment, make sure to change scc pass
  // to not insert any sync_safe in that function ( guess).
  // AU.addRequired<SccProfile>();

  // TODO: make sure we no longer need either of these
  //AU.addRequiredID(LoopSimplifyID);
  //AU.addPreservedID(LoopSimplifyID);
  AU.addRequired<LoopInfo>();

  //AU.addPreserved<DominatorTree>();
  //AU.addPreserved<LoopInfo>();

  //TODO: Check is this correct flag???
  // if(doEscapeOpt)  {
  AU.addRequired<ThreadEscapeAnalysis>();
  // }
}

bool OptTsan::doInitialization(Module &M) {
  /* if (doOptTsan == false)
    return false; */

  LLVMContext& context = M.getContext();
  _context = &context;

  // Some handy types.
  VoidTy   = Type::getVoidTy(context );
  FloatTy  = Type::getFloatTy(context);
  DoubleTy = Type::getDoubleTy(context);
  Int8Ty   = Type::getInt8Ty(M.getContext());
  Int16Ty  = Type::getInt16Ty(M.getContext());
  Int32Ty  = Type::getInt32Ty(M.getContext());
  Int64Ty  = Type::getInt64Ty(M.getContext());
  Int64PtrTy  = Type::getInt64PtrTy(M.getContext());

  VoidPtrTy    = Type::getInt32PtrTy(context);
  VoidPtrPtrTy = Type::getInt32PtrTy(context)->getPointerTo(0);

  // Enumerate all functions whose address is taken (other than in a direct call),
  // and place them in a NULL-terminated list.
  std::vector<Constant*> L;
  for (Module::iterator F = M.begin(); F != M.end(); ++F) {
    if (F->isDeclaration())
      continue;
    for (Function::use_iterator U = F->use_begin(); U != F->use_end(); ++U) {
      // Skip direct calls.
      if (CallInst* call = dyn_cast<CallInst>(*U)) {
        if (call->getCalledFunction() == F)
          continue;
      }
      L.push_back(ConstantExpr::getPointerCast(F, VoidPtrTy));
      break;
    }
  }

  // null-terminate
  L.push_back(ConstantPointerNull::get(VoidPtrTy));

  /* This is somehow confusing the thread start function in llvm-head in May 2014. Was working fine in llvm-3.3. 
  // construct the global
  ArrayType* FnArrayTy = ArrayType::get(VoidPtrTy, L.size());
  GlobalVariable* v;

  StringRef ref1("SCCinstrumentedFunctionsList");
  M.getOrInsertGlobal(ref1, FnArrayTy);
  v = M.getNamedGlobal("SCCinstrumentedFunctionsList");
  assert(v);
  v->setConstant(true);
  v->setInitializer(ConstantArray::get(FnArrayTy, L));
  */

  return true;
}

void OptTsan::init(Module &M) 
{
  const DataLayout &TD = (getAnalysis<DataLayoutPass>()).getDataLayout();

  // Some handy types.
  SizeTy = TD.getIntPtrType(M.getContext());

  const Function* ThreadCreate = M.getFunction("pthread_create");
  assert(ThreadCreate);

  Type* Int8Ptr = PointerType::get(Int8Ty, 0);
}

// Must alias with Value instead of AccessInfo
static bool valuesMustAliasForMtrom(const Value *v1, const Value *v2, Pass &pass)
{

  if(!(isa<PointerType>(v1->getType()) && isa<PointerType>(v1->getType())))
    return false;

  // They must both use a constant size, else alias analysis can't be applied.
  if(v1 == v2)   // no size check?
    return true;
  /* if (!A.isConstantSize() || !B.isConstantSize())
    return false; */

  AliasAnalysis& aa = pass.getAnalysis<AliasAnalysis>();
  return aa.alias(v1, 4, v2, 4) == AliasAnalysis::MustAlias;
}

//------------------------------------------------------------------------------
// Access classification
//------------------------------------------------------------------------------

// A single memory access.
struct AccessInfo : public ilist_node<AccessInfo> 
{
  enum AccessType { None, Load, Store, ExclusiveLoad };

  AccessInfo()
    : type(None), original(NULL), inst(NULL), addr(NULL), size(NULL), alignment(1)
  { 
    stModeOnly = false;
    mutexProtected = false;
    escaping = true;
  }
  AccessInfo(AccessType t, Instruction* i)
    : type(t), original(i), inst(i), addr(NULL), size(NULL), alignment(1)
  {   
    stModeOnly = false;  
    mutexProtected = false;
    escaping = true; 
    tsanInstr = NULL;
  }
  AccessInfo(const AccessInfo &a)
    : ilist_node<AccessInfo>(),
      type(a.type), original(a.original), inst(a.inst), addr(a.addr), size(a.size),
      alignment(a.alignment)
  {
    stModeOnly = a.stModeOnly;
    mutexProtected = a.mutexProtected;
    escaping = a.escaping;
    tsanInstr = a.tsanInstr;
  }

  // Attempts to classify an instruction as a memory access instruction.
  // An instruction can have at most two memory accesses (e.g., memcpy).
  // Returns the number of memory accesses found (some instructions, e.g.
  // the intrinstic memcpy, can have two accesses).
  static int classify(const DataLayout& TD, LLVMContext& context, Instruction* inst, AccessInfo a[2]);
  static bool isAtomicOp(Instruction* inst);

  void setSizeFromAddr(const DataLayout& TD, LLVMContext& context);
  void setAlignmentFromAddr(const DataLayout& TD);
  bool isLoad() const { return type == Load || type == ExclusiveLoad; }
  bool isStore() const { return type == Store; }
  bool isConstantSize() const { return dyn_cast<ConstantInt>(size) != NULL; }
  uint64_t constantSize() const { return cast<ConstantInt>(size)->getZExtValue(); }

  AccessType type;
  // Instruction containing the original access (debugging only!)
  Instruction* original;
  // The access should be instrumented immediately before 'inst'.
  // Note that 'inst' need not access 'addr'.  For example, ACO will
  // access in a different basic block than the actual access.
  Instruction* inst;
  // The base address and size of the access.
  Value* addr;
  Value* size;
  // Alignment of this access
  unsigned alignment;
  Instruction *tsanInstr;

  bool stModeOnly; // Only executed in single threaded mode. But the memory it accesses may be escaping to other threads
  bool mutexProtected; // always accessed in every phase with same lock
  bool escaping; // in systems with restart, this needs to be instrumented only for restarts, not conflicts
};

int AccessInfo::classify(const DataLayout& TD, LLVMContext& context, Instruction* inst, AccessInfo all[2]) 
{
  AccessInfo& a = all[0];
  AccessInfo& b = all[1];
  a = AccessInfo();
  b = AccessInfo();

  // Cache for keeping statistics.
  static std::set<Instruction*> seen;

  // Load?
  if (LoadInst* load = dyn_cast<LoadInst>(inst)) {
    a = AccessInfo(Load, inst);
    a.addr = load->getPointerOperand();
    a.setSizeFromAddr(TD, context);
    a.setAlignmentFromAddr(TD);
    // fprintf(stderr, "Load : a.alignment %d load->alignment %d\n", a.alignment, load->getAlignment());
    // if (load->getAlignment() != 0) 
    if (load->getAlignment() != 0 && load->getAlignment() >= a.alignment) // Madan: 2011/12/9 : added 2nd cond 
      a.alignment = load->getAlignment();
    return 1;
  }

  // Store?
  if (StoreInst* store = dyn_cast<StoreInst>(inst)) {
    a = AccessInfo(Store, inst);
    a.addr = store->getPointerOperand();
    a.setSizeFromAddr(TD, context);
    a.setAlignmentFromAddr(TD);
    // fprintf(stderr, "Store : a.alignment %d store->alignment %d\n", a.alignment, store->getAlignment());
    // if (store->getAlignment() != 0)
    if (store->getAlignment() != 0 && store->getAlignment() >= a.alignment) // Madan: 2011/12/9 : added 2nd cond
      a.alignment = store->getAlignment();
    return 1;
  }

  return 0;
}

bool AccessInfo::isAtomicOp(Instruction* inst) 
{
  if (LoadInst *LI = dyn_cast<LoadInst>(inst))
    return LI->isAtomic();
  else if(StoreInst *SI = dyn_cast<StoreInst>(inst)) 
    return SI->isAtomic();
  return false;
}

void AccessInfo::setSizeFromAddr(const DataLayout& TD, LLVMContext& context) 
{
  assert(addr);
  Type* Ty = cast<PointerType>(addr->getType())->getElementType();
  if (Ty->isSized()) {
    size = ConstantInt::get(Type::getInt64Ty(context), TD.getTypeStoreSize(Ty));
    // size = ConstantInt::get(Type::getInt64Ty(getGlobalContext()), TD.getTypeStoreSize(Ty));
  }
}

void AccessInfo::setAlignmentFromAddr(const DataLayout& TD) 
{
  assert(addr);
  Type* Ty = cast<PointerType>(addr->getType())->getElementType();
  alignment = (unsigned)TD.getPrefTypeAlignment(Ty);
}

// A pair of memory accesses.
struct AccessPairInfo 
{
  AccessPairInfo(AccessInfo &a, AccessInfo &b) : A(a), B(b) {}

  // A pair of accesses: 'A' may dominate 'B', but 'B' doesn't dominate 'A'.
  AccessInfo &A;
  AccessInfo &B;

  // Basic analysis
  bool dominates;     // true iff 'A' dominates 'B'
  bool sameTask;   // true iff 'A' and 'B' are in the same task
  bool mustAlias;     // true iff 'A' and 'B' must alias

  // PRE analysis
  Instruction* join;  // this dominates 'A' and 'B' in the same task
};

// Collect all the tsan_read* and tsan_write* calls. 
// Then, find the redundant ones based on SAO and insert in redundant set
void OptTsan::findRedundantAccesses(Function &F, set<Instruction *>& redundant) 
{
  AccessInfoList accesses;
  accesses.clear();

  time_t t1 = time(NULL);

  DominatorTree& DT = getAnalysis<DominatorTreeWrapperPass>().getDomTree();
  // AliasAnalysis &AA = getAnalysis<AliasAnalysis>();
  const DataLayout &TD = (getAnalysis<DataLayoutPass>()).getDataLayout();

  // Do a simple pass to gather all accesses.  Make this a depth-first
  // traversal over the dominator tree so that each instruction on the
  // access list appears before all instructions it dominates.
  std::vector<DomTreeNode*> stack;
  stack.push_back(DT.getRootNode());

  while (!stack.empty()) {
    DomTreeNode* node = stack.back();
    stack.pop_back();

    // Check for Loads and Stores and do some trivial optimizations.
    BasicBlock* bb = node->getBlock();

    // first, get all instruction in an array
    SmallVector<Instruction *, 20> bbInstructions;
    for(BasicBlock::iterator inst = bb->begin(); inst != bb->end(); inst++) {
      Instruction *I = inst;
      bbInstructions.push_back(I);
    }

    for(int i = 0, ie = bbInstructions.size(); i < ie; i++) {
      Instruction *I = bbInstructions[i];
      tsan_rdwr_type typ =tsanReadWrite(I);
      if(typ == TSAN_NONE) continue;

      // get next instruction. If it is load/store, it could be tsan_write etc.
      Instruction *nextInstr = (i+1 < ie) ? bbInstructions[i+1] : NULL;
      if(nextInstr && (isa<StoreInst>(nextInstr) || isa<LoadInst>(nextInstr))) {
        AccessInfo infos[2];
        for (int j = 0, n = AccessInfo::classify(TD, *_context, nextInstr, infos); j < n; ++j) {
          AccessInfo &A = infos[j];
          A.tsanInstr = I;
          assert(A.inst);
          assert(A.addr);
          assert(A.type != AccessInfo::None);

          const GlobalVariable *GV = dyn_cast<GlobalVariable>(GetUnderlyingObject(A.addr, &TD, 0));
          // const GlobalVariable *GV = dyn_cast<GlobalVariable>((A.addr));
          if(GV && GV->isConstant()) {
            continue;
          }

          accesses.push_back(new AccessInfo(A));
        } // for each of A
      } // if next instruction is a load/store
    } // for each instruction in bb

    // Recurse.
    for (DomTreeNode::iterator iter = node->begin(); iter != node->end(); ++iter)
      stack.push_back(*iter);
  }

  // runEscapeOpt(F, accesses);
  runSAO(F, accesses, redundant);
}


// Are these two instructions in same task ( without scc_endtask?)
bool OptTsan::instrsInSameTask(Instruction* I1, Instruction* I2)
{
  // We assume that I1 dominates I2 and that tasks have been
  // properly formed (so EndTask will always start the block).
  // This requires that ALL paths I1->I2 are in the same task.
  if (I1->getParent() == I2->getParent()) return true;
  if (isEndTask(I2->getParent())) return false;

  std::vector<BasicBlock*> Stack;
  BasicBlockSet Visited;
  BasicBlock* BB = I2->getParent();

  for (pred_iterator PI = pred_begin(BB), PE = pred_end(BB); PI != PE; ++PI)
    Stack.push_back(*PI);
  Visited.insert(BB);

  while (!Stack.empty()) {
    BB = Stack.back();
    Stack.pop_back();

    if (BB == I1->getParent()) continue;
    else if (Visited.count(BB)) continue;
    else Visited.insert(BB);

    if (isEndTask(BB))
      return false;

    // We should never get to the start of the function since all
    // backwards paths from I2 eventually end at I1.
    assert(pred_begin(BB) != pred_end(BB));

    for (pred_iterator PI = pred_begin(BB), PE = pred_end(BB);
         PI != PE; ++PI)
      Stack.push_back(*PI);
  }

  return true;
}

bool OptTsan::existsPathInSameTask(Instruction* I1, Instruction* I2)
{
  // Returns true iff there exists a path from I1->I2 which does not
  // cross a task boundary.  Assumes tasks have been properly formed.
  if (I1->getParent() == I2->getParent()) return true;
  if (isEndTask(I2->getParent())) return false;

  std::vector<BasicBlock*> Stack;
  BasicBlockSet Visited;
  BasicBlock* BB = I2->getParent();

  for (pred_iterator PI = pred_begin(BB), PE = pred_end(BB); PI != PE; ++PI)
    Stack.push_back(*PI);
  Visited.insert(BB);

  while (!Stack.empty()) {
    BB = Stack.back();
    Stack.pop_back();

    // Found a backwards path from I2 -> I1.
    if (BB == I1->getParent())
      return true;

    if (Visited.count(BB)) continue;
    else Visited.insert(BB);

    // EndTask point?
    if (isEndTask(BB) || pred_begin(BB) == pred_end(BB))
      continue;

    for (pred_iterator PI = pred_begin(BB), PE = pred_end(BB);
         PI != PE; ++PI)
      Stack.push_back(*PI);
  }

  return false;
}


// 
void OptTsan::computePREJoin(AccessPairInfo &info)
{
  DominatorTree& dt =  getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  AccessInfo &A = info.A;
  AccessInfo &B = info.B;

  // Don't bother with this if 'A' dominates 'B'.
  if (info.dominates)
    return;

  // Make sure there is a path from 'A->B' or 'B->A'.
  // Otherwise, PRE is pointless.
  if (!existsPathInSameTask(A.inst, B.inst) &&
      !existsPathInSameTask(B.inst, A.inst))
    return;

  // Find the first basic block which dominates both 'A' and 'B'.
  // This might not exist if, for example, 'A' or 'B' is dead.
  BasicBlock* common = dt.findNearestCommonDominator(A.inst->getParent(),
                                                     B.inst->getParent());
  if (!common) return;
  assert(common != A.inst->getParent());  // already checked this ('info.dominates')
  assert(common != B.inst->getParent());  // 'B' never dominates 'A'

  // We'll join at the end of the common dominator.
  Instruction* join = &(common->back());

  // It must be in the same task as 'A' and 'B'.
  info.sameTask = instrsInSameTask(join, A.inst) &&
                     instrsInSameTask(join, B.inst);
  if (!info.sameTask)
    return;

  // Make sure the 'addr' and 'size' of each access is available at 'join'.
  if (!isValueAvailableAt(A.addr, join) || !isValueAvailableAt(A.size, join) ||
      !isValueAvailableAt(B.addr, join) || !isValueAvailableAt(B.size, join))
    return;

  info.join = join;
}

static bool GepHasVariableIndexAfter(GetElementPtrInst* GEP, unsigned i)
{
  // Returns true if 'GEP' has any variable indices after or including 'i'.
  for (; i < GEP->getNumOperands(); ++i) {
    if (!isa<ConstantInt>(GEP->getOperand(i)))
      return true;
  }
  return false;
}


// PRE (Partial Redundancy Elimination)
bool OptTsan::isValueAvailableAt(Value* v, Instruction *at)
{
  DominatorTree& dt =  getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  // Constants and Arguments are always available.
  if (isa<Constant>(v) || isa<Argument>(v))
    return true;

  // Instructions must dominate 'at' or be copyable to 'at'.
  // TODO: lots of ways this can be improved
  if (Instruction* inst = dyn_cast<Instruction>(v)) {
    if (dt.dominates(inst, at))
      return true;
    // GEPs are copyable if the the base ptr is available and indices are const.
    if (GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(inst)) {
      if (!isValueAvailableAt(gep->getPointerOperand(), at))
        return false;
      if (GepHasVariableIndexAfter(gep, 0))
        return false;
      return true;
    }
  }

  return false;
}


void OptTsan::makeValueAvailableAt(Value* v, Instruction *at)
{
  DominatorTree& dt = getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  // Assumes 'isValueAvailableAt(v, at)'.
  // In most cases, we don't need to do anything.
  // In some cases, we need to move the instruction to before 'at'.
  if (GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(v)) {
    if (!dt.dominates(gep, at))
      // TODO: clone instead? (would need to update 'v')
      //  gep->clone()->insertBefore(at);
      gep->moveBefore(at);
  }
}

// SAO (Subsequent Access Optimization)
bool OptTsan::accessIsRedundantWith(AccessPairInfo &info)
{
  AccessInfo &A = info.A;
  AccessInfo &B = info.B;

  // Returns true if 'B' should be optimized due to the presence of 'A'.
  // Assumes that 'A' may dominate 'B' but 'B' doesn't dominate 'A'.

  // Check the conditions for SAO.
  if (!info.sameTask || !info.mustAlias || !info.dominates)
    return false;

  // The accesses are redundant, so 'B' can be removed.
  // Upgrade 'A' to an exclusive load if necessary.
  if (A.isLoad() && B.type != AccessInfo::Load) {
    A.type = AccessInfo::ExclusiveLoad;
  }

  // If PRE was applied, make sure 'A' is available at the join point.
  if (info.join) {
    makeValueAvailableAt(A.addr, info.join);
    makeValueAvailableAt(A.size, info.join);
    A.inst = info.join;
  }

  if (info.join) {
    DEBUG(dbgs() << "*** SAO+PRE ***********************************************\n");
  } else {
    DEBUG(dbgs() << "*** SAO ***************************************************\n");
  }
  DEBUG(dbgs() << (*A.original));
  DEBUG(dbgs() << (*B.original));

  return true;
}



// Do successive access optimization. Put the redundantone in redundant for later use
void OptTsan::runSAO(Function &F, AccessInfoList &accesses, set<Instruction *>& redundant)
{
  // fprintf(stderr,"Running SAO with %d accesses..", accesses.size());
  DEBUG(dbgs() << "[SCC] ... runSAO ...\n");

  // Iterate over pairs of accesses.
  // Note that that dominators appear before all their dominees.
  set<AccessInfo *> reduudantInfos;
  for (AccessInfoList::iterator A = accesses.begin(); A != accesses.end(); ++A) {
    if(reduudantInfos.count(A)) continue; // speed-up
    AccessInfoList::iterator B = A;
    ++B;

    while (B != accesses.end()) {
      if(reduudantInfos.count(B)) {
        ++B;
        continue; // speed-up
      }
      AccessPairInfo info = analyzeAccessPair(*A, *B);
      if (accessIsRedundantWith(info)) {
        NumSAO++;
        // fprintf(stderr,"Found SAO redundant access\n");
        redundant.insert((*B).tsanInstr); // mark B as redundant
        reduudantInfos.insert(B);
      }
      ++B;

      // Remove this 'B' and keep going.
      /* if (info.join) {
        NumPRESAO++;
        if (B->isLoad()) NumPRESAOLoads++; else NumPRESAOStores++;
        if (A->isLoad()  && B->isLoad())  NumPRESAOLoadLoads++;
        if (A->isLoad()  && B->isStore()) NumPRESAOStoreLoads++;
        if (A->isStore() && B->isLoad())  NumPRESAOStoreLoads++;
        if (A->isStore() && B->isStore()) NumPRESAOStoreStores++;
      } else {
        NumSAO++;
        if (B->isLoad()) NumSAOLoads++; else NumSAOStores++;
        if (A->isLoad()  && B->isLoad())  NumSAOLoadLoads++;
        if (A->isLoad()  && B->isStore()) NumSAOLoadStores++;
        if (A->isStore() && B->isLoad())  NumSAOStoreLoads++;
        if (A->isStore() && B->isStore()) NumSAOStoreStores++;
      } */
    }
  }
  // fprintf(stderr," Done\n");
}


//------------------------------------------------------------------------------
// Optimizations
//------------------------------------------------------------------------------

static AccessInfoList::iterator eraseAccessAndAdvance(AccessInfoList& accesses,
                                                      AccessInfoList::iterator A) 
{
  AccessInfoList::iterator next = AccessInfoList::getNext(A);
  accesses.erase(A);
  return next;
}

//
// Basic analysis about a pair of accesses.
//

static bool accessesMustAlias(AccessInfo &A, AccessInfo &B, Pass &pass) 
{
  // Returns true iff 'A' must alias 'B'.
  AliasAnalysis& aa = pass.getAnalysis<AliasAnalysis>();

  // They must both use a constant size, else alias analysis can't be applied.
  if (A.addr == B.addr && A.size == B.size)
    return true;
  if (!A.isConstantSize() || !B.isConstantSize())
    return false;
  return aa.alias(A.addr, A.constantSize(), B.addr, B.constantSize())
         == AliasAnalysis::MustAlias;
}


AccessPairInfo OptTsan::analyzeAccessPair(AccessInfo &A, AccessInfo &B) 
{
  // Assumes that 'A' may dominate 'B' but 'B' doesn't dominate 'A'.
  const DominatorTree& dt = getAnalysis<DominatorTreeWrapperPass>().getDomTree();

  AccessPairInfo info(A,B);
  info.dominates = dt.dominates(A.inst, B.inst);
  info.sameTask = true; 
  info.mustAlias = accessesMustAlias(A, B, *this);
  info.join = NULL;
  computePREJoin(info);

  return info;
}


// Return : 1 if read, 2 if write, 0 otherwise
OptTsan::tsan_rdwr_type OptTsan::tsanReadWrite(Instruction *I)
{
  CallInst *callInst = dyn_cast<CallInst>(I);
  if(!callInst) return TSAN_NONE;

  CallSite cs(I);
  Function *cf = cs.getCalledFunction();
  if(!cf) return TSAN_NONE
;
  std::string fname = cf->getName();
  if(strncmp(fname.c_str(), "__tsan_read", 11) == 0)
    return TSAN_READ;
  else if(strncmp(fname.c_str(), "__tsan_write", 12) == 0)
    return TSAN_WRITE;
  else
    return TSAN_NONE;
}



bool OptTsan::runOnFunction(Function &F) 
{
  bool rc = false;
  /* if (doOptTsan == false) {
    return false;
  } */

  std::string fname = F.getName();
  // std::cerr << "Processing function " << fname << "\n";

  Module *M = F.getParent();
  init(*M);

  bool calledInMultiThreaded = calledInMultiThreadedMode(&F);

  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();

  // Find mtrom values if used
  Function *markMtrom = M->getFunction("scc_assure_mtrom");
  std::set<const Value *> mtromValues;
  if(calledInMultiThreaded) {
   for(Function::iterator bb = F.begin(); bb != F.end(); bb++) {
    for (BasicBlock::iterator inst = bb->begin(); inst != bb->end(); ++inst) {
      if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
        const CallSite cs(inst);
        if (markMtrom && cs.getCalledFunction() == markMtrom) {
          const CallInst* CI = dyn_cast<CallInst>(inst);
          const Value *value = CI->getOperand(0);
          if(value) {
            mtromValues.insert(value);
            value = EA.getRealUnderlyingObject(value);
            if(value) mtromValues.insert(value);
          }
        }
      } // if callInst
    } // for each instruction in BB
   } // for each basic block
 } // check this only if called in MT mode

  // Do SAO if base-opt is chosen
  set<Instruction *> redundant;
  if(doBaseOpt) {
    findRedundantAccesses(F, redundant);
  }

  // Make a pass to remove unnecessary tsan* calls
  // int storesRemoved = 0, loadsRemoved = 0;
  for(Function::iterator bb = F.begin(), E = F.end(); bb != E; ++bb) {

    // first, get all instruction in an array
    SmallVector<Instruction *, 20> bbInstructions;
    for(BasicBlock::iterator inst = bb->begin(); inst != bb->end(); inst++) {
      Instruction *I = inst;
      bbInstructions.push_back(I);
    }

    SmallVector<Instruction *,8> instrsToRemove;

    for(int i = 0, ie = bbInstructions.size(); i < ie; i++) {
      Instruction *I = bbInstructions[i];
      tsan_rdwr_type typ =tsanReadWrite(I);
      if(typ == TSAN_NONE) continue;

      // get next instruction. If it is load/store, it could be tsan_write etc.
      Instruction *nextInstr = (i+1 < ie) ? bbInstructions[i+1] : NULL;
      if(nextInstr && (isa<StoreInst>(nextInstr) || isa<LoadInst>(nextInstr))) {
        Value *v = NULL;
        bool isLoad = true;

        if(StoreInst *store = dyn_cast<StoreInst>(nextInstr)) {
          v = store->getPointerOperand();
          isLoad = false;
        }
        else if(LoadInst *load = dyn_cast<LoadInst>(nextInstr)) {
          v = load->getPointerOperand();
        }

        /* else if(BitCastInst *bitcast = dyn_cast<BitCastInst>(inst)) {
          v = bitcast->getOperand(0);
        }
        else if(GetElementPtrInst* GepA = dyn_cast<GetElementPtrInst>(inst)) {
          v = GepA->getPointerOperand();
        } */

        bool removeInstrumentation = false;

        if(SccRemoveAll) {
          removeInstrumentation = true;
        }
        else if(redundant.count(I) > 0) {
          removeInstrumentation = true;
        }
        else if(v && !(calledInMultiThreaded && accessIsEscaping(v,nextInstr)))  {
          removeInstrumentation = true;
        }

        // see if constant global
        if(!removeInstrumentation && v) {
          const Value *underlyingValue = EA.getRealUnderlyingObject(v);
          const GlobalVariable *GV = dyn_cast<GlobalVariable>(underlyingValue);
          if(GV && GV->isConstant()) {
            removeInstrumentation = true;
          }
        }

        if(v && doTsanStackOpt && !removeInstrumentation) {
          removeInstrumentation =  accessIsStackOnly(v, nextInstr);
        }

        // check if mtrom value
        if(!removeInstrumentation && v) {
          bool mtRomPtr = (mtromValues.count(v) > 0);
          const Value *underlyingValue = EA.getRealUnderlyingObject(v);
          if(!mtRomPtr && underlyingValue && mtromValues.count(underlyingValue) > 0)
            mtRomPtr = true;

          // if still not mtrom ptr, check must alias
          for(std::set<const Value *>::const_iterator itr = mtromValues.begin(); !mtRomPtr && itr != mtromValues.end(); itr++) {
            Value *mtromAddr = const_cast<Value *>(*itr);
            if(valuesMustAliasForMtrom(v, mtromAddr, *this) || valuesMustAliasForMtrom(underlyingValue, mtromAddr, *this))
                mtRomPtr = true;
          }

          if(mtRomPtr) {
            NumMTROM++;
            removeInstrumentation = true;
          }
        }

        if(removeInstrumentation) {
          instrsToRemove.push_back(I); // remoev the tsan_read/wrte call
          isLoad ? NumLoadsRemoved++ : NumStoresRemoved++;

          // also remove tge previous instruction if it was a bitcast instruction
          if(i > 0) {
            Instruction *prev = bbInstructions[i-1];
            BitCastInst *bitcast = dyn_cast<BitCastInst>(prev);
            if(bitcast && !prev->isUsedOutsideOfBlock(bb)) {
              instrsToRemove.push_back(prev);
            }
          }
        }
        else {
          isLoad ?  NumLoadsKept++ : NumStoresKept++;
        }
      }
    }

    // Now remove the calls. TBD: also rmeove the bitcast before that
    for(size_t i = 0; i < instrsToRemove.size(); i++) {
      Instruction *instr = instrsToRemove[i];
      BitCastInst *bitcast = dyn_cast<BitCastInst>(instr);
      if(!bitcast || instr->use_empty()) {
        instr->eraseFromParent();
        rc = true;
      }
      /* else {
        Value *v = dyn_cast<Value>(instr);
        if(v && v->use_empty()) {
          instr->eraseFromParent();
          rc = true;
        }
      }  */
    }
  } // for each BB


  return rc;
}


// Is the DSNode ( points-to set) of the Value accessible from another thread?
// Also consider phase of original instruction.
bool OptTsan::accessIsEscaping(Value *value, Instruction *inst) 
{
  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  return (EA.valueMayEscape(value, inst) != SCC_NON_ESCAPING_NODE);
}

// Is this access only accessing stack location? false return value is conservative.
bool OptTsan::accessIsStackOnly(Value *value, Instruction *inst) 
{
  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  return EA.valueIsStackOnly(value, inst);
}

// Is this function called in multi-threaded mode
bool OptTsan::calledInMultiThreadedMode(Function *func)
{
  ThreadEscapeAnalysis& EA = getAnalysis<ThreadEscapeAnalysis>();
  return EA.calledInMultiThreadedMode(func);
}


int64_t GetOffsetFromIndex(GetElementPtrInst* GEP, unsigned Start,
                            const DataLayout &TD) 
{
  // Skip over the first indices.
  gep_type_iterator GTI = gep_type_begin(GEP);
  for (unsigned i = 1; i < Start; ++i, ++GTI)
    /*skip along*/;
  
  // Compute the offset implied by the rest of the indices.
  // Technically, this could be negative (think array indices in C).
  int64_t Offset = 0;
  for (unsigned i = Start; i < GEP->getNumOperands(); ++i, ++GTI) {
    ConstantInt *C = dyn_cast<ConstantInt>(GEP->getOperand(i));
    assert(C && "assumed constant GEP indices!");

    if (C->isZero())
      continue;

    // Structs: add the field offset.
    if (StructType *Ty = dyn_cast<StructType>(*GTI)) {
      Offset += TD.getStructLayout(Ty)->getElementOffset(C->getZExtValue());

    // Arrays: multiply by the element size.
    } else {
      uint64_t Size = TD.getTypeAllocSize(GTI.getIndexedType());
      Offset += Size * C->getSExtValue();
    }
  }

  return Offset;
}

}  // namespace

//------------------------------------------------------------------------------
// Pass Registration
//------------------------------------------------------------------------------

static RegisterPass<OptTsan> X("opttsan", "Optimize tsan calls");

FunctionPass *createOptTsanPass() 
{ 
    return new OptTsan(); 
}

}  // namespace llvm
