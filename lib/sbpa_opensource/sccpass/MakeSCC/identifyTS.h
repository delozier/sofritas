// Helper graphs to identify threaded sections in program
// released under the University of Santa Cruz Open Source License

#ifndef _SCC_IDENTIFY_TS_
#define _SCC_IDENTIFY_TS_

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/DenseSet.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Support/Debug.h"
#include <map>
#include <set>
#include <algorithm>

using std::vector;
using std::map;
using std::set;

namespace llvm {

// class LoopInfo;
class Loop;
class DominatorTree;
struct PostDominatorTree;
class DominanceFrontier;
}

using namespace llvm;
using namespace std;


// A graph node for a custom CFG after deletion of certain edges,
// and possibly splitting of some nodes
class SplicedCFGNode
{
   const BasicBlock *_bb; // originally derived from this BB

   short _startInstrPos;  // only as instrs if part of original BB
   short _endInstrPos;

   vector<SplicedCFGNode *> _outEdges;
   vector<SplicedCFGNode *> _inEdges;

   // Dominators and Post-dominators. These are initially empty

   unsigned _id:29; // position in nods array
   unsigned _color:2; // used during dominator and post-dom creation
   bool     _exitBlk:1;

  public:
    SplicedCFGNode *iDom; 
    SplicedCFGNode *iPostDom; 

    typedef enum {WHITE = 0, GRAY = 1, BLACK = 2} COLOR;
    SplicedCFGNode(const BasicBlock *b) { 
      _bb = b; 

      iDom = NULL;
      iPostDom = NULL;

      // Start and end positions -1 means no 
      // _startInstrPos = -1; _endInstrPos = SHORT_MAX; 
      _id = 0; 
      _color = WHITE;
      _exitBlk = false;
    }

    const BasicBlock *getBlock() { return _bb; }

    vector<SplicedCFGNode *>& getOutEdges() { return _outEdges; }
    void addOutEdge(SplicedCFGNode *dest) {
      for(size_t i = 0, ie = _outEdges.size(); i < ie; i++) {
        if(_outEdges[i] == dest)
          return;
      }
      _outEdges.push_back(dest);
    }

    bool operator()(const SplicedCFGNode *i1, const SplicedCFGNode *i2) const {
      return (i1 < i2); // TBD: pointer comnparison, bad for repeatability
    }

    vector<SplicedCFGNode *>& getInEdges() { return _inEdges; }
    void addInEdge(SplicedCFGNode *src) {
      for(size_t i = 0, ie = _inEdges.size(); i < ie; i++) {
        if(_inEdges[i] == src)
          return;
      }
      _inEdges.push_back(src);
    }

    void setId(unsigned id) { _id = id; }
    unsigned getId()  const { return _id; } 
    void setColor(COLOR c) { _color = c; }
    COLOR getColor() const { return ((COLOR) _color) ; }

    // has scc_sxit, and no out edges. If any other node only points to it,
    // recustively mark those as exit blocks also
    bool checkAndMarkExitBlock(); 
    bool isExitBlk() const { return _exitBlk; }

    void printName(); 
};

// Custom CFG graph for a function. Operations we need:
// 1) Splitnode at a point
// 2. Dominates/post dominates answers
// 3. Remove special edges
class SplicedCFG
{
  Function *_func;
  vector<SplicedCFGNode *> _nodes; 
  SplicedCFGNode *_sentinnelSink;   // when there are multiple terminals in graph, add a dummy sink to simplify algo

  map<const BasicBlock *, SplicedCFGNode *>  _nodeForBB;

  // Only for those which are split in this graph
  map<const BasicBlock *, vector<SplicedCFGNode *> >  _splitNodesForBB; 

    // private functions used during dominator tree and post-dominator tree construction
    SplicedCFGNode *intersectDom(SplicedCFGNode *n1, SplicedCFGNode *n2, unsigned *nodeOrder);
    SplicedCFGNode *intersectPDom(SplicedCFGNode *n1, SplicedCFGNode *n2, unsigned *nodeOrder);

    // do dfs startign from here
    void computePostOrder(SplicedCFGNode *node, bool postdom, vector<SplicedCFGNode *>& orderedNodes);

  public:
    SplicedCFG(Function *func); // constructs the spliced CFG
    ~SplicedCFG(); // deletes all nodes within it

    Function *getFunction() { return _func; }
    int getNodeFor(const BasicBlock *bb, vector<SplicedCFGNode *>& nodes); // can have multiple if split BB
    SplicedCFGNode *getNodeFor(const BasicBlock *bb) { return _nodeForBB[bb]; }
    bool isSplitNode(const BasicBlock *bb);

    vector<SplicedCFGNode *>& getNodes() { return _nodes; }

    bool buildDominatorTree();
    bool buildPostDominatorTree();
    void RemoveExitBranches();

    // Useful for debugging issues
    void printDominators();
    void printPostDominators();

    bool dominates(const Instruction *i1, const Instruction *i2); // i1 always appears befre i2
    bool postDominates(const Instruction *i1, const Instruction *i2); // i1 alwats appears after i2

    const Instruction *findNearestCommonDominator(const Instruction *i1, const Instruction *i2);
    const Instruction *findNearestCommonPostDominator(const Instruction *i1, const Instruction *i2);

    // Used to expand envelope outside loop
    Instruction *getLoopDominator(Loop *loop);
    Instruction *getLoopPostDominator(Loop *loop);
};

// A structure to hold CallInst, suitable for TSA.
// for create and start_ts, it goes to dominator. For join and end_ts, goes to post-dominators.
class SCC_CallInstDomPostDom
{
  public:
    Instruction *ci; // call or invoke instruction
    Instruction *domOrPostDom; // If callinst is StatTs, it will go towards dominance.
                               // Else, it goes towards post-dominance

    set<SCC_CallInstDomPostDom *> postDominators;  // outgoing edges - the domOrPostDom is post dominated by this
    set<SCC_CallInstDomPostDom *> dominators; // incoming edges  - these dominate this domOrPostDom

    bool included; // included in some TS


    SCC_CallInstDomPostDom(Instruction *c) { ci = c; domOrPostDom = c; included = false;}
    BasicBlock *getParent() {
        Instruction *i = domOrPostDom ? domOrPostDom : ci;
        return i->getParent();
    }

    static bool dominates(SplicedCFG *splicedCfg, SCC_CallInstDomPostDom& a, SCC_CallInstDomPostDom& b)
    {
      return splicedCfg->dominates(a.ci, b.ci);
    }
};

// Sort call insts by dominance relationships
struct SCC_sortCallInsts
{
   SplicedCFG *spCfg;

   SCC_sortCallInsts(SplicedCFG *cfg) { spCfg = cfg; }

   bool operator()(CallInst *a, CallInst *b)
   {
      return spCfg->dominates(a,b);
   }
};

#endif
