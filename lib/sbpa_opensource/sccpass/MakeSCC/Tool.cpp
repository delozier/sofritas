#include "AccessInfo.h"
#include "llvm/IR/Function.h"
#include "Tool.h"

#include <iostream>

namespace llvm{

bool ORCATool::Init(Module &M){
  Mod = &M;

  tlsTid = new GlobalVariable(M,
			      Type::getInt32Ty(M.getContext()),
			      false, /*isConstant*/
			      GlobalValue::LinkageTypes::ExternalLinkage,
			      nullptr, /*initializer*/
			      "mamaTid",
			      nullptr, /* Insert Before*/
			      GlobalValue::ThreadLocalMode::GeneralDynamicTLSModel);

  mapRegionBase = new GlobalVariable(M,
				     Type::getInt64Ty(M.getContext()),
				     false,
				     GlobalValue::LinkageTypes::ExternalLinkage,
				     nullptr,
				     "_mapRegionBase");

  Type **functionArgs = new Type*[3];
  functionArgs[0] = Type::getInt64Ty(M.getContext());
  //functionArgs[1] = Type::getInt32Ty(M.getContext());

  //ArrayRef<Type*> args(functionArgs,2);
  ArrayRef<Type*> args(functionArgs,1);

  Constant *InstFuncConst =
    M.getOrInsertFunction("MAMA_acquireLoad",
                          FunctionType::get(Type::getVoidTy(M.getContext()),
                                            args,false));
  assert(InstFuncConst != NULL);
  LoadHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(LoadHookFunction != NULL);

  InstFuncConst = 
    M.getOrInsertFunction("MAMA_acquireStore",
			  FunctionType::get(Type::getVoidTy(M.getContext()),
					    args,false));
  assert(InstFuncConst != NULL);
  StoreHookFunction = dyn_cast<Function>(InstFuncConst);
  assert(StoreHookFunction != NULL);

  Function *PthreadCreateFn = M.getFunction("pthread_create");
  if(PthreadCreateFn != NULL){
    InstFuncConst = 
      M.getOrInsertFunction("ORCA_threadCreate",
                            PthreadCreateFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAThreadCreateFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAThreadCreateFunction != NULL);
  }

  Function *PthreadMutexLockFn = M.getFunction("pthread_mutex_lock");
  if(PthreadMutexLockFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("ORCA_mutexLock",
                            PthreadMutexLockFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAMutexLockFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAMutexLockFunction != NULL);
  }

  Function *PthreadMutexTrylockFn = M.getFunction("pthread_mutex_trylock");
  if(PthreadMutexTrylockFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("ORCA_mutexTrylock",
                            PthreadMutexTrylockFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAMutexTrylockFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAMutexTrylockFunction != NULL);
  }

  Function *PthreadMutexUnlockFn = M.getFunction("pthread_mutex_unlock");
  if(PthreadMutexUnlockFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("ORCA_mutexUnlock",
                            PthreadMutexUnlockFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAMutexUnlockFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAMutexUnlockFunction != NULL);
  }

  Function *PthreadCondWaitFn = M.getFunction("pthread_cond_wait");
  if(PthreadCondWaitFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("MAMA_condWait",
                            PthreadCondWaitFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCACondWaitFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCACondWaitFunction != NULL);
  }

  Function *PthreadCondSignalFn = M.getFunction("pthread_cond_signal");
  if(PthreadCondSignalFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("MAMA_condSignal",
                            PthreadCondSignalFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCACondSignalFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCACondSignalFunction != NULL);
  }

  Function *PthreadCondBroadcastFn = M.getFunction("pthread_cond_broadcast");
  if(PthreadCondBroadcastFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("MAMA_condBroadcast",
                            PthreadCondBroadcastFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCACondBroadcastFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCACondBroadcastFunction != NULL);
  }

  Function *PthreadJoinFn = M.getFunction("pthread_join");
  if(PthreadJoinFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("ORCA_join",
                            PthreadJoinFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCAThreadJoinFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCAThreadJoinFunction != NULL);
  }

  Function *PthreadBarrierWaitFn = M.getFunction("pthread_barrier_wait");
  if(PthreadBarrierWaitFn != NULL){
    InstFuncConst =
      M.getOrInsertFunction("ORCA_barrierWait",
                            PthreadBarrierWaitFn->getFunctionType());
    assert(InstFuncConst != NULL);
    ORCABarrierWaitFunction = dyn_cast<Function>(InstFuncConst);
    assert(ORCABarrierWaitFunction != NULL);
  }

  return true;
}

bool ORCATool::Access(Function &F, const AccessInfo &info,
                        const uint64_t accessSize){
  Instruction *inst = info.inst;
  Value *addr = nullptr;
  Value *isStore = nullptr;

  if(info.type == AccessInfo::AccessType::Load ||
     info.type == AccessInfo::AccessType::ExclusiveLoad){
    LoadInst *loadInst = dyn_cast<LoadInst>(inst);
    addr = loadInst->getPointerOperand();

    Value *castResult = 
      CastInst::CreatePointerCast(addr,
				  Type::getInt64Ty(Mod->getContext()),
				  "",info.inst);

    SmallVector<Value*,1> operands;
    operands.push_back(castResult);
    
    CallInst::Create(LoadHookFunction,operands,"",info.inst);
  }else{
    StoreInst *storeInst = dyn_cast<StoreInst>(inst);
    addr = storeInst->getPointerOperand();

    Value *castResult = 
      CastInst::CreatePointerCast(addr,
				  Type::getInt64Ty(Mod->getContext()),
				  "",info.inst);

    SmallVector<Value*,1> operands;
    operands.push_back(castResult);
    
    CallInst::Create(StoreHookFunction,operands,"",info.inst);
  }
  
  return true;
}

bool ORCATool::HandleInvokeInst(BasicBlock& bb, InvokeInst *inst){
  std::string calledFunctionName = inst->getCalledFunction()->getName().str();
  SmallVector<Value*,0> operands;
  if(calledFunctionName.compare("pthread_create") == 0){
    assert(ORCAThreadCreateFunction != NULL);
    inst->setCalledFunction(ORCAThreadCreateFunction);
  }else if(calledFunctionName.compare("pthread_join") == 0){
    assert(ORCAThreadJoinFunction != NULL);
    inst->setCalledFunction(ORCAThreadJoinFunction);
  }else if(calledFunctionName.compare("pthread_barrier_wait") == 0){
    assert(ORCABarrierWaitFunction != NULL);
    inst->setCalledFunction(ORCABarrierWaitFunction);
  }else if(calledFunctionName.compare("pthread_mutex_lock") == 0){
    assert(ORCAMutexLockFunction != NULL);
    inst->setCalledFunction(ORCAMutexLockFunction);
  }else if(calledFunctionName.compare("pthread_mutex_trylock") == 0){
    assert(ORCAMutexTrylockFunction != NULL);
    inst->setCalledFunction(ORCAMutexTrylockFunction);
  }else if(calledFunctionName.compare("pthread_mutex_unlock") == 0){
    assert(ORCAMutexUnlockFunction != NULL);
    inst->setCalledFunction(ORCAMutexUnlockFunction);
  }else if(calledFunctionName.compare("pthread_cond_wait") == 0){
    assert(ORCACondWaitFunction != NULL);
    inst->setCalledFunction(ORCACondWaitFunction);
  }else if(calledFunctionName.compare("pthread_cond_signal") == 0){
    assert(ORCACondSignalFunction != NULL);
    inst->setCalledFunction(ORCACondSignalFunction);
  }else if(calledFunctionName.compare("pthread_cond_broadcast") == 0){
    assert(ORCACondBroadcastFunction != NULL);
    inst->setCalledFunction(ORCACondBroadcastFunction);
  }
  return true;
}

bool ORCATool::HandleCallInst(BasicBlock& bb, CallInst *inst){
  std::string calledFunctionName = inst->getCalledFunction()->getName().str();
  SmallVector<Value*,0> operands;
  if(calledFunctionName.compare("pthread_create") == 0){
    assert(ORCAThreadCreateFunction != NULL);
    inst->setCalledFunction(ORCAThreadCreateFunction);
  }else if(calledFunctionName.compare("pthread_join") == 0){
    assert(ORCAThreadJoinFunction != NULL);
    inst->setCalledFunction(ORCAThreadJoinFunction);
  }else if(calledFunctionName.compare("pthread_barrier_wait") == 0){
    assert(ORCABarrierWaitFunction != NULL);
    inst->setCalledFunction(ORCABarrierWaitFunction);
  }else if(calledFunctionName.compare("pthread_mutex_lock") == 0){
    assert(ORCAMutexLockFunction != NULL);
    inst->setCalledFunction(ORCAMutexLockFunction);
  }else if(calledFunctionName.compare("pthread_mutex_trylock") == 0){
    assert(ORCAMutexTrylockFunction != NULL);
    inst->setCalledFunction(ORCAMutexTrylockFunction);
  }else if(calledFunctionName.compare("pthread_mutex_unlock") == 0){
    assert(ORCAMutexUnlockFunction != NULL);
    inst->setCalledFunction(ORCAMutexUnlockFunction);
  }else if(calledFunctionName.compare("pthread_cond_wait") == 0){
    assert(ORCACondWaitFunction != NULL);
    inst->setCalledFunction(ORCACondWaitFunction);
  }else if(calledFunctionName.compare("pthread_cond_signal") == 0){
    assert(ORCACondSignalFunction != NULL);
    inst->setCalledFunction(ORCACondSignalFunction);
  }else if(calledFunctionName.compare("pthread_cond_broadcast") == 0){
    assert(ORCACondBroadcastFunction != NULL);
    inst->setCalledFunction(ORCACondBroadcastFunction);
  }
  return true;
}

}
