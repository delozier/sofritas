// For profiling execution cound of various lines in code
// Released under the University of Santa Cruz Open Source License

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include <map>
#include <vector>

namespace llvm {
class SccProfile : public ModulePass 
{
    bool runOnModule(Module &M);
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {}
    virtual void releaseMemory() {
    }

  private:
    Constant *sampleCall;

    // Data read from previous profile
    float *_task_task_overlap;  // A NxN matrix, where N = (max_threads * n_static_tasks_per_threads)
    uint32_t *_bb_task_map; // bb to task id map ( n_bbs 8 N, where N is as defined above)
    uint32_t _n_bbs; 
    uint32_t _n_tasks; 
    uint32_t _n_threads;
    uint32_t _n_composite_tasks;  // = _n_threads * _n_tasks
    std::map<const BasicBlock *, unsigned int> _bbIds; // used in using profile data

    void insertModuleInit(Module &M);
    void insertSampleCall(int n, BasicBlock *B);
    void insertBBMap(const std::vector<BasicBlock*> &bbs);

  public:
    static char ID;
    SccProfile() : ModulePass(ID), sampleCall(0) {
      _task_task_overlap = NULL;
      _bb_task_map = NULL;
      _n_bbs = 0;
      _n_threads = 0;
      _n_tasks = 0;
      _n_composite_tasks = 0;
      // initializeSccProfilePass(*PassRegistry::getPassRegistry());
    }
    ~SccProfile() {}
    virtual const char *getPassName() const { return "Pthread Block Profiler"; }

    bool readHistogram();
    void printHistogram();

    bool getTasks(BasicBlock *bb, std::vector<uint32_t>& taskIds);
    float tasksOverlap(int k, int l);
};

} // llvm
