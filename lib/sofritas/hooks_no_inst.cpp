#include <cstdint>
#include <cstdlib>

#include <pthread.h>

extern "C" void SOFRITAS_acquireLock(uint64_t addr, bool isWrite)
{
}

extern "C" void SOFRITAS_acquireLoad(uint64_t addr)
{
}

extern "C" void SOFRITAS_acquireStore(uint64_t addr)
{
}

extern "C" void SOFRITAS_init()
{
}

extern "C" void SOFRITAS_END_OFR()
{
}


extern "C" void SOFRITAS_set_stage(int stage){
}

extern "C" void SOFRITAS_print_lock(void *addr){
}
  
extern "C" void SOFRITAS_print_debug(const char *message){
}

extern "C" int SOFRITAS_release(void *addr){
  return 1;
}

extern "C" void SOFRITAS_clear(void *addr){
}

extern "C" void SOFRITAS_allocate(void *addr, unsigned long size){
}

extern "C" void SOFRITAS_release_object(void *addr, unsigned long size){
}

extern "C" void SOFRITAS_release_array(void *addr, unsigned long size, unsigned long num){
}

extern "C" int SOFRITAS_require_mutex_lock(void *addr){
  return 1;
}

extern "C" void SOFRITAS_disable(){
}

extern "C" void SOFRITAS_enable(){
}

extern "C" int SOFRITAS_barrier_no_release(pthread_barrier_t *barrier)
{
  return pthread_barrier_wait(barrier);
}
