#include <memory/heap.hpp>

#include <sys/mman.h>
#include <iostream>
#include <cassert>

using namespace sofritas;

bool Heap::_initialized = false;
char* Heap::_slabPos = nullptr;
char* Heap::_mmapPos = nullptr;
std::mutex Heap::_slabLock;

address Heap::_slabStart;
address Heap::_slabEnd;

std::mutex Heap::_hugePagesLock;
uint64_t Heap::_remainingHugePages;

void Heap::InitializeHeap(memory_size size)
{
  if(_initialized) return;

  _remainingHugePages = (uint64_t)HUGEPAGES_AVAILABLE * 2048 * 1024;

  _slabPos = (char*)mmap(0,size,PROT_READ | PROT_WRITE,
			 MAP_PRIVATE | MAP_ANONYMOUS,0,0);
  _slabStart = (address)_slabPos;
  _slabEnd = _slabStart + size;

  if(_slabPos == MAP_FAILED){
    perror("mmap: ");
  }

  // Allocate mmaps from the end of the heap, sbrk from the top
  _mmapPos = _slabPos + size;

  _initialized = true;
}

bool Heap::reserveHugePages(uint64_t size)
{
  _hugePagesLock.lock();
  if(_remainingHugePages > size){
    _remainingHugePages -= size;
    _hugePagesLock.unlock();
    return true;
  }else{
    _hugePagesLock.unlock();
    return false;
  }
}

void* Heap::HandleSBRK(size_t size)
{
  if(!_initialized){
    InitializeHeap(HEAP_SIZE);
  }

  _slabLock.lock();
  void *res = _slabPos;
  _slabPos += size;
  assert(_slabPos < _mmapPos);
  _slabLock.unlock();

  return res;
}

void* Heap::HandleMMAP(void *addr, size_t size, int prot,
		       int flags, int fd, off_t offset)
{
  if(!_initialized){
    InitializeHeap(HEAP_SIZE);
  }
 
  _slabLock.lock();
  _mmapPos -= size;
  assert(_mmapPos > _slabPos);
  _slabLock.unlock();

  return _mmapPos;
}

int Heap::HandleMUNMAP(void *addr, size_t size)
{
  // TODO: Implement free-list for MMAP allocations
  return 0;
}
