#include <memory/shadowspace.hpp>
#include <memory/heap.hpp>

#include <atomic>
#include <cassert>
#include <cstring>
#include <iostream>

#include <asm/mman.h>
#include <sys/mman.h>

using namespace sofritas;

void ShadowSpace::Initialize(address base, size_t size, 
			     size_t shadowSize, size_t shadowPerByte)
{
  _memoryBase = base;

  assert((size * shadowSize) % shadowPerByte == 0);
  _size = (size * shadowSize) / shadowPerByte;

  _base = (char*)mmap(0,_size,
		      PROT_READ | PROT_WRITE,
		      MAP_PRIVATE | MAP_ANONYMOUS,0,0);

  if((void*)_base == MAP_FAILED){
    std::cerr << "Failed to allocate shadowspace with mmap\n";
    exit(1);
  }

  switch(shadowPerByte){
  case 1:
    _shift = 0;
    break;
  case 2:
    _shift = 1;
    break;
  case 4:
    _shift = 2;
    break;
  case 8:
    _shift = 3;
    break;
  default:
    assert(false);
  }

  _shadowSize = shadowSize;
  _shadowPerByte = shadowPerByte;

  std::cout << "ShadowSpace: [Base: " << (void*)_base << ", MemBase: " << 
    (void*)_memoryBase << ", Size: " << _size << ", ShadowSize: " <<
    _shadowSize << ", PerByte: " << shadowPerByte << 
    ", Shift: " << _shift << "]\n";
}

uint64_t ShadowSpace::getBase()
{
  return (uint64_t)_base.load(std::memory_order::memory_order_relaxed);
}

address ShadowSpace::getIndex(address addr)
{
  address diff = addr - _memoryBase;
  address index = diff;
  if(_shift != 0){
    index = index >> _shift;
  }else{
    index *= _shadowSize;
  }
  return index;
}

void ShadowSpace::Clear()
{
  madvise((void*)_base,_size,MADV_DONTNEED);
  /*
  char * new_base = (char*)mmap((void*)_base,_size,PROT_READ | PROT_WRITE,
		      MAP_FIXED | MAP_ANONYMOUS | MAP_PRIVATE | MAP_HUGETLB | MAP_HUGE_2MB,0,0);
  assert(new_base == _base);
  */
  std::atomic_thread_fence(std::memory_order_seq_cst);
}

void ShadowSpace::ClearSFR()
{
  //madvise((void*)_base,4096,MADV_DONTNEED);
  madvise((void*)_base,_size,MADV_DONTNEED);
  std::atomic_thread_fence(std::memory_order_seq_cst);
}

char* ShadowSpace::getMetadata(address addr)
{
  char *ptr = _base + getIndex(addr);
  assert(ptr >= _base && ptr < (_base + _size));
  return ptr;
}

void ShadowSpace::HandleReallocate(void *p, size_t size)
{
  address indexStart = getIndex((address)p);
  if(_shadowPerByte > 1){
    size = size / _shadowPerByte;
  }
  memset(_base + indexStart,0,size * _shadowSize);
}
