#include <memory/internal_heap.hpp>

#include <sys/mman.h>
#include <cstdio>
#include <iostream>

using namespace sofritas;

bool InternalHeap::_initialized = false;
char* InternalHeap::_slabPos = nullptr;

address InternalHeap::_slabStart;
address InternalHeap::_slabEnd;

void InternalHeap::Initialize(memory_size size)
{
  if(_initialized) return;

  std::cout << "Initializing internal memory\n";

  _slabPos = (char*)mmap(0,size,PROT_READ | PROT_WRITE,
			 MAP_PRIVATE | MAP_ANONYMOUS,0,0);
  _slabStart = (address)_slabPos;
  _slabEnd = _slabStart + size;

  if(_slabPos == MAP_FAILED){
    perror("mmap: ");
  }

  _initialized = true;
}

void* InternalHeap::internal_malloc(size_t size)
{
  if(!_initialized){
    Initialize(INTERNAL_HEAP_SIZE);
  }

  void *res = _slabPos;
  _slabPos += size;
  return res;
}

void InternalHeap::internal_free(void *p)
{
  // TODO: Implement free-list for internal memory
}
