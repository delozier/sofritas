#include <memory/heap.hpp>
#include <memory/internal_heap.hpp>
#include <memory/shadowspace.hpp>
#include <sofritas/runtime.hpp>
#include <sofritas/lock.hpp>

#include <signal.h>
#include <unistd.h>

#include <sys/mman.h>

#include <cstring>
#include <cstdlib>

#include <unordered_map>
#include <new>
#include <thread>

uint64_t _mapRegionBase;

sofritas::ShadowSpace _lockShadowSpace;
sofritas::ShadowSpace _threadShadowSpaces[sofritas::Lock::MAX_THREADS];

uint64_t _globalSlabStart;
uint64_t _globalSlabEnd;

__thread uint64_t _threadMapBase;

__thread TID myTid;

#ifdef SOFRITAS_HANDLE_GLOBALS
uint64_t _globalDataMapRegionBase;

sofritas::ShadowSpace _globalLockShadowSpace;

uint64_t _globalDataStart;
uint64_t _globalDataEnd;

__thread uint64_t _globalDataThreadMapBase;

extern char etext;
extern char edata;
extern char end;
#endif

sofritas::AtomicThread* _threads[sofritas::Lock::MAX_THREADS];
std::atomic<uint64_t> _threadMapBases[sofritas::Lock::MAX_THREADS];

void orcaExit(){
  std::cerr << "Exiting SOFRITAS\n";
  sofritas::Runtime::cleanup();
}

void malloc_hook(void *p, size_t size){
  sofritas::Runtime::handleMalloc(p,size);
}

void lock_debug(int sig, siginfo_t *si, void *unused);
extern "C" void MallocHook_AddNewHook(void (*fn)(void*, size_t size));

namespace sofritas{

TID Runtime::_nextTid;
std::queue<TID> Runtime::_reuseTids;
std::mutex Runtime::_tidLock;

__thread AtomicThread* _sofritasThread = nullptr;

std::unordered_map<pthread_cond_t*,emulated_cond_var*> Runtime::_condition_variables;
std::mutex Runtime::_cond_var_lock;

std::mutex _slabLock;
bool Runtime::_initialized = false; 

void Runtime::handleMalloc(void *p, size_t size)
{
  if(_nextTid == 2){
    // Don't bother if it's the main thread.  It will release all of its locks at join 
    // anyways
    return;
  }

  // Clear locks on all threads
  _lockShadowSpace.HandleReallocate(p,size);

  for(int c = 1; c < _nextTid; c++){
    _threadShadowSpaces[c].HandleReallocate(p,size);
  }
}

void Runtime::init()
{
  initShadowMem();

  myTid = 1;
  _sofritasThread = new (get_internal_memory(sizeof(AtomicThread))) AtomicThread(myTid);
  _threads[myTid] = _sofritasThread;
  _nextTid = 2;

  _lockShadowSpace.Initialize(Heap::_slabStart,HEAP_SIZE,
			      sizeof(lock_state_type),1);

  _mapRegionBase = _lockShadowSpace.getBase();

  _threadShadowSpaces[myTid].Initialize(Heap::_slabStart,HEAP_SIZE,
					 sizeof(thread_state_type),4);
  _threadMapBase = _threadShadowSpaces[myTid].getBase();
  _threadMapBases[1] = _threadMapBase;

  if((void*)_threadMapBase == MAP_FAILED){
    perror("mmap: ");
  }

  for(int c = 2; c < sofritas::Lock::MAX_THREADS; c++){
    _threadMapBases[c] = 0;
  }

  atexit(orcaExit);

  struct sigaction sad;

  sad.sa_flags = SA_SIGINFO;
  sigemptyset(&sad.sa_mask);
  sad.sa_sigaction = lock_debug;
  if (sigaction(SIGFPE, &sad, NULL) == -1){
    fprintf(stderr,"Failed to initialize lock debug handler\n");
  }

  MallocHook_AddNewHook(malloc_hook);

  _initialized = true;

  std::cout << "Done initializing\n";
}

TID Runtime::getTidBit(TID tid)
{
  return 1 << (tid + 3);
}

void Runtime::initShadowMem()
{
  if(_initialized){
    return;
  }

  InternalHeap::Initialize(INTERNAL_HEAP_SIZE);
  Heap::InitializeHeap(HEAP_SIZE);
  _globalSlabStart = Heap::_slabStart;
  _globalSlabEnd = Heap::_slabEnd;
}

void* Runtime::get_internal_memory(size_t size){
  if(!_initialized){
    initShadowMem();
  }

  return InternalHeap::internal_malloc(size);;
}

void Runtime::cleanup()
{
#ifdef SOFRITAS_LOCK_STATS
  std::cout << "Load Checks: " << Lock::loadChecks << "\n";
  std::cout << "Store Checks: " << Lock::storeChecks << "\n";
  std::cout << "Load Acquires: " << Lock::loadAcquires << "\n";
  std::cout << "Store Acquires: " << Lock::storeAcquires << "\n\n";
  std::cout << "Unlocked Acquires: " << Lock::unlockedAcquires << "\n";
  std::cout << "Exclusive Acquires: " << Lock::exclusiveReadAcquires << "\n";
  std::cout << "Shared Acquires: " << Lock::sharedReadAcquires << "\n";
  std::cout << "Write Acquires: " << Lock::writeAcquires << "\n\n";
  std::cout << "Re-acquires: " << Lock::reacquires << "\n\n";
  std::cout << "Releases: " << Lock::releases << "\n";
  std::cout << "Batch releases: " << Lock::batchReleases << "\n";
#endif
}

void Runtime::setThreadStatus(ThreadState state)
{
  _sofritasThread->setState(state);
}

bool Runtime::acquireLock(size_t addr, bool isWrite)
{
  // Sometimes might be null during startup (before init)
  if(_sofritasThread == nullptr || _sofritasThread->isDisabled()){
    return false;
  }

#ifdef SOFRITAS_LOCK_STATS
  if(isWrite){
    Lock::storeChecks++;
  }else{
    Lock::loadChecks++;
  }
#endif

#ifndef SOFRITAS_INLINE
  if(Lock::isOwner(addr,myTid,isWrite)){
    return true;
  }
#endif

#ifndef SOFRITAS_INLINE
  _sofritasThread->_currentAddr = addr;
  _sofritasThread->_currentIsWrite = isWrite;
#endif
  Lock::acquireHeapLock(addr,myTid,isWrite);

  return true;
}

void Runtime::releaseAll(bool isLock)
{
#ifdef SOFRITAS_LOCK_STATS
  Lock::batchReleases++;
#endif
  if(!isLock){
    _threadShadowSpaces[myTid].Clear();
  }else{
    _threadShadowSpaces[myTid].ClearSFR();
  }
}

bool Runtime::releaseLock(size_t addr)
{
  if(!IN_SOFRITAS_HEAP(addr)){
    return false;
  }

#ifdef SOFRITAS_LOCK_STATS
  Lock::releases++;
#endif
  Lock::release(addr,myTid);

  return true;
}

void Runtime::releaseObject(size_t addr, size_t size)
{
  size_t current = addr;
  size_t maxAddr = addr + size;

  while(current < maxAddr){
    releaseLock(current);
    current++;
  }
}

void Runtime::releaseArray(size_t addr, size_t size, size_t num)
{
  for(size_t c = 0; c < num; c++){
    releaseLock(addr + (size * c));
  }
}

void Runtime::clearLock(size_t addr)
{
  Lock::clear(addr);
}

void Runtime::requireMutex(size_t addr)
{
  if(addr < _globalSlabStart || addr >= _globalSlabEnd){
    return;
  }
  Lock::requireMutex(addr);
}

void Runtime::disableSOFRITAS()
{
  _sofritasThread->disable();
}

void Runtime::enableSOFRITAS()
{
  _sofritasThread->enable();
}

void Runtime::threadStart()
{
  if(_sofritasThread != nullptr){
    return;
  }

  _tidLock.lock();

  assert(_nextTid < sofritas::Lock::MAX_THREADS);

  if(_reuseTids.size() > 0){
    myTid = _reuseTids.front();
    _reuseTids.pop();
    _sofritasThread = _threads[myTid];
    _threadMapBase = _threadShadowSpaces[myTid].getBase();
    Runtime::setThreadStatus(RUNNING);
  }else{
    myTid = _nextTid++;
    _sofritasThread = new (get_internal_memory(sizeof(AtomicThread))) AtomicThread(myTid);
    _threads[myTid] = _sofritasThread;
    _threadShadowSpaces[myTid].Initialize(Heap::_slabStart,HEAP_SIZE,
					   sizeof(thread_state_type),4);
    _threadMapBase = _threadShadowSpaces[myTid].getBase();
    
    if((void*)_threadMapBase == MAP_FAILED){
      std::cerr << "Failed to allocate thread shadow space!\n";
      exit(1);
    }

    _threadMapBases[myTid] = _threadMapBase;
  }

  _tidLock.unlock();
}

void Runtime::threadExit()
{
  releaseAll();
  _tidLock.lock();
  _reuseTids.push(myTid);
  _tidLock.unlock();
  Runtime::setThreadStatus(EXITED);
}

void Runtime::beforeJoin()
{
  releaseAll();
  Runtime::setThreadStatus(JOINED);
}

void Runtime::afterJoin()
{
  Runtime::setThreadStatus(RUNNING);
}

void Runtime::beforeBarrier()
{
  releaseAll();
  Runtime::setThreadStatus(BARRIER);
}

void Runtime::afterBarrier()
{
  Runtime::setThreadStatus(RUNNING);
}

emulated_cond_var* Runtime::getCondVar(pthread_cond_t *cond)
{
  _cond_var_lock.lock();
  emulated_cond_var *cond_var = _condition_variables[cond];
  
  if(cond_var == nullptr){
    cond_var = new (get_internal_memory(sizeof(emulated_cond_var))) emulated_cond_var;
    _condition_variables[cond] = cond_var;
  }

  _cond_var_lock.unlock();

  return cond_var;
}

void Runtime::condWait(pthread_cond_t *cond, pthread_mutex_t *mutex)
{
  emulated_cond_var *cond_var = getCondVar(cond);
  assert(cond_var != nullptr);

  // Have to init before starting the spin wait to ensure that the node is available to be
  // woken up before locks can be stolen via WAITING state
  cond_var_node node;
  cond_var->init_wait(&node);
  Runtime::setThreadStatus(WAITING);
  releaseAll();
#ifdef SOFRITAS_RFR
  pthread_mutex_unlock(mutex);
#endif
  cond_var->wait(&node);
#ifdef SOFRITAS_RFR
  pthread_mutex_lock(mutex);
#endif
  Runtime::setThreadStatus(RUNNING);
}

void Runtime::condSignal(pthread_cond_t *cond)
{
  emulated_cond_var *cond_var = getCondVar(cond);
  if(cond_var != nullptr){
    cond_var->signal();
  }
}

void Runtime::condBroadcast(pthread_cond_t *cond)
{
  emulated_cond_var *cond_var = getCondVar(cond);
  if(cond_var != nullptr){
    cond_var->broadcast();
  }
}

}

void lock_debug(int sig, siginfo_t *si, void *unused) 
{
  for(int c = 1; c < sofritas::Lock::MAX_THREADS; c++){
    sofritas::AtomicThread *thread = _threads[c];
    if(thread == nullptr){
      continue;
    }
    std::cerr << "Thread: " << c << "\n";
    std::cerr << "----------------------------------------\n";
    std::cerr << thread->stateToString() << " , AcqWrite?: " << thread->_currentIsWrite << "\n";
    uint64_t curAddr = thread->_currentAddr;
    if(curAddr != 0){
      sofritas::Lock::printState(curAddr,std::cerr);
    }
    std::cerr << "\n";
  }
}
