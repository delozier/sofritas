#include <memory/heap.hpp>
#include <sofritas/runtime.hpp>

#ifdef SOFRITAS_SFR
#define SOFRITAS_RFR 1
#endif

extern __thread uint64_t _threadMapBase;
extern __thread TID myTid;

extern uint64_t _globalSlabStart;
extern uint64_t _globalSlabEnd;
extern uint64_t _mapRegionBase;

extern char _orcaDataBase;

extern "C" void* SOFRITAS_sbrk(size_t size){
  return sofritas::Heap::HandleSBRK(size);
}

extern "C" void* SOFRITAS_mmap(void *addr, size_t size, int prot,
			       int flags, int fd, off_t offset){
  return sofritas::Heap::HandleMMAP(addr,size,prot,flags,fd,offset);
}

extern "C" int SOFRITAS_munmap(void *addr, size_t size){
  return sofritas::Heap::HandleMUNMAP(addr,size);
}

extern "C" void SOFRITAS_acquireLock(uint64_t addr, bool isWrite)
{
  sofritas::Runtime::acquireLock(addr,isWrite);
}

extern "C" void SOFRITAS_acquireLoad(uint64_t addr) __attribute__((always_inline));

extern "C" void SOFRITAS_acquireLoad(uint64_t addr)
{
  if(addr < _globalSlabStart || addr >= _globalSlabEnd){
    return;
  }

#ifdef SOFRITAS_INLINE  
  uint64_t diff = addr - _globalSlabStart;
  uint64_t index = diff >> 2;
  thread_state_type *lockPtr = 
    (thread_state_type*)(index + _threadMapBase);
  thread_state_type lockVal = *lockPtr;

  thread_state_type mask = sofritas::Lock::THREAD_LOCK_MASKS[addr & 0x3];

  if(!(lockVal & mask)){ 
#ifdef SOFRITAS_ACQUIRE_OPT
    index = diff * sizeof(lock_state_type);
    std::atomic<lock_state_type> *lockStatePtr = (std::atomic<lock_state_type>*)
      (index + _mapRegionBase);
    
    lock_state_type state = 
      lockStatePtr->load(std::memory_order::memory_order_relaxed);
    lock_state_type checkedState = state;
    lock_state_type owner = SOFRITAS_LOCK_OWNER(state);
    lock_state_type ownedState = myTid << 4;
    ownedState = ownedState | sofritas::Lock::MAYBE_HELD;

    // Try to quickly reacquire shared readers
    if(state == sofritas::Lock::MAYBE_HELD ||
       state == ownedState){
      lock_state_type newState = state | sofritas::Lock::UPDATING;
      if(lockStatePtr->compare_exchange_strong(state,newState,std::memory_order::memory_order_acquire)){
        if(state == checkedState){
          *lockPtr = lockVal | (mask ^ 0x55);
        }
	lockStatePtr->store(state,std::memory_order::memory_order_release);
      }
    }else{
#endif
#endif // SOFRITAS_INLINE
      sofritas::Runtime::acquireLock(addr,false);
#ifdef SOFRITAS_ACQUIRE_OPT
    }
#endif
#ifdef SOFRITAS_INLINE
  }
#endif
}

extern "C" void SOFRITAS_acquireLoadAligned(uint64_t addr) __attribute__((always_inline));

extern "C" void SOFRITAS_acquireLoadAligned(uint64_t addr)
{
  if(addr < _globalSlabStart || addr >= _globalSlabEnd){
    return;
  }

#ifdef SOFRITAS_INLINE  
  uint64_t diff = addr - _globalSlabStart;
  uint64_t index = diff >> 2;
  thread_state_type *lockPtr = 
    (thread_state_type*)(index + _threadMapBase);
  thread_state_type lockVal = *lockPtr;

  if(!(lockVal & 0x3)){ 
    // Try to acquire in shared read
    uint64_t indexState = (addr - _globalSlabStart) * sizeof(lock_state_type);
    std::atomic<lock_state_type> *lockStatePtr = (std::atomic<lock_state_type>*)
      (indexState + _mapRegionBase);
    if(lockStatePtr->load(std::memory_order::memory_order_relaxed) == 0x2){
      lock_state_type expected = 0x2;
      if(lockStatePtr->compare_exchange_strong(expected,expected | 0x1,
					       std::memory_order::memory_order_acquire)){
	*lockPtr = 0x1;
	lockStatePtr->store(expected,std::memory_order::memory_order_release);
	return;
      }
    }
#endif // SOFRITAS_INLINE
    sofritas::Runtime::acquireLock(addr,false);
#ifdef SOFRITAS_INLINE
  }
#endif
}

extern "C" void SOFRITAS_acquireStoreAligned(uint64_t addr) __attribute__((always_inline));

extern "C" void SOFRITAS_acquireStoreAligned(uint64_t addr)
{
  if(addr < _globalSlabStart || addr >= _globalSlabEnd){
    return;
  }

#ifdef SOFRITAS_INLINE
  uint64_t diff = addr - _globalSlabStart;
  uint64_t index = diff >> 2;
  thread_state_type *lockPtr = 
    (thread_state_type*)(index + _threadMapBase);
  thread_state_type lockVal = *lockPtr;
  
  if(!(lockVal & 0x2)){
#endif // SOFRITAS_INLINE
      sofritas::Runtime::acquireLock(addr,true);
#ifdef SOFRITAS_INLINE
  }
#endif // SOFRITAS_INLINE
}

extern "C" void SOFRITAS_acquireStore(uint64_t addr) __attribute__((always_inline));

extern "C" void SOFRITAS_acquireStore(uint64_t addr)
{
  if(addr < _globalSlabStart || addr >= _globalSlabEnd){
    return;
  }

#ifdef SOFRITAS_INLINE
  uint64_t diff = addr - _globalSlabStart;
  uint64_t index = diff >> 2;
  thread_state_type *lockPtr = 
    (thread_state_type*)(index + _threadMapBase);
  thread_state_type lockVal = *lockPtr;

  thread_state_type mask = sofritas::Lock::THREAD_LOCK_MASKS[addr & 0x3];

  if(!(lockVal & mask & 0xAA)){
#ifdef SOFRITAS_ACQUIRE_OPT
    index = diff * sizeof(lock_state_type);
    std::atomic<lock_state_type> *lockStatePtr = (std::atomic<lock_state_type>*)
      (index + _mapRegionBase);
    
    lock_state_type state = 
      lockStatePtr->load(std::memory_order::memory_order_relaxed);
    lock_state_type checkedState = state;
    lock_state_type owner = SOFRITAS_LOCK_OWNER(state);
    
    lock_state_type ownedState = myTid << 4;
    lock_state_type ownedStateMutex = ownedState | 0xE;
    ownedState = ownedState | 0xA;
    
    // Try to quickly reacquire shared readers
    if(state == ownedState ||
       state == ownedStateMutex){
      lock_state_type newState = state | sofritas::Lock::UPDATING;
      if(lockStatePtr->compare_exchange_strong(
           state,newState,std::memory_order::memory_order_acquire)){
	if(state == checkedState){
	  *lockPtr = lockVal | mask;
	}
	lockStatePtr->store(state,std::memory_order::memory_order_release);
      }
    }else{
#endif
#endif // SOFRITAS_INLINE
      sofritas::Runtime::acquireLock(addr,true);
#ifdef SOFRITAS_ACQUIRE_OPT
    }
#endif
#ifdef SOFRITAS_INLINE
  }
#endif // SOFRITAS_INLINE
}

extern "C" void __attribute__ ((noinline)) SOFRITAS_passMapRegion(uint64_t mapBase,
								  uint64_t mapRegion){
  // Empty, just used to hook from PIN
  printf("Passing map regions %lu %lu\n",mapBase,mapRegion);
}

extern "C" void SOFRITAS_init()
{
  sofritas::Runtime::init();
  SOFRITAS_passMapRegion(_globalSlabStart,_mapRegionBase);
}

struct ORCA_thread_args{
  void* (*start)(void *);
  void *arg;
};

extern "C" void __attribute__ ((noinline)) SOFRITAS_passThreadRegion(uint64_t threadMapBase){
  // Empty, just used to hook from PIN
  printf("%lx\n",threadMapBase);
  printf("Passing thread map region\n");
}

extern "C" void SOFRITAS_createThread()
{
  sofritas::Runtime::threadStart();
}

extern "C" void SOFRITAS_exitThread()
{
  sofritas::Runtime::threadExit();
}

extern "C" void* ORCA_threadHook(void * arg)
{
  ORCA_thread_args *args = (ORCA_thread_args*)arg;

  SOFRITAS_createThread();
  SOFRITAS_passThreadRegion(_threadMapBase);
  void* retVal = args->start(args->arg);
  SOFRITAS_exitThread();

  return retVal;
}

extern "C" void SOFRITAS_beforeJoin()
{
  sofritas::Runtime::beforeJoin();
}

extern "C" void SOFRITAS_afterJoin()
{
  sofritas::Runtime::afterJoin();
}

extern "C" void SOFRITAS_beforeBarrier()
{
  sofritas::Runtime::beforeBarrier();
}

extern "C" void SOFRITAS_afterBarrier()
{
  sofritas::Runtime::afterBarrier();
}

extern "C" int SOFRITAS_join(pthread_t t, void **fn)
{
  SOFRITAS_beforeJoin();
  int ret = pthread_join(t,fn);
  SOFRITAS_afterJoin();
  return ret;
}

extern "C" int SOFRITAS_barrierWait(pthread_barrier_t *barrier)
{
  SOFRITAS_beforeBarrier();
  int ret = pthread_barrier_wait(barrier);
  SOFRITAS_afterBarrier();
  return ret;
}

extern "C" int SOFRITAS_barrier_no_release(pthread_barrier_t *barrier)
{
  return pthread_barrier_wait(barrier);
}

extern "C" int SOFRITAS_threadCreate(pthread_t *thread, const pthread_attr_t *attr, void* (*start)(void*), void *arg)
{
  ORCA_thread_args *args = 
    new (sofritas::Runtime::get_internal_memory(sizeof(ORCA_thread_args))) ORCA_thread_args;
  args->start = start;
  args->arg = arg;

  return pthread_create((pthread_t*)thread,(const pthread_attr_t*)attr,ORCA_threadHook,(void*)args);
}

//extern "C" int SOFRITAS_mutexLock(pthread_mutex_t *mutex) __attribute__((always_inline));
extern "C" int SOFRITAS_mutexLock(pthread_mutex_t *mutex) { 
#ifdef SOFRITAS_SFR
  sofritas::Runtime::releaseAll(true);
  return pthread_mutex_lock(mutex);
#else
#ifdef SOFRITAS_RFR
  return pthread_mutex_lock(mutex);
#else
  return 0;
#endif
#endif  
}

//extern "C" int SOFRITAS_mutexTryLock(pthread_mutex_t *mutex) __attribute__((always_inline));
extern "C" int SOFRITAS_mutexTrylock(pthread_mutex_t *mutex) { 
#ifdef SOFRITAS_SFR
  sofritas::Runtime::releaseAll(true);
  return pthread_mutex_trylock(mutex);
#else
#ifdef SOFRITAS_RFR
  return pthread_mutex_trylock(mutex);
#else
  return 0; 
#endif
#endif
}

//extern "C" int SOFRITAS_mutexUnLock(pthread_mutex_t *mutex) __attribute__((always_inline));
extern "C" int SOFRITAS_mutexUnlock(pthread_mutex_t *mutex) { 
#ifdef SOFRITAS_RFR
  sofritas::Runtime::releaseAll(true);
  return pthread_mutex_unlock(mutex);
#else
  return 0;
#endif
}

extern "C" int SOFRITAS_condWait(pthread_cond_t *cond, pthread_mutex_t *mutex)
{
  sofritas::Runtime::condWait(cond,mutex);
  return 0;
}

extern "C" int SOFRITAS_condSignal(pthread_cond_t *cond)
{
  sofritas::Runtime::condSignal(cond);
  return 0;
}

extern "C" int SOFRITAS_condBroadcast(pthread_cond_t *cond)
{
  sofritas::Runtime::condBroadcast(cond);
  return 0;
}

extern "C" void SOFRITAS_set_stage(int stage){
}

extern "C" void SOFRITAS_print_lock(void *addr){
  sofritas::Lock::printState((uint64_t)addr,std::cerr);
}
  
extern "C" void SOFRITAS_print_debug(const char *message){
}

extern "C" void SOFRITAS_END_OFR()
{
  sofritas::Runtime::releaseAll();
}

extern "C" int SOFRITAS_release(void *addr){
  sofritas::Runtime::releaseLock((size_t)addr);
  return 1;
}

extern "C" void SOFRITAS_clear(void *addr){
  sofritas::Runtime::releaseLock((size_t)addr);
  // TODO
}

extern "C" void SOFRITAS_allocate(void *addr, unsigned long size){
  // TODO
}

extern "C" void SOFRITAS_release_object(void *addr, unsigned long size){
  sofritas::Runtime::releaseObject((size_t)addr,size);
}

extern "C" void SOFRITAS_release_array(void *addr, unsigned long size, unsigned long num){
  sofritas::Runtime::releaseArray((size_t)addr,size,num);
}

extern "C" int SOFRITAS_require_mutex_lock(void *addr){
  sofritas::Runtime::requireMutex((size_t)addr);
  return 1;
}

extern "C" void SOFRITAS_disable(){
  sofritas::Runtime::disableSOFRITAS();
}

extern "C" void SOFRITAS_enable(){
  sofritas::Runtime::enableSOFRITAS();
}
