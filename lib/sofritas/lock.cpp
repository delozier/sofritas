#include <sofritas/lock.hpp>
#include <sofritas/runtime.hpp>
#include <memory/heap.hpp>
#include <memory/shadowspace.hpp>

#include <thread>

using namespace sofritas;

#define BASE_CONSISTENCY std::memory_order::memory_order_relaxed

extern sofritas::ShadowSpace _lockShadowSpace;
extern sofritas::ShadowSpace _threadShadowSpaces[sofritas::Lock::MAX_THREADS];

extern sofritas::AtomicThread* _threads[sofritas::Lock::MAX_THREADS];

constexpr thread_state_type Lock::THREAD_LOCK_MASKS[4];

#ifdef SOFRITAS_LOCK_STATS
std::atomic<unsigned long> Lock::loadChecks;
std::atomic<unsigned long> Lock::storeChecks;

std::atomic<unsigned long> Lock::loadAcquires;
std::atomic<unsigned long> Lock::storeAcquires;

std::atomic<unsigned long> Lock::unlockedAcquires;
std::atomic<unsigned long> Lock::exclusiveReadAcquires;
std::atomic<unsigned long> Lock::sharedReadAcquires;
std::atomic<unsigned long> Lock::writeAcquires;

std::atomic<unsigned long> Lock::reacquires;

std::atomic<unsigned long> Lock::releases;
std::atomic<unsigned long> Lock::batchReleases;
#endif

std::atomic<lock_state_type>* Lock::getLockState(address addr)
{
  return (std::atomic<lock_state_type>*)_lockShadowSpace.getMetadata(addr);
}

thread_state_type* Lock::getThreadLock(address addr, TID tid)
{
  return (thread_state_type*)_threadShadowSpaces[tid].getMetadata(addr);
}

void Lock::waitOnThreadLock(uint64_t addr, TID tid, bool isWrite)
{
  bool conflicting = true;
  while(conflicting){
    thread_state_type *otherThreadLock = getThreadLock(addr,tid);
    if(isWrite){
      // If we're trying to get write privs, check for any existing owners
      conflicting = checkThreadLock(otherThreadLock,addr,false);
    }else{
      // If we're trying to get read privs, check for existing write owners
      conflicting = checkThreadLock(otherThreadLock,addr,true);
    }
  }
}

void Lock::requireMutex(uint64_t addr)
{
  std::atomic<lock_state_type> *lockPtr = nullptr;
  lockPtr = getLockState(addr);
  lock_state_type expected = *lockPtr;
  lock_state_type newState = expected | Lock::MUTEX;
  *lockPtr = newState;
}

bool Lock::acquireHeapLock(uint64_t addr, TID tid, bool isWrite)
{
  return acquire(addr,tid,isWrite);
}

bool Lock::acquire(uint64_t addr, TID tid, bool isWrite)
{
  // Not set, so acquire
  std::atomic<lock_state_type> *lockPtr = 
    getLockState(addr);
  thread_state_type *threadLockPtr = 
    getThreadLock(addr,tid);

  lock_state_type expected = lockPtr->load(std::memory_order::memory_order_relaxed) & 0xFFFE;
  lock_state_type newState = expected;

  newState = expected | UPDATING;

  //unsigned int tries = 0;

  while(!lockPtr->compare_exchange_strong(
	  expected,newState,std::memory_order::memory_order_acquire)){
    expected = expected & 0xFFFE;
    newState = expected | UPDATING;
  }

  if(newState & MUTEX){
    // Check for mutex
    isWrite = true;
  }

#ifdef SOFRITAS_LOCK_STATS
  if(isWrite){
    storeAcquires++;
  }else{
    loadAcquires++;
  }
#endif
  
  lock_state_type owner = SOFRITAS_LOCK_OWNER(newState);

  // Check to see if we can acquire the lock and wait on any current
  // lock holders

  // First, check if lock is possibly still owned
  if(newState & MAYBE_HELD){
    // If writer might exist, check against that writer's state
    if(newState & WRITE){
#ifdef SOFRITAS_LOCK_STATS
      writeAcquires++;
#endif
      // Possible for this thread to be the cached owner, then we don't need to check
      // because nobody has touched the lock since this thread batch released it
      // (TODO: Check how often this happens)
      if(owner != tid){
	waitOnThreadLock(addr,owner,isWrite);
      }else{
#ifdef SOFRITAS_LOCK_STATS
	reacquires++;
#endif
      }
    }else{
#ifdef SOFRITAS_LOCK_STATS
      if(owner != 0){
        exclusiveReadAcquires++;
      }else{
	sharedReadAcquires++;
      }
#endif
      // If not a writer and acquiring write privileges, check sole reader or shared readers
      if(isWrite){
	if(owner != 0){
	  if(owner != tid){
	    waitOnThreadLock(addr,owner,isWrite);
	  }else{
#ifdef SOFRITAS_LOCK_STATS
	    reacquires++;
#endif
	  }
	}else{
	  for(unsigned int c = 1; c < sofritas::Lock::MAX_THREADS; c++){
	    if(c == tid){
	      continue;
	    }
	    if(_threadShadowSpaces[c].getBase() != 0){
	      waitOnThreadLock(addr,c,isWrite);
	    }
	  }
	}
      }
      // No checks if !isWrite and !WRITE (acquiring as a shared reader)
    }
  }
#ifdef SOFRITAS_LOCK_STATS
  else{
    unlockedAcquires++;
  }
#endif

  // Wipe out any old possible owners before setting new state
  lock_state_type newLockState = newState & MUTEX;
  newLockState |= MAYBE_HELD;

  acquireThreadLock(threadLockPtr,addr,isWrite);

  // Change our own thread state to reflect the correct state
  if(isWrite){
    newLockState |= WRITE; // Indicate that lock might be write-owned
    newLockState |= (tid << 4); // Indicate that lock has a single owner
  }else{
    if(newState & MAYBE_HELD){
      // Was held, so check if we're the new reader or a shared one
      if(newState & WRITE){
	// Was held by a writer, so we must be the new exclusive reader
	newLockState |= (tid << 4);
      }
      // If it was held by a reader, no need to set owner because it's now
      // assumed to be shared by readers (and will be checked by an acquiring
      // writer to see if ANY shared readers still exist)
    }else{
      // Was not held, so add self as the exclusive reader
      newLockState |= (tid << 4);
    }
  }

  // Release the protecting lock
  lockPtr->store(newLockState,std::memory_order::memory_order_release);

  return true;
}

void Lock::validateState(address addr)
{
  bool hasReader = false;
  bool hasWriter = false;
  for(int c = 1; c < MAX_THREADS; c++){
    if(_threadShadowSpaces[c].getBase() == 0){
      continue;
    }
    thread_state_type *threadState = getThreadLock(addr,c);
    if(checkThreadLock(threadState,addr,true)){
      assert(!hasWriter);
      hasWriter = true;
    }else if(checkThreadLock(threadState,addr,false)){
      hasReader = true;
    }
  }
  assert(!(hasWriter && hasReader));
}

bool Lock::isOwner(uint64_t addr, TID tid, bool isWrite)
{
  thread_state_type *threadLockPtr = getThreadLock(addr,tid);
  return checkThreadLock(threadLockPtr,addr,isWrite);
}

bool Lock::release(uint64_t addr, TID tid)
{
  thread_state_type *threadLockPtr = nullptr;
  threadLockPtr = getThreadLock(addr,tid);
  releaseThreadLock(threadLockPtr,addr);
  return true;
}

void Lock::clear(uint64_t addr)
{
  for(int c = 1; c < MAX_THREADS; c++){
    thread_state_type *tPtr = getThreadLock(addr,c);
    releaseThreadLock(tPtr,addr);
  }
}

void Lock::printState(uint64_t addr, std::ostream &out)
{
  std::atomic<lock_state_type> *lockState = getLockState(addr);
  lock_state_type state = *lockState;
  lock_state_type owner = SOFRITAS_LOCK_OWNER(state);

  out << "Addr: " << (void*)addr << ", State: " << (void*)lockState << "\n";

  if(state & MUTEX){
    out << "MUTEX" << ",";
  }

  if(state & UPDATING){
    out << "UPDATING" << ","; 
  }

  if(state & MAYBE_HELD){
    out << "MAYBE_HELD" << ",";

    if(state & WRITE){
      out << "WRITE" << ",";
    }else{
      out << "READ" << ",";
    }
  }else{
    if((state & WRITE) || (owner != 0)){
      out << "StateError?,";
    }
  }

  out << " OWNER: " << owner << "\n";

  out << "[";
  
  for(int c = 1; c < MAX_THREADS; c++){
    if(_threadShadowSpaces[c].getBase() == 0){
      continue;
    }
    thread_state_type *threadState = getThreadLock(addr,c);
    if(checkThreadLock(threadState,addr,true)){
      out << "W,";
    }else if(checkThreadLock(threadState,addr,false)){
      out << "R,";
    }else{
      out << "X,";
    }
  }

  out << "]\n";
}

thread_state_type Lock::getThreadLockMask(uint64_t addr)
{
  // First, get the last byte of addr
  return THREAD_LOCK_MASKS[addr & 0x3];
}

void Lock::acquireThreadLock(thread_state_type *threadLock, 
			     uint64_t addr,bool isWrite)
{
  thread_state_type lockMask = getThreadLockMask(addr);
  if(!isWrite){
    // Mask off write bits for ownership status if acquiring for read
    lockMask = lockMask & 0x55;
  }
  *threadLock = *threadLock | lockMask;
}

void Lock::releaseThreadLock(thread_state_type *threadLock, 
			     uint64_t addr)
{
  thread_state_type lockMask = getThreadLockMask(addr);
  lockMask = lockMask ^ 0xFF;
  *threadLock = *threadLock & lockMask;
}

bool Lock::checkThreadLock(thread_state_type *threadLock, 
			   uint64_t addr,bool isWrite)
{
  thread_state_type lockMask = getThreadLockMask(addr);
  if(isWrite){
    return (*threadLock & lockMask & 0xAA);
  }else{
    return (*threadLock & lockMask & 0x55);
  }	    
}
